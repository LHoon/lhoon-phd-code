
import java.io.*;
import java.util.*;

import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class POSTagger
{
	public enum IDF
	{	DOC, TOK, VAL	};

	final static int MAX_TOKENS = IDF.values().length;
	private static String LINE;
	private static String TAB = "\t";
	private static String[] TOKENS;

	public static void main(String[] args) throws Exception
  	{
  		StringBuilder sBuilder = new StringBuilder();
  		StringUtil stringUtil = new StringUtil();
		MaxentTagger tagger = new MaxentTagger("./models/english-bidirectional-distsim.tagger");
		BufferedReader reader = new BufferedReader(new FileReader("./reports/trimmed-free-cat-idf.tsv"));

		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = stringUtil.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			// sBuilder.append(TOKENS[IDF.TOK.ordinal()]);
			// sBuilder.append(" ");
			List<HasWord> sent = Sentence.toWordList(TOKENS[IDF.TOK.ordinal()]);
    		System.out.println(tagger.tagSentence(sent));
		}
		reader.close();



		// List<List<HasWord>> sentences = MaxentTagger.tokenizeText(sBuilder.toString().trim());
		// for (List<String> sentence : sentences)
		// {
		// 	ArrayList<TaggedWord> tSentence = tagger.tagSentence(sentence);
		// 	System.out.println(Sentence.listToString(tSentence, false));
		// }
	}
}
