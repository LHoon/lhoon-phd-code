import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ReportRatingWC
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String IN_FOLDER = "./dataset/";
	final static String OUT_FOLDER = "./dataset/reports/";
	final static boolean DROID = true;
	final static boolean IOS = false;
	final static boolean FREE = true;
	final static boolean PAID = false;

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer DRD_REVIEW_COUNT = 0;
	private static Integer DRD_LINES_READ = 0;
	private static Integer IOS_REVIEW_COUNT = 0;
	private static Integer IOS_LINES_READ = 0;

	private static List<String> BAD_LIST = new ArrayList<String>();
	private static Set<String> CATEGORIES = new HashSet<String>();

	static BufferedReader READER;
	static String LINE;
	static Map<String, Map<Integer, List<Integer>>> CATEGORY_SIZES = new TreeMap<String, Map<Integer, List<Integer>>>();
	static Map<String, Integer[]> CATEGORY_RATINGS = new TreeMap<String, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		parseCategories(DROID);
		parseCategories(IOS);
		writeMap("ratings-per-category", "tsv", CATEGORY_RATINGS);
		prepareCCMap(CATEGORY_SIZES);
		writeList("bad-lines", "tsv", BAD_LIST);

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(formatNumber(REVIEW_COUNT) + " reviews parsed, of " + formatNumber(LINES_READ) +  " lines read, with " + formatNumber(BAD_LIST.size()) + " bad lines. " + formatNumber(DRD_REVIEW_COUNT) + " Android reviews parsed, of " + formatNumber(DRD_LINES_READ) +  " Android lines read and " + formatNumber(IOS_REVIEW_COUNT) + " iOS reviews parsed, of " + formatNumber(IOS_LINES_READ) +  " iOS lines read, in " + timeTaken);
	}

	private static void parseCategories(boolean isDroid) throws IOException
	{
		CATEGORIES.clear();
		loadCategories(getPlatform(isDroid));
		readCategories(getPlatform(isDroid));
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static String getPlatform(boolean isDroid)
	{	return isDroid ? "droid" : "ios";	}

	private static void loadCategories(String platform) throws IOException
	{
		READER = new BufferedReader(new FileReader(String.format("%s%s-categories.txt", IN_FOLDER, platform)));
		try
		{
			for ( LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				if ( !LINE.equals("") )
				{	CATEGORIES.add(minorClean(LINE));	}
			}
		}
		finally
		{	READER.close();	}
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void readCategories(String platform) throws IOException
	{
		for ( String category : CATEGORIES )
		{
			readReviews(platform, getCatName(category, FREE));
			readReviews(platform, getCatName(category, PAID));
		}
	}

	private static void readReviews(String platform, String category) throws IOException
	{
		boolean isOk;
		List<Integer> CCList = new ArrayList<Integer>();
		Integer charCount;
		Integer rating;
		Integer[] ratings = new Integer[] {0,0,0,0,0};
		String[] tokens;
		String clean;
		String platformCategory = String.format("%s-%s", platform, category);
		String filePath = String.format("%s%s/data/%s.tsv", IN_FOLDER, platform, category);
		Map<Integer, List<Integer>> ratingSizes = new TreeMap<Integer, List<Integer>>();

		READER = new BufferedReader(new FileReader(filePath));
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.tsv", OUT_FOLDER, "ratings-per-category"), true));
		try
		{
			for ( LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				if ( LINE.equals("") )	{	continue;	}

				tokens = LINE.split(TAB);
				if ( platform.equals("droid") )
				{	DRD_LINES_READ++;	}
				else
				{	IOS_LINES_READ++;	}
				LINES_READ++;
				if ( tokens.length == MAX_DATA_TOKENS
					&& tokens[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
				{
					charCount = Integer.parseInt(tokens[AppData.TITLE_CC.ordinal()]) + Integer.parseInt(tokens[AppData.BODY_CC.ordinal()]);

					rating = Integer.parseInt(tokens[AppData.RATING.ordinal()]);
					ratings[rating - 1]++;
					CCList = ratingSizes.containsKey(rating) ? ratingSizes.get(rating) : new ArrayList<Integer>();
					CCList.add(charCount);
					ratingSizes.put(rating, CCList);

					if ( platform.equals("droid") )
					{	DRD_REVIEW_COUNT++;	}
					else
					{	IOS_REVIEW_COUNT++;	}
					REVIEW_COUNT++;
					continue;
				}
				BAD_LIST.add(String.format("%s%s%s", platformCategory, TAB, LINE));
			}
			writeCCForCategory(platform, category, ratingSizes);
			CATEGORY_SIZES.put(platformCategory, ratingSizes);
			CATEGORY_RATINGS.put(platformCategory, ratings);
			writer.write(prepLine(platformCategory, ratings));
			writer.newLine();
		}
		finally
		{
			writer.close();
			READER.close();
			// writeSet(fileName, "tsv", reviewSet, true);
		}
	}

	private static void writeSet(String fileName, String extension, Set<String> reviews, boolean doCount, boolean isDroid) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
		for ( String review : reviews )
		{
			writer.write(review);
			writer.newLine();
		}
		writer.close();
	}

	private static void writeList(String fileName, String extension, List<String> theList) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.tsv", OUT_FOLDER, fileName, extension)));
		for ( String line : theList )
		{
			writer.write(line);
			writer.newLine();
		}
		writer.close();
	}

	private static void writeCCForCategory(String platform, String category, Map<Integer, List<Integer>> theMap) throws IOException
	{
		List<Integer> ratingCCTotal = new ArrayList<Integer>();
		Map<Integer, List<Integer>> resultMap = new TreeMap<Integer, List<Integer>>();
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s/cc-%s.tsv", OUT_FOLDER, platform, category)));

		for ( Entry<Integer, List<Integer>> entry : theMap.entrySet() )
		{
			for ( Integer charCount : entry.getValue() )
			{
				writer.write(String.format("%s%s%s", String.valueOf(entry.getKey()), TAB, String.valueOf(charCount)));
				writer.newLine();
			}
		}
		writer.close();
	}

	private static void prepareCCMap(Map<String, Map<Integer, List<Integer>>> theMap) throws IOException
	{
		String platform;
		String category;

		Integer rating;
		List<Integer> CCList;
		Map<Integer, List<Integer>>	droidCCMap = new TreeMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> iosCCMap = new TreeMap<Integer, List<Integer>>();

		for ( Entry<String, Map<Integer, List<Integer>>> entry : theMap.entrySet() )
		{
			platform = entry.getKey().split("-")[0];
			category = entry.getKey().split("-")[1];

			for ( Entry<Integer, List<Integer>> ratedEntry : entry.getValue().entrySet() )
			{
				if ( platform.equals("droid") )
				{
					CCList = droidCCMap.containsKey(ratedEntry.getKey()) ?
					droidCCMap.get(ratedEntry.getKey()) : new ArrayList<Integer>();
					CCList.addAll(ratedEntry.getValue());
					droidCCMap.put(ratedEntry.getKey(), CCList);
				}
				else
				{
					CCList = iosCCMap.containsKey(ratedEntry.getKey()) ?
					iosCCMap.get(ratedEntry.getKey()) : new ArrayList<Integer>();
					CCList.addAll(ratedEntry.getValue());
					iosCCMap.put(ratedEntry.getKey(), CCList);
				}
			}
		}
		writeCCMap("droid", droidCCMap);
		writeCCMap("ios", iosCCMap);
	}

	private static void writeCCMap(String platform, Map<Integer, List<Integer>> theMap) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s-cc.tsv", OUT_FOLDER, platform)));
		for ( Entry<Integer, List<Integer>> entry : theMap.entrySet() )
		{
			for ( Integer charCount : entry.getValue() )
			{
				writer.write(String.format("%s%s%s", entry.getKey(), TAB, charCount));
				writer.newLine();
			}
		}
		writer.close();
	}

	private static void writeMap(String fileName, String extension, Map<String, Integer[]> theMap) throws IOException
	{
		Integer[] droidTotals = new Integer[] {0,0,0,0,0};
		Integer[] iosTotals = new Integer[] {0,0,0,0,0};
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension), true));
		try
		{
			for ( Entry<String, Integer[]> entry : theMap.entrySet() )
			{
				if ( entry.getKey().split("-")[0].equals("droid") )
				{	droidTotals = sumRatings(droidTotals, entry.getValue());	}
				else
				{	iosTotals = sumRatings(iosTotals, entry.getValue());	}
			}
			writer.write(prepLine("droid-totals", droidTotals));
			writer.newLine();
			writer.write(prepLine("ios-totals", iosTotals));
			writer.newLine();
		}
		finally
		{	writer.close();	}
	}

	private static Integer[] sumRatings(Integer[] totals, Integer[] ratings)
	{
		for ( int i = 0; i < totals.length; i++ )
		{	totals[i] += ratings[i];	}
		return totals;
	}

	private static String prepLine(String platformCategory, Integer[] ratings)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(platformCategory);
		for ( int i = 0; i < ratings.length; i++ )
		{
			sb.append(TAB);
			sb.append(ratings[i]);
		}
		return sb.toString();
	}
}
