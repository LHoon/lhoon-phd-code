import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class AggregateAF
{
	public enum AF
	{	AUTHOR, TOTAL, FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static int MAX_TOKENS = AF.values().length;
	final static int MAP_COUNT = 6; 	// 1, 2, 3, 4, 5, n reviews
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/authors-rerun/";

	// Numerical indexes to track the review processing
	private static String AUTHOR_VAL = "";
	private static Integer COUNT = 0;
	private static Integer TOTAL_FREQ = 0;

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Integer VALUE;

	private static Map<Integer, Integer> AF_MAP = new TreeMap<Integer, Integer>();
	private static Map<Integer, Map<Integer, Integer>> RATED_MAPS = new TreeMap<Integer, Map<Integer, Integer>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		buildMaps();
		parseAllAFFile();
		writeRatedAF();
		writeAFs();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("AF report aggregated in " + timeTaken);
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static void add(Integer count)
	{
		Integer value = AF_MAP.containsKey(count) ? AF_MAP.get(count) : 0;
		value++;
		AF_MAP.put(count, value);
	}

	private static void buildMaps()
	{
		Map<Integer, Integer> nestedMap;
		for ( int i = 1; i <= MAP_COUNT; i++ )
		{
			RATED_MAPS.put(i, new HashMap<Integer, Integer>());
			nestedMap = RATED_MAPS.get(i);
			for ( int j = 1; j <= 5; j++ )
			{	nestedMap.put(j, 0);	}
		}
	}

	private static void parseAllAFFile() throws Exception
	{
		try
		{
			READER = new BufferedReader(new FileReader(FOLDER + "/all-af.tsv"));
			int revCount = 0;
			Integer count = 0;
			Map<Integer, Integer> nestedMap;
			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				TOKENS = LINE.split(TAB);
				if ( TOKENS.length == MAX_TOKENS )
				{
					COUNT = Integer.parseInt(TOKENS[AF.TOTAL.ordinal()]);
					TOTAL_FREQ += COUNT;
					add(COUNT);

					revCount =  COUNT > 5 ? revCount = 6 : COUNT;
					nestedMap = RATED_MAPS.get(revCount);
					addCounts(nestedMap, getCounts(TOKENS));
					RATED_MAPS.put(revCount, nestedMap);
				}
			}
			READER.close();
		}
		catch (Exception e)
		{	System.err.println("Error: " + e.getMessage());	}
	}

	// private static void test(String[] tokens, int[] counts)
	// {
	// 	for ( int i = 0; i < TOKENS.length; i ++)
	// 	{
	// 		if ( i < 2)	{	System.out.print(TOKENS[i] + TAB);	}
	// 		else
	// 		{
	// 			System.out.print(TOKENS[i]+ " > "+ counts[i-2]+TAB);
	// 		}
	// 	}
	// 	System.out.println();
	// }

	private static int[] getCounts(String[] toks)
	{
		int[] result = new int[5];
		for ( int i = 0; i < result.length; i++ )
		{	result[i] = Integer.parseInt(toks[i+2]);	}
		return result;
	}

	private static Map<Integer, Integer> addCounts(Map<Integer, Integer> theMap, int[] counts)
	{
		int currentVal = 0;
		for ( int i = 0; i < counts.length; i++ )
		{
			currentVal = theMap.get(i+1);

			theMap.put(i+1, (currentVal + counts[i]));
		}
		return theMap;
	}

	private static void writeRatedAF() throws IOException
	{
		for ( Entry<Integer, Map<Integer, Integer>> entry : RATED_MAPS.entrySet() )
		{
			WRITER = new BufferedWriter(new FileWriter(FOLDER + "/all-" + entry.getKey() + "-review-af-report.tsv"));
			for ( Entry<Integer, Integer> nestedEntry : entry.getValue().entrySet() )
			{
				S_BUILDER.setLength(0);
				S_BUILDER.append(nestedEntry.getKey());
				S_BUILDER.append(TAB);
				S_BUILDER.append(nestedEntry.getValue());
				WRITER.write(S_BUILDER.toString().trim());
				WRITER.newLine();
			}
			WRITER.close();
		}
	}

	private static void writeAFs() throws IOException
	{
		int total = AF_MAP.size();
		double proportion = 0.0;
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "all-af-report.tsv"));

		for ( Entry<Integer, Integer> entry : AF_MAP.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());
			S_BUILDER.append(TAB);

			proportion = (double) (entry.getKey() * entry.getValue())/TOTAL_FREQ*100;

			S_BUILDER.append(proportion+"%");

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}
}


/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
