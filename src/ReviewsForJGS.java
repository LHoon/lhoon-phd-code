import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ReviewsForJGS
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum OUTPUT
	{	CATEGORY, APP_ID, PRICE, REVIEW_ID, REVIEW_DATE, APP_VERSION, RATING, AUTHOR_ID, WORD_COUNT
	};


	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/jgs-reviews/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String REPORT_FOLDER = FOLDER + "reports/";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer BAD_LINES = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};	// 1, 2, 3, 4, 5
	private static String AUTHOR = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Set<String> AUTHORS = new HashSet<String>();
	private static Map<String, Integer> REVIEW_RATING = new HashMap<String, Integer>();
	private static Map<String, String> REVIEWS = new HashMap<String, String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/categories.txt");
		writeHeader();
		parseReviews();
		System.out.println(REVIEWS.size());
		System.out.println(REVIEW_RATING.size());
		writeBadLines();
		// writeReviews();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATEGORIES.size() + " categories processed, " + formatNumber(REVIEW_COUNT) + " reviews written, from " + formatNumber(AUTHORS.size()) + " authors, with " + formatNumber(BAD_SET.size()) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("./reviews/clean-data/tab/%s.tsv", getCatName(category, isFree));
	}

	private static String getCatName(String category, boolean isFree)
	{	return String.format("%s_%s", getPrice(isFree), category);	}

	private static String getPrice(boolean isFree)
	{	return isFree ? "free" : "paid";	}

	private static void loadCategories(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )
			{	CATEGORIES.add(LINE.toLowerCase(Locale.ENGLISH).trim());	}
		}
		READER.close();
	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws Exception
	{
		for ( String category : CATEGORIES )
		{
			parseFile(category, FREE);
			parseFile(category, PAID);
		}
	}

	private static void parseFile(String category, boolean isFree) throws IOException
	{
		int rating = 0;
		int wordCount = 0;
		String author;
		String appId;
		String revId;
		String date;
		String version;

		READER = new BufferedReader(new FileReader(getCategoryFileName(category, isFree)));
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "all-reviews.tsv", true));

		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;

			TOKENS = LINE.split(TAB);

			if ( TOKENS.length == MAX_DATA_TOKENS
				&& TOKENS[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				if ( TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length > 1 )
				{
					AUTHORS.add(TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1]);
					S_BUILDER.setLength(0);
					S_BUILDER.append(category);
					S_BUILDER.append(TAB);
					S_BUILDER.append(TOKENS[AppData.APP_ID.ordinal()]);
					S_BUILDER.append(TAB);
					S_BUILDER.append(getPrice(isFree));
					S_BUILDER.append(TAB);
					S_BUILDER.append(TOKENS[AppData.REVIEW_ID.ordinal()]);
					S_BUILDER.append(TAB);
					S_BUILDER.append(TOKENS[AppData.REVIEW_DATE.ordinal()]);
					S_BUILDER.append(TAB);
					S_BUILDER.append(TOKENS[AppData.APP_VERSION.ordinal()]);
					S_BUILDER.append(TAB);
					S_BUILDER.append(TOKENS[AppData.RATING.ordinal()]);
					S_BUILDER.append(TAB);
					S_BUILDER.append(TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1]);
					S_BUILDER.append(TAB);
					S_BUILDER.append(String.valueOf(Integer.parseInt(TOKENS[AppData.TITLE_WC.ordinal()]) + Integer.parseInt(TOKENS[AppData.BODY_WC.ordinal()])));

					WRITER.write(S_BUILDER.toString().trim());
					WRITER.newLine();
					REVIEW_COUNT++;
					continue;
				}
			}
			BAD_SET.add(String.format("%s%s%s", getCategoryFileName(category, isFree), TAB, LINE));
		}
		READER.close();
	}

	private static void writeHeader() throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "all-reviews.tsv"));
		WRITER.write("CATEGORY	APP_ID	PRICE	REVIEW_ID	REVIEW_DATE	APP_VERSION	RATING	AUTHOR_ID	WORD_COUNT");
		WRITER.newLine();
		WRITER.close();
	}

	/**	CATEGORY, APP_ID, PRICE, REVIEW_ID, REVIEW_DATE, APP_VERSION, RATING, AUTHOR_ID, WORD_COUNT
	*/
	private static void addReview(String category, boolean isFree, String reviewId, Integer rating, String author, int wordCount)
	{
		String[] output = new String[] {category, "", getPrice(isFree), reviewId, "", "", String.valueOf(rating), author, String.valueOf(wordCount)};
		output[OUTPUT.APP_ID.ordinal()] = TOKENS[AppData.APP_ID.ordinal()];
		output[OUTPUT.REVIEW_DATE.ordinal()] = TOKENS[AppData.REVIEW_DATE.ordinal()];
		output[OUTPUT.APP_VERSION.ordinal()] = TOKENS[AppData.APP_VERSION.ordinal()];

		REVIEW_RATING.put(reviewId, rating);
		REVIEWS.put(reviewId, delimitLine(output, TAB));
		REVIEW_COUNT++;
	}

	private static String delimitLine(String[] tokens, String delimiter)
	{
		S_BUILDER.setLength(0);
		for ( String token : tokens )
		{
			S_BUILDER.append(token);
			S_BUILDER.append(delimiter);
		}
		return S_BUILDER.toString().trim();
	}

	private static void writeReviews() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "all-reviews.tsv"));
		REVIEW_RATING = sortByComparator(REVIEW_RATING);
		for ( Entry<String, Integer> entry : REVIEW_RATING.entrySet() )
		{
			WRITER.write(REVIEWS.get(entry.getKey()));
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeBadLines() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.tsv"));

		for ( String line : BAD_SET )
		{
			WRITER.write(line.trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
