import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class CountTerms
{
	public enum Review
	{	APP_ID, APP_NAME, REVIEW_ID, RATING, REVIEW	};

	final static int MAX_TOKENS = Review.values().length;
	final static String TAB = "\t";

	private static Integer REVIEWS_SCORED = 0;
	private static Integer LINES_READ = 0;
	private static Integer APP_TERMS = 0;
	private static Integer STAR_TERMS = 0;
	private static String CAT_PRICE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static TextCleaner CLEANER;
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static Set<String> SURVEYED_IDS = new HashSet<String>();
	private static Set<String> TAG_WORDS = new HashSet<String>();
	private static Set<String> STOP_WORDS = new HashSet<String>();
	private static Set<String> SENT_WORDS = new HashSet<String>();
	private static Map<String, String[]> REVIEWS = new HashMap<String, String[]>();
	private static Map<String, Double> REVIEW_SCORES = new HashMap<String, Double>();
	private static Map<String, Map<String, Integer>> CAT_APP_MAP = new HashMap<String, Map<String, Integer>>();
	private static Map<String, Map<String, Integer>> CAT_STAR_MAP = new HashMap<String, Map<String, Integer>>();

	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}

		CAT_PRICE = args[0];
		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, sU);

		loadSet(STOP_WORDS, "./assets/mysql-stopwords.txt");
		loadSet(STOP_WORDS, "./assets/aux-stopwords.txt");
		loadSet(SENT_WORDS, "./assets/positive-words.txt");
		loadSet(SENT_WORDS, "./assets/negative-words.txt");
		loadSet(TAG_WORDS, "./assets/sorted-lhsbtags.txt");
		loadSet(SURVEYED_IDS, "./thesis/surveyed-reviews.txt");

		parseReviews(CAT_PRICE);
		countReviewTerms();
		// System.out.println(SURVEYED_IDS.size());
		countReviewScores();
		// writeScores();

		// printMap(CAT_APP_MAP);
		// printMap(CAT_STAR_MAP);
		// printMapTotalsForApp("468666845");
		// printMapTotals();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CAT_PRICE + TAB + formatNumber(REVIEWS.size()) + TAB + "term-counted reviews" + TAB + formatNumber(LINES_READ) + TAB + "lines read, in " + timeTaken);
	}

	private static String formatNumber(int value)	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static String catStarPath()	{	return String.format("./thesis/cat-star-tf/%s.tsv", CAT_PRICE);	}

	private static String catAppPath()	{	return String.format("./thesis/cat-app-tf/%s.tsv", CAT_PRICE);	}

	private static String catPath()		{   return String.format("./thesis/cat-tf/%s.tsv", CAT_PRICE);	}

	/**
	 * Populates the any auxilliary dictionaries based on filename param.
	 */
	private static void loadSet(Set<String> theSet, String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = READER.readLine(); LINE != null; LINE = READER.readLine() )
		{	theSet.add(LINE.toLowerCase(Locale.ENGLISH).trim());	}
		READER.close();
	}

	private static boolean lineIsValid()
	{
		if ( TOKENS.length != MAX_TOKENS ) 		{	return false;	}
		if ( TOKENS[Review.RATING.ordinal()].equals("1") || TOKENS[Review.RATING.ordinal()].equals("2")
			|| TOKENS[Review.RATING.ordinal()].equals("3") || TOKENS[Review.RATING.ordinal()].equals("4")
			|| TOKENS[Review.RATING.ordinal()].equals("5") )	{	return true;	}
		else	{	return false;	}
	}

	private static void parseReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader("./thesis/stabilised-reviews/stabilised-" + CAT_PRICE + ".tsv"));
		for ( LINE = READER.readLine(); LINE != null; LINE = READER.readLine() )
		{
			LINES_READ++;
			TOKENS = LINE.split(TAB);
			if( !lineIsValid() )	{	continue;	}
			REVIEWS.put(TOKENS[Review.REVIEW_ID.ordinal()], TOKENS);
		}
		READER.close();
	}

	private static void countReviewTerms()
	{
		String[] reviewTokens = {"", ""};
		for ( String[] tokens : REVIEWS.values() )
		{
			reviewTokens = tokens[Review.REVIEW.ordinal()].split("\\ ");
			for ( String word : reviewTokens )
			{
				if ( !STOP_WORDS.contains(word) && word.length() > 1)
				{
					addToMap(CAT_APP_MAP, tokens[Review.APP_ID.ordinal()], word);
					addToMap(CAT_STAR_MAP, tokens[Review.RATING.ordinal()], word);
				}
			}
		}
	}

	private static void countReviewScores() throws IOException
	{
		double[] scores = {0.0, 0.0, 0.0};
		double tagScore = 0.0;
		Integer tagCount = 0;
		Integer wordCount = 0;
		String[] reviewTokens = {"", ""};
		Map<String, Integer> reviewTerms = new HashMap<String, Integer>();

		for ( String revID : SURVEYED_IDS )
		{
			if ( !REVIEWS.containsKey(revID) )
			{	writeScore(revID);	}
			else
			{
				String[] tokens = REVIEWS.get(revID);
				reviewTokens = tokens[Review.REVIEW.ordinal()].split("\\ ");
				for ( String word : reviewTokens )
				{
					if ( !STOP_WORDS.contains(word) && word.length() > 1)
					{
						if ( TAG_WORDS.contains(word) )	{	tagCount++;	}
						wordCount = reviewTerms.containsKey(word) ? reviewTerms.get(word) : 0;
						wordCount++;
						reviewTerms.put(word, wordCount);
					}
				}
				scores = calculateScore(reviewTerms, tokens[Review.APP_ID.ordinal()], tokens[Review.RATING.ordinal()]);
				scores[2] = (double) tagCount;
				if ( wordCount > 0 && tagCount > 0 )
				{	tagScore = (double) Math.log( 1 + tagCount/wordCount );	}
				// System.out.println(score + TAB + tagScore + TAB + (score+tagScore));

				if(scores[0] < 0 || scores[1] < 0 )
				{	System.out.println(tokens[Review.REVIEW.ordinal()] + TAB + scores[0] + TAB + scores[1]);	}
				// writeScore(tokens, scores);//+tagCount));
				// if ( score > 2.0 )
				// {	REVIEW_SCORES.put(tokens[Review.REVIEW_ID.ordinal()], score);	}

				// System.out.println(revID+TAB+score+TAB+tagScore+TAB+(score+tagScore));
			}
			tagCount = 0;
			wordCount = 0;
			REVIEWS_SCORED++;
		}
	}

	private static int getTotalTermsInDoc(Map<String, Map<String, Integer>> theMap, String docKey)
	{
		int result = 0;
		Map<String, Integer> nestedMap = theMap.get(docKey);
		for ( Entry<String, Integer> word : nestedMap.entrySet() )
		{	result += word.getValue();	}
		return result;
	}

	private static double getTermDocCount(Map<String, Map<String, Integer>> theMap, String word)
	{
		int result = 0;
		for ( Entry<String, Map<String, Integer>> nestedEntry : theMap.entrySet() )
		{
			if ( nestedEntry.getValue().containsKey(word) )
			{	result++;	}
		}
		return result;
	}

	private static double[] calculateScore(Map<String, Integer> reviewTermMap, String appID, String rating)
	{
		double[] scores = {0.0, 0.0, 0.0};
		double appTotalTermsInDoc = 0.0;
		double starTotalTermsInDoc = 0.0;
		Integer reviewSize = 0;

		for ( Integer count : reviewTermMap.values())	{	reviewSize += count;	}

		appTotalTermsInDoc = (double) getTotalTermsInDoc(CAT_APP_MAP, appID) - reviewSize;
		starTotalTermsInDoc = (double) getTotalTermsInDoc(CAT_STAR_MAP, rating) - reviewSize;

		for ( Entry<String, Integer> reviewTerm : reviewTermMap.entrySet() )
		{
			if ( CAT_APP_MAP.get(appID).containsKey(reviewTerm.getKey())
				&& CAT_APP_MAP.get(appID).get(reviewTerm.getKey()) - reviewTerm.getValue() > 0 )
			{
				scores[0] += tfidf(CAT_APP_MAP, appID, reviewSize, reviewTerm.getKey(), reviewTerm.getValue(), appTotalTermsInDoc);
			}
			if ( CAT_STAR_MAP.get(rating).containsKey(reviewTerm.getKey())
				&& CAT_STAR_MAP.get(rating).get(reviewTerm.getKey()) - reviewTerm.getValue() > 0 )
			{
				scores[1] += tfidf(CAT_STAR_MAP, rating, reviewSize, reviewTerm.getKey(), reviewTerm.getValue(), starTotalTermsInDoc);
			}
		}
		return scores;
	}

	private static double tfidf(Map<String, Map<String, Integer>> theMap, String docKey, Integer reviewSize, String word, Integer wordCount, double totalTermsInDoc)
	{
		double termCount = (double) theMap.get(docKey).get(word) - wordCount;
		double countOfDocsWithTerm = (double) getTermDocCount(theMap, word);
		if ( theMap.get(docKey).get(word) == wordCount )//!!!!!!!!!!!!!!!!!
		{	return 0.0;	}

		countOfDocsWithTerm -= theMap.get(docKey).get(word) > wordCount ? 1.0 : 0.0;
		double totalNumOfDocuments = (double) theMap.size();

// double termOccurrences = this.documents.get(docId).get(token);
// double totalTermsInDoc = getTotalTermsInDoc(docId);
// double docOccurrences = getDocOccurrences(token);
// double totalDocuments = (double) this.documents.size();

// double tf = (termOccurrences / totalTermsInDoc);
// double df = this.logType ? (Math.log10(totalDocuments / docOccurrences)) : (Math.log(totalDocuments / docOccurrences));
		// System.out.println("TERM COUNT: " + termCount);
		// System.out.println("COUNT OF DOCS WITH TERM: " + countOfDocsWithTerm);
		// System.out.println("TOTAL NUM OF DOCS: " + totalNumOfDocuments);
		// System.out.println("TOTAL TERMS IN DOC: " + totalTermsInDoc);

		double tf = (termCount / totalTermsInDoc);
		double df = Math.log(1 + (totalNumOfDocuments / countOfDocsWithTerm));

		// System.out.println("TF: " + tf);
		// System.out.println("IDF: " + df);
		// System.out.println("TF/IDF: " + (tf*df));

		return ( tf * df );
	}

	private static void addToMap(Map<String, Map<String, Integer>> theMap, String key, String word)
	{
		Map<String, Integer> nestedMap = theMap.containsKey(key) ? theMap.get(key) : new HashMap<String, Integer>();
		Integer value = nestedMap.containsKey(word) ? nestedMap.get(word) : 0;
		value++;
		nestedMap.put(word, value);
		theMap.put(key, nestedMap);

	}

	private static void writeTFMap(Map<String, Map<String, Integer>> theMap, String fileName) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(fileName));
		for (Entry<String, Map<String, Integer>> entry : theMap.entrySet())
		{
			Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(entry.getValue()));
			resultMap.putAll(entry.getValue());
			for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
			{
				if ( nestedEntry.getValue() > 1 )
				{
					S_BUILDER.setLength(0);
					S_BUILDER.append(entry.getKey());
					S_BUILDER.append(TAB);
					S_BUILDER.append(nestedEntry.getKey());
					S_BUILDER.append(TAB);
					S_BUILDER.append(nestedEntry.getValue());
					WRITER.write(S_BUILDER.toString().trim());
					WRITER.newLine();
				}
			}
		}
		WRITER.close();
	}

	private static void printMap(Map<String, Map<String, Integer>> theMap)
	{
		for (Entry<String, Map<String, Integer>> entry : theMap.entrySet())
		{
			Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(entry.getValue()));
			resultMap.putAll(entry.getValue());
			for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
			{
				if ( nestedEntry.getValue() > 1 )
				{
					System.out.println(entry.getKey() + TAB + nestedEntry.getKey()
						+ TAB + nestedEntry.getValue());
				}
			}
		}
	}

	private static void printMapTotals()
	{
		for (Entry<String, Map<String, Integer>> entry : CAT_APP_MAP.entrySet())
		{
			Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(entry.getValue()));
			resultMap.putAll(entry.getValue());
			for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
			{	if ( nestedEntry.getValue() > 1 )	{	APP_TERMS++;	}	}
		}
		System.out.println("App Terms: " + APP_TERMS);
		for (Entry<String, Map<String, Integer>> entry : CAT_STAR_MAP.entrySet())
		{
			Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(entry.getValue()));
			resultMap.putAll(entry.getValue());
			for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
			{	if ( nestedEntry.getValue() > 1 )	{	STAR_TERMS++;	}	}
		}
		System.out.println("Star Terms: " + STAR_TERMS);
	}

	private static void printMapTotalsForApp(String appID)
	{
		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(CAT_APP_MAP.get(appID)));
		resultMap.putAll(CAT_APP_MAP.get(appID));

		for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
		{
			if ( nestedEntry.getValue() > 1 )
			{
				System.out.println(appID + TAB + nestedEntry.getKey()
					+ TAB + nestedEntry.getValue());
			}
		}

		for (Entry<String, Map<String, Integer>> entry : CAT_STAR_MAP.entrySet())
		{
			resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(entry.getValue()));
			resultMap.putAll(entry.getValue());
			for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
			{
				if ( nestedEntry.getValue() > 1 )
				{
					System.out.println(entry.getKey() + TAB + nestedEntry.getKey()
					+ TAB + nestedEntry.getValue());
				}
			}
		}
	}

	private static void printScores()
	{
		for (Entry<String, Double> entry : REVIEW_SCORES.entrySet())
		{	System.out.println(entry.getKey() + TAB + entry.getValue());	}
	}

	private static void writeScore(String line) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/scores/no-" + CAT_PRICE + ".tsv", true));
		S_BUILDER.setLength(0);
		S_BUILDER.append(line);
		WRITER.write(S_BUILDER.toString().trim());
		WRITER.newLine();
		WRITER.close();
	}

	private static void writeScore(String[] reviewTokens, double[] scores) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/scores/" + CAT_PRICE + ".tsv", true));
		S_BUILDER.setLength(0);
		for ( String token : reviewTokens )
		{
			S_BUILDER.append(token);
			S_BUILDER.append(TAB);
		}
		for ( double score : scores )
		{
			S_BUILDER.append(score);
			S_BUILDER.append(TAB);
		}
		WRITER.write(S_BUILDER.toString().trim());
		WRITER.newLine();
		WRITER.close();
	}

	private static void writeScores() throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/scores/moreThan2-" + CAT_PRICE + ".tsv"));
		for (Entry<String, Double> entry : REVIEW_SCORES.entrySet())
		{
			TOKENS = REVIEWS.get(entry.getKey());
			S_BUILDER.setLength(0);
			for ( String token : TOKENS )
			{
				S_BUILDER.append(token);
				S_BUILDER.append(TAB);
			}
			S_BUILDER.append(entry.getValue());
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
