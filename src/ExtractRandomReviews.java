import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class ExtractRandomReviews
{
	public enum Tokens
	{	APP_ID, APP_NAME, PRICE, RANK, REVIEW_ID, AUTHOR, DATE, VERSION, RATING, RAW_TITLE, RAW_BODY, RAW_TWC, RAW_BWC, CLEAN_TITLE, CLEAN_BODY, CLEAN_TWC, CLEAN_BWC	};

	final static Integer SEED = 1;
	final static int MAX_TOKENS = Tokens.values().length;
	final static int THRESHOLD = 30;
	final static String DELIMITER = "\\t";
	final static boolean READ = true;
	final static boolean WRITE = false;

	static String APP;
	static String CATEGORY;
	static Set<Integer> RNG_SET;
	static Map<Integer, List<String>> REVIEWS = new HashMap<Integer, List<String>>();

	static BufferedWriter WRITER;
	static Random GENERATOR = new Random(SEED);

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("[0] = CATEGORY, [1] = APP IDs.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CATEGORY = args[0];
		APP = args[1];
		parseFile(filePath("", READ));
		writeReviews();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Extraction completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	fileName name of the file to be loaded (appId)
	 * @param	isRead boolean value to indicate pre- or post-cull file
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String filePath(String append, boolean isRead)
	{
		// String folder = isRead ? CATEGORY : "culled-"+CATEGORY;
		String folder = CATEGORY;
		String file = (append.length() == 0) ? APP : APP+"-"+append;
		return String.format("./reviews/%s/%s.tsv", folder, file);
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String fileAppend, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath(fileAppend, WRITE), true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void writeReviews() throws IOException
	{
		String line;
		for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
		{
			int index = 0;
			RNG_SET = getRNGs(entry.getValue().size());
			for ( Integer i : RNG_SET )
			{
				line = entry.getValue().get(i);
				writeLine(entry.getKey().toString(), line);
				index++;
			}
		}
	}

	private static Set<Integer> getRNGs(int max)
	{
		Set<Integer> result = new HashSet<Integer>();
		while( result.size() < THRESHOLD )
		{	result.add(GENERATOR.nextInt((max) - 0) + 0);	}
		return result;
	}

	private static void parseFile(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			String[] toks = line.split(DELIMITER);
			if( toks.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating = Integer.parseInt(toks[Tokens.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			persist(rating, line);
		}
		reader.close();
	}

	private static void persist(int rating, String text)
	{
		List<String> reviewList = REVIEWS.containsKey(rating) ? REVIEWS.get(rating) : new ArrayList<String>();
		reviewList.add(text);
		REVIEWS.put(rating, reviewList);
	}
}
