import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.TextFile;

/**
 * This class will parse the review file and output the following:
 * 1) Sentiment Proportions in a) Positive, b) Negative, c) Inconsistent
 *      Positive, d) Inconsistent Negative, e) Neutral, and f) Spam.
 *
 * 2) Undetected words that are NOT in conjunctions-determiners.txt:
 *      i) File 1: English words (as per dictionary.txt & aux-dictionary.txt).
 *      ii) File 2: Non-English words.
 *
 */
public class SentimentVerifier
{
	public enum Polarity
	{	POSITIVE, NEGATIVE, NOISE	};
	public enum Classification
	{	POSITIVE, NEGATIVE, INCONSISTENT_POS, INCONSISTENT_NEG, NEUTRAL, SPAM	};
	public enum Columns
	{	RATING, REVIEW	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean TXT = true;
	final static boolean CSV = false;
	// Pattern.compile or as a String
	final static boolean PATTERN = true;
	final static boolean STRING = false;
	// Nest the Map with Sets or <String, String> only
	final static boolean NEST = true;
	final static boolean FLAT = false;

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static int[] COUNTS = {0, 0, 0};
	private static String[] TOKENS = {"", ""};

	private static Set<String> WORD_SET;
	private static Matcher THE_MATCHER;

	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static DecimalFormat DF2 = new DecimalFormat("###.##");

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/* Search Space Strings */
	// Positive word patterns
	private static String POSITIVE_SENTIMENTS = "";
	// Negative word patterns
	private static String NEGATIVE_SENTIMENTS = "";
	// Conjunction and Determiner patterns
	private static String NOISE_WORDS = "";

	/* Classification Counters */

	/* English and Jargon references */
	// English Language Reference
	private static Map<String, Set<String>> WORD_DICT = new TreeMap<String, Set<String>>();
	private static Map<String, String> ABBRV_TYPOS = new TreeMap<String, String>();

	// Patterns to cleanse text of non-alphanumerics
	private static Pattern ALPHA_NUMERICS = Pattern.compile("[0-9]+?|[\\w\\d/]+");

	public static void main ( String[] args ) throws Exception
	{
		long start = System.currentTimeMillis();

		/* Startup code to load asset files into memory for lookups */
		System.out.print("Loading... ");
		loadAssets();
		System.out.println("Loading Complete.");

		/* Execution code to trigger processing */
		readReviews(args[0]);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Sentiment Endorsement " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	folder String value of folder name
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	extension file type or extension, WITHOUT the . prefix
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String filePath(String folder, String fileName, String extension)
	{   return String.format("./%s/%s.%s", folder, fileName, extension);	}

	/**
	 * Generate filepath for asset files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isTxt true for csv files, false for txt
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String assetPath(String fileName, boolean isTxt)
	{   return filePath("assets", fileName, (isTxt ? "txt" : "csv") );	}

	/**
	 *	Minor String preparation, trims trailing spaces, lowercases
	 *	and enforces Locale.ENGLISH prior to returning it.
	 * @param	text String value to be processed
	 * @param	asPattern true to treat it as a Pattern, false for no
	 * @return	String value constructed from param, LowerCased and trimmed
	 */
	private static String prepareString(String text, boolean asPattern)
	{	return asPattern ? "\\b" + text.toLowerCase(Locale.ENGLISH).trim() + "\\b" : text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 *	Startup method to load up assets into memory.
	 */
	private static void loadAssets() throws IOException
	{
		// Pattern Sets, hence true parameter
		POSITIVE_SENTIMENTS = populateSentiments(assetPath("positive-words", TXT));
		NEGATIVE_SENTIMENTS = populateSentiments(assetPath("negative-words", TXT));
		NEGATIVE_SENTIMENTS += populateSentiments(assetPath("aux-negative-words", TXT));
		NOISE_WORDS = populateSentiments(assetPath("conjunctions-determiners", TXT));
		System.out.print("Assets Loaded. ");
	}

	/**
	 *	Generic method to load a input file word list into a param Set as
	 *	compiled regex Patterns or raw Strings, dependent on boolean param.
	 * @param	filepath filePath of the input resource to add to the Set
	 * @return	String value of the search space
	 */
	private static String populateSentiments(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			S_BUILDER.append(prepareString(LINE, STRING));
			S_BUILDER.append(" , ");
		}
		return S_BUILDER.toString().trim();
	}

	@SuppressWarnings("unchecked")
	/**
	 * Generic method to write results to file. Default write locations is the
	 * reports folder of this project.
	 * @param	results Entry value where key = rating and value = reviewList
	 * @param	filepath filePath of the input resource to write
	 * @param	doAppend boolean value where true appends to the existing file
	 */
	private static void reportSentiment(String text) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter("./wip/neg-inconsistencies.csv", true));
			WRITER.write(text);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	/**
	 * Iterate through all the Patterns and detect matches. Return the number of
	 * matches found in the String param provided.
	 * @param	theSet Pattern datastructure to iterate through to check against
	 * @param	p Pattern'ed text to perform the Match on
	 * @return	the number of matches
	 */
	private static int patternMatcher(String sentiment, Pattern p)
	{
		int matches = 0;
		THE_MATCHER = p.matcher(sentiment);
		while ( THE_MATCHER.find() )
		{	matches++;	}
		return matches;
	}

	/**
	 * Method to generate a Pattern from a text block. Output is in parentheses,
	 * with \b boundaries pre and post each word, and | pipes separating the
	 * words.
	 * @param 	text String value to be processed into a Pattern
	 * @return	Pattern object developed from the String param
	 */
	private static Pattern generatePattern(String text)
	{
		boolean firstLine = true;
		String[] tokens = text.split(" ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		S_BUILDER.append("(");
		for ( String word : tokens )
		{
			if(!firstLine)
			{	S_BUILDER.append("|");	}
			S_BUILDER.append(prepareString(word, PATTERN));
			firstLine = false;
		}
		S_BUILDER.append(")");
		return Pattern.compile(S_BUILDER.toString());
	}

	/**
	 * Method to count the number of each type of sentiment. As it stands, there
	 * are only positive, negative or noise type sentiments. Index 0 = Positive,
	 * Index 1 = Negative, Index 2 = everything else.
	 * @param	text perform the counting on
	 * @return	int array of positive, negative and noise word counts
	 */
	private static int[] countSentiments(String text)
	{
		int[] results = {0,0,0};
		Pattern p = generatePattern(text);
		results[0] = patternMatcher(POSITIVE_SENTIMENTS, p);
		results[1] = patternMatcher(NEGATIVE_SENTIMENTS, p);
		results[2] = patternMatcher(NOISE_WORDS, p);
		return results;
	}

	/**
	 * Parses the review file that is brought in as a param, splits each line
	 * into tokens to obtain the review content and rating, where the text is
	 * treated using stabiliseText() to lowercased, trimmed, cleaned of all
	 * non-alphanumerics and brute forced against typos. The cleaned text is
	 * then sentiment counted and checked for consistency in content and rating.
	 * @param	filePath String filepath of the review file
	 */
	private static void readReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		String rating;
		String review;
		String[] reviewTokens;

		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			reviewTokens = LINE.split("\\|\\|\\|\\|");
			if ( reviewTokens.length != 2 )
			{	continue;	}
			try
			{
				rating = reviewTokens[Columns.RATING.ordinal()];
				review = reviewTokens[Columns.REVIEW.ordinal()];
				int[] sentiments = countSentiments(review);

				String result = String.format("%s||||%s||||%s||||%s||||%s", sentiments[0], sentiments[1], sentiments[2], review, rating);
				reportSentiment(result);
			} catch (Exception e) {continue;}
		}
		READER.close();
	}
}
