import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ConsolidateAF
{
	public enum AF
	{	AUTHOR, TOTAL, ONE, TWO, THREE, FOUR, FIVE	};

	final static int MAX_TOKENS = AF.values().length;
	final static String TAB = "\t";

	// Numerical indexes to track the review processing
	private static String CAT_PRICE = "";
	private static String OUAFILE = "";
	private static String AUTHOR_VAL = "";
	private static Integer[] COUNTS = new Integer[] {0, 0, 0, 0, 0, 0};
	private static Integer CAT_PRICE_COUNT = 0;

	/** DICTIONARIES: English, Auxilliary */
	// Auxilliary dictionary containing jargon, product names, etc
	private static Set<String> CAT_PRICES = new TreeSet<String>();

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static String FOLDER = "./thesis/authors-paid/";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Integer VALUE;
	private static Integer[] VALUES;

	private static Map<String, Integer[]> RATED_AF = new HashMap<String, Integer[]>();
	private static Map<String, Integer> TOTAL_AF = new HashMap<String, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		// loadCatPrices("./category-list-no-us.txt");
		// loadCatPrices("./category-free.txt");
		loadCatPrices("./category-paid.txt");
		parseAFFiles();
		writeAFs();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CAT_PRICE_COUNT + TAB + " AF price-categories consoldated, in " + timeTaken);
	}

	private static void loadCatPrices(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )	{	CAT_PRICES.add(prepCatPriceLine(minorClean(LINE)));	}
		}
		READER.close();
	}

	private static String prepCatPriceLine(String catPrice)
	{	return String.format("%s%s-af.tsv", FOLDER, catPrice);	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	private static void parseAFFiles() throws Exception
	{
		try
		{
			for ( String catPrice : CAT_PRICES )
			{
				READER = new BufferedReader(new FileReader(catPrice));

				for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
				{
					TOKENS = LINE.split(TAB);
					if ( TOKENS.length == MAX_TOKENS )
					{
						AUTHOR_VAL = TOKENS[AF.AUTHOR.ordinal()];
						COUNTS[0] = Integer.parseInt(TOKENS[AF.TOTAL.ordinal()]);
						COUNTS[1] = Integer.parseInt(TOKENS[AF.ONE.ordinal()]);
						COUNTS[2] = Integer.parseInt(TOKENS[AF.TWO.ordinal()]);
						COUNTS[3] = Integer.parseInt(TOKENS[AF.THREE.ordinal()]);
						COUNTS[4] = Integer.parseInt(TOKENS[AF.FOUR.ordinal()]);
						COUNTS[5] = Integer.parseInt(TOKENS[AF.FIVE.ordinal()]);
						addToMaps(AUTHOR_VAL, COUNTS);
					}
				}
				CAT_PRICE_COUNT++;
				READER.close();
			}
		}
		catch (Exception e)
		{	System.err.println("Error: " + e.getMessage());	}
	}

	private static void addToMaps(String term, Integer[] counts)
	{
		VALUE = TOTAL_AF.containsKey(term) ? TOTAL_AF.get(term) : 0;
		VALUES = RATED_AF.containsKey(term) ? RATED_AF.get(term) : new Integer[] {0,0,0,0,0};
		VALUE += counts[0];
		for ( int i = 1; i < counts.length; i++ )
		{	VALUES[i-1] += counts[i];	}
		TOTAL_AF.put(term, VALUE);
		RATED_AF.put(term, VALUES);
	}

	private static void writeAFs() throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "all-af.tsv"));

		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(TOTAL_AF));
		resultMap.putAll(TOTAL_AF);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			AUTHOR_VAL = entry.getKey();
			COUNTS = RATED_AF.get(AUTHOR_VAL);
			S_BUILDER.setLength(0);
			S_BUILDER.append(AUTHOR_VAL);
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());

			for ( Integer i : COUNTS )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(i);
			}
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();

		}
		WRITER.close();
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
