import java.io.*;

import java.text.DecimalFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class LemmaReviewValuator
{
	public enum AppData
	{
		APP_ID, REV_ID, REV_DATE, APP_VER, T_WC, T_CC, B_WC,
		B_CC, RATING, AUTH_NAME, AUTH_ID, TITLE, BODY
	};

	public enum IDF
	{	DOC, TOK, VAL	};

	final static int MAX_IDF_TOKENS = IDF.values().length;
	final static int MAX_REV_TOKENS = AppData.values().length;
	final static String TAB = "\t";
	final static String PIPE = "\\|\\|";
	final static Pattern WORDS = Pattern.compile("[a-zA-z]+");
	final static Map<String, Double> EMPTY = new HashMap<String, Double>();

	private static Double SCORE;
	private static String CAT = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Matcher MATCHER;
	private static Stanford LEMMATISER;
	private static TextCleaner CLEANER;
	private static NGramer NGRAMER;
	private static Map<String, Map<String, Double>> CAT_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> PHRASE_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> STAR_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> REVIEWS = new TreeMap<String, Map<String, Double>>();


	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long start = System.currentTimeMillis();

		CLEANER = new TextCleaner("./assets", false, S_UTIL);
		NGRAMER = new NGramer(S_UTIL, 3, true);
		LEMMATISER = new Stanford();
		CAT = "free_health";//args[2].substring(18, 29);
		// String phraseLength = args[0];

		loadCatIDFs("./reports/entertainment/lemma-tfidf-tf-free_entertainment.tsv");
		loadStarIDFs("./reports/entertainment/lemma-tfidf-starf-free_entertainment.tsv");
		// loadPhraseIDFs("./reports/" + phraseLength + "phrase-freq/free-health-pfidf.tsv");
// writePhraseMap();
		parseReviews("./reviews/data/us_free_entertainment_reviews.csv");//20words-30reviews/287529757.tsv");
		writeResults("./reports/entertainment/lemma-tf-starf-fentertainment-scores.tsv");

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Valuation " + "completed in " + elapsed);
	}

	// private static void writePhraseMap()
	// {
	// 	for(Entry<String, Map<String, Double>> app : PHRASE_POINTS.entrySet())
	// 	{
	// 		for(Entry<String, Double> vals : app.getValue().entrySet())
	// 		{	System.out.println(app.getKey() + " > " + vals.getKey() + " > " + vals.getValue());	}
	// 	}
	// }

	// private static void writeStarMap()
	// {
	// 	for(Entry<String, Map<String, Map<String, Double>>> app : PHRASE_POINTS.entrySet())
	// 	{
	// 		System.out.println(app.getKey());
	// 		for(Entry<String, Map<String, Double>> doc : app.entrySet())
	// 		{
	// 			for(Entry<String, Double> vals : doc.entrySet())
	// 			{	System.out.println(doc.getKey() + " > " + vals.getKey() + " > " + vals.getValue());	}
	// 		}
	// 	}
	// }

	private static void writeResults(String filePath) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, filePath);
		for ( Entry<String, Map<String, Double>> app : REVIEWS.entrySet() )
		{//new DecimalFormat("#0.0000000000000000000000").format(score)
			Map<String, Double> preSort = app.getValue();
			Map<String, Double> sorted = new TreeMap<String, Double>(new hoon.util.ValueComparer<String, Double>(preSort));
			sorted.putAll(preSort);
			for ( Entry<String, Double> review : sorted.entrySet() )
			{
				TOKENS = new String[3];
				TOKENS[0] = app.getKey();
				TOKENS[1] = review.getKey();
				TOKENS[2] = new DecimalFormat("#0.0000000000000000000000").format(review.getValue());
				writer.writeLine(S_UTIL.delimitTokens(TOKENS, TAB));
				// System.out.println(S_UTIL.delimitTokens(TOKENS, TAB));
			}
		}
	}

	private static void parseReviews(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, PIPE);
			if( TOKENS.length != MAX_REV_TOKENS )
			{	continue;	}
			String appID = TOKENS[AppData.APP_ID.ordinal()];
			String revID = TOKENS[AppData.REV_ID.ordinal()];
			String rating = TOKENS[AppData.RATING.ordinal()];
			String review = TOKENS[AppData.TITLE.ordinal()] + " " + TOKENS[AppData.BODY.ordinal()];
			String raw = TOKENS[AppData.TITLE.ordinal()] + " " + TOKENS[AppData.BODY.ordinal()];
			review = CLEANER.correct(review);
			if ( S_UTIL.tokenise(review, "\\ ").length > 20 )
			{
				review = lemma(review);
				review = CLEANER.correct(review);
				saveScore(appID, revID, rating, raw, parseTokens(review, raw, appID, rating));
			}
		}
		reader.close();
	}

	private static Double parseTokens(String text, String raw, String doc, String rating)
	{
		SCORE = 0.0;
		MATCHER = WORDS.matcher(text);
		Map<String, Double> nestedMap;
		Set<String> phrases;
		String key = "";
		while ( MATCHER.find() )
		{
			LINE = S_UTIL.enforceAlphaNumerics(MATCHER.group());
			nestedMap = CAT_POINTS.containsKey(doc) ? CAT_POINTS.get(doc) : EMPTY;
			SCORE += nestedMap.containsKey(LINE) ? nestedMap.get(LINE) : 0.0;
			key = doc + TAB + rating;
			nestedMap = STAR_POINTS.containsKey(key) ? STAR_POINTS.get(key) : EMPTY;
			SCORE += nestedMap.containsKey(LINE) ? nestedMap.get(LINE) : 0.0;
		}
		// nestedMap = PHRASE_POINTS.containsKey(doc) ? PHRASE_POINTS.get(doc) : EMPTY;
		// if ( nestedMap != EMPTY )
		// {	phrases = NGRAMER.countPhrases(raw);
		// 	for ( String phrase : phrases )
		// 	{	SCORE += nestedMap.containsKey(phrase) ? nestedMap.get(phrase) : 0.0;	}
		// }

		return SCORE;
	}

	private static void saveScore(String appID, String revID, String rating, String review, Double score)
	{
		if ( score > 0.0 )
		{
			TOKENS = new String[3];
			TOKENS[0] = revID;
			TOKENS[1] = rating;
			TOKENS[2] = review;
			Map<String, Double> scoreMap = REVIEWS.containsKey(appID) ? REVIEWS.get(appID) : new HashMap<String, Double>();
			scoreMap.put(S_UTIL.delimitTokens(TOKENS, TAB), score);
			REVIEWS.put(appID, scoreMap);
		}
	}

	private static void loadCatIDFs(String filePath) throws IOException
	{
		Double value;
		Map<String, Double> nestedMap;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_IDF_TOKENS )	{	continue;	}
			try
			{
				value = Double.parseDouble(TOKENS[IDF.VAL.ordinal()]);
				nestedMap = CAT_POINTS.containsKey(TOKENS[IDF.DOC.ordinal()]) ? CAT_POINTS.get(TOKENS[IDF.DOC.ordinal()]) : new HashMap<String, Double>();
				nestedMap.put(TOKENS[IDF.TOK.ordinal()], value);
				CAT_POINTS.put(TOKENS[IDF.DOC.ordinal()], nestedMap);
			}
			catch (NumberFormatException nfe)	{	continue;	}
		}
		reader.close();
	}

	private static void loadStarIDFs(String filePath) throws IOException
	{
		Double value;
		String app = "";
		String key = "";
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		Map<String, Double> nestedMap;
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length == MAX_IDF_TOKENS )
			{
				try
				{
					value = Double.parseDouble(TOKENS[IDF.VAL.ordinal()]);
					key = app + TAB + TOKENS[IDF.DOC.ordinal()];
					nestedMap = STAR_POINTS.containsKey(key) ? STAR_POINTS.get(key) : new HashMap<String, Double>();
					nestedMap.put(TOKENS[IDF.TOK.ordinal()], value);
					STAR_POINTS.put(key, nestedMap);
				}
				catch (NumberFormatException nfe)	{	continue;	}
			}
			else
			{	app = LINE;	}
		}
		reader.close();
	}

	private static void loadPhraseIDFs(String filePath) throws IOException
	{
		Double value;
		Map<String, Double> nestedMap;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_IDF_TOKENS )	{	continue;	}
			try
			{
				value = Double.parseDouble(TOKENS[IDF.VAL.ordinal()]);
				nestedMap = PHRASE_POINTS.containsKey(TOKENS[IDF.DOC.ordinal()]) ? PHRASE_POINTS.get(TOKENS[IDF.DOC.ordinal()]) : new HashMap<String, Double>();
				nestedMap.put(TOKENS[IDF.TOK.ordinal()], value);
				PHRASE_POINTS.put(TOKENS[IDF.DOC.ordinal()], nestedMap);
			}
			catch (NumberFormatException nfe)	{	continue;	}
		}
		reader.close();
	}

	private static String lemma(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
