import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ConsolidateTF
{
	public enum TF
	{	TERM, TOTAL, ONE, TWO, THREE, FOUR, FIVE	};

	final static int MAX_TOKENS = TF.values().length;
	final static String TAB = "\t";

	// Numerical indexes to track the review processing
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";
	private static String TERM_VAL = "";
	private static Integer[] COUNTS = new Integer[] {0, 0, 0, 0, 0, 0};
	private static Integer CAT_PRICE_COUNT = 0;

	/** DICTIONARIES: English, Auxilliary */
	// Auxilliary dictionary containing jargon, product names, etc
	private static Set<String> CAT_PRICES = new TreeSet<String>();

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Integer VALUE;
	private static Integer[] VALUES;

	private static Map<String, Integer[]> RATED_TF = new HashMap<String, Integer[]>();
	private static Map<String, Integer> TOTAL_TF = new HashMap<String, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCatPrices("./category-list-no-us.txt");
		parseTFFiles();
		writeTFs();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CAT_PRICE_COUNT + TAB + " TF price-categories consoldated, in " + timeTaken);
	}

	private static void loadCatPrices(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )	{	CAT_PRICES.add(prepCatPriceLine(minorClean(LINE)));	}
		}
		READER.close();
	}

	private static String prepCatPriceLine(String catPrice)
	{	return String.format("./thesis/lang/%s-tf.tsv", catPrice);	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	private static void parseTFFiles() throws Exception
	{
		try
		{
			for ( String catPrice : CAT_PRICES )
			{
				READER = new BufferedReader(new FileReader(catPrice));

				for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
				{
					TOKENS = LINE.split(TAB);
					if ( TOKENS.length == MAX_TOKENS )
					{
						TERM_VAL = TOKENS[TF.TERM.ordinal()];
						COUNTS[0] = Integer.parseInt(TOKENS[TF.TOTAL.ordinal()]);
						COUNTS[1] = Integer.parseInt(TOKENS[TF.ONE.ordinal()]);
						COUNTS[2] = Integer.parseInt(TOKENS[TF.TWO.ordinal()]);
						COUNTS[3] = Integer.parseInt(TOKENS[TF.THREE.ordinal()]);
						COUNTS[4] = Integer.parseInt(TOKENS[TF.FOUR.ordinal()]);
						COUNTS[5] = Integer.parseInt(TOKENS[TF.FIVE.ordinal()]);
						addToMaps(TERM_VAL, COUNTS);
					}
				}
				CAT_PRICE_COUNT++;
				READER.close();
			}
		}
		catch (Exception e)
		{	System.err.println("Error: " + e.getMessage());	}
	}

	private static void addToMaps(String term, Integer[] counts)
	{
		VALUE = TOTAL_TF.containsKey(term) ? TOTAL_TF.get(term) : 0;
		VALUES = RATED_TF.containsKey(term) ? RATED_TF.get(term) : new Integer[] {0,0,0,0,0};
		VALUE += counts[0];
		for ( int i = 1; i < counts.length; i++ )
		{	VALUES[i-1] += counts[i];	}
		TOTAL_TF.put(term, VALUE);
		RATED_TF.put(term, VALUES);
	}

	private static void writeTFs() throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/lang/all-tf.tsv"));

		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(TOTAL_TF));
		resultMap.putAll(TOTAL_TF);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			TERM_VAL = entry.getKey();
			COUNTS = RATED_TF.get(TERM_VAL);
			S_BUILDER.setLength(0);
			S_BUILDER.append(TERM_VAL);
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());

			for ( Integer i : COUNTS )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(i);
			}
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();

		}
		WRITER.close();
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
