import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class SplitAF
{
	public enum AF
	{	AUTHOR, TOTAL, FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static int MAX_TOKENS = AF.values().length;
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/authors-rerun/";

	// Numerical indexes to track the review processing
	private static String AUTHOR_VAL = "";
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	private static Integer TOTAL_FREQ = 0;

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Integer VALUE;
	private static Set<String> COUNT_SET;

	private static Map<Integer, Integer[]> TOTALS_MAP = new TreeMap<Integer, Integer[]>();
	private static Map<Integer, Set<String>> COUNT_MAPS = new TreeMap<Integer, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		parseAllAFFile();
		writeAFs();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("AF report aggregated in " + timeTaken);
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static void parseAllAFFile() throws Exception
	{
		int value = 0;
		try
		{
			READER = new BufferedReader(new FileReader(FOLDER + "/all-af.tsv"));
			Map<Integer, Integer> nestedMap;
			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				TOKENS = LINE.split(TAB);
				if ( TOKENS.length == MAX_TOKENS )
				{
					COUNT = Integer.parseInt(TOKENS[AF.TOTAL.ordinal()]);
					TOTAL_FREQ += COUNT;
					COUNT_SET = COUNT_MAPS.containsKey(COUNT) ? COUNT_MAPS.get(COUNT) : new HashSet<String>();
					COUNT_SET.add(LINE);
					COUNT_MAPS.put(COUNT, COUNT_SET);
					COUNTS = TOTALS_MAP.containsKey(COUNT) ? TOTALS_MAP.get(COUNT) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
					for ( int i = AF.FONE.ordinal(); i < MAX_TOKENS; i++ )
					{
						value = Integer.parseInt(TOKENS[i]);
						COUNTS[i] += value;
					}
					TOTALS_MAP.put(COUNT, COUNTS);
				}
			}
			READER.close();
		}
		catch (Exception e)
		{	System.err.println("Error: " + e.getMessage());	}
	}

	private static String prepLine(Integer[] counts)
	{
		S_BUILDER.setLength(0);
		for ( Integer i : counts )
		{
			S_BUILDER.append(i.toString());
			S_BUILDER.append(TAB);
		}
		return S_BUILDER.toString().trim();
	}

	private static void writeAFs() throws IOException
	{

		BufferedWriter writer = new BufferedWriter(new FileWriter(FOLDER + "report-af.tsv"));
		for ( Entry<Integer, Set<String>> entry : COUNT_MAPS.entrySet() )
		{
			// fileName = FOLDER + entry.getKey() + "-review-af.tsv";
			// WRITER = new BufferedWriter(new FileWriter(FOLDER + entry.getKey() + "-review-af.tsv"));

			// for ( String line : entry.getValue() )
			// {
			// 	WRITER.write(line.trim());
			// 	WRITER.newLine();
			// }
			// WRITER.close();
			COUNTS = TOTALS_MAP.containsKey(entry.getKey()) ? TOTALS_MAP.get(entry.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			COUNTS[AF.AUTHOR.ordinal()] = entry.getKey();
			writer.write(prepLine(COUNTS));
			writer.newLine();
		}
		writer.close();
	}
}


/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
