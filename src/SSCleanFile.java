import java.io.*;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.TextFile;

public class CleanFile
{
	public enum Index
	{	AUTHOR, TITLE, REVIEW, RATING, ISO, VERSION, DATE, PRODUCT_ID, LANGS, APP_NAME, PLATFORM, REVIEW_ID	};

	// Numerical indexes to track the review processing
	private static Integer RAW_REVIEW_COUNT = 0;
	private static Integer REVIEW_COUNT = 0;
	final static int MAX_TOKENS = Index.values().length;
	final static String PIPE = "||";

	// regex for valid text
	private static Pattern VALID_PATTERN = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+");
	private static Pattern DUPE_CHAR_PATTERN = Pattern.compile("(\\w)\\1+");

	/** DICTIONARIES: English, Auxilliary */
	// English dictionary
	private static Map<String, HashSet<String>> ENG_DICTIONARY = new HashMap<String, HashSet<String>>();
	// Auxilliary dictionary containing jargon, product names, etc
	private static Set<String> AUX_DICTIONARY = new HashSet<String>();

	/** Commonly typo-ed and truncated words/expressions */
	// Abbreviations/Txt Spk, commonly typoed words and supplementary typos based on manual analysis dictionary containing expansions
	private static Map<String, String> ABBRV_TYPOS = new HashMap<String, String>();

	/** Tracking and monitoring structures */
	// Map of odd words we encounter. Key = word, Val = Frequency
	private static Map<String, Integer> ODD_WORDS = new HashMap<String, Integer>();
	private static Map<String, String> WORD_CORRECTIONS = new TreeMap<String, String>();

	/** File I/O */
	private static BufferedWriter WRITER;
	private static BufferedReader READER;

	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}
		loadReferenceData();
		parseReviewTextFile(args[0]);

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Text stabilisation on " + formatNumber(REVIEW_COUNT) + " reviews took " + timeTaken);
	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private static void loadReferenceData() throws Exception
	{
		String folder = "assets";
		// ENGLISH DICTIONARY
		loadDictionary(getFilePath(folder, "dictionary", "txt"));
		// AUXILLIARY DICTIONARY
		loadDictionary(AUX_DICTIONARY, getFilePath(folder, "aux-dictionary", "txt"));
		// APP NAMES
		loadDictionary(AUX_DICTIONARY, getFilePath(folder, "app-name-words", "txt"));
		// ABBREVIATION/TXT SPK
		loadTypos(ABBRV_TYPOS, getFilePath(folder, "abbrv-txt-spk", "csv"));
		// COMMONLY TYPO-ED WORDS
		loadTypos(ABBRV_TYPOS, getFilePath(folder, "common-typos", "csv"));
		// TRAINED TYPOS
		loadTypos(ABBRV_TYPOS, getFilePath(folder, "aux-common-typos", "csv"));
		// Norvig speller training corpus
		// NORVIG_SPELLER = new Spelling(getFilePath(folder, "corpus", "txt"));
		System.out.println("===================================");
		System.out.println("Loading of Reference Data Complete.");
		System.out.println("===================================");
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getFilePath(String folder, String fileName, String extension)
	{	return String.format("./%s/%s.%s", folder, fileName, extension);	}

	/**
	 * Populates the dictionary map based on the text file. The prefix of the
	 * word forms the key of the map, the word itself is added to the HashSet of
	 * values associated with that prefix. Makes parsing through the dictionary
	 * for string similarity jaroWinklerAlgos MUCH faster.
	 */
	private static void loadDictionary(String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for (String line : theFile)
		{
			line = minorClean(line);
			String prefix = getPrefix(line);
			HashSet<String> values = new HashSet<String>();

			if(ENG_DICTIONARY.containsKey(prefix))
			{   values = ENG_DICTIONARY.get(prefix);    }

			values.add(line);
			ENG_DICTIONARY.put(prefix, values);
		}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the any auxilliary dictionaries based on filename param.
	 */
	private static void loadDictionary(Set<String> theSet, String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for (String line : theFile)
		{	theSet.add(minorClean(line));	}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private static void loadTypos(Map<String, String> theMap, String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for(String line : theFile)
		{
			String[] tokens = line.split(",");
			theMap.put(minorClean(tokens[0]), minorClean(tokens[1]));
		}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Returns the first two characters of the String param. Failing that,
	 * returns "noprefix". SHOULD BE USED THROUGHOUT THIS SCRIPT FOR CONSISTENCY
	 * PERTAINING TO ENGLISH DICTIONARY CHECKS.
	 */
	private static String getPrefix(String word)
	{	return word.length() > 2 ? new String(word.substring(0,2)) : "noprefix";	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Dictionary checker. Run the param word against our English Dictionary
	 * data structure and return true if the word is found. Failing that, check
	 * agaist the Auxilliary dictionary, return true if found, or false if not.
	 */
	private static boolean checkDictionary(String word)
	{
		HashSet<String> subDictionary = ENG_DICTIONARY.get(getPrefix(word));
		if(subDictionary != null && subDictionary.contains(word))
		{   return true; }
		return AUX_DICTIONARY.contains(word);
	}

	/**
	 * Check if the param word has been previously stabilised from our
	 * ABBRV_TYPOS map. If the word has been previously stabilised, return the
	 * stabilised word, else, the word.
	 */
	private static String checkReplacements(String word)
	{	return ABBRV_TYPOS.containsKey(word) ? ABBRV_TYPOS.get(word) : word;	}

	/**
	 * Check if we have previously attempted spell correction of the param word
	 * from our WORD_CORRECTIONS map and UNCORRECTED_WORDS set. If the word has
	 * had previous correction attempts, return the result of the previous
	 * attempt, or failing that, the param word.
	 */
	private static String checkCorrections(String word)
	{
		if(checkDictionary(word) || ODD_WORDS.containsKey(word))
		{	return word;	}

		if(WORD_CORRECTIONS.containsKey(word))
		{	return WORD_CORRECTIONS.get(word);	}

		add(ODD_WORDS, word);
		return word;
	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	private static String stringTreatment(String review)
	{
		review = review.replaceAll("\\`","\\'");
		review = review.replaceAll("\\'m", "\\ am");
		review = review.replaceAll("\\'\\ ", "\\ ");
		review = review.replaceAll("\\ s ","s ");
		review = review.replaceAll("\\'ll", "\\ will");
		review = review.replaceAll("\\'re", "\\ are");
		review = review.replaceAll("\\'d","\\ would");
		review = review.replaceAll("\\'ve","\\ have");
		return minorClean(review);
	}

	/**
	 *
	 */
	private static String stabiliseText(String rawText)
	{	return applyLookup(stringTreatment(rawText));	}

	private static String applyLookup(String text)
	{
		boolean wordAdded = false;
		StringBuilder sBuilder = new StringBuilder();
		Matcher matcher = VALID_PATTERN.matcher(text);
		while (matcher.find())
		{
			String wordToAdd = minorClean(matcher.group());
			if(!wordToAdd.matches(".*\\d.*"))
			{
				if(ABBRV_TYPOS.containsKey(wordToAdd))
				{	wordToAdd = checkReplacements(wordToAdd);	}
				else
				{
					if(!checkDictionary(wordToAdd))
					{	wordToAdd = checkCorrections(wordToAdd);	}
				}
			}

			sBuilder.append(wordToAdd);
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)	// Remove the trailing space
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }
		return sBuilder.toString();
	}

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseReviewTextFile(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		WRITER = new BufferedWriter(new FileWriter(getFilePath("sportsmate", "cleanedReviews", "txt"), true));
		BufferedWriter ios = new BufferedWriter(new FileWriter(getFilePath("sportsmate", "iosReviews", "txt"), true));
		BufferedWriter and = new BufferedWriter(new FileWriter(getFilePath("sportsmate", "andReviews", "txt"), true));

		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split("\\|\\|");
			if( TOKENS.length < MAX_TOKENS ) {	continue;	}
			S_BUILDER.delete(0, S_BUILDER.length());
			for (int i = 0; i < MAX_TOKENS; i++)
			{
				// System.out.println(TOKENS[i]);
				if( i != Index.TITLE.ordinal() || i != Index.REVIEW.ordinal() )
				{	S_BUILDER.append((TOKENS[i].trim()));	}
				else
				{	S_BUILDER.append(stabiliseText(stringTreatment(TOKENS[i])).trim());	}
				if ( i != Index.REVIEW_ID.ordinal() )
				{	S_BUILDER.append(PIPE);	}
			}
			// System.out.println(TOKENS[Index.ISO.ordinal()].trim());
			if ( TOKENS[Index.ISO.ordinal()].trim().equals("ZZ") )
			{
				ios.write(S_BUILDER.toString().trim());
				ios.newLine();
			}
			else
			{
				and.write(S_BUILDER.toString().trim());
				and.newLine();
			}
			// System.out.println(S_BUILDER.toString());
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		READER.close();
		WRITER.close();
		ios.close();
		and.close();
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
