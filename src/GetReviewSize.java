import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class GetReviewSize
{
	public enum AppData
	{
		APP_ID, REV_ID, REV_DATE, APP_VER, T_WC, T_CC, B_WC,
		B_CC, RATING, AUTH_NAME, AUTH_ID, TITLE, BODY
	};

	public enum InData
	{	DATASET, APP, REVIEW, RATING, ANSWER	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int IN_TOKENS = InData.values().length;
	final static int APP_TOKENS = AppData.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String COMMA = ",";

	private static TextCleaner CLEANER;
	private static int TOTAL = 0;
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Map<String, Integer> REVIEWS = new HashMap<String, Integer>();
	private static Map<Integer, String> RESULTS = new HashMap<Integer, String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long start = System.currentTimeMillis();
		CLEANER = new TextCleaner("./assets", false, S_UTIL);
		parseIn("./reports/survey-10/html/data-decomp.csv");
		parseReviews();
		write("./reports/survey-10/html/data-decomp-wc.csv", "DATASET,APP,REVIEW,RATING,ANSWER,SIZE");
		parseAndWrite("./reports/survey-10/html/data-decomp.csv");
		// write("./reports/survey-10/html/data-decomp-wc.csv", Integer.toString(TOTAL));
		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Dataset Consolidation " + "completed in " + elapsed);
	}

	private static void write(String filePath, String line) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, filePath);
		writer.writeLine(line);
	}

	private static void parseIn(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.length() == 0 )	{	continue;	}
			TOKENS = S_UTIL.tokenise(LINE, COMMA);
			if( TOKENS.length != IN_TOKENS )
			{	continue;	}
			REVIEWS.put(TOKENS[InData.REVIEW.ordinal()], 0);
		}
		reader.close();
	}

	private static void parseReviews() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader("./reviews/data/us_free_health_reviews.csv"));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, PIPE);
			if( TOKENS.length != APP_TOKENS )
			{	continue;	}
			if (REVIEWS.containsKey(TOKENS[AppData.REV_ID.ordinal()]))
			{
				int size =  S_UTIL.tokenise(TOKENS[AppData.TITLE.ordinal()] + " " + TOKENS[AppData.BODY.ordinal()], "\\ ").length;
				REVIEWS.put(TOKENS[AppData.REV_ID.ordinal()], size);
			}
		}
		reader.close();
	}

	private static void parseAndWrite(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.length() == 0 )	{	continue;	}
			TOKENS = S_UTIL.tokenise(LINE, COMMA);
			if( TOKENS.length != IN_TOKENS )	{	continue;	}

			write("./reports/survey-10/html/data-decomp-wc.csv", S_UTIL.delimitTokens(TOKENS, COMMA)+COMMA+REVIEWS.get(TOKENS[InData.REVIEW.ordinal()]));
			// System.out.println(S_UTIL.delimitTokens(TOKENS, COMMA)+COMMA+REVIEWS.get(TOKENS[InData.REVIEW.ordinal()]));

			// REVIEWS.put(TOKENS[InData.REVIEW.ordinal()], 0);
		}
		reader.close();
	}


					// for ( int j = 0; j < counts[i]; j++ )
					// {	write("./reports/survey-10/html/data-decomp.csv", S_UTIL.delimitTokens(outLine, COMMA));	}
			// 	}
			// }
			// catch ( NumberFormatException nfe )	{	continue;	}

}
