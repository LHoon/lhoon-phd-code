import java.io.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class ExtractRandomSurveyReviews
{
	public enum Tokens
	{	APP_ID, APP_NAME, REVIEW_ID, RATING, TITLE, BODY, SCORE	};

	final static Integer SEED = 7;
	final static int MAX_TOKENS = Tokens.values().length;
	final static int THRESHOLD = 300;
	final static String TAB = "\t";
	final static boolean READ = true;
	final static boolean WRITE = false;

	static int REVIEW_COUNT = 0;
	static String FILE;
	static String LINE = "";
	static String[] TOKENS = new String[MAX_TOKENS];
	// static Set<String> OUT_SET = new HashSet<String>();
	static Map<Integer, List<String>> REVIEWS = new HashMap<Integer, List<String>>();

	static BufferedWriter WRITER;
	static Random GENERATOR = new Random(SEED);
	static StringUtil S_UTIL = new StringUtil();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Please provide price and category parameter (free_health, paid_entertainment, etc) for extraction parameters.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		FILE = args[0];
		parseFile();
		randomReviewsByStar();
		// writeResults();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Seeded random extraction completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @return	String value constructed from ./reports/survey-10/tiered/FILE.tsv
	 */
	private static String inPath()
	{	return String.format("./reports/survey-10/scored/%s-scores-moreThan10.tsv", FILE);	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @return	String value constructed from ./reports/survey-10/random/%s.tsv
	 */
	private static String outPath()
	{	return String.format("./reports/survey-live/random/%s-Scored.tsv", FILE);	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(outPath(), true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	// private static void getRandoms() throws IOException
	// {
	// 	String line;
	// 	int numOfRNG = THRESHOLD / REVIEWS.size();
	// 	for ( Entry<String, List<String>> rating : REVIEWS.entrySet() )
	// 	{
	// 		int[] rngArray = getRNGarray(rating.getValue().size(), numOfRNG);
	// 		for ( int i = 0; i < numOfRNG; i++ )
	// 		{	writeLine(rating.getValue().get(i));	}
	// 	}
	// }

	// private static void writeResults() throws IOException
	// {
	// 	for ( String line : OUT_SET )
	// 	{	writeLine(line);	}
	// }


	private static void randomReviewsByStar() throws IOException
	{
		int max = THRESHOLD/5;
		String fileName = "RawByStar";
		for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
		{	getNReviewsFromList(entry.getValue(), max, fileName);	}
	}

	private static void getNReviewsFromList(List<String> reviewList, int numExtracted, String fileName) throws IOException
	{
		String line;
		Set<Integer> randomSet = getRNGset(reviewList.size(), numExtracted);
		for ( Integer i : randomSet )
		{	writeLine(reviewList.get(i));	}
	}

	private static Set<Integer> getRNGset(int ceiling, int count)
	{
		Set<Integer> result = new HashSet<Integer>();
		while( result.size() < count )
		{	result.add(GENERATOR.nextInt(ceiling));	}
		return result;
	}

	private static void parseFile() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(inPath()));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split(TAB);
			if( TOKENS.length != MAX_TOKENS )	{	continue;	}
			int rating = 0;
			try
			{	rating = Integer.parseInt(TOKENS[Tokens.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			persist(rating, LINE);
		}
		reader.close();
	}

	private static void persist(int rating, String text)
	{
		List<String> reviewList = REVIEWS.containsKey(rating) ? REVIEWS.get(rating) : new ArrayList<String>();
		reviewList.add(text);
		REVIEWS.put(rating, reviewList);
	}
}
