import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class TabDroidIndex
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String IN_FOLDER = "./reviews/droid/index/";
	final static String OUT_FOLDER = "./dataset/droid/index/";
	final static String HEADER = "APP_NAME	APP_ID	COUNTRY	URL	PRICE	CURRENCY	BIN_SIZE	VERSION	RANK	#";
	final static boolean FREE = true;
	final static boolean PAID = false;

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;

	private static List<String> BAD_LIST = new ArrayList<String>();
	private static List<String> REPEAT_LIST = new ArrayList<String>();
	private static Set<String> CATEGORIES = new HashSet<String>();
	// private static Map<String, String> APP_IDS = new TreeMap<String, String>();
	// private static Map<String, Set<String>> APP_IDS = new TreeMap<String, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./dataset/droid-categories.txt");
		readCategories();
		// writeList("bad-lines", "txt", BAD_LIST, false);
		// writeMap("repeats", "tsv", APP_IDS, false);

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATEGORIES.size() + " categories parsed, with " + formatNumber(REVIEW_COUNT) + " reviews tabbed, of " + formatNumber(LINES_READ) +  " lines read with " + BAD_LIST.size() + " bad lines with " + REPEAT_LIST.size() + " repeats, in " + timeTaken);
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( !line.equals("") )
			{	CATEGORIES.add(minorClean(line));	}
		}
		reader.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void readCategories() throws IOException
	{
		for ( String category : CATEGORIES )
		{
			readIndexes(getCatName(category, FREE));
			readIndexes(getCatName(category, PAID));
		}
	}

	private static void readIndexes(String fileName) throws IOException
	{
		boolean isOk;
		Set<String> reviewSet = new HashSet<String>();
		String[] tokens;
		String clean;
		String appID;
		StringBuilder sb = new StringBuilder();
		Set<String> cats;
		BufferedReader reader = new BufferedReader(new FileReader(String.format("%sen_%s.csv", IN_FOLDER, fileName)));
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.tsv", OUT_FOLDER, fileName)));
		writer.write(HEADER);
		writer.newLine();
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( line.equals("") )	{	continue;	}

			tokens = line.split(PIPE);
			LINES_READ++;
			if ( tokens.length == MAX_META_TOKENS )
			{
				appID = tokens[AppMeta.APP_ID.ordinal()];
				clean = line.replaceAll(PIPE, TAB);
				if ( clean.split(TAB).length == MAX_META_TOKENS )
				{
					isOk = true;
					for ( String s : clean.split(TAB) )
					{
						if ( s.length() == 0 )	{	isOk = false;	}
					}
					if ( isOk )
					{
						writer.write(clean.trim());
						writer.newLine();
						continue;
					}
				}
			}
			BAD_LIST.add(String.format("%s%s%s", fileName, TAB, line));
			continue;
		}
		reader.close();
		writer.close();
	}

	private static void writeSet(String fileName, String extension, Set<String> reviews, boolean doCount) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
		for ( String review : reviews )
		{
			if ( doCount )	{	REVIEW_COUNT++;	}
			writer.write(review);
			writer.newLine();
		}
		writer.close();
	}

	private static void writeList(String fileName, String extension, List<String> reviews, boolean doCount) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
		for ( String review : reviews )
		{
			if ( doCount )	{	REVIEW_COUNT++;	}
			writer.write(review);
			writer.newLine();
		}
		writer.close();
	}

	private static void writeMap(String fileName, String extension, Map<String, Set<String>> apps, boolean doCount) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
		StringBuilder sb = new StringBuilder();
		for ( Entry<String, Set<String>> entry : apps.entrySet() )
		{
			if ( doCount )	{	REVIEW_COUNT++;	}
			if ( entry.getValue().size() > 1 )
			{
				sb.setLength(0);
				sb.append(entry.getKey());
				for ( String cat : entry.getValue() )
				{
					sb.append(TAB);
					sb.append(cat);
				}
				writer.write(sb.toString());
				writer.newLine();
			}
		}
		writer.close();
	}
}
