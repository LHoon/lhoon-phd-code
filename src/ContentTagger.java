import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public class ContentTagger
{
	public enum Review
	{	APP_ID, APP_NAME, PRICE, RANK, REVIEW_ID, AUTHOR, DATE, VERSION, RATING, R_TITLE, R_BODY, R_TWC, R_BWC, C_TITLE, C_BODY, C_TWC, C_BWC	};

	final static int NUM_TOKENS = Review.values().length;
	final static String TAG_DELIMITER = ":";
	final static String FILE_DELIMITER = "\t";

	static Map<String, List<String>> CONTENT_KEYS = new HashMap<String, List<String>>();
	static Map<String, String> TAGGED_REVIEWS = new HashMap<String, String>();
	static Set<String> STOP_WORDS = new HashSet<String>();
	static Set<String> POS_WORDS = new TreeSet<String>();
	static Set<String> NEG_WORDS = new TreeSet<String>();
	static Set<String> TAGGED_SET = new HashSet<String>();
	static Set<String> UNTAGGED = new HashSet<String>();
	static StringBuilder S_BUILDER = new StringBuilder();

	public static void main(String[] args)
	{
		try
		{
			loadKeywords();
			// String file = "387771637-2.tsv";
			// String path = "reviews/3/";

			String file = "287529757.tsv";
			String path = "reviews/us_free_health/20words-30reviews/";

			// writeTags(path+"tagged-"+file);
			loadWordSet("stop");
			loadWordSet("pos");
			loadWordSet("neg");  // WRITE THE CODE OT LOOP AND TAG PER SET
			System.out.println(STOP_WORDS.size() + " " +POS_WORDS.size() + " "+NEG_WORDS.size());
			writeHTMLHeader();
			parseReviews(path+file);
			System.out.println(UNTAGGED.size());
			writeHTML();
			writeHTMLFooter();
		}
		catch (IOException ioe)
		{	System.exit(1);	}
	}

	private static void loadWordSet(String type) throws IOException
	{
		String fileName = "./assets/";
		switch(type)
		{
			case "stop":	fileName += "stop-words.txt";
							break;
			case "pos":		fileName += "positive-words.txt";
							break;
			case "neg":		fileName += "negative-words.txt";
							break;
			default: break;
		}
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( type.equals("stop") )
			{	STOP_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
			else if ( type.equals("pos") )
			{	POS_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
			else if ( type.equals("neg") )
			{	NEG_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
		}
		reader.close();

	}

	private static void loadKeywords() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader("./assets/content-keys.txt"));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{
				String tokens[] = line.split(",");
				List<String> keyList = new ArrayList<String>();
				for ( int i = 1; i < tokens.length; i ++ )
				{	keyList.add(tokens[i]);	}
				String key = tokens[0].toLowerCase(Locale.ENGLISH).trim();
				CONTENT_KEYS.put(key, keyList);
			}
		}
		reader.close();
	}

	private static void parseReviews(String filename) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{	tagTest(line);	}
		}
		reader.close();
	}

	private static void tagContent(String line)
	{
		Set<String> cleanTags = new TreeSet<String>();
		Set<String> dirtyTags = new TreeSet<String>();

		String tokens[] = line.split(FILE_DELIMITER);
		if ( tokens.length == NUM_TOKENS )
		{
			String dirtyText = tokens[Review.R_TITLE.ordinal()] + " " + tokens[Review.R_BODY.ordinal()];
			String cleanText = tokens[Review.C_TITLE.ordinal()] + " " + tokens[Review.C_BODY.ordinal()];

			for(Entry<String, List<String>> entry : CONTENT_KEYS.entrySet())
			{
				for( String s : entry.getValue() )
				{
					if( dirtyText.contains(s) )
					{	dirtyTags.add(entry.getKey() + TAG_DELIMITER + s);	}
					if( cleanText.contains(s) )
					{	cleanTags.add(entry.getKey() + TAG_DELIMITER + s);	}
				}
			}
			String tags = formatTags(cleanTags, dirtyTags);
			TAGGED_REVIEWS.put(line, tags);
		}
	}

	private static void tagTest(String line)
	{
		Set<String> tags = new TreeSet<String>();
		String tokens[] = line.split(FILE_DELIMITER);
		boolean hasTag = false;

		int rating = 0;
		try
		{	rating += Integer.parseInt(tokens[Review.RATING.ordinal()]);	}
		catch (NumberFormatException nfe)
		{	return;	}
		if( rating == 1 || rating == 2 || rating == 3)
		{
			if ( tokens.length == NUM_TOKENS )
			{
				String dirtyText = tokens[Review.R_TITLE.ordinal()] + " " + tokens[Review.R_BODY.ordinal()];
				String cleanText = tokens[Review.C_TITLE.ordinal()] + " " + tokens[Review.C_BODY.ordinal()];

				for(Entry<String, List<String>> entry : CONTENT_KEYS.entrySet())
				{
					for( String s : entry.getValue() )
					{
						if( cleanText.contains(s) )
						{
							String tagged = tagString(s, entry.getKey());
							cleanText = cleanText.replaceAll(s, tagged);
							hasTag = true;
						}
						if( dirtyText.contains(s) )
						{
							String tagged = tagString(s, entry.getKey());
							dirtyText = dirtyText.replaceAll(s, tagged);
							hasTag = true;
						}
					}
				}
				cleanText = parseKeySets(cleanText);
				String result = "<tr><td class=\"reviewID\">" + tokens[Review.REVIEW_ID.ordinal()] + "</td><td class=\"rating\">"  + tokens[Review.RATING.ordinal()] + "</td><td>" + dirtyText + "</td><td>" + cleanText + "</td></tr>";
				if ( !hasTag )
				{	UNTAGGED.add(result);	};
				TAGGED_SET.add(result);
			}
		}
	}

	private static String tagString(String word, String tag)
	{	return "<span class=\"" + tag + "\">" + word + "</span>";	}

	private static String parseKeySets(String line)
	{
		String result = line;
		line = line.replaceAll("\\.", "\\|\\|");
		line = line.replaceAll("\\?", "\\|\\|");
		line = line.replaceAll("\\!", "\\|\\|");
		String[] sentences = line.split("\\|\\|");
		boolean doTag = false;
		String tag = "";
		int negCount = 0;
		int posCount = 0;
		Map<String,List<String>> tagValues = new HashMap<String, List<String>>();
		List<String> tokensToTag = new ArrayList<String>();
		for ( String sentence : sentences )
		{
			String[] tokens = sentence.split("\\ ");
			for ( String token : tokens )
			{
				if ( POS_WORDS.contains(token) )
				{
					posCount++;
					// tokensToTag = tagValues.containsKey("posword") ? tagValues.get("posword") : new ArrayList<String>();
					// tokensToTag.add(token);
					// tagValues.put("posword", tokensToTag);
					doTag = true;
				}
				else if ( NEG_WORDS.contains(token) )
				{
					negCount++;
					// tokensToTag = tagValues.containsKey("negword") ? tagValues.get("negword") : new ArrayList<String>();
					// tokensToTag.add(token);
					// tagValues.put("negword", tokensToTag);
					doTag = true;
				}
			}
			if ( doTag )
			{
				if ( negCount > posCount )
				{	tag = "negline";	}
				else
				{	tag = "posline";	}
				tokensToTag = tagValues.containsKey(tag) ? tagValues.get(tag) : new ArrayList<String>();
					tokensToTag.add(sentence);
				tagValues.put(tag, tokensToTag);
				doTag = false;
			}
		}
		for ( Entry<String, List<String>> entry : tagValues.entrySet())
		{
			for (String tok : entry.getValue() )
			{	result = result.replaceAll("\\b"+tok+"\\b", tagString(tok, entry.getKey()));	}
		}
		return result;
	}

	private static String formatTags(Set<String> cleanSet, Set<String> dirtySet)
	{
		// Set one = firstSet;
		// Set two = secondSet;
		// one.removeAll(secondSet);
		// two.removeAll(firstSet);

		Set<String> commonTags = new TreeSet<String>(cleanSet);
		Set<String> differentTags = new TreeSet<String>();

		differentTags.addAll(cleanSet);
		differentTags.addAll(dirtySet);

		commonTags.retainAll(dirtySet);
		differentTags.removeAll(commonTags);

		String cleanTags = "\tclean: ";
		String dirtyTags = "\tdirty: ";
		String commons = "";

		boolean cleanFirst = true;
		boolean dirtyFirst = true;

		for ( String s : differentTags )
		{
			if ( cleanSet.contains(s) )
			{
				if (!cleanFirst)
				{	cleanTags += ", " + s;	}
				else
				{
					cleanTags += s;
					cleanFirst = false;
				}
			}
			if ( dirtySet.contains(s) )
			{
				if (!dirtyFirst)
				{	dirtyTags += ", " + s;	}
				else
				{
					dirtyTags += s;
					dirtyFirst = false;
				}
			}
		}

		boolean isFirst = true;
		for ( String s : commonTags )
		{
			if (!isFirst)
			{	commons += ", " + s;	}
			else
			{
				commons += s;
				isFirst = false;
			}
		}
		return commons + dirtyTags + cleanTags;
	}

	private static void writeTags(String filename) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		try
		{
			for(Entry<String, String> entry : TAGGED_REVIEWS.entrySet())
			{
				S_BUILDER.append(entry.getKey());
				S_BUILDER.append(FILE_DELIMITER);
				S_BUILDER.append(entry.getValue());
				writer.write(S_BUILDER.toString().trim());
				writer.newLine();
				wipeBuilder();
			}
		}

		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}

	private static void writeHTML() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter("./reports/content-tags/test.html", true));
		try
		{
			for(String s : TAGGED_SET)
			{
				writer.write(s);
				writer.newLine();
			}
			writer.write("</table>");
			writer.newLine();
			writer.write("<br>");
			writer.newLine();
			writer.write("<table>");
			writer.newLine();
			for(String s : UNTAGGED)
			{
				writer.write(s);
				writer.newLine();
			}
		}

		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}

	private static void wipeBuilder()
	{
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }
	}

	private static void writeHTMLHeader() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter("./reports/content-tags/test.html", false));
		try
		{
			writer.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			writer.newLine();
			writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
			writer.newLine();
			writer.write("<head>");
			writer.newLine();
			writer.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"./categories.css\">");
			writer.newLine();
			writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
			writer.newLine();
			writer.write("<title>Tagged Content</title>");
			writer.newLine();
			writer.write("</head>");
			writer.newLine();
			writer.write("<body>");
			writer.newLine();
			writer.write("<table>");
			writer.newLine();
		}

		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}

	private static void writeHTMLFooter() throws IOException
	{

		BufferedWriter writer = new BufferedWriter(new FileWriter("./reports/content-tags/test.html", true));
		try
		{
			writer.write("</table>");
			writer.newLine();
			writer.write("</body>");
			writer.newLine();
			writer.write("</html>");
			writer.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}
}
