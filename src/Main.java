import java.io.*;
import java.util.Set;
import java.util.TreeSet;

public class Main
{
  public static void main(String[] args) throws IOException
  {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    String s;
    Set<Integer> absSet = new TreeSet<Integer>();

    boolean isJolly = false;
    boolean isConfirmed = false;

    int current;
    int previous = 0;
    int lineNum = 0;

    while ((s = in.readLine()) != null)
    {
        lineNum++;
        //Assuming String is to be split by space "\\ "
        String[] val = s.split("\\ ");

        if (val.length == 1) // Single sequence; auto jolly
        {
            isJolly = true;
            isConfirmed = true;
            continue;
        }
        if (val.length > 1)
        {
            try
            {
                previous = Integer.parseInt(val[0]);
                for (int i = 1; i < val.length; i++)
                {
                    current = Integer.parseInt(val[i]);

                    //false condition = non-unique sequence difference
                    // ASSUMPTION: the differences MUST BE unique
                    if ( !absSet.add(current - previous) )
                    {
                        isConfirmed = true;
                        break;
                    }

                    previous = current;
                }
            }
            catch (NumberFormatException nfe)
            {
                isConfirmed = true;
                System.err.println("Number Format Exception in line: "+ lineNum );
                break;
            }

            if ( !isConfirmed )
            {
                previous = absSet.first();
                for (Integer i : absSet)
                {
                    if ( i == previous )    {   continue;   }

                    if ( Math.abs(i - previous) != 1)   // non-successive difference
                    {   break;    }
                }
                isJolly = true;
            }
        }

        String result = isJolly ? "'Jolly'" : "'Not jolly'";
        System.out.println(result);
        isJolly = false;
        isConfirmed = false;
        absSet.clear();
    }
    in.close();
  }
}
