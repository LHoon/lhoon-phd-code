import java.io.*;

import java.text.DecimalFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class AggregateScores
{
	public enum Data
	{
		APP_ID, APP_NAME, REV_ID, RATING, TITLE, BODY, SCORE
	};

	final static int MAX_TOKENS = Data.values().length;
	final static String TAB = "\t";
	final static Pattern WORDS = Pattern.compile("[a-zA-z]+");

	private static Double HIGHEST = 0.0;//2202585031524119;
	private static Double LOWEST = 0.0;
	private static Double[] TIER = new Double[4];
	private static String FILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Set<String> RAW_SCORES = new HashSet<String>();
	private static Map<Integer, Set<String>> SCORES = new TreeMap<Integer, Set<String>>();


	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Please provide price and category parameter (free_health, paid_entertainment, etc) for analysis context.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		FILE = args[0];
		for ( int i = 1; i <=5; i++ )
		{
			parseScores(filePath(Integer.toString(i)));
			establishScoreTiers();
			aggregateScores();

			if (args.length > 1 && args[1].equals("print"))
			{	writeResults(Integer.toString(i));	}
			print();

			HIGHEST = 0.0;
			LOWEST = 0.0;
			RAW_SCORES.clear();
			SCORES.clear();
		}

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review value aggregation completed in " + elapsed);
	}

	private static String filePath(String rating)
	{	return String.format("./reports/survey-10/scored/%s-%s-star.tsv", FILE, rating);	}

	private static String outPath()
	{	return String.format("./reports/survey-10/tiered/%s.tsv", FILE);	}

	private static void writeResults(String rating) throws IOException
	{
		for ( Entry<Integer, Set<String>> entry : SCORES.entrySet() )
		{
			DelimitedWriter writer = new DelimitedWriter(TAB, outPath());
			for ( String review : entry.getValue() )
			{	writer.writeLine(review + TAB + entry.getKey());	}
		}
	}

	private static void parseScores(String filePath) throws IOException
	{
		boolean isFirst = true;
		Double value;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_TOKENS )	{	continue;	}
			try
			{
				value = Math.abs(Double.parseDouble(TOKENS[Data.SCORE.ordinal()]));
				// if ( value < 0.11 ) {	value = 0.11;	}
				// if ( value > 0.112 ) {	value = 0.112;	}
				if ( HIGHEST < value ) {	HIGHEST = value;	}
				if ( isFirst )
				{
					LOWEST = value;
					isFirst = false;
				}
				if ( LOWEST > value && value > 0.0 ) {	LOWEST = value;	}
				RAW_SCORES.add(LINE);
				// System.out.println(value + " > " + LINE);
			}
			catch (NumberFormatException nfe)	{	continue;	}
		}
		System.out.println(HIGHEST);
		System.out.println(LOWEST);
		reader.close();
	}

	private static void establishScoreTiers()
	{
		Double segment = ( HIGHEST - LOWEST) / TIER.length;
		for ( int i = 0; i < TIER.length; i++ )
		{
			TIER[i] = ( segment * ( i + 1 ) ) + LOWEST;
		System.out.println("Tier " + i + ": " + TIER[i]);	}
	}

	private static void aggregateScores()
	{
		Double value;
		for ( String line : RAW_SCORES )
		{
			Integer aggregateScore = 0;
			TOKENS = S_UTIL.tokenise(line, TAB);
			for ( int i = 0; i < TIER.length; i++ )
			{
				try
				{
					value = Math.abs(Double.parseDouble(TOKENS[Data.SCORE.ordinal()]));
					if( value < TIER[i])
					{
						aggregateScore = i;
						break;
					}
				}
				catch ( NumberFormatException nfe )	{	continue;	}
			}
			Set<String> nestedSet = SCORES.containsKey(aggregateScore) ? SCORES.get(aggregateScore) : new HashSet();
					nestedSet.add(line);
			SCORES.put(aggregateScore, nestedSet);
		}
	}

	private static void print()
	{
		System.out.println("Populated Tiers: " + SCORES.size());
		for ( Entry<Integer, Set<String>> entry : SCORES.entrySet() )
		{	System.out.println(entry.getKey() + " > " + entry.getValue().size());	}
	}
}
