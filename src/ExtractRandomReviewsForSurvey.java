import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class ExtractRandomReviewsForSurvey
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum Tokens
	{	APP_ID, REVIEW_ID, RATING, REVIEW, SCORE	};

	final static Integer SEED = 1;
	final static int MAX_TOKENS = Tokens.values().length;
	final static int THRESHOLD = 30;
	final static String DELIMITER = "\\t";
	final static boolean READ = true;
	final static boolean WRITE = false;

	static String FILE_NAME;
	static String CATEGORY;
	static String LINE;
	static String[] TOKENS;
	static StringBuilder S_BUILDER;
	static Set<Integer> RNG_SET;
	static Map<Integer, List<String>> REVIEWS = new HashMap<Integer, List<String>>();
	static Map<String, String> ID_NAME = new HashMap<String, String>();

	static BufferedReader READER;
	static BufferedWriter WRITER;
	static Random GENERATOR = new Random(SEED);

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("[0] = CATEGORY, [1] = APP IDs.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CATEGORY = args[0];
		FILE_NAME = args[1];
		getAppNames("./reviews/index/us_free_" + CATEGORY + ".csv");
		parseFile(filePath("", READ));
		writeReviews();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Extraction completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	fileName name of the file to be loaded (appId)
	 * @param	isRead boolean value to indicate pre- or post-cull file
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String filePath(String append, boolean isRead)
	{
		// String folder = isRead ? CATEGORY : "culled-"+CATEGORY;
		String folder = CATEGORY;
		String file = (append.length() == 0) ? FILE_NAME : FILE_NAME+"-"+append;
		return String.format("./reports/%s/%s.tsv", folder, file);
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String fileAppend, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath(fileAppend, WRITE), true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void writeReviews() throws IOException
	{
		String line;
		for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
		{
			int index = 0;
			RNG_SET = getRNGs(entry.getValue().size());
			for ( Integer i : RNG_SET )
			{
				line = entry.getValue().get(i);
				System.out.println(entry.getKey().toString() + "\t" + line);
				// writeLine(entry.getKey().toString(), line);
				index++;
				if ( index == 5 )
				{	break;	}
			}
		}
	}

	private static void getAppNames(String filePath) throws IOException
	{
		int appsFound = 0;
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split("\\|\\|");
			if( TOKENS.length < 9 ) {	continue;	}
			ID_NAME.put(TOKENS[AppMeta.APP_ID.ordinal()], TOKENS[AppMeta.APP_NAME.ordinal()]);
			// System.out.println(TOKENS[AppMeta.APP_ID.ordinal()] + "\t" + TOKENS[AppMeta.APP_NAME.ordinal()]);
		}
		READER.close();
	}

	private static Set<Integer> getRNGs(int max)
	{
		Set<Integer> result = new HashSet<Integer>();
		while( result.size() < THRESHOLD )
		{	result.add(GENERATOR.nextInt((max) - 0) + 0);	}
		return result;
	}

	private static void parseFile(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( String LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split(DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating = Integer.parseInt(TOKENS[Tokens.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String appName = ID_NAME.get(TOKENS[Tokens.APP_ID.ordinal()]);
			S_BUILDER = new StringBuilder(LINE);
			S_BUILDER.append("\t");
			S_BUILDER.append(appName);
			S_BUILDER.append("\t");
			S_BUILDER.append(TOKENS[Tokens.REVIEW_ID.ordinal()]);
			persist(rating, S_BUILDER.toString());
		}
		reader.close();
	}

	private static void persist(int rating, String text)
	{
		List<String> reviewList = REVIEWS.containsKey(rating) ? REVIEWS.get(rating) : new ArrayList<String>();
		reviewList.add(text);
		REVIEWS.put(rating, reviewList);
	}
}
