import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class ExtractSurveyReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static boolean CAT_TF = true;
	final static boolean STAR_TF = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";

	private static String IN_FILE = "";
	private static String CAT = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static TextCleaner CLEANER;
	private static TermCounter CAT_COUNTER;
	private static TermCounter STAR_COUNTER;
	private static TFIDFer TFIDFER;
	private static Stanford LEMMATISER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Please provide price and category parameter (free_health, paid_entertainment, etc) for analysis context.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT = args[0];
		CLEANER = new TextCleaner("./assets", false, S_UTIL);
		CAT_COUNTER = new TermCounter(S_UTIL);
		STAR_COUNTER = new TermCounter(S_UTIL);
		LEMMATISER = new Stanford();
		parseReviewsForTermFreq(reviewFilePath(DATA));
		writeTermFreqs(CAT_TF);
		writeTermFreqs(STAR_TF);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Term Frequency Analysis " + "completed in " + elapsed);
	}

	private static String reviewFilePath(boolean isData)
	{
		return isData ? String.format("./reviews/data/us_%s_reviews.csv", CAT) : String.format("./reviews/index/us_%s.csv", CAT);
	}

	private static String fileOutPath(String fileName)
	{   return String.format("./reports/survey/term-freq/cat-level/%s-star-tf/%s.tsv", CAT, fileName);	}

	private static void parseReviewsForTermFreq(String file) throws IOException
	{
		System.out.println(file);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split(PIPE);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			String appID = TOKENS[AppData.APP_ID.ordinal()];
			String rating = TOKENS[AppData.RATING.ordinal()];
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			review = CLEANER.correct(review);S
			review = lemmatisation(review);
			review = CLEANER.correct(review);
			CAT_COUNTER.countWords(appID, review);		// document is app id
			STAR_COUNTER.countWords(rating, review);	// document is rating
		}
		reader.close();
		//writeTFs



	}

	private static void writeTermFreqs(boolean isCategoryLvl) throws IOException
	{
		DelimitedWriter writer = isCategoryLvl ? new DelimitedWriter(TAB, "./reports/survey/term-freq/cat-level/" + CAT + ".tsv") : new DelimitedWriter(TAB, "./reports/survey/term-freq/star-level/" + CAT + ".tsv");
		if ( isCategoryLvl )
		{	CAT_COUNTER.writeTermFreqs(writer, 0);	}
		else
		{	STAR_COUNTER.writeTermFreqs(writer, 0);	}
	}

	private static String lemmatisation(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
