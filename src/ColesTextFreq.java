import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class ColesTextFreq
{
	public enum CommentTokens
	{	BRAND, ZONE, REGION, STORE, DEPT, JOB, EGMT, ORG, VAL, BASIC, LNMGR	};

	final static boolean BY_ZONE = true;
	final static boolean BY_TYPE = false;
	final static int NUM_TOKENS = CommentTokens.values().length;
	final static String TAB = "\t";

	private static Map<String, Map<String, Integer>> DOCUMENTS = new HashMap<String, Map<String, Integer>>();
	private static Map<String, Map<String, Double>> TFIDFS = new HashMap<String, Map<String, Double>>();
	private static Set<Comment> COMMENTS = new HashSet<Comment>();

	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static BufferedReader READER;
	private static Stanford LEMMATISER;
	private static TextCleaner CLEANER;
	private static TFIDFer TFIDFERZ;
	private static TFIDFer TFIDFERT;

	static boolean PRINT_TAGGED_ONLY = false;
	static String IN_FILE = "./comments/coles.tsv";
	// static String OUT_FILE = "./comments/coles-tf.html";

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if ( args.length == 1 )
		{
			try					{	Boolean.parseBoolean(args[0]);	}
			catch (Exception e)	{	PRINT_TAGGED_ONLY = true;	}
		}
		long start = System.currentTimeMillis();

		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", true, sU);
		TFIDFERZ = new TFIDFer(sU);
		TFIDFERT = new TFIDFer(sU);
		parseInput();

		DelimitedWriter writer = new DelimitedWriter(TAB, "./comments/coles-zone-sdist.tsv");
		countWords(BY_ZONE);
		TFIDFERZ.calculateTfIdfs();
		TFIDFERZ.writeTFIDFs(writer, 0);

		writer = new DelimitedWriter(TAB, "./comments/coles-type-sdist.tsv");
		countWords(BY_TYPE);
		TFIDFERT.calculateTfIdfs();
		TFIDFERT.writeTFIDFs(writer, 0);


		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static void parseInput() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(IN_FILE));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{	COMMENTS.add(new Comment(line, TAB, NUM_TOKENS));	}
		}
		reader.close();
	}

	private static void countWords(boolean byZone)
	{
		int index = 0;
		String docID = "";
		EnumSet<CommentTokens> tokenSet = EnumSet.of(CommentTokens.EGMT, CommentTokens.ORG, CommentTokens.VAL, CommentTokens.BASIC, CommentTokens.LNMGR);
		for ( Comment c : COMMENTS )
		{
			for ( CommentTokens commTok : tokenSet )
			{
				index = commTok.ordinal();
				if ( c.tokens[index].length() > 0 )
				{
					c.tokens[index] = CLEANER.correct(c.tokens[index]);
					if ( byZone )
					{
						docID = c.tokens[CommentTokens.ZONE.ordinal()];
						TFIDFERZ.countWords(docID, c.tokens[index]);
					}
					else
					{
						docID = commTok.toString();
						TFIDFERT.countWords(docID, c.tokens[index]);
					}
				}
			}
		}
	}

	private static String lemmatisation(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{	for (CoreLabel token: sentence.get(TokensAnnotation.class))	{	lemmas.add(token.get(LemmaAnnotation.class));	}	}
		return lemmas;
	}

	public String lemma(String text)
	{	return this.lemmatise(text).get(0);	}
}

class Comment
{
	public String[] tokens;
	public String rawLine;

	public Comment (String input, String delimiter, int arraySize)
	{
		this.initialiseTokens(arraySize);
		this.rawLine = input;
		this.populateTokens(input.split(delimiter));
	}

	private void initialiseTokens(int arraySize)
	{
		this.tokens = new String[arraySize];
		for ( int i = 0; i < this.tokens.length; i++ )
		{	this.tokens[i] = "";	}
	}

	private void populateTokens(String[] values)
	{
		for ( int i = 0; i < values.length; i++ )
		{
			if ( !values[i].equals("") || !values[i].equals(null) )
			{	this.tokens[i] = values[i].toLowerCase(Locale.ENGLISH).trim();	}
		}
	}

	public String toHtml()
	{
		String pre = "<td>";
		String post = "</td>";
		String result = "";
		for( String s : this.tokens )
		{
			if ( s.equals(null) )
			{	s = "";	}
			result += pre + s + post;
		}
		return result;
	}

	public String toString()
	{	return this.rawLine;	}
}
