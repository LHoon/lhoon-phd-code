import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.DelimitedWriter;
import hoon.textfx.TextCleaner;
import hoon.util.ValueComparer;

public class RootedPhraseFreq
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static Integer THRESHOLD;
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	// private static Map<String, int[]> PHRASES = new HashMap<String, int[]>();
	private static Map<String, Integer> PRE_SORT = new HashMap<String, Integer>();
	private static Map<String, Integer> POST_SORT;
	private static Map<String, Set<String>> ROOT_WORDS = new HashMap<String, Set<String>>();
	private static Map<String, Map<String, int[]>> PHRASES = new HashMap<String, Map<String, int[]>>();

	private static TextCleaner CLEANER;
	private static Stanford LEMMATISER;
	private static BufferedReader READER;
	private static DelimitedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("Provide review file for text frequency analysis and phrase size threshold.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT_PRICE = args[0];
		OUTFILE = "phrase-freq-" + CAT_PRICE.substring(3, CAT_PRICE.length());

		try	{	THRESHOLD = Integer.parseInt(args[1]);	}
		catch (NumberFormatException nfe)	{	System.exit(1);	}
		CLEANER = new TextCleaner("./assets", false);
		LEMMATISER = new Stanford();

		loadRoots("./reports/lemma-tfidf.tsv");
		findReviews(getPath(CAT_PRICE, DATA));//"./reviews/alls/us_free_all_reviews.csv");
		writePhrases();

		// POST_SORT = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(PRE_SORT));

		// DelimitedWriter writer = new DelimitedWriter(OUT_DELIMITER, "./reports/lemma-tfidf.tsv");
		// TFIDFER.calculateTfIdfs();
		// TFIDFER.writeTFIDFs(writer, 150);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static void writePhrases() throws IOException
	{
		TOKENS = new String[8];
		int[] values;
		POST_SORT = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(PRE_SORT));
		for(Entry<String, Map<String, int[]>> appEntry : PHRASES.entrySet())
		{
			if(appEntry.getValue().size() > 0)
			{
				sortPhrases(appEntry.getValue(), 5);
				WRITER = new DelimitedWriter(OUT_DELIMITER, "./reports/rootedphrases/" + appEntry.getKey() + ".tsv");
				for ( Entry<String, Integer> entry : POST_SORT.entrySet() ) //appEntry.getValue().entrySet() )
				{
					values = appEntry.getValue().get(entry.getKey()); //entry.getValue();
					TOKENS[0] = appEntry.getKey();
					TOKENS[1] = entry.getKey();
					for ( int i = 0; i < values.length; i++ )
					{	TOKENS[(i+2)] = String.valueOf(values[i]);	}
					WRITER.writeLine(WRITER.delimitTokens(TOKENS));
				}
			}
		}
	}

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			String appID = TOKENS[AppData.APP_ID.ordinal()];
			review = CLEANER.correct(review);
			// review = lemmatisation(review);
			// review = CLEANER.correct(review);
			extractRootedPhrases(appID, review, rating);
		}
		READER.close();
	}

	/**
	 * Method to extract N Gram phrases out of the line of text based on the indexOfKeyword provided and THRESHOLD for depth.
	 * If THRESHOLD = 3, the phrases 1, 2, 3, Key; 1, 2, Key, 3; 1, Key, 2, 3; Key, 1, 2, 3 are extracted.
	 * Phrases with only one word (i.e. only the keyword) are ignored.
	 * @param 	tokens text block source which has be split by space into a String[]
	 * @param 	indexOfKeyWord int index of the Key word
	 * @param 	ontType Ontology type that this phrase corresponds to
	 */
	private static void extractNGram(String[] tokens, List<Integer> keyList, String appID, int rating)
	{
		int tokenLength = tokens.length;
		for ( Integer keyIdx : keyList )
		{
			for(int i = 0; i <= THRESHOLD; i++)
			{
				int currentOffset = keyIdx - i;
				for (int j = 0; j <= THRESHOLD; j++)	// Loop through each permutation of results
				{
					int currentIdx = currentOffset + j;
					if (currentIdx >= 0 && currentIdx < tokenLength)	// Loop through each word
					{
						S_BUILDER.append(tokens[currentIdx].toString().replaceAll("[^\\p{L}\\p{N}]", "").toLowerCase(Locale.ENGLISH));
						S_BUILDER.append(" ");
					}
				}
				if(S_BUILDER.toString().matches(".*\\s+.*"))
				{	savePhrase(appID, S_BUILDER.toString().trim(), rating);	}
				S_BUILDER.delete(0, S_BUILDER.length());
			}
		}
	}

	private static void extractRootedPhrases(String appID, String text, int rating)
	{
		text = new StringBuilder(text.trim()).insert(0," ").toString();
		String[] tokens = text.split("\\ ");
		Set<String> rootSet = ROOT_WORDS.get(appID);
		List<Integer> keyIdx = new ArrayList<Integer>();
		for(int idx = 0; idx < tokens.length; idx++)
		{
			if ( rootSet.contains("\\b" + tokens[idx] + "\\b") )
			{	keyIdx.add(idx);	}	// System.out.println(tokens[idx] + " : " + idx);
		}
		extractNGram(tokens, keyIdx, appID, rating);
	}

	private static void loadRoots(String filePath) throws IOException
	{
		String boundary = "\\b";
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(OUT_DELIMITER);
			if( TOKENS.length != 3 )	{	continue;	}

			Set<String> tokenSet = ROOT_WORDS.containsKey(TOKENS[0]) ? ROOT_WORDS.get(TOKENS[0]) : new HashSet<String>();
			tokenSet.add(boundary + TOKENS[1] + boundary);
			ROOT_WORDS.put(TOKENS[0], tokenSet);
		}
		READER.close();
	}

	private static void printPatterns()
	{
		System.out.println(ROOT_WORDS.size());
		for ( Entry<String, Set<String>> entry : ROOT_WORDS.entrySet() )
		{	System.out.println(entry.getKey() + " > " + entry.getValue());	}
	}

	private static String lemmatisation(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}

	private static void savePhrase(String appId, String phrase, int rating)
	{
		Map<String, int[]> appPhrases = PHRASES.containsKey(appId) ? PHRASES.get(appId) : new HashMap<String, int[]>();
		int[] phraseCounts = appPhrases.containsKey(phrase) ? appPhrases.get(phrase) : new int[]{0, 0, 0, 0, 0, 0};
		phraseCounts[rating - 1]++;
		phraseCounts[5]++;;
		appPhrases.put(phrase, phraseCounts);
		PHRASES.put(appId, appPhrases);
	}

	/**
	 * Sort by total number of instances the phrase (map key) occurs (represented by value).
	 */
	private static void sortPhrases(Map<String, int[]> phraseMap, int index)
	{
		PRE_SORT.clear();
		POST_SORT.clear();
		for (Entry<String, int[]> entry : phraseMap.entrySet())
		{  PRE_SORT.put(entry.getKey(), entry.getValue()[index]);    }
		POST_SORT.putAll(PRE_SORT);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reports/%s.tsv", fileName);	}
}
	// private static void findReviews(String filePath) throws IOException
	// {
	// 	READER = new BufferedReader(new FileReader(filePath));
	// 	for ( LINE = ""; LINE != null; LINE = READER.readLine() )
	// 	{
	// 		TOKENS = LINE.split(IN_DELIMITER);
	// 		if( TOKENS.length != MAX_TOKENS )
	// 		{	continue;	}
	// 		int rating = 0;
	// 		try
	// 		{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
	// 		catch (NumberFormatException nfe)
	// 		{	continue;	}
	// 		String appId = TOKENS[AppData.APP_ID.ordinal()];
	// 		String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
	// 		review = CLEANER.correct(review);
	// 		review = lemmatisation(review);
	// 		review = CLEANER.correct(review);
	// 		TFIDFER.countWords(appId, review);
	// 	}
	// 	READER.close();
	// }

	// /**
	//  * Method to extract N Gram phrases out of the line of text based on the indexOfKeyword provided and THRESHOLD for depth.
	//  * If THRESHOLD = 3, the phrases 1, 2, 3, Key; 1, 2, Key, 3; 1, Key, 2, 3; Key, 1, 2, 3 are extracted.
	//  * Phrases with only one word (i.e. only the keyword) are ignored.
	//  */
	// private static void getNGram(String appID, String line, int rating)
	// {
	// 	int indexOfKeyWord = 0;
	// 	TOKENS = line.split("\\ ");
	// 	int tokenLength = TOKENS.length;
	// 	Pattern

	// 	if ( S_BUILDER.length() > 0 )
	// 	{	S_BUILDER.delete(0, S_BUILDER.length());	}

	// 	for(int i = 0; i <= THRESHOLD; i++)
	// 	{
	// 		int currentOffset = indexOfKeyWord - i;
	// 		if ( indexOfKeyWord < 0 )	{	continue;	}
	// 		boolean initialWord = true;
	// 		// Loop through each permutation of results
	// 		for (int j = 0; j <= THRESHOLD; j++)
	// 		{
	// 			int currentIdx = currentOffset + j;
	// 			// Loop through each word
	// 			if (currentIdx >= 0 && currentIdx < tokenLength)
	// 			{
	// 				S_BUILDER.append(TOKENS[currentIdx].toString().toLowerCase(Locale.ENGLISH));
	// 				S_BUILDER.append(" ");
	// 			}
	// 		}
	// 		// Store the phrase in the map if there are at least two words in the phrase
	// 		if(S_BUILDER.toString().matches(".*\\s+.*"))
	// 		{	savePhrase(S_BUILDER.toString().trim(), rating);  }
	// 		S_BUILDER.delete(0, S_BUILDER.length());
	// 	}
	// }

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
