import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class AppAF
{
	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum Data
	{	AUTHOR, TOTAL, CATEGORY, APP, REVIEWS, ONE, TWO, THREE, FOUR, FIVE	};


	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_TOKENS = Data.values().length;
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/authors-app/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String AUTHOR_FOLDER = FOLDER + "authors/";
	final static String REPORT_FOLDER = FOLDER + "reports/";
	final static String APP_FOLDER = FOLDER + "apps/";
	final static String HEADER = "AUTHOR	TOTAL	CATEGORY	APP	REVIEWS	ONE	TWO	THREE	FOUR	FIVE";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer BAD_LINES = 0;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> REVIEW_COUNTS = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Map<String, Map<String, Map<String, Integer[]>>> AUTHOR_RATINGS = new HashMap<String, Map<String, Map<String, Integer[]>>>();
	private static Map<String, Integer> AUTHOR_MAP = new HashMap<String, Integer>();
	private static List<String> RETURNING_AUTHORS = new ArrayList<String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadReviewCounts("./assets/review-counts.txt");
		parseReviews();
		writeAppReport();
		writeNAuthors();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(REVIEW_COUNTS.size() + " files processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, from " + formatNumber(AUTHOR_MAP.size()) + " authors, with " + formatNumber(BAD_SET.size()) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	// private static String getCategoryFileName(String category, boolean isFree)
	// {
	// 	String price = isFree ? "free" : "paid";
	// 	return String.format("./reviews/clean-data/tab/%s.tsv", getCatName(category, isFree));
	// }

	private static String getAFFileName(String count)
	{	return String.format("%s%s.tsv", FOLDER, getAFName(count));	}

	private static String getAFName(String count)
	{	return String.format("%s-author", count);	}

	private static void loadReviewCounts(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( !LINE.equals("") )
			{	REVIEW_COUNTS.add(minorClean(LINE));	}
		}
		reader.close();
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws Exception
	{
		for ( String count : REVIEW_COUNTS )	{	parseFile(count);	}
		AUTHOR_MAP = sortByComparator(AUTHOR_MAP);
	}

	private static void parseFile(String count) throws IOException
	{
		int rating = 0;
		String appId = "";
		String author = "";
		BufferedReader reader = new BufferedReader(new FileReader(APP_FOLDER + "returning-authors.tsv"));
		for ( String LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;
			TOKENS = LINE.split(TAB);
			if ( TOKENS.length == MAX_TOKENS )
			{
				appId = TOKENS[AppData.APP_ID.ordinal()];
				rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
				author = TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1];
				// addToMap(author, appId, getCatPrice(category, isFree), rating, isFree);
				continue;
			}
			BAD_SET.add(String.format("%s%s%s", getAFFileName(count), TAB, LINE));
		}
		reader.close();
	}

	private static void addToMap(String author, String appId, String category, int rating, boolean isFree)
	{
		// author's total review count
		Integer count = AUTHOR_MAP.containsKey(author) ? AUTHOR_MAP.get(author) : 0;
		count++;
		AUTHOR_MAP.put(author, count);	// Persist (AUTHOR_ID, TOTAL)

		// get categories the author has reviewed in, no entry pre-build as most authors are infrequent; dont waste mem with empty entries
		Map<String, Map<String, Integer[]>> categories = AUTHOR_RATINGS.containsKey(author) ? AUTHOR_RATINGS.get(author) : new TreeMap<String, Map<String, Integer[]>>();
		Map<String, Integer[]> apps = categories.containsKey(category) ? categories.get(category) : new HashMap<String, Integer[]>();
		Integer[] scores = apps.containsKey(appId) ? apps.get(appId) : new Integer[] {0, 0, 0, 0, 0, 0};
		scores[rating]++;
		scores[SCORES.TOTAL.ordinal()]++;
		apps.put(appId, scores);
		categories.put(category, apps);
		AUTHOR_RATINGS.put(author, categories);
		REVIEW_COUNT++;
	}

	private static void writeNAuthors() throws IOException
	{
		Set<String> authors;
		Set<Integer> reviewCounts = new TreeSet<Integer>();
		reviewCounts.addAll(AUTHOR_MAP.values());
		for ( Integer count : reviewCounts )
		{
			authors = new HashSet<String>();
			for ( Entry<String, Integer> author : AUTHOR_MAP.entrySet() )
			{
				if ( author.getValue() == count )
				{	authors.add(author.getKey());	}
			}
			writeNAuthor(authors, count);
		}
	}

	private static void writeNAuthor(Set<String> authors, int count) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(FOLDER + count + "-authors.tsv"));
		writer.write(HEADER);
		writer.newLine();
		for ( String author : authors )
		{
			Map<String, Map<String, Integer[]>> categories = AUTHOR_RATINGS.get(author);
			for ( Entry<String, Map<String, Integer[]>> category : categories.entrySet() )
			{
				for ( Entry<String, Integer[]> app : category.getValue().entrySet() )
				{
					S_BUILDER.setLength(0);
					S_BUILDER.append(author);
					S_BUILDER.append(TAB);
					S_BUILDER.append(String.valueOf(AUTHOR_MAP.get(author)));
					S_BUILDER.append(TAB);
					S_BUILDER.append(category.getKey());
					S_BUILDER.append(TAB);
					S_BUILDER.append(app.getKey());
					for ( Integer score : app.getValue() )
					{
						S_BUILDER.append(TAB);
						S_BUILDER.append(score);
					}
					writer.write(S_BUILDER.toString().trim());
					writer.newLine();
				}
			}
		}
		writer.close();
	}

	private static void writeAppReport() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(APP_FOLDER + "returning-authors.tsv"));
		writer.write(HEADER);
		writer.newLine();
		for ( Entry<String, Integer> author : AUTHOR_MAP.entrySet() )
		{
			if ( AUTHOR_RATINGS.get(author.getKey()).size() != author.getValue() )
			{
				for ( Entry<String, Map<String, Integer[]>> category : AUTHOR_RATINGS.get(author.getKey()).entrySet() )
				{
					for ( Entry<String, Integer[]> app : category.getValue().entrySet() )
					{
						if ( app.getValue()[SCORES.TOTAL.ordinal()] > 1 )
						{
							S_BUILDER.setLength(0);
							S_BUILDER.append(author.getKey());
							S_BUILDER.append(TAB);
							S_BUILDER.append(String.valueOf(AUTHOR_MAP.get(author.getKey())));
							S_BUILDER.append(TAB);
							S_BUILDER.append(category.getKey());
							S_BUILDER.append(TAB);
							S_BUILDER.append(app.getKey());
							for ( Integer score : app.getValue() )
							{
								S_BUILDER.append(TAB);
								S_BUILDER.append(score);
							}
							writer.write(S_BUILDER.toString().trim());
							writer.newLine();
						}
					}
				}
			}
		}
		writer.close();
	}

	private static void writeBadLines() throws Exception
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.txt"));

		for ( String line : BAD_SET )
		{
			writer.write(line.trim());
			writer.newLine();
		}
		writer.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
