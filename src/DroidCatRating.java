import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class DroidCatRating
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum AF
	{	FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/droid-cat-rating/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String REPORT_FOLDER = FOLDER + "reports/";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer CATS_COUNTED = 0;
	private static Integer BAD_LINES = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};	// 1, 2, 3, 4, 5
	private static String AUTHOR = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Map<String, Integer[]> APP_RATINGS = new TreeMap<String, Integer[]>();
	private static Map<String, Integer[]> CAT_RATINGS = new TreeMap<String, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/droid-categories.txt");
		parseReviews();
		writeCatSummary();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATEGORIES.size() + " categories processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, with " + formatNumber(BAD_SET.size()) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("./reviews/clean-droid-trim/data/%s.tsv", getCatName(category, isFree));
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )
			{	CATEGORIES.add(minorClean(LINE));	}
		}
		READER.close();
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws Exception
	{
		for ( String category : CATEGORIES )
		{
			parseFile(category, FREE);
			parseFile(category, PAID);
		}
	}

	private static void parseFile(String category, boolean isFree) throws IOException
	{
		int rating;
		String appID;
		READER = new BufferedReader(new FileReader(getCategoryFileName(category, isFree)));
		System.out.println("Parsing " + getCategoryFileName(category, isFree));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;

			TOKENS = LINE.split(TAB);

			if ( TOKENS.length == MAX_DATA_TOKENS
				&& TOKENS[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
				appID = TOKENS[AppData.APP_ID.ordinal()];
				addToMap(appID, category, rating, isFree);
				continue;
			}
			BAD_SET.add(String.format("%s%s%s", getCategoryFileName(category, isFree), TAB, LINE));
		}
		READER.close();
		writeAppSummary(getCatName(category, isFree));
	}

	private static void addToMap(String appID, String category, int rating, boolean isFree)
	{
		Integer[] ratings = APP_RATINGS.containsKey(appID) ? APP_RATINGS.get(appID) : new Integer[] {0, 0, 0, 0, 0, 0}; // total, 1, 2, 3, 4, 5 star
		ratings[0]++;
		ratings[rating]++;
		APP_RATINGS.put(appID, ratings);
		REVIEW_COUNT++;
	}

	private static void writeAppSummary(String fileName) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(CAT_FOLDER + fileName + ".tsv"));
		Integer[] total = new Integer[] {0, 0, 0, 0, 0, 0}; // total, 1, 2, 3, 4, 5 star
		StringBuilder sb = new StringBuilder();
		for ( Entry<String, Integer[]> app : APP_RATINGS.entrySet() )
		{
			sb.setLength(0);
			sb.append(app.getKey());
			for ( int i = 0; i < app.getValue().length; i++ )
			{
				sb.append(TAB);
				sb.append(formatNumber(app.getValue()[i]));
				total[i] += app.getValue()[i];
			}
			WRITER.write(sb.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
		CAT_RATINGS.put(fileName, total);
		APP_RATINGS.clear();
	}

	private static void writeCatSummary() throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "category-rating-report.tsv"));
		Integer[] total = new Integer[] {0, 0, 0, 0, 0, 0}; // total, 1, 2, 3, 4, 5 star
		StringBuilder sb = new StringBuilder();
		for ( Entry<String, Integer[]> app : CAT_RATINGS.entrySet() )
		{
			sb.setLength(0);
			sb.append(app.getKey());
			for ( int i = 0; i < app.getValue().length; i++ )
			{
				sb.append(TAB);
				sb.append(formatNumber(app.getValue()[i]));
				total[i] += app.getValue()[i];
			}
			WRITER.write(sb.toString().trim());
			WRITER.newLine();
		}

		sb.setLength(0);
		sb.append("TOTAL");
		for ( int i = 0; i < total.length; i++ )
		{
			sb.append(TAB);
			sb.append(formatNumber(total[i]));
		}
		WRITER.write(sb.toString().trim());
		WRITER.newLine();
		WRITER.close();
	}

	private static void writeBadLines() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.txt"));

		for ( String line : BAD_SET )
		{
			WRITER.write(line.trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
