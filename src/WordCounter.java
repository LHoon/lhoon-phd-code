import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCounter
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	static TextCleaner CLEANER;

	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static boolean CLEAN = true;
	final static boolean DIRTY = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static int RATING_RNG = 5;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = ",";

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static Integer THRESHOLD;
	private static Integer MIN_WORDS;
	private static Integer MIN_REVIEWS;
	private static String OUTFILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static List<Integer> RNG_VALUES = new ArrayList<Integer>();
	private static Map<String, String> APP_DETAILS = new HashMap<String, String>();
	private static Map<String, Map<Integer, List<String>>> REVIEWS = new TreeMap<String, Map<Integer, List<String>>>();
	private static Set<String> STOP_WORDS = new HashSet<String>();
	private static Map<String, int[]> CLEAN_COUNT = new TreeMap<String,int[]>();
	private static Map<String, int[]> DIRTY_COUNT = new TreeMap<String,int[]>();

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("No file provided for Word Counting.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		OUTFILE = args[0];
		String category = OUTFILE;
		try
		{
			MIN_WORDS = Integer.parseInt(args[1]);
			MIN_REVIEWS = Integer.parseInt(args[2]);
		}
		catch ( Exception e )
		{
			THRESHOLD = 20;
			MIN_WORDS = 10;
			MIN_REVIEWS = 30;
		}
		CLEANER = new TextCleaner();

		System.out.println("Extracting random reviews from " + category + ".");
		loadStopWords();
		findReviews(getPath(category, DATA));
		writeWordCounts(CLEAN);
		writeWordCounts(DIRTY);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reviews/%s.tsv", fileName);	}

	private static void loadStopWords() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader("./assets/stop-words.txt"));
		for ( String line = ""; line != null; line = reader.readLine() )
		{	STOP_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
		reader.close();
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String filePath, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void writeWordCounts(boolean isClean) throws IOException
	{
		Map<String, int[]> map;
		String type;
		if(isClean)
		{
			type = "clean-wc-";
			map = CLEAN_COUNT;
		}
		else
		{
			type = "dirty-wc-";
			map = DIRTY_COUNT;
		}

		for(Entry<String, int[]> entry : map.entrySet())
		{
			LINE = entry.getKey() + OUT_DELIMITER + entry.getValue()[0] + OUT_DELIMITER + entry.getValue()[1];
			writeLine("./reports/" + type + OUTFILE + ".csv", LINE);
		}
	}

	private static void findReviews(String filePath) throws IOException
	{
// int i = 0;
		READER = new BufferedReader(new FileReader(filePath));
		Set<String> idSet = APP_DETAILS.keySet();
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			countWords(review, DIRTY);
			countWords(review, CLEAN);
// i++;
// if(i>5)break;
		}
		READER.close();
	}

	private static void writeCounts(boolean isClean)
	{
		for(Entry<String, int[]> entry : CLEAN_COUNT.entrySet())
		{	System.out.println(entry.getKey() + "," + entry.getValue()[0] + "," + entry.getValue()[1]);	}
	}

	private static void countWords(String text, boolean isClean)
	{
		Set<String> uniqueWords = new HashSet<String>();
		if(isClean)
		{	text = CLEANER.applyLookup(text);	}
		String[] words = text.toLowerCase(Locale.ENGLISH).trim().split(" ");
		for ( String word : words )
		{
			if( !STOP_WORDS.contains(word) )
			{
				saveWord(word, isClean, 0); //word,isClean,global #
				uniqueWords.add(word);
			}
		}
		for ( String u : uniqueWords )
		{	saveWord(u, isClean, 1);	}
	}

	private static void saveWord(String word, boolean isClean, int index)
	{
		int[] values = {0,0};
		if ( isClean && CLEAN_COUNT.containsKey(word) )
		{	values = CLEAN_COUNT.get(word);	}
		if ( !isClean && DIRTY_COUNT.containsKey(word) )
		{	values = DIRTY_COUNT.get(word);	}
		values[index]++;
		if(isClean)
		{	CLEAN_COUNT.put(word,values);	}
		else
		{	DIRTY_COUNT.put(word,values);	}
	}
}

class TextCleaner
{
	private Map<String, String> abbrvTypos;
	private Pattern validPattern;
	private StringBuilder sBuilder;

	public TextCleaner() throws IOException
	{
		abbrvTypos = new TreeMap<String, String>();
		validPattern = Pattern.compile("[a-zA-Z]+");//[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+");
		sBuilder = new StringBuilder();
		loadTypos("./assets/abbrv-txt-spk.csv");
		loadTypos("./assets/common-typos.csv");
		loadTypos("./assets/aux-common-typos.csv");
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private void loadTypos(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{
				String tokens[] = line.split(",");
				abbrvTypos.put(minorClean(tokens[0]), minorClean(tokens[1]));
			}
		}
		reader.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	public String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	public String stringTreatment(String review)
	{
		review = minorClean(review);
		review = review.replaceAll("(\\.)\\1{1,}", ".");
		review = review.replaceAll("\\`","\\'");
		review = review.replaceAll("\\&","\\and");
		review = review.replaceAll("\\bwon't\\b","will not");
		review = review.replaceAll("\\bcan t\\b"," can't ");
		review = review.replaceAll("\\bdon t\\b","don't");
		review = review.replaceAll("\\bcan't\\b","cannot");
		review = review.replaceAll("'m\\b", "\\ am");
		review = review.replaceAll("\\bs\\b","s ");
		review = review.replaceAll("'ll\\b", "\\ will");
		review = review.replaceAll("'re\\b", "\\ are");
		review = review.replaceAll("'s\\b", "\\ is");
		review = review.replaceAll("\\bi'd\\b","\\ i had");
		review = review.replaceAll("'d\\b","\\ would");
		review = review.replaceAll("'ve\\b","\\ have");
		review = review.replaceAll("n't\\b","\\ not");
		return minorClean(review);
	}

	public String applyLookup(String text)
	{
		boolean wordAdded = false;
		if ( sBuilder.length() > 0 )
		{   sBuilder.delete(0, sBuilder.length());    }

		Matcher matcher = validPattern.matcher(stringTreatment(text));
		while (matcher.find())
		{
			String wordToAdd = matcher.group();
			if(!wordToAdd.matches(".*\\d.*"))
			{	wordToAdd = abbrvTypos.containsKey(wordToAdd) ? wordToAdd = abbrvTypos.get(wordToAdd) : wordToAdd;	}

			sBuilder.append(wordToAdd);
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)	// Remove the trailing space
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }
		return sBuilder.toString();
	}
}
