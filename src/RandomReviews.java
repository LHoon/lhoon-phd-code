import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;

import hoon.util.*;

public class RandomReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};
	public enum Tokens
	{	APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	final static Integer SEED = 7
	;
	final static int MAX_TOKENS = Tokens.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";

	static boolean DO_WRITE = false;
	static int THRESHOLD = 20;
	static String CATEGORY;
	static Set<Integer> RNG_SET;
	private static Map<String, String> ID_NAME = new HashMap<String, String>();
	static Map<Integer, List<String>> REVIEWS = new HashMap<Integer, List<String>>();

	static StringUtil S_UTIL = new StringUtil();
	static BufferedWriter WRITER;
	static Random GENERATOR = new Random(SEED);

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if ( args.length < 1 )
		{
			System.out.println("Please provide price_category parameter (free_health, paid_entertainment, etc) for extraction parameters.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CATEGORY = args[0];
		if ( args.length >= 2 )
		{
			try
			{	THRESHOLD = Integer.parseInt(args[1]);	}
			catch ( NumberFormatException nfe )
			{
				System.err.println("Provide a valid number for review count.");
				System.exit(1);
			}
		}
		if ( args.length > 2 && args[2].equals("print") )
		{	DO_WRITE = true;	}

		System.out.println("Parsing from " + indexFile() + ".");
		loadAppNames();

		System.out.println("Parsing from " + inFile() + ".");
		parseFile(inFile());

		System.out.println("Extracting " + THRESHOLD + " reviews.");


		System.out.println("Random Reviews by Star");
		System.out.println("========================================");
		randomReviewsByStar();

		System.out.println("Random Reviews across Stars");
		System.out.println("========================================");
		randomReviews();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Extraction completed in " + elapsed);
	}

	private static String indexFile()
	{	return String.format("./reviews/index/us_%s.csv", CATEGORY);	}

	private static void loadAppNames() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(indexFile()));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			String tokens[] = line.split("\\|\\|");
			if( tokens.length < 9 ) {	continue;	}
			ID_NAME.put(tokens[AppMeta.APP_ID.ordinal()], tokens[AppMeta.APP_NAME.ordinal()]);
			// System.out.println(TOKENS[AppMeta.APP_ID.ordinal()] + "\t" + TOKENS[AppMeta.APP_NAME.ordinal()]);
		}
		reader.close();
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @return	String value constructed from ./reviews/data/us_CATEGORY_reviews.csv
	 */
	private static String inFile()
	{	return String.format("./reviews/data/us_%s_reviews.csv", CATEGORY);	}

	private static void parseFile(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			String[] toks = line.split(PIPE);
			if( toks.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating = Integer.parseInt(toks[Tokens.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String review = toks[Tokens.REVIEW_TITLE.ordinal()] + " " + toks[Tokens.REVIEW_BODY.ordinal()];
			if ( S_UTIL.tokenise(review, "\\ ").length > 10 )
			{	persist(rating, line);	}
		}
		reader.close();
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String fileName, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(outFile(fileName), true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static String outFile(String fileName)
	{	return String.format("./reports/survey-live/random/%s-%s.tsv", CATEGORY, fileName);	}

	private static void persist(int rating, String text)
	{
		List<String> reviewList = REVIEWS.containsKey(rating) ? REVIEWS.get(rating) : new ArrayList<String>();
		reviewList.add(text);
		REVIEWS.put(rating, reviewList);
		// System.out.println(text);
	}

	private static String prepareLine(String code, String raw)
	{
		String rawToks[] = S_UTIL.tokenise(raw, PIPE);
		String tokens[] = { rawToks[Tokens.APP_ID.ordinal()] + code,
							ID_NAME.get(rawToks[Tokens.APP_ID.ordinal()]), rawToks[Tokens.REVIEW_ID.ordinal()],
							rawToks[Tokens.RATING.ordinal()], rawToks[Tokens.REVIEW_TITLE.ordinal()],
							rawToks[Tokens.REVIEW_BODY.ordinal()] };
		return S_UTIL.delimitTokens(tokens, TAB);
	}

	private static void randomReviewsByStar() throws IOException
	{
		int max = THRESHOLD/5;
		String fileName = "RawByStar";
		for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
		{	getNReviewsFromList(entry.getValue(), max, fileName, "S");	}
	}

	private static void randomReviews() throws IOException
	{
		String fileName = "RawStraight";
		List<String> combinedList = new ArrayList<String>();
		for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
		{	combinedList.addAll(entry.getValue());	}
		getNReviewsFromList(combinedList, THRESHOLD, fileName, "R");
	}

	private static void getNReviewsFromList(List<String> reviewList, int numExtracted, String fileName, String code) throws IOException
	{
		String line;
		RNG_SET = getRNGset(reviewList.size(), numExtracted);
		for ( Integer i : RNG_SET )
		{
			line = prepareLine(code, reviewList.get(i));
			if( DO_WRITE )	{	writeLine(fileName, line);	}
			System.out.println(line);
		}
	}

	private static Set<Integer> getRNGset(int ceiling, int count)
	{
		Set<Integer> result = new HashSet<Integer>();
		while( result.size() < count )
		{	result.add(GENERATOR.nextInt(ceiling));	}
		return result;
	}
}
