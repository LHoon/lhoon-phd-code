public class TfIdfSpike
{
	public static void main(String[] args)
	{
		String review = "";

		double termOccurrences = 3.0;
		double totalTermsInDoc = 100.0;
		double termFreq = ( termOccurrences / totalTermsInDoc );
		System.out.println(termFreq);

		double numOfDocOccurrences = 1000.0;
		double totDocs = 10000000.0;
		double temp = totDocs / numOfDocOccurrences;
		double docFreq = Math.log10( totDocs / numOfDocOccurrences );
		System.out.println(docFreq);

		double tfidf = termFreq * docFreq;
		System.out.println(tfidf);
	}
}
