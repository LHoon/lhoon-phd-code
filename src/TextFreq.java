import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class TextFreq
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static boolean DO_APPEND = true;
	final static boolean NO_APPEND = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	private static String CAT_PRICE = "";
	private static String OUTFILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Map<String, Map<String, Integer>> DOCUMENTS = new HashMap<String, Map<String, Integer>>();
	private static Map<String, Integer> PRE_SORT = new HashMap<String, Integer>();
	private static Map<String, Integer> POST_SORT;
	private static Map<String, Map<String, Double>> TFIDFS = new HashMap<String, Map<String, Double>>();

	private static TextCleaner CLEANER;
	private static TFIDFer TFIDFER;
	private static Stanford LEMMATISER;

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Provide review file for text frequency analysis.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT_PRICE = args[0];
		OUTFILE = "lemma-tfidf-tf-" + CAT_PRICE.substring(3, CAT_PRICE.length());

		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, sU);
		TFIDFER = new TFIDFer(sU);
		LEMMATISER = new Stanford();
		findReviews(getPath(CAT_PRICE, DATA));//"./reviews/alls/us_free_all_reviews.csv");

		DelimitedWriter writer = new DelimitedWriter(OUT_DELIMITER, "./reports/entertainment/" + OUTFILE + ".tsv");
		TFIDFER.calculateTfIdfs();
		TFIDFER.writeTFIDFs(writer, 0);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reports/%s.tsv", fileName);	}

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String appId = TOKENS[AppData.APP_ID.ordinal()];
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			review = CLEANER.correct(review);
			review = lemmatisation(review);
			review = CLEANER.correct(review);
			TFIDFER.countWords(appId, review);
		}
		READER.close();
	}

	private static String lemmatisation(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		// Create StanfordCoreNLP object properties, with POS tagging
		// (required for lemmatization), and lemmatization
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");

		// StanfordCoreNLP loads a lot of models, so you probably
		// only want to do this once per execution
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);
		// run all Annotators on this text
		this.pipeline.annotate(document);
		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			// Iterate over all tokens in a sentence; Retrieve and add the
			// lemma for each word into the list of lemmas
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
