import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class SurveyScores
{
	public enum Score
	{	APP_ID, APP_NAME, REVIEW_ID, RATING, REVIEW, SCORE	};
	// 336963964	My24	455862521	2	i wish they would fix this app something changed some time ago and now the default club selection view full schedule option does not work it is very frustrating it used to work well now i have to view by class and then see when one is being taught instead of viewing by date full schedule come one hour fitness people fix the app	1.2821935121736776

	public enum Survey
	{	QN, APP_ID, APP_NAME, REVIEW_ID, RATING, REVIEW_T, REVIEW_B, U1, U2, U3, U4, U5, RESPONDENTS, SCORE	};
	// Q20	292223170R	MapMyRIDE GPS Cycling	6701535	3	Mapping function off	Good app but it traced my route different than actual	2	0	1	8	3	14	8.83

	final static int SCORE_TOKENS = Score.values().length;
	final static int SURVEY_TOKENS = Survey.values().length;
	final static String TAB = "\t";

	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static Map<String, String[]> SURVEYS = new HashMap<String, String[]>();
	private static Map<String, String> SCORES = new TreeMap<String, String>();

	public static void main(String[] args) throws Exception
	{
		parseScores();
		parseSurvey();
		writeNewScores();
	}

	private static void parseScores() throws IOException
	{
		Double score = 0.0;
		READER = new BufferedReader(new FileReader("./thesis/scores/free_health.tsv"));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(TAB);
			if ( TOKENS.length != SCORE_TOKENS ) 	{	continue;	}
			SCORES.put(TOKENS[Score.REVIEW_ID.ordinal()], TOKENS[Score.SCORE.ordinal()]);
		}
		READER.close();
	}

	private static void parseSurvey() throws IOException
	{
		READER = new BufferedReader(new FileReader("./thesis/surveyed-reviews.tsv"));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(TAB);
			if ( TOKENS.length != SURVEY_TOKENS ) 	{	continue;	}
			SURVEYS.put(TOKENS[Survey.REVIEW_ID.ordinal()], TOKENS);
		}
		READER.close();
	}

	private static void writeNewScores()
	{
		// WRITER = new BufferedWriter(new FileWriter("./thesis/new-survey-scores.tsv"));
		String newScore = "";
		String oldScore = "";
		for (Entry<String, String[]> entry : SURVEYS.entrySet())
		{
			oldScore = entry.getValue()[Survey.SCORE.ordinal()];
			newScore = SCORES.containsKey(entry.getKey()) ? SCORES.get(entry.getKey()) : "0.0";
			System.out.println(arrayToString(entry.getValue(), SURVEY_TOKENS) + TAB + newScore);
		}
		// WRITER.close();
	}

	private static String arrayToString(String[] array, int stopIndex)
	{
		S_BUILDER.setLength(0);
		for ( int i = 0; i < stopIndex; i++ )
		{
			S_BUILDER.append(array[i]);
			S_BUILDER.append(TAB);
		}
		return S_BUILDER.toString();
	}

	private static void addToMap(Map<String, Map<String, Integer>> theMap, String key, String word)
	{
		Map<String, Integer> nestedMap = theMap.containsKey(key) ? theMap.get(key) : new HashMap<String, Integer>();
		Integer value = nestedMap.containsKey(word) ? nestedMap.get(word) : 0;
		value++;
		nestedMap.put(word, value);
		theMap.put(key, nestedMap);

	}

	private static void printMap(Map<String, Map<String, Integer>> theMap)
	{
		for (Entry<String, Map<String, Integer>> entry : theMap.entrySet())
		{
			Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(entry.getValue()));
			resultMap.putAll(entry.getValue());
			for (Entry<String, Integer> nestedEntry : resultMap.entrySet())
			{
				if ( nestedEntry.getValue() > 1 )
				{
					System.out.println(entry.getKey() + TAB + nestedEntry.getKey()
						+ TAB + nestedEntry.getValue());
				}
			}
		}
	}

	private static void writeScore(String[] reviewTokens, double score) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/scores/surveyed-scores.tsv"));
		S_BUILDER.setLength(0);
		for ( String token : reviewTokens )
		{
			S_BUILDER.append(token);
			S_BUILDER.append(TAB);
		}
		S_BUILDER.append(score);
		WRITER.write(S_BUILDER.toString().trim());
		WRITER.newLine();
		WRITER.close();
	}
}
