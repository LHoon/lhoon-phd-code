import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class FreePaidAF
{
	public enum AF
	{	AUTHOR, TOTAL, FREE, PAID	};

	final static int MAX_TOKENS = AF.values().length;
	final static boolean INPUT = true;
	final static boolean OUTPUT = false;
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/author-cat-clean-data/";
	final static String AUTHOR_FOLDER = FOLDER + "authors/";

	// Numerical indexes to track the review processing
	private static Integer LINES_READ = 0;

	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Map<Integer, Integer[]> AF_MAP = new TreeMap<Integer, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		parseAFs();
		writeAFReport();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(formatNumber(LINES_READ) + " lines read, with " + formatNumber(AF_MAP.size()) + " authors, read in " + timeTaken);
	}

	private static String getFileName(boolean isInput)
	{
		String file = isInput ? "af-report" : "af-compressed";
		return String.format("%s%s.tsv", AUTHOR_FOLDER, file);
	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseAFs() throws Exception
	{
		BufferedReader reader = new BufferedReader(new FileReader(getFileName(INPUT)));
		int frequency;
		String[] tokens;
		Integer[] counts;
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( !line.equals("") ) {	LINES_READ++;	}
			tokens = line.split(TAB);
			if ( tokens.length != MAX_TOKENS )	{	continue;	}
			frequency = Integer.parseInt(tokens[AF.TOTAL.ordinal()]);
			counts = AF_MAP.containsKey(frequency) ? AF_MAP.get(frequency) : new Integer[] {0,0};
			counts[0] += Integer.parseInt(tokens[AF.FREE.ordinal()]);
			counts[1] += Integer.parseInt(tokens[AF.PAID.ordinal()]);

			AF_MAP.put(frequency, counts);
		}
		reader.close();
	}

	private static void writeAFReport() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(getFileName(OUTPUT)));
		for ( Entry<Integer, Integer[]> entry : AF_MAP.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue()[0]);
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue()[1]);
			S_BUILDER.append(TAB);
			writer.write(S_BUILDER.toString().trim());
			writer.newLine();
		}
		writer.close();
	}
}
