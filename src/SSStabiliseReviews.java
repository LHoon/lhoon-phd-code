import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import util.TextFile;

public class StabiliseReviews
{
	//javac -classpath ../lib/joda-time-2.1-sources.jar:../lib/joda-time.jar:../lib/jollyday-0.4.7-sources.jar:../lib/jollyday.jar:../lib/stanford-corenlp-1.3.4-javadoc.jar:../lib/stanford-corenlp-1.3.4-models.jar:../lib/stanford-corenlp-1.3.4-sources.jar:../lib/stanford-corenlp-1.3.4.jar:../lib/xom.jar:../lib/stanford-corenlp-1.3.4:../lib/vasa-utils.jar StabiliseWords.java
	//java -Xms1224m -Xmx1224m -classpath ../lib/joda-time-2.1-sources.jar:../lib/joda-time.jar:../lib/jollyday-0.4.7-sources.jar:../lib/jollyday.jar:../lib/stanford-corenlp-1.3.4-javadoc.jar:../lib/stanford-corenlp-1.3.4-models.jar:../lib/stanford-corenlp-1.3.4-sources.jar:../lib/stanford-corenlp-1.3.4.jar:../lib/xom.jar:../lib/stanford-corenlp-1.3.4:../lib/vasa-utils.jar:./ StabiliseWords ../app_reviews/cleaned_reviews/reviewText.csv

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer RAW_WORD_COUNT = 0;
	private static Integer TREATED_WORD_COUNT = 0;

	// regex for valid text
	private static Pattern VALID_PATTERN = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+");
	private static Pattern DUPE_CHAR_PATTERN = Pattern.compile("(\\w)\\1+");
	// Lemmatiser
	private static StanfordLemmatizer stanLemma = new StanfordLemmatizer();
	// Norvig Speller
	private static Spelling NORVIG_SPELLER;

	/** DICTIONARIES: English, Auxilliary */
	// English dictionary
	private static Map<String, HashSet<String>> ENG_DICTIONARY = new HashMap<String, HashSet<String>>();
	// Auxilliary dictionary containing jargon, product names, etc
	private static Set<String> AUX_DICTIONARY = new HashSet<String>();

	/** Commonly typo-ed and truncated words/expressions */
	// Abbreviations/Txt Spk, commonly typoed words and supplementary typos based on manual analysis dictionary containing expansions
	private static Map<String, String> ABBRV_TYPOS = new HashMap<String, String>();

	/** Tracking and monitoring structures */
	// Map of odd words we encounter. Key = word, Val = Frequency
	private static Map<String, Integer> ODD_WORDS = new HashMap<String, Integer>();
	private static Map<String, String> WORD_CORRECTIONS = new TreeMap<String, String>();

	/** File I/O */
	private static BufferedWriter REVIEW_WRITER;
	private static TextFile INPUT_FILE;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}

		loadReferenceData();
		parseReviewTextFile(args[0]);
		writeFile(ODD_WORDS, "oddWords");
		writeFile(WORD_CORRECTIONS, "corrections");
	reportResults();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Text stabilisation on " + formatNumber(REVIEW_COUNT) + " reviews took " + timeTaken);
	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private static void loadReferenceData() throws Exception
	{
		String folder = "assets";
		// ENGLISH DICTIONARY
		loadDictionary(getFilePath(folder, "dictionary", "txt"));
		// AUXILLIARY DICTIONARY
		loadDictionary(AUX_DICTIONARY, getFilePath(folder, "aux-dictionary", "txt"));
		// APP NAMES
		loadDictionary(AUX_DICTIONARY, getFilePath(folder, "app-name-words", "txt"));
		// ABBREVIATION/TXT SPK
		loadTypos(ABBRV_TYPOS, getFilePath(folder, "abbrv-txt-spk", "csv"));
		// COMMONLY TYPO-ED WORDS
		loadTypos(ABBRV_TYPOS, getFilePath(folder, "common-typos", "csv"));
		// TRAINED TYPOS
		loadTypos(ABBRV_TYPOS, getFilePath(folder, "aux-common-typos", "csv"));
		// Norvig speller training corpus
		// NORVIG_SPELLER = new Spelling(getFilePath(folder, "corpus", "txt"));
		System.out.println("===================================");
		System.out.println("Loading of Reference Data Complete.");
		System.out.println("===================================");
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getFilePath(String folder, String fileName, String extension)
	{	return String.format("./%s/%s.%s", folder, fileName, extension);	}

	/**
	 * Populates the dictionary map based on the text file. The prefix of the
	 * word forms the key of the map, the word itself is added to the HashSet of
	 * values associated with that prefix. Makes parsing through the dictionary
	 * for string similarity jaroWinklerAlgos MUCH faster.
	 */
	private static void loadDictionary(String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for (String line : theFile)
		{
			line = minorClean(line);
			String prefix = getPrefix(line);
			HashSet<String> values = new HashSet<String>();

			if(ENG_DICTIONARY.containsKey(prefix))
			{   values = ENG_DICTIONARY.get(prefix);    }

			values.add(line);
			ENG_DICTIONARY.put(prefix, values);
		}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the any auxilliary dictionaries based on filename param.
	 */
	private static void loadDictionary(Set<String> theSet, String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for (String line : theFile)
		{	theSet.add(minorClean(line));	}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private static void loadTypos(Map<String, String> theMap, String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for(String line : theFile)
		{
			String[] tokens = line.split(",");
			theMap.put(minorClean(tokens[0]), minorClean(tokens[1]));
		}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Returns the first two characters of the String param. Failing that,
	 * returns "noprefix". SHOULD BE USED THROUGHOUT THIS SCRIPT FOR CONSISTENCY
	 * PERTAINING TO ENGLISH DICTIONARY CHECKS.
	 */
	private static String getPrefix(String word)
	{	return word.length() > 2 ? new String(word.substring(0,2)) : "noprefix";	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Dictionary checker. Run the param word against our English Dictionary
	 * data structure and return true if the word is found. Failing that, check
	 * agaist the Auxilliary dictionary, return true if found, or false if not.
	 */
	private static boolean checkDictionary(String word)
	{
		HashSet<String> subDictionary = ENG_DICTIONARY.get(getPrefix(word));
		if(subDictionary != null && subDictionary.contains(word))
		{   return true; }
		return AUX_DICTIONARY.contains(word);
	}

	/**
	 * Check if the param word has been previously stabilised from our
	 * ABBRV_TYPOS map. If the word has been previously stabilised, return the
	 * stabilised word, else, the word.
	 */
	private static String checkReplacements(String word)
	{	return ABBRV_TYPOS.containsKey(word) ? ABBRV_TYPOS.get(word) : word;	}

	/**
	 * Check if we have previously attempted spell correction of the param word
	 * from our WORD_CORRECTIONS map and UNCORRECTED_WORDS set. If the word has
	 * had previous correction attempts, return the result of the previous
	 * attempt, or failing that, the param word.
	 */
	private static String checkCorrections(String word)
	{
		if(checkDictionary(word) || ODD_WORDS.containsKey(word))
		{	return word;	}

		if(WORD_CORRECTIONS.containsKey(word))
		{	return WORD_CORRECTIONS.get(word);	}

		add(ODD_WORDS, word);
		// String correctedWord = NORVIG_SPELLER.correct(word);
		// System.err.println("Correcting: " + word);
		// if(!correctedWord.equals(word))
		// {	ODD_CORRECTIONS.put(word, correctedWord);	}
		// else
		// {	ODD_WORDS.containsKey(word) ? ODD_WORDS.put(word, (ODD_WORDS.get(word) + 1)) : ODD_WORDS.put(word, 1);	}
		return word;
	}

	/**
	 * Process the lemmatisation results for the word.
	 */
	private static List<String> getLemma(String word)
	{
		List<String> lemmaList = new ArrayList<String>();
		if(Pattern.matches("[a-zA-Z]+", word))
		{   lemmaList = stanLemma.lemmatize(word);  }

		if(!lemmaList.isEmpty())
		{
			for(String lemmaWord : lemmaList)
			{	lemmaWord = minorClean(lemmaWord);	}
		}
		return lemmaList;
	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	private static String stringTreatment(String review)
	{
		review = review.replaceAll("\\`","\\'");
		review = review.replaceAll("\\'m", "\\ am");
		review = review.replaceAll("\\'\\ ", "\\ ");
		review = review.replaceAll("\\ s ","s ");
		review = review.replaceAll("\\'ll", "\\ will");
		review = review.replaceAll("\\'re", "\\ are");
		review = review.replaceAll("\\'d","\\ would");
		review = review.replaceAll("\\'ve","\\ have");
		return minorClean(review);
	}

	/**
	 *
	 */
	private static String stabiliseText(String rawText)
	{	return applyLookup(stringTreatment(rawText));	}

	private static String applyLookup(String text)
	{
		boolean wordAdded = false;
		StringBuilder sBuilder = new StringBuilder();
		Matcher matcher = VALID_PATTERN.matcher(text);
		while (matcher.find())
		{
			String wordToAdd = minorClean(matcher.group());
			if(!wordToAdd.matches(".*\\d.*"))
			{
				if(ABBRV_TYPOS.containsKey(wordToAdd))
				{	wordToAdd = checkReplacements(wordToAdd);	}
				else
				{
					if(!checkDictionary(wordToAdd))
					{	wordToAdd = checkCorrections(wordToAdd);	}
				}
			}

			sBuilder.append(wordToAdd);
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)	// Remove the trailing space
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }
		return sBuilder.toString();
	}

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseReviewTextFile(String fileName) throws IOException
	{
		try
		{
			INPUT_FILE = new TextFile(fileName);
			REVIEW_WRITER = new BufferedWriter(new FileWriter(getFilePath("sportsmate", "stabilisedReviews", "txt"), true));
			for (String line : INPUT_FILE)
			{

				REVIEW_WRITER.write(stabiliseText(stringTreatment(line)));
				REVIEW_WRITER.newLine();
				REVIEW_COUNT++;
				if(REVIEW_COUNT % 250 == 0)
				{
					REVIEW_WRITER.flush();
					System.gc();
				}
				if(REVIEW_COUNT%25000 == 0)
				{	reportProgress();	}
			}
		}
		catch (IOException ioe)
		{	System.err.println("I/O Error: " + ioe.getMessage());	}
		finally
		{
			System.out.println(formatNumber(REVIEW_COUNT) + " reviews processed in total.");
			INPUT_FILE.close();
			REVIEW_WRITER.close();
		}
	}

	private static void reportProgress()
	{	System.out.println(String.format("%s reviews processed, with %s raw unique words encountered; %s treated unique words.",formatNumber(REVIEW_COUNT), formatNumber(RAW_WORD_COUNT), formatNumber(TREATED_WORD_COUNT)));	}

	private static void reportResults()
	{
		DecimalFormat df2 = new DecimalFormat("###.##");
		double percentage = Double.valueOf(df2.format((double)ODD_WORDS.size()/TREATED_WORD_COUNT * 100));
		System.out.println(percentage + "% of " + formatNumber(TREATED_WORD_COUNT) + " are odd words.");

		int difference = TREATED_WORD_COUNT - RAW_WORD_COUNT;
		percentage = Double.valueOf(df2.format((double) difference/RAW_WORD_COUNT * 100));
		System.out.println(percentage + "% expansion in words.");
	}

	/**
	 * Write to file the Map values.
	 */
	private static void writeFile(Map<String, ?> theMap, String fileName) throws IOException
	{
		// Map<String, ?> resultMap = new TreeMap<String, ?>(new ValueComparer<String, ?>(theMap));
		// // resultMap.putAll(theMap);

		for (Entry<String, ?> entry : theMap.entrySet())
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(getFilePath("reports", fileName, "csv")));
			bw.write(entry.getKey() + "," + entry.getValue());
			bw.newLine();
			bw.close();
		}
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}

class StanfordLemmatizer
{
	protected StanfordCoreNLP pipeline;

	public StanfordLemmatizer()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatize(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}
}

class Spelling
{

	private final HashMap<String, Integer> nWords = new HashMap<String, Integer>();

	public Spelling(String file) throws IOException
	{
		BufferedReader in = new BufferedReader(new FileReader(file));
		Pattern p = Pattern.compile("\\w+");
		for(String temp = ""; temp != null; temp = in.readLine())
		{
			Matcher m = p.matcher(temp.toLowerCase());
			while(m.find()) nWords.put((temp = m.group()), nWords.containsKey(temp) ? nWords.get(temp) + 1 : 1);
		}
		in.close();
	}

	private final ArrayList<String> edits(String word)
	{
		ArrayList<String> result = new ArrayList<String>();
		for(int i=0; i < word.length(); ++i) result.add(word.substring(0, i) + word.substring(i+1));
		for(int i=0; i < word.length()-1; ++i) result.add(word.substring(0, i) + word.substring(i+1, i+2) + word.substring(i, i+1) + word.substring(i+2));
		for(int i=0; i < word.length(); ++i) for(char c='a'; c <= 'z'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i+1));
		for(int i=0; i <= word.length(); ++i) for(char c='a'; c <= 'z'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i));
		return result;
	}

	public final String correct(String word)
	{
		if(nWords.containsKey(word)) return word;
		ArrayList<String> list = edits(word);
		HashMap<Integer, String> candidates = new HashMap<Integer, String>();
		for(String s : list) if(nWords.containsKey(s)) candidates.put(nWords.get(s),s);
		if(candidates.size() > 0) return candidates.get(Collections.max(candidates.keySet()));
		for(String s : list) for(String w : edits(s)) if(nWords.containsKey(w)) candidates.put(nWords.get(w),w);
		return candidates.size() > 0 ? candidates.get(Collections.max(candidates.keySet())) : word;
	}
}
