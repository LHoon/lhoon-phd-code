import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class CatRatedAFJGS
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum AF
	{	FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static boolean DATA = true;
	final static boolean IDX = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/jgs-reviews/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String REPORT_FOLDER = FOLDER + "reports/";
	final static String CATEGORY = "health";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer CATS_COUNTED = 0;
	private static Integer BAD_LINES = 0;
	private static Integer CHECK = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};	// 1, 2, 3, 4, 5
	private static String AUTHOR = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Map<String, Integer> APP_RANKS = new HashMap<String, Integer>();
	private static Map<String, String> APP_NAMES = new HashMap<String, String>();
	private static Map<String, Set<String>> APP_AUTHORS = new HashMap<String, Set<String>>();
	private static Map<String, Integer[]> APP_RATINGS = new HashMap<String, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		parseFiles(FREE);
		writeReport(FREE);
		parseFiles(PAID);
		writeReport(PAID);
		// parseFiles(PAID);

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		// System.out.println(CATS_COUNTED + " categories processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, from " + formatNumber(AUTHORS.size()) + " authors, with " + formatNumber(BAD_LINES) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	private static String getFileName(boolean isData, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return isData ? String.format("./reviews/clean-data/tab/%s_%s.tsv", price, CATEGORY) : String.format("./reviews/clean-index/us_%s_%s.csv", price, CATEGORY);
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseFiles(boolean isFree) throws Exception
	{
		parseIndexFile(isFree);
		parseDataFile(isFree);
		CATS_COUNTED++;
	}

	private static void parseIndexFile(boolean isFree) throws IOException
	{
		APP_RANKS.clear();
		APP_NAMES.clear();
		String file = getFileName(IDX, isFree);
		READER = new BufferedReader(new FileReader(file));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}

			TOKENS = LINE.split(PIPE);
			if ( TOKENS.length == MAX_META_TOKENS )
			{
				APP_RANKS.put(TOKENS[AppMeta.APP_ID.ordinal()], Integer.parseInt(TOKENS[AppMeta.RANK.ordinal()]));
				APP_NAMES.put(TOKENS[AppMeta.APP_ID.ordinal()], TOKENS[AppMeta.APP_NAME.ordinal()].toLowerCase(Locale.ENGLISH).trim());
				continue;
			}
			BAD_SET.add(String.format("%s%s%s", file, TAB, LINE));
		}
		READER.close();
	}

	private static void parseDataFile(boolean isFree) throws IOException
	{
		int rating = 0;
		String appId = "";
		String author = "";
		String file = getFileName(DATA, isFree);
		String[] data = new String[] {"", ""};
		Set<String> authorSet;
		Integer[] appRatings;

		Map<String, String[]> appReviews;

		APP_AUTHORS.clear();
		APP_RATINGS.clear();
		READER = new BufferedReader(new FileReader(file));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;

			TOKENS = LINE.split(TAB);

			if ( TOKENS.length == MAX_DATA_TOKENS
				&& TOKENS[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				if ( TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length > 1 )
				{
					appId = TOKENS[AppData.APP_ID.ordinal()];
					author = TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1];
					authorSet = APP_AUTHORS.containsKey(appId) ? APP_AUTHORS.get(appId) : new HashSet<String>();
					authorSet.add(author);
					APP_AUTHORS.put(appId, authorSet);
					rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
					appRatings = APP_RATINGS.containsKey(appId) ? APP_RATINGS.get(appId) : new Integer[] {0,0,0,0,0};
					appRatings[rating-1]++;	// OFFSET FOR ZERO INDEX
					APP_RATINGS.put(appId, appRatings);
					REVIEW_COUNT++;
					continue;
				}
			}
			BAD_SET.add(String.format("%s%s%s", file, TAB, LINE));
		}
		READER.close();
	}

	// private static void prepareReport() throws Exception
	// {
	// 	Map<String, String[]> appReviews;
	// 	Set<String> authors;
	// 	String appId;

	// 	int one;
	// 	int two;
	// 	int three;
	// 	int four;
	// 	int five;
	// 	int total;
	// 	for ( Entry<String, Map<String, String[]>> app : REVIEWS.entrySet() )
	// 	{
	// 		authors = new HashSet<String>();
	// 		one = 0;
	// 		two = 0;
	// 		three = 0;
	// 		four = 0;
	// 		five = 0;
	// 		total = 0;
	// 		appId = app.getKey();
	// 		appReviews = app.getValue();
	// 		for ( Entry<String, String[]> review : appReviews.entrySet() )
	// 		{
	// 			authors.add(review.getValue()[0]);
	// 			if ( review.getValue()[1].equals("1") )
	// 			{	one++;	}
	// 			else if ( review.getValue()[1].equals("2") )
	// 			{	two++;	}
	// 			else if ( review.getValue()[1].equals("3") )
	// 			{	three++;	}
	// 			else if ( review.getValue()[1].equals("4") )
	// 			{	four++;	}
	// 			else if ( review.getValue()[1].equals("5") )
	// 			{	five++;	}

	// 			total++;
	// 		}
	// 		S_BUILDER.setLength(0);
	// 		S_BUILDER.append(appId);
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(total));
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(authors.size()));
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(one));
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(two));
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(three));
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(four));
	// 		S_BUILDER.append(TAB);
	// 		S_BUILDER.append(String.valueOf(five));
	// 		APP_REPORT.put(appId, S_BUILDER.toString().trim());
	// 	}
	// }

	private static void writeReport(boolean isFree) throws IOException
	{
		String price = isFree ? "free" : "paid";
		WRITER = new BufferedWriter(new FileWriter(FOLDER + CATEGORY + "-" + price + "-app-af" + ".tsv"));
		WRITER.write("RANK	NAME	APP_ID	REVIEWS	AUTHORS	1	2	3	4	5");
		WRITER.newLine();
		Map<String, Integer> sorted = sortByComparator(APP_RANKS);
		String line;
		int total;
		Set<String> authorSet;
		Integer[] appRatings;
		for ( Entry<String, Integer> entry : sorted.entrySet() )
		{
			total = 0;
			appRatings = APP_RATINGS.containsKey(entry.getKey()) ? APP_RATINGS.get(entry.getKey()) : new Integer[] {0,0,0,0,0};
			authorSet = APP_AUTHORS.containsKey(entry.getKey()) ? APP_AUTHORS.get(entry.getKey()) : new HashSet<String>();
			for ( Integer rating : appRatings )
			{	total+=rating;	}

			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getValue());
			S_BUILDER.append(TAB);
			S_BUILDER.append(APP_NAMES.get(entry.getKey()));
			S_BUILDER.append(TAB);
// AppID, # Reviews, # of Authors, 1, 2, 3, 4, 5 star count

			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(String.valueOf(total));
			S_BUILDER.append(TAB);
			S_BUILDER.append(String.valueOf(authorSet.size()));
			for ( Integer i : appRatings )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(i));
			}

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}

		WRITER.close();
		// clearMaps();
	}

	private static void writeMap(Map<String, Integer[]> theMap, String range) throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(REPORT_FOLDER + range + "-af" + ".tsv"));

		for ( Entry<String, Integer[]> entry: theMap.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue()[0]);
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue()[1]);
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeBadLines() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.tsv"));

		for ( String line : BAD_SET )
		{
			WRITER.write(line.trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
