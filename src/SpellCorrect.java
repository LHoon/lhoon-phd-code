import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;
import java.util.TreeMap;

import uk.ac.shef.wit.simmetrics.similaritymetrics.JaroWinkler;
import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import util.TextFile;

public class SpellCorrect
{
	//javac -classpath ./lib/joda-time-2.1-sources.jar:./lib/joda-time.jar:./lib/jollyday-0.4.7-sources.jar:./lib/jollyday.jar:./lib/stanford-corenlp-1.3.4-javadoc.jar:./lib/stanford-corenlp-1.3.4-models.jar:./lib/stanford-corenlp-1.3.4-sources.jar:./lib/stanford-corenlp-1.3.4.jar:./lib/xom.jar:./lib/simmetrics_jar_v1_6_2_d07_02_07.jar:./lib/stanford-corenlp-1.3.4:./lib/vasa-utils.jar SpellCorrect.java
	//java -Xms1224m -Xmx1224m -classpath ./lib/joda-time-2.1-sources.jar:./lib/joda-time.jar:./lib/jollyday-0.4.7-sources.jar:./lib/jollyday.jar:./lib/stanford-corenlp-1.3.4-javadoc.jar:./lib/stanford-corenlp-1.3.4-models.jar:./lib/stanford-corenlp-1.3.4-sources.jar:./lib/stanford-corenlp-1.3.4.jar:./lib/xom.jar:./lib/simmetrics_jar_v1_6_2_d07_02_07.jar:./lib/stanford-corenlp-1.3.4:./lib/vasa-utils.jar:./ SpellCorrect

	// Jaro-Winkler, returns 0.00 to 1.00, higher result = higher similarity
	private static JaroWinkler jaroWinklerAlgo = new JaroWinkler();
	// Levenshtein, minimum number of edits needed to match, lower is better.
	// However, this implementation returns a val between 0.00 and 1.00.
	private static Levenshtein levenshteinAlgo = new Levenshtein();

	/** Lemmatiser */
	private static StanfordLemmatizer stanLemma = new StanfordLemmatizer();

	/** DICTIONARIES: English, Auxilliary */
	// English dictionary
	private static Set<String> DICTIONARY = new HashSet<String>();
	private static Set<String> RAW_WORDS = new HashSet<String>();
	private static Set<String> UNTREATED_WORDS = new HashSet<String>();

	private static Map<String, String> RESULTS = new HashMap<String, String>();
	private static Map<String, String> NORVIG_RESULTS = new HashMap<String, String>();

	private static Spelling NORVIG_SPELLER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if(args.length < 1)
		{
			System.err.println("No odd word file provided for processing.");
			System.exit(1);
		}
		long startTime = System.currentTimeMillis();

		NORVIG_SPELLER = new Spelling(getFilePath("big"));
		loadReferenceData();
		parseTextFile(args[0]);
		attemptSpellCorrection();
		reportResults();
		writeFile(RESULTS, "./reports/distCorrection"+args[1]);
		writeFile(NORVIG_RESULTS, "./reports/norvCorrection"+args[1]);
		writeFile(UNTREATED_WORDS, "./reports/remainingWords"+args[1]);
		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("String matching for " + formatNumber(RAW_WORDS.size()) + " words took " + timeTaken);
	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private static void loadReferenceData()
	{
		loadDictionary("dictionary");		// ENGLISH DICTIONARY
		loadDictionary("aux-dictionary");	// AUXILLIARY DICTIONARY
		System.out.println("Dictionary references loaded.");
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getFilePath(String fileName)
	{   return String.format("./%s/%s.txt", "assets", fileName);    }

	/**
	 * Populates the dictionary map based on the text file. The prefix of the
	 * word forms the key of the map, the word itself is added to the HashSet of
	 * values associated with that prefix. Makes parsing through the dictionary
	 * for string similarity jaroWinklerAlgos MUCH faster.
	 */
	private static void loadDictionary(String fileName)
	{
		TextFile theFile = new TextFile(getFilePath(fileName));
		for (String line : theFile)
		{
			line = minorClean(line);
			if(line != null || line != "")
			DICTIONARY.add(line);
		}

		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{   return text.toLowerCase(Locale.ENGLISH).trim(); }

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Dictionary checker. Run the param word against our English Dictionary
	 * data structure and return true if the word is found. Failing that, check
	 * agaist the Auxilliary dictionary, return true if found, or false if not.
	 */
	private static boolean checkDictionary(String word)
	{	return DICTIONARY.contains(word);	}

	private static String getLemma(String word)
	{
		if(Pattern.matches("[a-zA-Z]+", word))
		{
			List<String> lemmas = stanLemma.lemmatize(word);
			if(!lemmas.isEmpty())
			{
				if(!lemmas.get(0).equals(word))
				{	return lemmas.get(0);	}
			}
		}
		return null;
	}

	/**
	 * Get the Jaro-Winkler distance between two strings.
	 */
	private static double getJaroDistance(String recA, String recB)
	{   return jaroWinklerAlgo.getSimilarity(recA, recB);	}

	/**
	 * Get the Levenshtein distance between two strings.
	 */
	private static double getLeveDistance(String recA, String recB)
	{   return levenshteinAlgo.getSimilarity(recA, recB);	}

	/**
	 * Word Stemmer. Apply word stemming on the param word and return the
	 * result.
	 */
	private static String getWordStem(String word)
	{
		Stemmer stemmer = new Stemmer();
		for( char c : word.toCharArray())
		{    stemmer.add(c);   }
		stemmer.stem();
		return stemmer.toString();
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseTextFile(String fileName)
	{
		TextFile tf = new TextFile(fileName);
		for (String line : tf)
		{   RAW_WORDS.add(line.split(",")[0]);    }
		tf.close();
	}

	private static void attemptSpellCorrection()
	{
		int idx = 0;
		int treatedIdx = 0;
		for(String word : RAW_WORDS)
		{
			idx++;
			String bestFit = norvigSpeller(word);

			if(!bestFit.equals(word))
			{	NORVIG_RESULTS.put(word, bestFit);	}
			else
			{
				bestFit = spellCheck(word);
				if(!bestFit.equals(word))
				{	RESULTS.put(word, bestFit);	}
			}

			if(!RESULTS.containsKey(word) && !NORVIG_RESULTS.containsKey(word))
			{	UNTREATED_WORDS.add(word);	}

			int totalResults = RESULTS.size() + NORVIG_RESULTS.size();
			if(totalResults % 250 == 0 && totalResults > 0 && totalResults != treatedIdx)
			{
				System.out.println("Corrections for " + formatNumber(totalResults) + " out of " + formatNumber(idx) + " words suggested.");
				treatedIdx = totalResults;
			}
		}
		System.out.println(formatNumber(RAW_WORDS.size()) + " words processed in total.");
	}

	private static String norvigSpeller(String word)
	{	return NORVIG_SPELLER.correct(word);	}

	private static String spellCheck(String word)
	{
		String result = englishBestFit(word);
		String lemWord = null;
		if(word.equals(result))
		{
			lemWord = getLemma(word);
			if(lemWord != null)
			{	result = englishBestFit(lemWord);	}
		}
		return result;
	}

		/**
	 * This method will attempt to find the closest fitting English word to the
	 * parameter provided, using the Jaro-Winkler distance. If a suitable match
	 * is found (distance more than 0.95 or dictionary already contains such a
	 * word), the replacement word will be returned, otherwise, a "" String
	 * value is returned. There is no need to check the REPLACEMENT_WORDS map as
	 * the spellChecker method does that before attempting to stem or lookup the
	 * dictionary.
	 */
	private static String englishBestFit(String word)
	{
		double distance = 0.0;
		String result = word;
		for( String s : DICTIONARY)
		{
			double jaroDist = getJaroDistance(s, word);
			double leveDist = getLeveDistance(s, word);

			double largerDist = 0.0;
			if(jaroDist > leveDist)
			{   largerDist = jaroDist;   }
			else
			{   largerDist = leveDist;   }

			if(largerDist > distance && largerDist > 0.85)
			{
				distance = largerDist;
				result = s;
			}
		}
		return result;
		// System.out.println("TOTAL OF : " + formatNumber(RESULT_COUNT) + " WORDS FOUND WITH LEVENSHTEIN DISTANCE OF MORE THAN 0.85 FROM " + formatNumber(i) + " WORDS ANALYSED.");
	}


	// private static String removeDuplicateChars(List<String> rawList)
	// {
	// 	String text = listToString(rawList);

	// 	Matcher matcher = DUPE_CHAR_PATTERN.matcher(text);
	// 	while (matcher.find())
	// 	{
	// 		String matcherVal = matcher.group();
	// 		if(matcherVal.length() > 2)
	// 		{
	// 			String trimmedWord = word;
	// 			int wordIdx =
	// 			int index = 0;
	// 			while (!checkDictionary(trimmedWord))
	// 			{
	// 				if(index >= 5)
	// 				{	break;	}
	// 				else
	// 				{	index++;	}
	// 				switch(index)
	// 				{
	// 					case 1: trimmedWord = trimmedWord.replaceAll(matcherVal, matcherVal.substring(0,2));
	// 							break;
	// 					case 2:	trimmedWord = NORVIG_SPELLER.correct(trimmedWord);
	// 							break;
	// 					case 3:	trimmedWord = trimmedWord.replaceAll(matcherVal, matcherVal.substring(0,1));
	// 							break;
	// 					case 4:	trimmedWord = NORVIG_SPELLER.correct(trimmedWord);
	// 							break;
	// 					default:break;
	// 				}
	// 				word = trimmedWord;
	// 			}
	// 		}
	// 	}
	// 	return text;
	// }

	/**private static void printResultsToConsole()
	{
		boolean wordAdded = false;
		StringBuilder sBuilder = new StringBuilder();
		for(Entry<Double, List<String>> entry : OUTPUT.entrySet())
		{
			sBuilder.append(entry.getKey());
			sBuilder.append(" : ");
			for( String word : entry.getValue() )
			{
				sBuilder.append(word);
				sBuilder.append(", ");
				wordAdded = true;
			}
			if(wordAdded)
			{
				sBuilder.deleteCharAt(sBuilder.length() - 1);
				System.out.println(sBuilder.toString());
				sBuilder.delete(0, sBuilder.length());
			}
		}
	}*/

	private static void reportResults()
	{
		DecimalFormat df2 = new DecimalFormat("###.##");
		int totalResults = NORVIG_RESULTS.size() + RESULTS.size();
		double percentage = Double.valueOf(df2.format((double) totalResults/RAW_WORDS.size() * 100));
		System.out.println(totalResults + " words (" + percentage + "%) have been auto-corrected words.");
	}

	/**
	 * Write to file the Map values.
	 */
	private static void writeFile(Map<String, String> theMap, String fileName)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName + ".txt"));
			for (Entry<String, String> entry : theMap.entrySet())
			{
				bw.write(String.format("%s,%s", entry.getKey(), entry.getValue()));
				bw.newLine();
			}
			bw.close();
		} catch (IOException ioe)
		{   System.err.println("Error occurred while attempting to write " + fileName);}
		theMap.clear();
	}

	/**
	 * Write to file the Set values.
	 */
	private static void writeFile(Set<String> theSet, String fileName)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName + ".txt"));
			for (String entry : theSet)
			{
				bw.write(entry);
				bw.newLine();
			}
			bw.close();
		} catch (IOException ioe)
		{   System.err.println("Error occurred while attempting to write " + fileName);}
		theSet.clear();
	}
}

/**
 *  http://tartarus.org/~martin/PorterStemmer/java.txt
 */
class Stemmer
{  private char[] b;
   private int i,     /* offset into b */
			   i_end, /* offset to end of stemmed word */
			   j, k;
   private static final int INC = 50;
					 /* unit of size whereby b is increased */
   public Stemmer()
   {  b = new char[INC];
	  i = 0;
	  i_end = 0;
   }

   /**
	* Add a character to the word being stemmed.  When you are finished
	* adding characters, you can call stem(void) to stem the word.
	*/

   public void add(char ch)
   {  if (i == b.length)
	  {  char[] new_b = new char[i+INC];
		 for (int c = 0; c < i; c++) new_b[c] = b[c];
		 b = new_b;
	  }
	  b[i++] = ch;
   }


   /** Adds wLen characters to the word being stemmed contained in a portion
	* of a char[] array. This is like repeated calls of add(char ch), but
	* faster.
	*/

   public void add(char[] w, int wLen)
   {  if (i+wLen >= b.length)
	  {  char[] new_b = new char[i+wLen+INC];
		 for (int c = 0; c < i; c++) new_b[c] = b[c];
		 b = new_b;
	  }
	  for (int c = 0; c < wLen; c++) b[i++] = w[c];
   }

   /**
	* After a word has been stemmed, it can be retrieved by toString(),
	* or a reference to the internal buffer can be retrieved by getResultBuffer
	* and getResultLength (which is generally more efficient.)
	*/
   public String toString() { return new String(b,0,i_end); }

   /**
	* Returns the length of the word resulting from the stemming process.
	*/
   public int getResultLength() { return i_end; }

   /**
	* Returns a reference to a character buffer containing the results of
	* the stemming process.  You also need to consult getResultLength()
	* to determine the length of the result.
	*/
   public char[] getResultBuffer() { return b; }

   /* cons(i) is true <=> b[i] is a consonant. */

   private final boolean cons(int i)
   {  switch (b[i])
	  {  case 'a': case 'e': case 'i': case 'o': case 'u': return false;
		 case 'y': return (i==0) ? true : !cons(i-1);
		 default: return true;
	  }
   }

   /* m() measures the number of consonant sequences between 0 and j. if c is
	  a consonant sequence and v a vowel sequence, and <..> indicates arbitrary
	  presence,

		 <c><v>       gives 0
		 <c>vc<v>     gives 1
		 <c>vcvc<v>   gives 2
		 <c>vcvcvc<v> gives 3
		 ....
   */

   private final int m()
   {  int n = 0;
	  int i = 0;
	  while(true)
	  {  if (i > j) return n;
		 if (! cons(i)) break; i++;
	  }
	  i++;
	  while(true)
	  {  while(true)
		 {  if (i > j) return n;
			   if (cons(i)) break;
			   i++;
		 }
		 i++;
		 n++;
		 while(true)
		 {  if (i > j) return n;
			if (! cons(i)) break;
			i++;
		 }
		 i++;
	   }
   }

   /* vowelinstem() is true <=> 0,...j contains a vowel */

   private final boolean vowelinstem()
   {  int i; for (i = 0; i <= j; i++) if (! cons(i)) return true;
	  return false;
   }

   /* doublec(j) is true <=> j,(j-1) contain a double consonant. */

   private final boolean doublec(int j)
   {  if (j < 1) return false;
	  if (b[j] != b[j-1]) return false;
	  return cons(j);
   }

   /* cvc(i) is true <=> i-2,i-1,i has the form consonant - vowel - consonant
	  and also if the second c is not w,x or y. this is used when trying to
	  restore an e at the end of a short word. e.g.

		 cav(e), lov(e), hop(e), crim(e), but
		 snow, box, tray.

   */

   private final boolean cvc(int i)
   {  if (i < 2 || !cons(i) || cons(i-1) || !cons(i-2)) return false;
	  {  int ch = b[i];
		 if (ch == 'w' || ch == 'x' || ch == 'y') return false;
	  }
	  return true;
   }

   private final boolean ends(String s)
   {  int l = s.length();
	  int o = k-l+1;
	  if (o < 0) return false;
	  for (int i = 0; i < l; i++) if (b[o+i] != s.charAt(i)) return false;
	  j = k-l;
	  return true;
   }

   /* setto(s) sets (j+1),...k to the characters in the string s, readjusting
	  k. */

   private final void setto(String s)
   {  int l = s.length();
	  int o = j+1;
	  for (int i = 0; i < l; i++) b[o+i] = s.charAt(i);
	  k = j+l;
   }

   /* r(s) is used further down. */

   private final void r(String s) { if (m() > 0) setto(s); }

   /* step1() gets rid of plurals and -ed or -ing. e.g.

		  caresses  ->  caress
		  ponies    ->  poni
		  ties      ->  ti
		  caress    ->  caress
		  cats      ->  cat

		  feed      ->  feed
		  agreed    ->  agree
		  disabled  ->  disable

		  matting   ->  mat
		  mating    ->  mate
		  meeting   ->  meet
		  milling   ->  mill
		  messing   ->  mess

		  meetings  ->  meet

   */

   public final void step1()
   {  if (b[k] == 's')
	  {  if (ends("sses")) k -= 2; else
		 if (ends("ies")) setto("i"); else
		 if (b[k-1] != 's') k--;
	  }
	  if (ends("eed")) { if (m() > 0) k--; } else
	  if ((ends("ed") || ends("ing")) && vowelinstem())
	  {  k = j;
		 if (ends("at")) setto("ate"); else
		 if (ends("bl")) setto("ble"); else
		 if (ends("iz")) setto("ize"); else
		 if (doublec(k))
		 {  k--;
			{  int ch = b[k];
			   if (ch == 'l' || ch == 's' || ch == 'z') k++;
			}
		 }
		 else if (m() == 1 && cvc(k)) setto("e");
	 }
   }

   /* step2() turns terminal y to i when there is another vowel in the stem. */

   private final void step2() { if (ends("y") && vowelinstem()) b[k] = 'i'; }

   /* step3() maps double suffices to single ones. so -ization ( = -ize plus
	  -ation) maps to -ize etc. note that the string before the suffix must give
	  m() > 0. */

   public final void step3() { if (k == 0) return; /* For Bug 1 */ switch (b[k-1])
   {
	   case 'a': if (ends("ational")) { r("ate"); break; }
				 if (ends("tional")) { r("tion"); break; }
				 break;
	   case 'c': if (ends("enci")) { r("ence"); break; }
				 if (ends("anci")) { r("ance"); break; }
				 break;
	   case 'e': if (ends("izer")) { r("ize"); break; }
				 break;
	   case 'l': if (ends("bli")) { r("ble"); break; }
				 if (ends("alli")) { r("al"); break; }
				 if (ends("entli")) { r("ent"); break; }
				 if (ends("eli")) { r("e"); break; }
				 if (ends("ousli")) { r("ous"); break; }
				 break;
	   case 'o': if (ends("ization")) { r("ize"); break; }
				 if (ends("ation")) { r("ate"); break; }
				 if (ends("ator")) { r("ate"); break; }
				 break;
	   case 's': if (ends("alism")) { r("al"); break; }
				 if (ends("iveness")) { r("ive"); break; }
				 if (ends("fulness")) { r("ful"); break; }
				 if (ends("ousness")) { r("ous"); break; }
				 break;
	   case 't': if (ends("aliti")) { r("al"); break; }
				 if (ends("iviti")) { r("ive"); break; }
				 if (ends("biliti")) { r("ble"); break; }
				 break;
	   case 'g': if (ends("logi")) { r("log"); break; }
   } }

   /* step4() deals with -ic-, -full, -ness etc. similar strategy to step3. */

   private final void step4() { switch (b[k])
   {
	   case 'e': if (ends("icate")) { r("ic"); break; }
				 if (ends("ative")) { r(""); break; }
				 if (ends("alize")) { r("al"); break; }
				 break;
	   case 'i': if (ends("iciti")) { r("ic"); break; }
				 break;
	   case 'l': if (ends("ical")) { r("ic"); break; }
				 if (ends("ful")) { r(""); break; }
				 break;
	   case 's': if (ends("ness")) { r(""); break; }
				 break;
   } }

   /* step5() takes off -ant, -ence etc., in context <c>vcvc<v>. */

   private final void step5()
   {   if (k == 0) return; /* for Bug 1 */ switch (b[k-1])
	   {  case 'a': if (ends("al")) break; return;
		  case 'c': if (ends("ance")) break;
					if (ends("ence")) break; return;
		  case 'e': if (ends("er")) break; return;
		  case 'i': if (ends("ic")) break; return;
		  case 'l': if (ends("able")) break;
					if (ends("ible")) break; return;
		  case 'n': if (ends("ant")) break;
					if (ends("ement")) break;
					if (ends("ment")) break;
					/* element etc. not stripped before the m */
					if (ends("ent")) break; return;
		  case 'o': if (ends("ion") && j >= 0 && (b[j] == 's' || b[j] == 't')) break;
									/* j >= 0 fixes Bug 2 */
					if (ends("ou")) break; return;
					/* takes care of -ous */
		  case 's': if (ends("ism")) break; return;
		  case 't': if (ends("ate")) break;
					if (ends("iti")) break; return;
		  case 'u': if (ends("ous")) break; return;
		  case 'v': if (ends("ive")) break; return;
		  case 'z': if (ends("ize")) break; return;
		  default: return;
	   }
	   if (m() > 1) k = j;
   }

   /* step6() removes a final -e if m() > 1. */

   private final void step6()
   {  j = k;
	  if (b[k] == 'e')
	  {  int a = m();
		 if (a > 1 || a == 1 && !cvc(k-1)) k--;
	  }
	  if (b[k] == 'l' && doublec(k) && m() > 1) k--;
   }

   /** Stem the word placed into the Stemmer buffer through calls to add().
	* Returns true if the stemming process resulted in a word different
	* from the input.  You can retrieve the result with
	* getResultLength()/getResultBuffer() or toString().
	*/
   public void stem()
   {  k = i - 1;
	  if (k > 1) { step1(); step2(); step3(); step4(); step5(); step6(); }
	  i_end = k+1; i = 0;
   }
}

class StanfordLemmatizer
{
	protected StanfordCoreNLP pipeline;

	public StanfordLemmatizer()
	{
		// Create StanfordCoreNLP object properties, with POS tagging
		// (required for lemmatization), and lemmatization
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");

		// StanfordCoreNLP loads a lot of models, so you probably
		// only want to do this once per execution
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatize(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);

		// run all Annotators on this text
		this.pipeline.annotate(document);

		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			// Iterate over all tokens in a sentence; Retrieve and add the
			// lemma for each word into the list of lemmas
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}
}

class Spelling
{

	private final HashMap<String, Integer> nWords = new HashMap<String, Integer>();

	public Spelling(String file) throws IOException
	{
		BufferedReader in = new BufferedReader(new FileReader(file));
		Pattern p = Pattern.compile("\\w+");
		for(String temp = ""; temp != null; temp = in.readLine())
		{
			Matcher m = p.matcher(temp.toLowerCase());
			while(m.find()) nWords.put((temp = m.group()), nWords.containsKey(temp) ? nWords.get(temp) + 1 : 1);
		}
		in.close();
	}

	private final ArrayList<String> edits(String word)
	{
		ArrayList<String> result = new ArrayList<String>();
		for(int i=0; i < word.length(); ++i) result.add(word.substring(0, i) + word.substring(i+1));
		for(int i=0; i < word.length()-1; ++i) result.add(word.substring(0, i) + word.substring(i+1, i+2) + word.substring(i, i+1) + word.substring(i+2));
		for(int i=0; i < word.length(); ++i) for(char c='a'; c <= 'z'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i+1));
		for(int i=0; i <= word.length(); ++i) for(char c='a'; c <= 'z'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i));
		return result;
	}

	public final String correct(String word)
	{
		if(nWords.containsKey(word)) return word;
		ArrayList<String> list = edits(word);
		HashMap<Integer, String> candidates = new HashMap<Integer, String>();
		for(String s : list) if(nWords.containsKey(s)) candidates.put(nWords.get(s),s);
		if(candidates.size() > 0) return candidates.get(Collections.max(candidates.keySet()));
		for(String s : list) for(String w : edits(s)) if(nWords.containsKey(w)) candidates.put(nWords.get(w),w);
		return candidates.size() > 0 ? candidates.get(Collections.max(candidates.keySet())) : word;
	}

	// public static void main(String args[]) throws IOException
	// {
	// 	if(args.length > 0) System.out.println(());
	// }
}
