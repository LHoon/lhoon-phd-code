import java.io.*;

import java.text.DecimalFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class ReviewValuatorWithZeroesMinWords
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REV_ID, REV_DATE, APP_VER, T_WC, T_CC, B_WC,
		B_CC, RATING, AUTH_NAME, AUTH_ID, TITLE, BODY
	};

	public enum IDF
	{	DOC, TOK, VAL, LOG	};

	final static boolean CAT = true;
	final static boolean STAR = false;
	final static boolean ZERO = true;
	final static boolean NON_ZERO = false;

	final static int MAX_IDF_TOKENS = IDF.values().length;
	final static int MAX_REV_TOKENS = AppData.values().length;

	final static String TAB = "\t";
	final static String PIPE = "\\|\\|";
	final static Pattern WORDS = Pattern.compile("[a-zA-z]+");
	final static Map<String, Double> EMPTY = new HashMap<String, Double>();

	private static Double SCORE;
	private static String FILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Matcher MATCHER;
	private static Stanford LEMMATISER;
	private static TextCleaner CLEANER;
	private static Map<String, String> ID_NAME = new HashMap<String, String>();
	private static Map<String, Map<String, Double>> CAT_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> STAR_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> REVIEWS = new TreeMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> ZEROES = new TreeMap<String, Map<String, Double>>();


	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Please provide price and category parameter (free_health, paid_entertainment, etc) for analysis context.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		FILE = args[0];
		CLEANER = new TextCleaner("./assets", false, S_UTIL);

		loadAppNames();
		loadIDFs(CAT);
		loadIDFs(STAR);
		// writeidfMap();
		LEMMATISER = new Stanford();
		parseReviews();
		writeResults(NON_ZERO);
		// writeResults(ZERO);


		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review valuation completed in " + elapsed);
	}

	private static String idfPath(boolean isCategoryLvl)
	{
		return isCategoryLvl ? String.format("./reports/survey-10/tfidf/%s-cat-tfidf.tsv", FILE) : String.format("./reports/survey-10/tfidf/%s-star-tfidf.tsv", FILE);
	}

	private static String indexPath()
	{	return String.format("./reviews/index/us_%s.csv", FILE);	}

	private static String reviewPath()
	{	return String.format("./reviews/data/us_%s_reviews.csv", FILE);	}

	private static String outPath(boolean isZero)
	{
		return isZero ? String.format("./reports/survey-10/scored/%s-zeroes-moreThan10.tsv", FILE) : String.format("./reports/survey-10/scored/%s-scores-moreThan10.tsv", FILE);
	}

	private static void loadAppNames() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(indexPath()));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split("\\|\\|");
			if( TOKENS.length < 9 ) {	continue;	}
			ID_NAME.put(TOKENS[AppMeta.APP_ID.ordinal()], TOKENS[AppMeta.APP_NAME.ordinal()]);
			// System.out.println(TOKENS[AppMeta.APP_ID.ordinal()] + "\t" + TOKENS[AppMeta.APP_NAME.ordinal()]);
		}
		reader.close();
	}

	private static void loadIDFs(boolean isCategoryLvl) throws IOException
	{
		Double value;
		BufferedReader reader = new BufferedReader(new FileReader(idfPath(isCategoryLvl)));
		Map<String, Double> nestedMap;
		Map<String, Map<String, Double>> idfMap = new HashMap<String, Map<String, Double>>();
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_IDF_TOKENS )
			{	continue;	}
			try
			{
				value = Double.parseDouble(TOKENS[IDF.LOG.ordinal()]);
				nestedMap = idfMap.containsKey(TOKENS[IDF.DOC.ordinal()]) ? idfMap.get(TOKENS[IDF.DOC.ordinal()]) : new HashMap<String, Double>();
				nestedMap.put(TOKENS[IDF.TOK.ordinal()], value);
				idfMap.put(TOKENS[IDF.DOC.ordinal()], nestedMap);
			}
			catch (NumberFormatException nfe)	{	continue;	}
		}
		reader.close();
		if ( isCategoryLvl )
		{	CAT_POINTS.putAll(idfMap);	}
		else
		{	STAR_POINTS.putAll(idfMap);	}
	}

	private static void writeidfMap()
	{
		for(Entry<String, Map<String, Double>> doc : CAT_POINTS.entrySet())
		{
			for(Entry<String, Double> vals : doc.getValue().entrySet())
			{	System.out.println(doc.getKey() + " > " + vals.getKey() + " > " + vals.getValue());	}
		}
	}

	private static void writeResults(boolean isZero) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, outPath(isZero));
		Map<String, Map<String, Double>> resultMap = isZero ? ZEROES : REVIEWS;
		for ( Entry<String, Map<String, Double>> app : resultMap.entrySet() )
		{
			Map<String, Double> preSort = app.getValue();
			Map<String, Double> sorted = new TreeMap<String, Double>(new hoon.util.ValueComparer<String, Double>(preSort));
			sorted.putAll(preSort);
			for ( Entry<String, Double> review : sorted.entrySet() )
			{
				// writer.writeLine(new DecimalFormat("#0.0000000000000000000000").format(review.getValue()));
				TOKENS = new String[4];
				TOKENS[0] = app.getKey();				//app ID
				TOKENS[1] =	ID_NAME.get(app.getKey());	//app name
				TOKENS[2] = review.getKey();			//review, rating, text
				TOKENS[3] = new DecimalFormat("#0.00").format(review.getValue());

				TOKENS[0] = TOKENS[0] + "H";			//adding the H annotation

				writer.writeLine(S_UTIL.delimitTokens(TOKENS, TAB));
				// System.out.println(S_UTIL.delimitTokens(TOKENS, TAB));
			}
		}
	}

	private static void parseReviews() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(reviewPath()));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, PIPE);
			if( TOKENS.length != MAX_REV_TOKENS )
			{	continue;	}
			String appID = TOKENS[AppData.APP_ID.ordinal()];
			String revID = TOKENS[AppData.REV_ID.ordinal()];
			String rating = TOKENS[AppData.RATING.ordinal()];
			String review = TOKENS[AppData.TITLE.ordinal()] + " " + TOKENS[AppData.BODY.ordinal()];
			String raw = TOKENS[AppData.TITLE.ordinal()] + TAB + TOKENS[AppData.BODY.ordinal()];
			int size =  S_UTIL.tokenise(review, "\\ ").length;
			if ( size > 10 )
			{
				review = CLEANER.correct(review);
				// review = lemma(review);
				review = CLEANER.correct(review);
				saveScore(appID, revID, rating, raw, countScore(review, appID, rating, size));
			}
		}
		reader.close();
	}

	private static Double countScore(String text, String appID, String rating, int size)
	{
		SCORE = 0.0;
		MATCHER = WORDS.matcher(text);
		Map<String, Double> nestedMap;

		while ( MATCHER.find() )
		{
			LINE = S_UTIL.enforceAlphaNumerics(MATCHER.group());
			nestedMap = CAT_POINTS.containsKey(appID) ? CAT_POINTS.get(appID) : EMPTY;
			SCORE += nestedMap.containsKey(LINE) ? nestedMap.get(LINE) : 0.0;
			nestedMap = STAR_POINTS.containsKey(rating) ? STAR_POINTS.get(rating) : EMPTY;
			SCORE += nestedMap.containsKey(LINE) ? nestedMap.get(LINE) : 0.0;
		}
		return SCORE/size;
	}

	private static void saveScore(String appID, String revID, String rating, String review, Double score)
	{
		TOKENS = new String[3];
		TOKENS[0] = revID;
		TOKENS[1] = rating;
		TOKENS[2] = review;
		if ( score != 0.0 )
		{
			Map<String, Double> scoreMap = REVIEWS.containsKey(appID) ? REVIEWS.get(appID) : new HashMap<String, Double>();
			scoreMap.put(S_UTIL.delimitTokens(TOKENS, TAB), score);
			REVIEWS.put(appID, scoreMap);
		}
		else
		{
			Map<String, Double> scoreMap = ZEROES.containsKey(appID) ? ZEROES.get(appID) : new HashMap<String, Double>();
			scoreMap.put(S_UTIL.delimitTokens(TOKENS, TAB), score);
			ZEROES.put(appID, scoreMap);
		}
	}

	private static String lemma(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
