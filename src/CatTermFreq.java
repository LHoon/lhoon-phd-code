import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class CatTermFreq
{
	public enum TF
	{	DOC_ID, TOKEN, COUNT	};

	final static int MAX_TOKENS = TF.values().length;
	final static String TAB = "\t";

	private static String IN_FILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static PFIDFer PFIDFER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long start = System.currentTimeMillis();
		StringUtil sU = new StringUtil();
		PFIDFER = new PFIDFer(sU);
		loadTFs();

		PFIDFER.calculatePfIdfs();
		System.out.println("calculated");
		DelimitedWriter writer = new DelimitedWriter(TAB, "./reports/5phrase-freq/free-health-pfidf.tsv");
		PFIDFER.writePFIDFs(writer, 0);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static void loadTFs() throws IOException
	{
		File dir = new File("reports/5phrase-freq/");
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null)
		{
			for (File child : directoryListing)
			{
				if (!child.getName().equals(".DS_Store"))
				{	// System.out.println(child.getAbsolutePath());
					BufferedReader reader = new BufferedReader(new FileReader(child));
					for ( LINE = ""; LINE != null; LINE = reader.readLine() )
					{
						TOKENS = LINE.split(TAB);
						if( TOKENS.length != MAX_TOKENS )
						{	continue;	}
						int count = 0;
						try
						{	count += Integer.parseInt(TOKENS[TF.COUNT.ordinal()]);	}
						catch (NumberFormatException nfe)
						{	continue;	}
						PFIDFER.loadPF(TOKENS);
					}
				reader.close();
				}
			}
		}
	}
}
