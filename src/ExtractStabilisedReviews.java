import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

// import flanagan.control.FirstOrder;
// import flanagan.analysis.Stat;


public class ExtractStabilisedReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static Integer WRITTEN = 0;
	private static Integer THRESHOLD;

	private static DateTime DATE = new DateTime();
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";
	private static Map<String, DateTime> APP_BIRTHDAY = new HashMap<String, DateTime>();
	private static Map<String, DateTime> APP_LASTDAY = new HashMap<String, DateTime>();
	private static Map<String, String> APP_DETAILS = new HashMap<String, String>();
	private static Map<String, Map<String, DateTime>> VERSIONS = new HashMap<String, Map<String, DateTime>>();
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringBuilder S_BUILDER_2 = new StringBuilder();
	// private static Map<String, DateTime> VERSIONS = new TreeMap<String, DateTime>();
	private static DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("MMM dd, yyyy");
	private static Map<Integer, List<String>> REVIEWS = new TreeMap<Integer, List<String>>();
	private static Set<String> PROBLEM_REVIEWS = new HashSet<String>();

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;


	/* English and Jargon references */
	// English Language Reference
	private static Map<String, String> ABBRV_TYPOS = new TreeMap<String, String>();

	// Patterns to cleanse text of non-alphanumerics
	private static Pattern VALID_PATTERN = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+"); // Pattern.compile("[0-9]+?|[\\w\\d/]+");

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("Insufficient args provided for Review Extraction Phase.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT_PRICE = args[0];
		try
		{	THRESHOLD = Integer.parseInt(args[1]);	}
		catch ( NumberFormatException nfe )
		{	THRESHOLD = 10;	}
		OUTFILE = "top" + THRESHOLD + "-" + CAT_PRICE.substring(3, CAT_PRICE.length());

		getAppDetails(getPath(CAT_PRICE, INDEX));
		System.out.println("Finding reviews for top " + THRESHOLD + " apps.");
		findReviews(getPath(CAT_PRICE, DATA));
		writeReviewSpread();
		writeProblemReviews();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reports/%s.tsv", fileName);	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getAssetPath(String fileName)
	{	return String.format("./%s/%s.%s", "assets", fileName, "csv");	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private static void loadReferenceData() throws Exception
	{
		String folder = "assets";
		// ABBREVIATION/TXT SPK
		loadTypos(ABBRV_TYPOS, getAssetPath("abbrv-txt-spk"));
		// COMMONLY TYPO-ED WORDS
		loadTypos(ABBRV_TYPOS, getAssetPath("common-typos"));
		// TRAINED TYPOS
		loadTypos(ABBRV_TYPOS, getAssetPath("aux-common-typos"));
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	private static String stringTreatment(String review)
	{
		review = minorClean(review);
		review = review.replaceAll("\\`","\\'");
		review = review.replaceAll("won't","will not");
		review = review.replaceAll("can't","cannot");
		review = review.replaceAll("'m", "\\ am");
		review = review.replaceAll("\\ s ","s ");
		review = review.replaceAll("'ll", "\\ will");
		review = review.replaceAll("'re", "\\ are");
		review = review.replaceAll("'d","\\ would");
		review = review.replaceAll("'ve","\\ have");
		review = review.replaceAll("n't","\\ not");
		// review = review.replaceAll("\\'\\ ", "\\ ");
		return minorClean(review);
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private static void loadTypos(Map<String, String> theMap, String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			String[] tokens = LINE.split(",");
			theMap.put(minorClean(tokens[0]), minorClean(tokens[1]));
		}
		READER.close();
		System.out.println(fileName + " loaded.");
	}

	private static String applyLookup(String text)
	{
		boolean wordAdded = false;
		if ( S_BUILDER_2.length() > 0 )
		{   S_BUILDER_2.delete(0, S_BUILDER_2.length());    }

		Matcher matcher = VALID_PATTERN.matcher(text);
		while (matcher.find())
		{
			String wordToAdd = minorClean(matcher.group());
			if(!wordToAdd.matches(".*\\d.*"))
			{	wordToAdd = ABBRV_TYPOS.containsKey(wordToAdd) ? wordToAdd = ABBRV_TYPOS.get(wordToAdd) : wordToAdd;	}

			S_BUILDER_2.append(wordToAdd);
			S_BUILDER_2.append(" ");
			wordAdded = true;
		}

		if(wordAdded)	// Remove the trailing space
		{   S_BUILDER_2.deleteCharAt(S_BUILDER_2.length() - 1);   }
		return S_BUILDER_2.toString();
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String filePath, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(line);
			WRITER.newLine();
			WRITTEN++;
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void writeAgeRelease() throws IOException
	{
		for( Entry<String, Map<String, DateTime>> entry : VERSIONS.entrySet() )
		{
			DateTime bday = APP_BIRTHDAY.get(entry.getKey());
			DateTime lday = APP_LASTDAY.get(entry.getKey());
			int days = Days.daysBetween(bday, lday).getDays();
			if ( days == 0 ) { days = 1;	}
			int releases = entry.getValue().size();
			double proportion = (double) releases/days;
			double rounded = Math.floor(10000 * proportion + 0.5) / 10000;
			LINE = APP_DETAILS.get(entry.getKey()).replaceAll(",", "") + ", " + days + ", " + releases + ", " + rounded;
			writeLine(outPath(OUTFILE), LINE);
		}
	}

	private static void writeProblemReviews() throws IOException
	{
		for( String problem : PROBLEM_REVIEWS )
		{	writeLine(outPath(OUTFILE+"-errors"), problem);	}
	}

	private static void writeReviewSpread() throws IOException
	{
		for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
		{
			for ( String line : entry.getValue() )
			{	writeLine(outPath(OUTFILE+"-"+entry.getKey()+"stars"), line);	}
		}
	}

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		Set<String> idSet = APP_DETAILS.keySet();
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{
				PROBLEM_REVIEWS.add(LINE);
				continue;
			}
			if ( idSet.contains(TOKENS[AppData.APP_ID.ordinal()]) )
			{
				int rating = 0;
				try
				{
					rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
				} catch (NumberFormatException nfe)
				{
					PROBLEM_REVIEWS.add(LINE);
					continue;
				}
				String formattedReview = formatTokens(TOKENS);
				// if( Integer.parseInt(formattedReview.split(OUT_DELIMITER)[4]) > 5)
				saveReviewToStructure(formattedReview, rating);
			}
		}
		READER.close();
	}

	private static void saveReviewToStructure(String review, int rating)
	{
		List<String> ratedReviews = REVIEWS.containsKey(rating) ? REVIEWS.get(rating) : new ArrayList<String>();
		ratedReviews.add(review);
		REVIEWS.put(rating, ratedReviews);
	}

	private static String formatTokens(String[] tokens)
	{
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }

		S_BUILDER.append(APP_DETAILS.get(tokens[AppData.APP_ID.ordinal()]));
		S_BUILDER.append(tokens[AppData.REVIEW_ID.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		String authorLine = tokens[AppData.AUTHOR_ID.ordinal()];
		String authorID = "";
		int authorIndex = authorLine.indexOf("=");
		if ( authorIndex != -1 )
		{	authorID = authorLine.substring((authorIndex + 1), authorLine.length());	}
		else
		{	authorID = authorLine;	}
		S_BUILDER.append(authorID);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(tokens[AppData.REVIEW_DATE.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(tokens[AppData.APP_VERSION.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(tokens[AppData.RATING.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		String reviewTitle = applyLookup(stringTreatment(tokens[AppData.REVIEW_TITLE.ordinal()]));
		S_BUILDER.append(reviewTitle);
		S_BUILDER.append(OUT_DELIMITER);
		String reviewBody = applyLookup(stringTreatment(tokens[AppData.REVIEW_BODY.ordinal()]));
		S_BUILDER.append(reviewBody);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(Integer.toString(reviewTitle.split(" ").length));
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(Integer.toString(reviewBody.split(" ").length));
		S_BUILDER.append(OUT_DELIMITER);

		return S_BUILDER.toString().trim();
	}

	// public enum AppMeta
	// {	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	private static String formatAppDetails(String[] tokens)
	{
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }

		S_BUILDER.append(tokens[AppMeta.APP_ID.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(tokens[AppMeta.APP_NAME.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(tokens[AppMeta.PRICE.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);
		S_BUILDER.append(tokens[AppMeta.RANK.ordinal()]);
		S_BUILDER.append(OUT_DELIMITER);

		return S_BUILDER.toString();
	}

	private static void getAppDetails(String filePath) throws IOException
	{
		int appsFound = 0;
		String appMetaDetails = "";
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if(appsFound > THRESHOLD)
			{	break;	}
			TOKENS = LINE.split(IN_DELIMITER);

			if( TOKENS.length != MAX_META_TOKENS )
			{	continue;	}

			appMetaDetails = formatAppDetails(TOKENS);
			if( TOKENS.length < 9 ) {	continue;	}
			APP_DETAILS.put(TOKENS[AppMeta.APP_ID.ordinal()], appMetaDetails);
			appsFound++;
		}
		READER.close();
	}

	private static void determineDate(String appID, String version, String dateString)
	{
		try
		{
			DATE = DateTime.parse(dateString, DATE_FORMATTER);
			Map<String, DateTime> appVersions = VERSIONS.containsKey(appID) ? VERSIONS.get(appID) : new TreeMap<String, DateTime>();
			DateTime value = appVersions.containsKey(version) ? appVersions.get(version) : new DateTime();
			if ( DATE.isAfter(value) )
			{	DATE = value;	}
			appVersions.put(version,DATE);
			VERSIONS.put(appID, appVersions);
			checkFirstAndLastDay(DATE, appID);
		} catch (Exception e) {	e.printStackTrace();	}
	}

	private static void checkFirstAndLastDay(DateTime currDate, String appID)
	{
		if(!APP_BIRTHDAY.containsKey(appID))
		{	APP_BIRTHDAY.put(appID, currDate);	}
		else
		{
			DateTime birthday =	APP_BIRTHDAY.get(appID);
			if(birthday.isAfter(currDate))
			{	APP_BIRTHDAY.put(appID, currDate);	}
		}

		if(!APP_LASTDAY.containsKey(appID))
		{	APP_LASTDAY.put(appID, currDate);	}
		else
		{
			DateTime lastday = APP_LASTDAY.get(appID);
			if(currDate.isAfter(lastday))
			{	APP_LASTDAY.put(appID, currDate);	}
		}

	}
}
