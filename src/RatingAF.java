import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class RatingAF
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	// public enum RatingAF
	// {	FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "-all/categories";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer CATS_COUNTED = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};	// 1, 2, 3, 4, 5
	private static String AUTHOR = "";
	private static String CAT_PRICE = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Set<String> FREE_CATS = new TreeSet<String>();
	private static Set<String> PAID_CATS = new TreeSet<String>();
	private static Set<String> CATEGORIES = new TreeSet<String>();

	private static Map<String, Integer> AUTHOR_MAP = new HashMap<String, Integer>();
	private static Map<String, Integer[]> PAID_MAP = new HashMap<String, Integer[]>();
	private static Map<String, Integer[]> FREE_MAP = new HashMap<String, Integer[]>();

	private static Map<String, Integer> CAT_AUTH_MAP = new HashMap<String, Integer>();
	private static Map<String, Integer[]> CAT_MAP = new HashMap<String, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCatPrices("./category-free.txt", FREE);
		loadCatPrices("./category-paid.txt", PAID);
		loadCategories("./assets/categories.txt");

		StringUtil sU = new StringUtil();
		parseReviewCategories();
		writeAF();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATS_COUNTED + " categories processed, " + formatNumber(REVIEW_COUNT) + "reviews decomposed, " + formatNumber(LINES_READ) +  " lines read, in " + timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("./reviews/data/us_%s_%s_reviews.csv", price, category);
	}

	private static String prepCatPriceInFile(String catPrice)
	{	return String.format("./reviews/data/us_%s_reviews.csv", catPrice);	}

	private static String prepCatPriceOutFile(String catPrice)
	{	return String.format("%s%s-af.tsv", FOLDER, catPrice);	}

	private static void loadCatPrices(String fileName, boolean isFree) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )
			{
				if ( isFree )	{	FREE_CATS.add(prepCatPriceInFile(minorClean(LINE)));	}
				else	{	PAID_CATS.add(prepCatPriceInFile(minorClean(LINE)));	}
			}
		}
		READER.close();
	}

	private static void loadCategories(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )	{	CATEGORIES.add(minorClean(LINE));	}
		}
		READER.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	private static boolean lineIsValid()
	{
		if ( TOKENS.length != MAX_DATA_TOKENS )	{	return false;	}
		if ( TOKENS[AppData.RATING.ordinal()].equals("1")
			|| TOKENS[AppData.RATING.ordinal()].equals("2")
			|| TOKENS[AppData.RATING.ordinal()].equals("3")
			|| TOKENS[AppData.RATING.ordinal()].equals("4")
			|| TOKENS[AppData.RATING.ordinal()].equals("5") )
		{	return true;	}
		else	{	return false;	}
	}

	private static void parseReviews() throws Exception
	{
		for ( String catPrice : FREE_CATS )
		{
			parseFile(catPrice, FREE);
			CATS_COUNTED++;
		}
		for ( String catPrice : PAID_CATS )
		{
			parseFile(catPrice, PAID);
			CATS_COUNTED++;
		}
	}

	private static void parseReviewCategories() throws Exception
	{
		for ( String category : CATEGORIES )
		{

			parseFile(getCategoryFileName(category, FREE), FREE);
			CATS_COUNTED++;
			parseFile(getCategoryFileName(category, PAID), PAID);
			CATS_COUNTED++;

			writeCatAF(category);

			AUTHOR_MAP.clear();
			FREE_MAP.clear();
			PAID_MAP.clear();
		}
	}

	private static void parseFile(String fileName, boolean isFree) throws IOException
	{
		int rating = 0;
		String author = "";
		READER = new BufferedReader(new FileReader(fileName));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			LINES_READ++;
			TOKENS = LINE.split(PIPE);
			if ( !lineIsValid() )
			{
				if ( !LINE.equals("") )
				{	System.out.println("Read Error: " + fileName + "||" + LINE);	}
				continue;
			}
			REVIEW_COUNT++;

			rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);

			if ( TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length != 2 )
			{	continue;	}
			author = TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1];

			addToMap(author, rating, isFree);
		}
		READER.close();
	}

	private static void addToMap(String author, int rating, boolean isFree)
	{
		COUNT = AUTHOR_MAP.containsKey(author) ? AUTHOR_MAP.get(author) : 0;
		if ( isFree )
		{
			COUNTS = FREE_MAP.containsKey(author) ? FREE_MAP.get(author) : new Integer[] {0, 0, 0, 0, 0};
		}
		else
		{
			COUNTS = PAID_MAP.containsKey(author) ? PAID_MAP.get(author) : new Integer[] {0, 0, 0, 0, 0};
		}
		COUNT++;
		COUNTS[rating-1]++;
		AUTHOR_MAP.put(author, COUNT);
		if ( isFree )	{	FREE_MAP.put(author, COUNTS);	}
		else	{	PAID_MAP.put(author, COUNTS);	}
	}

	private static void writeCatAF(String category) throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/authors"+ FOLDER +"/" + category + "-af" + ".tsv"));

		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(AUTHOR_MAP));
		resultMap.putAll(AUTHOR_MAP);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			AUTHOR = entry.getKey();
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());
			getPriceCounts(AUTHOR, FREE);
			getPriceCounts(AUTHOR, PAID);

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeAF() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/authors"+ FOLDER +"/" + "all-af" + ".tsv"));

		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(AUTHOR_MAP));
		resultMap.putAll(AUTHOR_MAP);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			AUTHOR = entry.getKey();
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());
			getPriceCounts(AUTHOR, FREE);
			getPriceCounts(AUTHOR, PAID);

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void getPriceCounts(String author, boolean isFree)
	{
		int total = 0;
		if ( isFree )
		{
			COUNTS = FREE_MAP.containsKey(author) ? FREE_MAP.get(author) : new Integer[] {0, 0, 0, 0, 0};
		}
		else
		{
			COUNTS = PAID_MAP.containsKey(author) ? PAID_MAP.get(author) : new Integer[] {0, 0, 0, 0, 0};
		}

		for ( Integer i : COUNTS )
		{
			total += i;
			S_BUILDER.append(TAB);
			S_BUILDER.append(i);
		}
		S_BUILDER.append(TAB);
		S_BUILDER.append(total);
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
