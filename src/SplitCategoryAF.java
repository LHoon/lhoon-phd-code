import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class SplitCategoryAF
{
	public enum AF
	{	AUTHOR, TOTAL, FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static int MAX_TOKENS = AF.values().length;
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/authors-rerun/categories/";

	// Numerical indexes to track the review processing
	private static String AUTHOR_VAL = "";
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	private static Integer TOTAL_FREQ = 0;

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Integer VALUE;
	private static Set<String> COUNT_SET;
	private static Set<String> CATEGORIES = new HashSet<String>();

	private static Map<Integer, Integer[]> TOTALS_MAP = new TreeMap<Integer, Integer[]>();
	private static Map<String, Integer[]> ALL_MAP = new TreeMap<String, Integer[]>();
	private static Map<Integer, Set<String>> COUNT_MAPS = new TreeMap<Integer, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/categories.txt");
		parseCatAFFiles();
		writeAll();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("AF report aggregated in " + timeTaken);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )	{	CATEGORIES.add(minorClean(LINE)+"-af");	}
		}
		READER.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static void parseCatAFFiles() throws Exception
	{
		int value = 0;
		try
		{
			for ( String category : CATEGORIES )
			{
				READER = new BufferedReader(new FileReader(FOLDER + category + ".tsv"));
				for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
				{
					TOKENS = LINE.split(TAB);
					if ( TOKENS.length == MAX_TOKENS )
					{
						COUNT = Integer.parseInt(TOKENS[AF.TOTAL.ordinal()]);
						TOTAL_FREQ += COUNT;
						COUNT_SET = COUNT_MAPS.containsKey(COUNT) ? COUNT_MAPS.get(COUNT) : new HashSet<String>();
						COUNT_SET.add(LINE);
						COUNT_MAPS.put(COUNT, COUNT_SET);
						COUNTS = TOTALS_MAP.containsKey(COUNT) ? TOTALS_MAP.get(COUNT) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
						for ( int i = AF.TOTAL.ordinal(); i < MAX_TOKENS; i++ )
						{
							value = Integer.parseInt(TOKENS[i]);
							COUNTS[i] += value;
						}
						TOTALS_MAP.put(COUNT, COUNTS);
					}
				}
				READER.close();
				writeCatAFs(category);
				TOTALS_MAP.clear();
				COUNT_MAPS.clear();
			}
		}
		catch (Exception e)
		{	e.printStackTrace();	}
	}

	private static String prepLine(Integer[] counts)
	{
		S_BUILDER.setLength(0);
		for ( Integer i : counts )
		{
			S_BUILDER.append(i.toString());
			S_BUILDER.append(TAB);
		}
		return S_BUILDER.toString().trim();
	}

	private static void writeCatAFs(String category) throws IOException
	{
		Integer[] total = new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "reports/" + category + "-report-af.tsv"));
		for ( Entry<Integer, Set<String>> entry : COUNT_MAPS.entrySet() )
		{
			COUNTS = TOTALS_MAP.containsKey(entry.getKey()) ? TOTALS_MAP.get(entry.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			COUNTS[AF.AUTHOR.ordinal()] = entry.getKey();
			for ( int i = 1; i < COUNTS.length; i++ )
			{ 	total[i] +=	COUNTS[i];	}
			WRITER.write(prepLine(COUNTS));
			WRITER.newLine();
		}
		ALL_MAP.put(category, total);
		WRITER.write(prepLine(total));
		WRITER.newLine();
		WRITER.close();
	}

	private static void writeAll() throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "reports/all-report-af.tsv"));
		for ( Entry<String, Integer[]> entry : ALL_MAP.entrySet() )
		{
			COUNTS = ALL_MAP.containsKey(entry.getKey()) ? ALL_MAP.get(entry.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			WRITER.write(entry.getKey() + TAB + prepLine(COUNTS));
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeAFs() throws IOException
	{

		BufferedWriter writer = new BufferedWriter(new FileWriter(FOLDER + "report-af.tsv"));
		for ( Entry<Integer, Set<String>> entry : COUNT_MAPS.entrySet() )
		{
			// fileName = FOLDER + entry.getKey() + "-review-af.tsv";
			WRITER = new BufferedWriter(new FileWriter(FOLDER + entry.getKey() + "-review-af.tsv"));

			for ( String line : entry.getValue() )
			{
				WRITER.write(line.trim());
				WRITER.newLine();
			}
			WRITER.close();
			COUNTS = TOTALS_MAP.containsKey(entry.getKey()) ? TOTALS_MAP.get(entry.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			COUNTS[AF.AUTHOR.ordinal()] = entry.getKey();
			writer.write(prepLine(COUNTS));
			writer.newLine();
		}
		writer.close();
	}
}
