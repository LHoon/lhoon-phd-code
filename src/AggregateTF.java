import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class AggregateTF
{
	public enum TF
	{	TERM, TOTAL, ONE, TWO, THREE, FOUR, FIVE	};

	final static int MAX_TOKENS = TF.values().length;
	final static String TAB = "\t";

	// Numerical indexes to track the review processing
	private static String TERM_VAL = "";
	private static Integer COUNT = 0;
	private static Integer TOTAL_FREQ = 0;

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Integer VALUE;

	private static Map<Integer, Integer> TF_MAP = new TreeMap<Integer, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		parseAllTFFile();
		writeTFs();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("TF report aggregated in " + timeTaken);
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static void add(Integer count)
	{
		Integer value = TF_MAP.containsKey(count) ? TF_MAP.get(count) : 0;
		value++;
		TF_MAP.put(count, value);
	}

	private static void parseAllTFFile() throws Exception
	{
		try
		{
			READER = new BufferedReader(new FileReader("./thesis/lang/all-tf.tsv"));

			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				TOKENS = LINE.split(TAB);
				if ( TOKENS.length == MAX_TOKENS )
				{
					COUNT = Integer.parseInt(TOKENS[TF.TOTAL.ordinal()]);
					TOTAL_FREQ += COUNT;
					add(COUNT);
				}
			}
			READER.close();
		}
		catch (Exception e)
		{	System.err.println("Error: " + e.getMessage());	}
	}

	private static void writeTFs() throws IOException
	{
		int total = TF_MAP.size();
		double proportion = 0.0;
		WRITER = new BufferedWriter(new FileWriter("./thesis/lang/all-tf-report.tsv"));

		Map<Integer, Integer> resultMap = new TreeMap<Integer, Integer>(new ValueComparer<Integer, Integer>(TF_MAP));
		resultMap.putAll(TF_MAP);

		for ( Entry<Integer, Integer> entry : resultMap.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());
			S_BUILDER.append(TAB);

			proportion = (double) (entry.getKey() * entry.getValue())/TOTAL_FREQ*100;

			S_BUILDER.append(proportion+"%");

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}
}


/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
