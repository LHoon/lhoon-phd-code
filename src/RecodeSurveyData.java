import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class RecodeSurveyData
{
	public enum InData
	{	RATING, APP, DATA_CODE, REVIEW, NOT_AT_ALL, NOT_VERY, UNSURE, SLIGHTLY_USEFUL, VERY_USEFUL, NOT_APPLICABLE };

	public enum OutData
	{	DATASET, APP_ID, REVIEW_ID, RATING, ANSWER	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int IN_TOKENS = InData.values().length;
	final static int OUT_TOKENS = OutData.values().length;
	final static String TAB = "\t";
	final static String COMMA = ",";

	private static int TOTAL = 0;
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Set<String> IN_DATA = new HashSet<String>();
	private static Set<String> OUT_DATA = new HashSet<String>();
	private static Map<Integer, String> RESULTS = new HashMap<Integer, String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long start = System.currentTimeMillis();
		write("./reports/survey-10/html/data-decomp.csv", "DATASET,APP,REVIEW,RATING,ANSWER");
		parseIn("./reports/survey-10/html/consolidated.csv");
		write("./reports/survey-10/html/data-decomp.csv", Integer.toString(TOTAL));
		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Dataset Consolidation " + "completed in " + elapsed);
	}

	private static void write(String filePath, String line) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, filePath);
		writer.writeLine(line);
	}

	private static void parseIn(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		int idx = 0;
		String[] outLine = new String[OUT_TOKENS];
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.length() == 0 )	{	continue;	}
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != IN_TOKENS )
			{	continue;	}
			try
			{
				int counts[] = {0, 0, 0, 0, 0, 0};

				counts[0] = Integer.parseInt(TOKENS[InData.NOT_AT_ALL.ordinal()]);
				counts[1] = Integer.parseInt(TOKENS[InData.NOT_VERY.ordinal()]);
				counts[2] = Integer.parseInt(TOKENS[InData.UNSURE.ordinal()]);
				counts[3] = Integer.parseInt(TOKENS[InData.SLIGHTLY_USEFUL.ordinal()]);
				counts[4] = Integer.parseInt(TOKENS[InData.VERY_USEFUL.ordinal()]);
				counts[5] = Integer.parseInt(TOKENS[InData.NOT_APPLICABLE.ordinal()]);
				for ( int i = 0; i < counts.length; i++ )
				{
					TOTAL+=counts[i];
					if ( counts[i] == 0 )	{	continue;	}
					outLine[OutData.DATASET.ordinal()] = TOKENS[InData.DATA_CODE.ordinal()];
					outLine[OutData.APP_ID.ordinal()] = TOKENS[InData.APP.ordinal()];
					outLine[OutData.REVIEW_ID.ordinal()] = TOKENS[InData.REVIEW.ordinal()];
					outLine[OutData.RATING.ordinal()] = TOKENS[InData.RATING.ordinal()];
					outLine[OutData.ANSWER.ordinal()] = Integer.toString(i+1);

					for ( int j = 0; j < counts[i]; j++ )
					{	write("./reports/survey-10/html/data-decomp.csv", S_UTIL.delimitTokens(outLine, COMMA));	}
				}
			}
			catch ( NumberFormatException nfe )	{	continue;	}
		}
		reader.close();
	}

	private static void writeFile(String fileName, String line)
	{
		System.out.println(line);
		// for(Entry<String, String[]> entry : METAMAP.entrySet())
		// {
		// 	System.out.print(entry.getKey() + " > ");

		// 	for(String s : entry.getValue())
		//  	{	System.out.print(s + " , ");	}
		//  	System.out.println();
		// }
	}
}
