import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class CalculateTFIDFMinWords
{
	public enum TermFreq
	{	DOC_ID, TERM, FREQ	};

	final static boolean CAT = true;
	final static boolean STAR = false;
	final static int MAX_TOKENS = TermFreq.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";

	private static Integer THRESHOLD = 0;
	private static String FILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static TFIDFer TFIDFER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("Please provide price_category parameter (free_health, paid_entertainment, etc) and mininum word count threshold for analysis context.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		FILE = args[0];
		try
		{
			THRESHOLD = Integer.parseInt(args[1]);
		} catch (NumberFormatException nfe)
		{
			System.out.println("Please provide numerical value for mininum word count threshold.");
			System.exit(1);
		}
		TFIDFER = new TFIDFer(S_UTIL);
		loadTermFreq(termFreqFilePath(CAT), CAT);
		TFIDFER.calculateTfIdfs();
		writeTFIDF(CAT);
		TFIDFER.clear();
		loadTermFreq(termFreqFilePath(STAR), STAR);
		TFIDFER.calculateTfIdfs();
		writeTFIDF(STAR);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("TFIDF analysis completed in " + elapsed);
	}

	private static String termFreqFilePath(boolean isCategoryLvl)
	{
		return isCategoryLvl ? String.format("./reports/survey-10/term-freq/cat-level/%s.tsv", FILE) : String.format("./reports/survey-10/term-freq/star-level/%s.tsv", FILE);
	}

	private static String fileOutPath(boolean isCategoryLvl)
	{
		return isCategoryLvl ? String.format("./reports/survey-10/tfidf/%s-cat-tfidf.tsv", FILE) : String.format("./reports/survey-10/tfidf/%s-star-tfidf.tsv", FILE);
	}

	private static void loadTermFreq(String file, boolean isCategoryLvl) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split(TAB);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			TFIDFER.loadTF(TOKENS);
		}
		reader.close();
	}

	private static void writeTFIDF(boolean isCategoryLvl) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, fileOutPath(isCategoryLvl));
		TFIDFER.writeTFIDFs(writer, 0);
	}
}
