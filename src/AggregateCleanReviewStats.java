import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;

import util.TextFile;

public class AggregateCleanReviewStats
{
	public enum AppStats
	{	APP_ID, APP_NAME, REVIEW_ID, RATING, RAW_COUNT, CLEAN_COUNT, POSITIVE_COUNT, NEGATIVE_COUNT, STOP_COUNT, SIGNAL_COUNT	};

	public enum Stats
	{	RATING, RAW_COUNT, CLEAN_COUNT, POSITIVE_COUNT, NEGATIVE_COUNT, STOP_COUNT, SIGNAL_COUNT	};

	public enum Sentiment
	{	POS, NEG, INC_POS, INC_NEG, NEUTRAL, SPAM	};


	final static int MAX_STAT_TOKENS = AppStats.values().length;
	final static String TAB = "\t";

	// Numerical indexes to track the review processing
	private static Integer LINE_COUNT = 0;
	private static Integer GOOD = 0;
	private static Integer BAD = 0;
	private static Integer LOW = 0;
	private static Integer MED = 0;
	private static Integer HIGH = 0;
	private static Integer SHORT = 0;

	private static Integer SHORT_THRESHOLD = 5;
	private static Integer LOW_THRESHOLD = 5;
	private static Integer MED_THRESHOLD = 30;
	// private static Integer HIGH_THRESHOLD = ;
	private static Double THRESHOLD = 0.5;
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static Integer[] STATS = {0,0,0,0,0,0,0};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static DecimalFormat DF2 = new DecimalFormat("###.##");
	private static Map<Integer, Integer> COUNT_MAP = new TreeMap<Integer, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		// long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}

		OUTFILE = args[0];
		// OUTFILE = CAT_PRICE.substring(3, CAT_PRICE.length());// + "-" + THRESHOLD + "-words";
		// System.out.println(OUTFILE);

		parseStatFile();
		// printMap();
		// double good = (double) GOOD/LINE_COUNT;
		// double bad = (double) BAD/LINE_COUNT;
		double s = (double) SHORT/LINE_COUNT;
		double m = (double) MED/LINE_COUNT;
		double l = (double) HIGH/LINE_COUNT;
		double gs = (double) GOOD/SHORT;
		System.out.println( OUTFILE + TAB  + LINE_COUNT + TAB + DF2.format(s) + TAB + DF2.format(gs));//  + TAB + DF2.format(l));
		// System.out.println(OUTFILE + TAB  + LINE_COUNT + TAB + BAD + TAB + GOOD + TAB + DF2.format(bad));
		// System.out.println("Output file columns: ");
		// System.out.println("APP_ID, APP_NAME, REVIEW_ID, RATING, REVIEW_TEXT, RAW_COUNT, CLEAN_COUNT, POSITIVE_COUNT, NEGATIVE_COUNT, STOP_COUNT, SIGNAL_COUNT");

		// long millis = System.currentTimeMillis() - startTime;
		// String timeTaken = String.format("%d minute(s) and %d second(s).",
		// 	TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
		// 	- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		// System.out.println("Stat Aggregation on " + formatNumber(LINE_COUNT) + " reviews took " + timeTaken);
	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void add(Map<Integer, Integer> theMap, Integer key)
	{
		Integer value = theMap.containsKey(key) ? theMap.get(key) : 0;
		value++;
		theMap.put(key, value);
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseStatFile() throws Exception
	{
		try
		{
			double signalRatio = 0.0;
			double noiseRatio = 0.0;
			READER = new BufferedReader(new FileReader("./thesis/reports/" + OUTFILE + ".tsv"));

			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				TOKENS = LINE.split(TAB);
				if ( TOKENS.length != MAX_STAT_TOKENS ) 		{	continue;	}

				try
				{
					for ( int i = 3; i < MAX_STAT_TOKENS; i++ )
					{	STATS[i-3] = Integer.parseInt(TOKENS[i].trim());	}

					// if ( STATS[Stats.RAW_COUNT.ordinal()] < LOW_THRESHOLD )		{	LOW++;	}
					// else if ( STATS[Stats.RAW_COUNT.ordinal()] > MED_THRESHOLD )	{	HIGH++;	}
					// else {	MED++;	}

					if ( STATS[Stats.RAW_COUNT.ordinal()] <= SHORT_THRESHOLD )
					{
						SHORT++;
						signalRatio = (double) STATS[Stats.SIGNAL_COUNT.ordinal()] / STATS[Stats.RAW_COUNT.ordinal()];
						if ( signalRatio > 0.5 )
						{
							GOOD++;
							System.out.println(TOKENS[AppStats.REVIEW_ID.ordinal()]);
						}
					}

					// signalRatio = (double) STATS[Stats.SIGNAL_COUNT.ordinal()] / STATS[Stats.RAW_COUNT.ordinal()];
					// noiseRatio = (double) (STATS[Stats.CLEAN_COUNT.ordinal()] - STATS[Stats.SIGNAL_COUNT.ordinal()]) / STATS[Stats.CLEAN_COUNT.ordinal()];
					// if ( signalRatio > THRESHOLD ) //STATS[Stats.SIGNAL_COUNT.ordinal()] < 5 ||
					// {
					// 	// System.out.print(TOKENS[AppStats.REVIEW_ID.ordinal()] + " Signal: ");
					// 	// System.out.print(STATS[Stats.SIGNAL_COUNT.ordinal()] + "/" + STATS[Stats.CLEAN_COUNT.ordinal()] + "= " + DF2.format(signalRatio));
					// 	// System.out.println(TAB + "Noise: " + (STATS[Stats.CLEAN_COUNT.ordinal()] - STATS[Stats.SIGNAL_COUNT.ordinal()]) + "/" + STATS[Stats.CLEAN_COUNT.ordinal()] + "= " + DF2.format(noiseRatio));
					// 	// System.out.println(TOKENS[AppStats.RAW_COUNT.ordinal()] + TAB + TOKENS[AppStats.SIGNAL_COUNT.ordinal()]);
					// 	// add(COUNT_MAP, STATS[Stats.RAW_COUNT.ordinal()]);
					// 	GOOD++;
					// }
					// else
					// {	BAD++;	}

					// if ( LINE_COUNT == 10 )	{	break;	}
					// if ( LINE_COUNT%25000 == 0 )	{	reportProgress();	}
					LINE_COUNT++;
				}
				catch (NumberFormatException nfe)
				{	continue;	}
			}
		}
		catch (Exception e) 	{e.printStackTrace();}
		// {	System.err.println("Error: " + e.getMessage());	}
		finally
		{	READER.close();	}
	}

	private static void printMap()
	{
		for (Entry<Integer, Integer> entry : COUNT_MAP.entrySet())
		{	System.out.println(entry.getKey() + " > " + entry.getValue());	}
	}

	private static void reportProgress()
	{	System.out.println(String.format("%s lines processed.",formatNumber(LINE_COUNT)));	}

	// private static void reportResults()
	// {
	// 	DecimalFormat df2 = new DecimalFormat("###.##");
	// 	double percentage = Double.valueOf(df2.format((double)ODD_WORDS.size()/TREATED_WORD_COUNT * 100));
	// 	System.out.println(percentage + "% of " + formatNumber(TREATED_WORD_COUNT) + " are odd words.");

	// 	int difference = TREATED_WORD_COUNT - RAW_WORD_COUNT;
	// 	percentage = Double.valueOf(df2.format((double) difference/RAW_WORD_COUNT * 100));
	// 	System.out.println(percentage + "% expansion in words.");
	// }

	/**
	 * Write to file the Map values.
	 */
	private static void writeStatFile(Map<String, Integer> theMap, String fileName) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/stats/" + OUTFILE + ".tsv"));
		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(theMap));
		resultMap.putAll(theMap);

		for (Entry<String, Integer> entry : theMap.entrySet())
		{
			// BufferedWriter bw = new BufferedWriter(new FileWriter(getFilePath("reports", fileName, "csv")));
			WRITER.write(entry.getKey() + "," + entry.getValue());
			WRITER.newLine();
			WRITER.close();
		}
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
