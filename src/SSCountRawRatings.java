import java.io.*;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.TextFile;

public class CountRawRatings
{
	public enum Index
	{	RATING, ISO, APP_NAME	};

	// Numerical indexes to track the review processing
	final static int MAX_TOKENS = Index.values().length;
	final static String PIPE = ",";

	/** File I/O */
	private static BufferedWriter WRITER;
	private static BufferedReader READER;

	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();


	private static Map<String, Integer[]> AND_MAP = new TreeMap<String, Integer[]>();
	private static Map<String, Integer[]> IOS_MAP = new TreeMap<String, Integer[]>();
	// private static Map<String, Integer> ALL_MAP = new TreeMap<String, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}
		parseReviewTextFile(args[0]);
		// printResults("ratings", ALL_MAP);
		printResults("ratings-android", AND_MAP);
		printResults("ratings-ios", IOS_MAP);

		// long millis = System.currentTimeMillis() - startTime;
		// String timeTaken = String.format("%d minute(s) and %d second(s).",
			// TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			// - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		// System.out.println("Text stabilisation on " + formatNumber(REVIEW_COUNT) + " reviews took " + timeTaken);
	}

	private static void add(Map<String, Integer[]> theMap, String key, String rating) throws Exception
	{
		try
		{
			Integer[] value = theMap.containsKey(key) ? theMap.get(key) : new Integer[]{0,0,0,0,0};
			value[Integer.valueOf(rating)-1]++;
			theMap.put(key, value);
		}
		catch (NumberFormatException nfe)
		{	System.out.println(rating);	}
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseReviewTextFile(String fileName) throws Exception
	{
		READER = new BufferedReader(new FileReader(fileName));

		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(",");
			if( TOKENS.length < MAX_TOKENS ) {	continue;	}
			S_BUILDER.delete(0, S_BUILDER.length());
			if ( TOKENS[Index.ISO.ordinal()].trim().equals("ZZ") )
			{	add(AND_MAP, TOKENS[Index.APP_NAME.ordinal()], TOKENS[Index.RATING.ordinal()]);	}
			// {	System.out.println(TOKENS[Index.APP_NAME.ordinal()]+","+TOKENS[Index.RATING.ordinal()]);	}
			else
			{	add(IOS_MAP, TOKENS[Index.APP_NAME.ordinal()], TOKENS[Index.RATING.ordinal()]);	}
			// add(ALL_MAP, TOKENS[Index.RATING.ordinal()]);
		}
		READER.close();
	}

	private static String getFilePath(String folder, String fileName, String extension)
	{	return String.format("./%s/%s.%s", folder, fileName, extension);	}

	private static void printResults(String file, Map<String, Integer[]> theMap) throws IOException
	{
		WRITER = new BufferedWriter(new FileWriter(getFilePath("sportsmate", file, "txt"), true));

		for ( Map.Entry<String, Integer[]> entry : theMap.entrySet() )
		{
			S_BUILDER.delete(0, S_BUILDER.length());
    		S_BUILDER.append(entry.getKey());
    		S_BUILDER.append(" stars > ");
    		for ( Integer rating : entry.getValue() )
    		{
    			S_BUILDER.append(rating);
    			S_BUILDER.append(",");
    		}
    		WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}

		WRITER.close();
	}
}
