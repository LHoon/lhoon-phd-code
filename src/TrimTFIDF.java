import java.io.*;

import java.text.DecimalFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class TrimTFIDF
{
	public enum IDF
	{	DOC, TOK, VAL	};

	final static int MAX_TOKENS = IDF.values().length;
	final static String TAB = "\t";

	private static Integer THRESHOLD;
	private static Set<String> STOP_WORDS = new HashSet<String>();
	private static String IN_FILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();
	private static TFIDFer TFIDFER;

	private static Map<String, Map<String, Double>> IDFS = new TreeMap<String, Map<String, Double>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long start = System.currentTimeMillis();
		if ( args.length != 3 )
		{	THRESHOLD = new Integer(10);	}
		else
		{
			try {	THRESHOLD = Integer.parseInt(args[2]);	}
			catch ( NumberFormatException nfe ) {	THRESHOLD = new Integer(10);	}
		}
		loadIDFs(args[0]);
		IDFSelection(args[1]);
		// TFIDFER = new TFIDFer(sU);
		// TFIDFER.calculateTfIdfs();
		// System.out.println("calculated");
		// DelimitedWriter writer = new DelimitedWriter(TAB, "./reports/paid-star-tfidf.tsv");
		// TFIDFER.writeTFIDFs(writer, 0);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static void loadStops() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader("./assets/conjunctions-determiners.txt"));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.length() > 0 )
			{	STOP_WORDS.add(S_UTIL.minorClean(LINE));	}
		}
	}

	private static void loadIDFs(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		Double val = new Double(0.0);
		Map<String, Double> nestedMap;
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			try {	val = Double.parseDouble(TOKENS[IDF.VAL.ordinal()]);	}
			catch ( NumberFormatException nfe ) {	continue;	}

			nestedMap = IDFS.containsKey(TOKENS[IDF.DOC.ordinal()]) ? IDFS.get(TOKENS[IDF.DOC.ordinal()]) : new TreeMap<String, Double>();
			nestedMap.put(TOKENS[IDF.TOK.ordinal()], val);
			IDFS.put(TOKENS[IDF.DOC.ordinal()], nestedMap);
		}
		reader.close();
	}

	private static void IDFSelection(String fileName) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, "./reports/"+fileName+".tsv");
		int current;
		int maxNum;
		TOKENS = new String[3];

		for( Entry<String, Map<String, Double>> entry : IDFS.entrySet() )
		{
			Map<String, Double> preSort = entry.getValue();
			Map<String, Double> sorted = new TreeMap<String, Double>(new hoon.util.ValueComparer<String, Double>(preSort));
			sorted.putAll(preSort);
			current = 0;
			maxNum = (int) sorted.size()*THRESHOLD/100;
			for ( Entry<String, Double> value : sorted.entrySet() )
			{
				if ( !STOP_WORDS.contains(entry.getKey()))
				{
					TOKENS[0] = entry.getKey();
					TOKENS[1] = value.getKey();
					TOKENS[2] = new DecimalFormat("#0.0000000000000000000000").format(value.getValue());
					writer.writeLine(S_UTIL.delimitTokens(TOKENS, writer.delimiter()));
					current++;
					if( current >= maxNum )	{	break;	}//System.out.println(current + " / " + maxNum + " / " + sorted.size()); break;	}
				}
			}
		}
	}
}
