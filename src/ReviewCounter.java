import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ReviewCounter
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/final/";

	private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();
	private static Map<String, Integer> REVIEW_COUNT = new HashMap<String, Integer>();


	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/categories.txt");
		parseReviews();

		for ( Entry<String, Integer> app : REVIEW_COUNT.entrySet() )
		{
			System.out.println(app.getKey() + TAB + app.getValue());
		}

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		// return String.format("./reviews/data/us_%s_reviews.csv", getCatName(category, isFree));
		return String.format("./reviews/clean-data/tab/%s.tsv", getCatName(category, isFree));
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( !line.equals("") )
			{	CATEGORIES.add(minorClean(line));	}
		}
		reader.close();
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws Exception
	{
		for ( String category : CATEGORIES )
		{
			parseFile(category, FREE);
			parseFile(category, PAID);
		}
	}

	private static void parseFile(String category, boolean isFree) throws IOException
	{
		int count = 0;
		String appID = "";
		String[] tokens;
		BufferedReader reader = new BufferedReader(new FileReader(getCategoryFileName(category, isFree)));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( line.equals("") )	{	continue;	}

			tokens = line.split(TAB);

			if ( tokens.length == MAX_DATA_TOKENS
				&& tokens[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				appID = tokens[AppData.APP_ID.ordinal()];
				count = REVIEW_COUNT.containsKey(appID) ? REVIEW_COUNT.get(appID) : 0;
				count++;
				REVIEW_COUNT.put(appID, count);
				continue;
			}
			BAD_SET.add(String.format("%s%s%s", getCategoryFileName(category, isFree), TAB, line));
		}
		reader.close();
		sortMap();
	}

	private static void sortMap()
	{	REVIEW_COUNT = sortByComparator(REVIEW_COUNT);	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
