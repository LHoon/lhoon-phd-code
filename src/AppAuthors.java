import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class AppAuthors
{
	public enum Data
	{	 AUTHOR, TOTAL, CATEGORY, APP, REVIEWS, ONE, TWO, THREE, FOUR, FIVE	};


	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_TOKENS = Data.values().length;
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/authors-app/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String AUTHOR_FOLDER = FOLDER + "authors/";
	final static String REPORT_FOLDER = FOLDER + "reports/";
	final static String APP_FOLDER = FOLDER + "apps/";
	final static String HEADER = "AUTHOR	TOTAL	CATEGORY	APP	REVIEWS	ONE	TWO	THREE	FOUR	FIVE";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer BAD_LINES = 0;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> REVIEW_COUNTS = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Map<String, Map<String, Map<String, Integer[]>>> AUTHOR_RATINGS = new HashMap<String, Map<String, Map<String, Integer[]>>>();
	private static Map<String, Integer> AUTHOR_MAP = new HashMap<String, Integer>();
	private static List<String> RETURNING_AUTHORS = new ArrayList<String>();

	private static Map<String, Map<Integer, Integer>> REVISITS = new TreeMap<String, Map<Integer, Integer>>();
	private static Map<String, Map<Integer, Integer>> APP_AUTHORS = new TreeMap<String, Map<Integer, Integer>>();
	private static Map<String, Set<String>> AUTHOR_CAT = new HashMap<String, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		// loadReviewCounts("./assets/review-counts.txt");
		parseFile();
		writeCatAF(REVISITS, "cat-authors.tsv", true);
		writeCatAF(APP_AUTHORS, "app-authors.tsv", false);
		writeCatAF();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(REVIEW_COUNTS.size() + " files processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, from " + formatNumber(AUTHOR_MAP.size()) + " authors, with " + formatNumber(BAD_SET.size()) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{	return NumberFormat.getNumberInstance(Locale.US).format(value);	}

	private static void parseFile() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(APP_FOLDER + "returning-authors.tsv"));
		Integer count;
		Integer reviews;
		String author;
		String app;
		String category;
		Map<Integer, Integer> authors;
		Set<String> categories;
		for ( String LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;
			TOKENS = LINE.split(TAB);
			if ( TOKENS[Data.AUTHOR.ordinal()].equals("AUTHOR") )	{	continue;	}
			if ( TOKENS.length == MAX_TOKENS )
			{
				app = TOKENS[Data.APP.ordinal()];
				author = TOKENS[Data.AUTHOR.ordinal()];
				category = TOKENS[Data.CATEGORY.ordinal()];
				reviews = Integer.parseInt(TOKENS[Data.REVIEWS.ordinal()]);

				authors = REVISITS.containsKey(category) ? REVISITS.get(category) : new TreeMap<Integer, Integer>();
				count = authors.containsKey(reviews) ? authors.get(reviews) : 0;
				count++;
				authors.put(reviews, count);
				REVISITS.put(category, authors);

				authors = APP_AUTHORS.containsKey(app) ? APP_AUTHORS.get(app) : new TreeMap<Integer, Integer>();
				count = authors.containsKey(reviews) ? authors.get(reviews) : 0;
				count++;
				authors.put(reviews, count);
				APP_AUTHORS.put(app, authors);

				categories = AUTHOR_CAT.containsKey(author) ? AUTHOR_CAT.get(author) : new HashSet<String>();
				categories.add(category);
				AUTHOR_CAT.put(author, categories);
				continue;
			}
			BAD_SET.add(LINE);
		}
		reader.close();
	}

	private static void writeCatAF(Map<String, Map<Integer, Integer>> theMap, String fileName, boolean isCat) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(APP_FOLDER+fileName));

		String header = isCat ? "CATEGORY	REVIEWS	AUTHOR_COUNT" : "APP	REVIEWS	AUTHOR_COUNT";
		writer.write(header);
		writer.newLine();

		for ( Entry<String, Map<Integer, Integer>> nestedMap : theMap.entrySet() )
		{
			for ( Entry<Integer, Integer> author : nestedMap.getValue().entrySet() )
			{
				S_BUILDER.setLength(0);
				S_BUILDER.append(nestedMap.getKey());
				S_BUILDER.append(TAB);
				S_BUILDER.append(author.getKey());
				S_BUILDER.append(TAB);
				S_BUILDER.append(author.getValue());
				writer.write(S_BUILDER.toString().trim());
				writer.newLine();
			}
		}
		writer.close();
	}

		private static void writeCatAF() throws IOException
	{
		Integer authorCount;
		Map<Integer, Integer> catAuthors = new TreeMap<Integer, Integer>();

		for ( Entry<String, Set<String>> entry : AUTHOR_CAT.entrySet() )
		{
			authorCount = catAuthors.containsKey(entry.getValue().size()) ? catAuthors.get(entry.getValue().size()) : 0;
			authorCount++;
			catAuthors.put(entry.getValue().size(), authorCount);
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(APP_FOLDER+ "cat-af.tsv"));
		writer.write("CAT_COUNT	AUTHOR_COUNT");
		writer.newLine();

		for ( Entry<Integer, Integer> entry : catAuthors.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());
			writer.write(S_BUILDER.toString().trim());
			writer.newLine();
		}
		writer.close();
	}

	private static void writeBadLines() throws Exception
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.txt"));

		for ( String line : BAD_SET )
		{
			writer.write(line.trim());
			writer.newLine();
		}
		writer.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
