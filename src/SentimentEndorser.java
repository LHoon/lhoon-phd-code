import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.TextFile;

/**
 * This class will parse the review file and output the following:
 * 1) Sentiment Proportions in a) Positive, b) Negative, c) Inconsistent
 *      Positive, d) Inconsistent Negative, e) Neutral, and f) Spam.
 *
 * 2) Undetected words that are NOT in conjunctions-determiners.txt:
 *      i) File 1: English words (as per dictionary.txt & aux-dictionary.txt).
 *      ii) File 2: Non-English words.
 *
 */
public class SentimentEndorser
{
	public enum Polarity
	{	POSITIVE, NEGATIVE, NOISE	};
	public enum Classification
	{	POSITIVE, NEGATIVE, INCONSISTENT_POS, INCONSISTENT_NEG, NEUTRAL, SPAM	};
	public enum Columns
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean TXT = true;
	final static boolean CSV = false;
	// Pattern.compile or as a String
	final static boolean PATTERN = true;
	final static boolean STRING = false;
	// Nest the Map with Sets or <String, String> only
	final static boolean NEST = true;
	final static boolean FLAT = false;

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static int[] COUNTS = {0, 0, 0};
	private static String[] TOKENS = {"", ""};

	private static Set<String> WORD_SET;
	private static Matcher THE_MATCHER;

	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static DecimalFormat DF2 = new DecimalFormat("###.##");

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/* Search Space Strings */
	// Positive word patterns
	private static String POSITIVE_SENTIMENTS = "";
	// Negative word patterns
	private static String NEGATIVE_SENTIMENTS = "";
	// Conjunction and Determiner patterns
	private static String NOISE_WORDS = "";

	/* Classification Counters */

	/* English and Jargon references */
	// English Language Reference
	private static Map<String, Set<String>> WORD_DICT = new TreeMap<String, Set<String>>();
	private static Map<String, String> ABBRV_TYPOS = new TreeMap<String, String>();

	// Patterns to cleanse text of non-alphanumerics
	private static Pattern ALPHA_NUMERICS = Pattern.compile("[0-9]+?|[\\w\\d/]+");
	// private static Pattern ALPHA_NUMERICS = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+");
	// private static Pattern DUPE_CHAR_PATTERN = Pattern.compile("(\\w)\\1+");

	/* Results: Datastructures to house the results of processing */
	// Sentiment Distributions
	private static Map<Classification, Integer> SENTIMENT_DISTRIBUTION = new HashMap<Classification, Integer>();
	// Reviews and ratings
	private static Map<Classification, Map<Integer, List<String>>> POLARISED_TEXT = new HashMap<Classification, Map<Integer, List<String>>>();
	private static Map<Classification, Map<String, Integer>> SENTIMENT_FREQ = new HashMap<Classification, Map<String, Integer>>();
	private static Map<Integer, List<String>> NESTED_MAP;
	private static Map<String, Integer> WORD_MAP;

	public static void main ( String[] args ) throws Exception
	{
		long start = System.currentTimeMillis();

		/* Startup code to load asset files into memory for lookups */
		System.out.print("Loading... ");
		loadAssets();
		generateStructures();
		System.out.println("Loading Complete.");

		/* Execution code to trigger processing */
		// test();
		readReviews(args[0]);
		writeSentimentDistribution();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Sentiment Endorsement " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	folder String value of folder name
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	extension file type or extension, WITHOUT the . prefix
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String filePath(String folder, String fileName, String extension)
	{   return String.format("./%s/%s.%s", folder, fileName, extension);	}

	/**
	 * Generate filepath for report files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isTxt true for csv files, false for txt
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String reportPath(String fileName, boolean isTxt)
	{   return filePath("reports", fileName, (isTxt ? "txt" : "csv") );	}

	/**
	 * Generate filepath for asset files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isTxt true for csv files, false for txt
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String assetPath(String fileName, boolean isTxt)
	{   return filePath("assets", fileName, (isTxt ? "txt" : "csv") );	}

	/**
	 * Generate the filepath for wip files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String wipPath(String fileName)
	{	return String.format("./wip/%s.csv", fileName);	}

	/**
	 *	Minor String preparation, trims trailing spaces, lowercases
	 *	and enforces Locale.ENGLISH prior to returning it.
	 * @param	text String value to be processed
	 * @param	asPattern true to treat it as a Pattern, false for no
	 * @return	String value constructed from param, LowerCased and trimmed
	 */
	private static String prepareString(String text, boolean asPattern)
	{	return asPattern ? "\\b" + text.toLowerCase(Locale.ENGLISH).trim() + "\\b" : text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Returns the first two characters of the String param. Failing that,
	 * returns "noprefix". SHOULD BE USED THROUGHOUT THIS SCRIPT FOR CONSISTENT
	 * ENGLISH DICTIONARY CHECK BEHAVIOUR.
	 * @param	text String value from which the prefix is extracted
	 * @return 	String value of the first two characters, OR noprefix
	 */
	private static String getPrefix(String text)
	{	return text.length() > 3 ? new String(text.substring(0,3)) : "noprefix";	}

	/**
	 * Calculates the percentage of the first param from the second param and
	 * formats it to at most 2 decimal places.
	 * @param	amount first int value from which the percentage is calculated
	 * @param	sum the second int value that the first param is divided by
	 * @return	double result formatted to two decimal places
	 */
	private static double getProportion(int amount, int sum)
	{	return Double.valueOf(DF2.format((double) amount/sum * 100));	}


	/**
	 *	Startup method to load up assets into memory.
	 */
	private static void loadAssets() throws IOException
	{
		// Pattern Sets, hence true parameter
		POSITIVE_SENTIMENTS = populateSentiments(assetPath("positive-words", TXT));
		NEGATIVE_SENTIMENTS = populateSentiments(assetPath("negative-words", TXT));
		NEGATIVE_SENTIMENTS += populateSentiments(assetPath("aux-negative-words", TXT));
		NOISE_WORDS = populateSentiments(assetPath("conjunctions-determiners", TXT));
		// Dictionary of known English words and Jargon; true = nested Map
		loadMap(WORD_DICT, assetPath("dictionary",TXT), NEST);
		loadMap(WORD_DICT, assetPath("aux-dictionary",TXT), NEST);
		// Abbreviations/Txt Speak, Common Typos, Trained Typos
		loadMap(ABBRV_TYPOS, assetPath("abbrv-txt-spk",CSV), FLAT);
		loadMap(ABBRV_TYPOS, assetPath("common-typos",CSV), FLAT);
		loadMap(ABBRV_TYPOS, assetPath("aux-common-typos",CSV), FLAT);
		System.out.print("Assets Loaded. ");
	}

	/**
	 *	Startup method to generate the SENTIMENT_FREQ structure.
	 */
	private static void generateStructures()
	{
		for ( Classification polarity : Classification.values() )
		{
			Map<String, Integer> nestedMap = new HashMap<String, Integer>();
			SENTIMENT_FREQ.put(polarity, nestedMap);
			// Map<Integer, List<String>> nestedMap = new TreeMap<Integer, List<String>>();
			// for ( int i = 1; i <=5 ; i++)
			// {	nestedMap.put(i, new ArrayList<String>());	}
			// POLARISED_TEXT.put(polarity, nestedMap);
		}
		System.out.print("Structures Generated. ");
	}

	/**
	 *	Generic method to load a input file word list into a param Set as
	 *	compiled regex Patterns or raw Strings, dependent on boolean param.
	 * @param	filepath filePath of the input resource to add to the Set
	 * @return	String value of the search space
	 */
	private static String populateSentiments(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			S_BUILDER.append(prepareString(LINE, STRING));
			S_BUILDER.append(",");
		}
		return S_BUILDER.toString().trim();
	}

	@SuppressWarnings("unchecked")
	/**
	 *	Generic method to load a input file word list into a param Set as
	 *	compiled regex Patterns or raw Strings, dependent on boolean param.
	 * @param	theSet datastructure for the values to be added into
	 * @param	filepath filePath of the input resource to add to the Set
	 */
	private static void loadSet(Set theSet, String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{	theSet.add( Pattern.compile(prepareString(LINE, PATTERN)) );	}
		READER.close();
	}

	@SuppressWarnings("unchecked")
	/**
	 * Populates the dictionary Map based on the text file. The prefix of the
	 * word forms the key of the map, the word itself is added to the HashSet of
	 * values associated with that prefix. Makes parsing through the dictionary
	 * for string similarity jaroWinklerAlgos MUCH faster.
	 * @param 	theMap datastructure for the values to be added into
	 * @param	filepath filePath of the input resource to add to the Map
	 * @param	isNested true to treat it as a nested Map (with prefixes)
	 */
	private static void loadMap(Map theMap, String fileName, boolean isNested) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		String mapKey;
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			LINE = prepareString(LINE, STRING);
			if ( !isNested )
			{
				TOKENS = LINE.split(",");
				if(TOKENS.length == 2)
				{	theMap.put(TOKENS[0], TOKENS[1]);	}
			}
			else
			{
				mapKey = getPrefix(LINE);
				WORD_SET = !theMap.containsKey(mapKey) ? new TreeSet<String>() : (TreeSet<String>) theMap.get(mapKey);
				WORD_SET.add(LINE);
				theMap.put(mapKey, WORD_SET);
			}
		}
		READER.close();
	}

	@SuppressWarnings("unchecked")
	/**
	 * Generic method to write results to file. Default write locations is the
	 * reports folder of this project.
	 * @param	results Entry value where key = rating and value = reviewList
	 * @param	filepath filePath of the input resource to write
	 * @param	doAppend boolean value where true appends to the existing file
	 */
	private static void reportSentiment(int rating, String text, String filePath) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(rating + "||||" + text);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	/**
	 * Generic method to print SENTIMENT_DISTRIBUTION to console.
	 */
	private static void wordCounts()
	{
		int sumOfReviews = 0;
		int rawCount = 0;
		for ( Entry<Classification, Integer> entry :  SENTIMENT_DISTRIBUTION.entrySet() )
		{	sumOfReviews += entry.getValue();	}
		for ( Classification polarity : Classification.values()	)
		{
			rawCount = SENTIMENT_DISTRIBUTION.get(polarity);
			System.out.println(polarity + ": " + getProportion(rawCount, sumOfReviews) + "% (" + rawCount + ")");
		}
	}

	/**
	 * Generic method to print SENTIMENT_DISTRIBUTION to console.
	 */
	private static void reportSentimentDistribution()
	{
		int sumOfReviews = 0;
		int rawCount = 0;
		for ( Entry<Classification, Integer> entry :  SENTIMENT_DISTRIBUTION.entrySet() )
		{	sumOfReviews += entry.getValue();	}
		for ( Classification polarity : Classification.values()	)
		{
			rawCount = SENTIMENT_DISTRIBUTION.get(polarity);
			System.out.println(polarity + ": " + getProportion(rawCount, sumOfReviews) + "% (" + rawCount + ")");
		}
	}

	/**
	 * Method to write the SENTIMENT_DISTRIBUTION proportions to file.
	 */
	private static void writeSentimentDistribution() throws IOException
	{
		int rawCount = 0;
		int sumOfReviews = 0;
		for ( Entry<Classification, Integer> entry :  SENTIMENT_DISTRIBUTION.entrySet() )
		{	sumOfReviews += entry.getValue();	}
		try
		{
			WRITER = new BufferedWriter(new FileWriter(reportPath("sentiment-distribution", TXT), true));
			WRITER.write("Polarity Distributions for " + sumOfReviews + " reviews:");
			WRITER.newLine();
			for ( Classification polarity : Classification.values()	)
			{
				rawCount = SENTIMENT_DISTRIBUTION.containsKey(polarity) ? SENTIMENT_DISTRIBUTION.get(polarity) : 0;
				WRITER.write(polarity + ": " + getProportion(rawCount, sumOfReviews) + "% (" + rawCount + ")");
				WRITER.newLine();
			}
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	/**
	 * Iterate through all the Patterns and detect matches. Return the number of
	 * matches found in the String param provided.
	 * @param	theSet Pattern datastructure to iterate through to check against
	 * @param	text perform the Match on
	 * @return	the number of matches
	 */
	private static int patternMatcher(String sentiment, String text)
	{
		int matches = 0;
		THE_MATCHER = generatePattern(text).matcher(sentiment);
		while ( THE_MATCHER.find() )
		{	matches++;	}
		THE_MATCHER = null;
		return matches;
	}

	/**
	 * Method to generate a Pattern from a text block. Output is in parentheses,
	 * with \b boundaries pre and post each word, and | pipes separating the
	 * words.
	 * @param 	text String value to be processed into a Pattern
	 * @return	Pattern object developed from the String param
	 */
	private static Pattern generatePattern(String text)
	{
		boolean firstLine = true;
		String[] tokens = text.split(" ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		S_BUILDER.append("(");
		for ( String word : tokens )
		{
			if(!firstLine)
			{	S_BUILDER.append("|");	}
			S_BUILDER.append(prepareString(word, PATTERN));
			firstLine = false;
		}
		S_BUILDER.append(")");
		return Pattern.compile(S_BUILDER.toString());
	}

	/**
	 * Iterate through all the Words and detect matches. Return the number of
	 * matches found in the String param provided.
	 * @param	text perform the Match on
	 * @return	the number of matches
	 */
	private static int wordMatcher(String text)
	{
		int matches = 0;
		TOKENS = text.split(" ");
		for ( String word : TOKENS)
		{
			matches += ( WORD_DICT.containsKey(getPrefix(word))
						&& WORD_DICT.get(getPrefix(word)).contains(word) )
						? 1 : 0;
		}
		return matches;
	}

	/**
	 * Method to count the number of each type of sentiment. As it stands, there
	 * are only positive, negative or noise type sentiments. Index 0 = Positive,
	 * Index 1 = Negative, Index 2 = everything else.
	 * @param	text perform the counting on
	 * @return	int array of positive, negative and noise word counts
	 */
	private static int[] countSentiments(String text)
	{
		COUNTS[0] = patternMatcher(POSITIVE_SENTIMENTS, text);
		COUNTS[1] = patternMatcher(NEGATIVE_SENTIMENTS, text);
		COUNTS[2] = patternMatcher(NOISE_WORDS, text);
		return COUNTS;
	}

	/**
	 * Method to count the number of words and their types. As it stands, there
	 * are only English/Jargon/App Name words, depending on the dictionaries
	 * loaded at startup. Index 0 = Total # of words in the text param,
	 * Index 1 = English/Jargon words, Index 2 = Unidentified words.
	 * @param	text perform the counting on
	 * @return	int array of total, english and unidentified word counts
	 */
	private static int[] countWords(String text)
	{
		COUNTS[0] = text.split(" ").length;	// # words in total
		COUNTS[1] = wordMatcher(text);		// # English or Jargon
		COUNTS[2] = COUNTS[0] - COUNTS[1];	// # unindentified words
		return COUNTS;
	}

	/**
	 * Increments the occurances in the SENTIMENT_DISTRIBUTION Map.
	 * Classification is the Key, number of occurances is the Value.
	 * @param	polarity Classification Key to increment in SENTIMENT_DISTRIBUTION
	 */
	private static void incrementInMap(Classification polarity)
	{
		int frequency = SENTIMENT_DISTRIBUTION.containsKey(polarity)
						? (SENTIMENT_DISTRIBUTION.get(polarity) + 1) : 1;
		SENTIMENT_DISTRIBUTION.put(polarity, frequency);
	}

	/**
	 * Increments the occurances in the SENTIMENT_FREQ Map.
	 * Classification is the Key, number of occurances is the Value.
	 * @param	polarity Classification Key to increment in SENTIMENT_FREQ
	 * @param	text the String of text to be tokenised and counted
	 */
	private static void incrementWordsInPolarity(Classification polarity, String text)
	{
		TOKENS = text.split(" ");
		WORD_MAP = SENTIMENT_FREQ.get(polarity);
		for ( String word : TOKENS)
		{
			int frequency = WORD_MAP.containsKey(word)
						? (WORD_MAP.get(word) + 1) : 1;
			WORD_MAP.put(word, frequency);
		}
		SENTIMENT_FREQ.put(polarity, WORD_MAP);
	}

	/**
	 * Persists the data into the POLARISED_TEXT Map. Classification is the Key,
	 * number of occurances is the Value, where the Value stores:
	 * rating => List of Text pertaining to the rating.
	 * @param	polarity Classification Key in POLARISED_TEXT
	 * @param	text String text to be stored
	 * @param	rating int rating to be stored, also the Key for polarity values
	 */
	private static void persistInMap(Classification polarity, String text, int rating)
	{
		NESTED_MAP = POLARISED_TEXT.get(polarity);
		List<String> textList = NESTED_MAP.get(rating);
		textList.add(text);
		NESTED_MAP.put(rating, textList);
		POLARISED_TEXT.put(polarity, NESTED_MAP);
	}

	/**
	 * Checks if the general sentiment of the review content matches the rating
	 * that is provided as a param, then returns the Classification. Non-ID'ed
	 * words are auto classified as SPAM.
	 * @param	sentiments collection of sentiment counts
	 * @param	rating value to compare the general sentiment against
	 * @return	Classification value corresponding to the comparison result
	 */
	private static Classification checkConsistency(String text, int rating)
	{
		COUNTS = countSentiments(text);
		int polarValue = calculatePolarity(COUNTS);
		Classification polarity = Classification.SPAM;

		switch (rating)
		{
			case 1:	polarity = ( polarValue < 0 ) ? Classification.NEGATIVE
								: Classification.INCONSISTENT_NEG;
					break;
			case 2:	polarity = ( polarValue < 0 ) ? Classification.NEGATIVE
								: Classification.INCONSISTENT_NEG;
					break;
			case 3:	if ( polarValue == 0 )
					{
						if ( COUNTS[0] != 0 || COUNTS[1] != 0 )
						{	polarity = Classification.NEUTRAL;	}
					}
					else if ( polarValue > 0 )
					{	polarity = Classification.INCONSISTENT_POS; }
					else if ( polarValue < 0 )
					{	polarity = Classification.INCONSISTENT_NEG; }
					break;
			case 4: polarity = ( polarValue > 0 ) ? Classification.POSITIVE
								: Classification.INCONSISTENT_POS;
					break;
			case 5:	polarity = ( polarValue > 0 ) ? Classification.POSITIVE
								: Classification.INCONSISTENT_POS;
					break;
			default:	break;
		}
		incrementInMap(polarity);
		incrementWordsInPolarity(polarity, text);
		return polarity;
	}

	/**
	 * Calculate the general sentiment from the sentiment array param. Returns
	 * negative values for NEGATIVE, positive for POSITIVE, zero for NEUTRAL.
	 * @param	sentiments collection to calculate general sentiment
	 * @return	result indicative of the general sentiment
	 */
	private static int calculatePolarity(int[] sentiments)
	{	return (sentiments[Polarity.NOISE.ordinal()] > sentiments[Polarity.POSITIVE.ordinal()] + sentiments[Polarity.NEGATIVE.ordinal()]) ? 0 : sentiments[Polarity.POSITIVE.ordinal()] - sentiments[Polarity.NEGATIVE.ordinal()];	}

	/**
	 * Splits the param rawText into alphanumeric characters only and returns it
	 * as a new String. Might cause issues with memory overhead.
	 * @param	rawText raw text to be processed
	 * @return	String result of the processed text
	 */
	private static String stabiliseText(String rawText)
	{
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		rawText = rawText.replaceAll("'", "");
		THE_MATCHER = ALPHA_NUMERICS.matcher(rawText);
		String word;
		while (THE_MATCHER.find())
		{
			word = ABBRV_TYPOS.containsKey(THE_MATCHER.group())
					? ABBRV_TYPOS.get(THE_MATCHER.group()) : THE_MATCHER.group();
			S_BUILDER.append(prepareString(word, STRING));
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}

	private static void test()
	{
		String review = "WOOOOOOO! btw gr8 zv Holy crap this app is wicked . rss groupon espn barcode icloud evernote I really love it!";
		review = stabiliseText(review);
		int rating = 3;
		// checkConsistency(review, rating);
		// COUNTS = countWords(review);
		// WORD_SET = WORD_DICT.get("noprefix");
		// for ( String word : WORD_SET)
		// {	System.out.println(word);	}
		COUNTS = countSentiments(review);
		System.out.println(COUNTS[0]);
		System.out.println(COUNTS[1]);
		System.out.println(COUNTS[2]);
		for ( int i = 1; i <= 5; i++)
		{
			Classification polarity = checkConsistency(review, i);
			System.out.println("Rating = " + i + polarity);
		}
		// System.out.println("Unidentified words: " + countWords(review)[2]);
		// System.out.println(review);
		// for (Classification p : Classification.values()) { System.out.println(p);}
	}

	/**
	 * Parses the review file that is brought in as a param, splits each line
	 * into tokens to obtain the review content and rating, where the text is
	 * treated using stabiliseText() to lowercased, trimmed, cleaned of all
	 * non-alphanumerics and brute forced against typos. The cleaned text is
	 * then sentiment counted and checked for consistency in content and rating.
	 * @param	filePath String filepath of the review file
	 */
	private static void readReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		int rating = 0;
		String review;
		String[] reviewTokens;

		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			reviewTokens = LINE.split("\\|\\|");
			if ( reviewTokens.length < 13 )
			{	continue;	}
			try
			{
				rating = Integer.parseInt(reviewTokens[Columns.RATING.ordinal()]);
				review = reviewTokens[Columns.REVIEW_TITLE.ordinal()]
						+ " " + reviewTokens[Columns.REVIEW_BODY.ordinal()];
				review = stabiliseText(review);
				reportSentiment(rating, review, reportPath(checkConsistency(review, rating).toString(), CSV));
			} catch (Exception e) {continue;}
		}
		READER.close();
	}
}
