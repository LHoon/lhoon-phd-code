import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class CatPriceAF
{
	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum AF
	{	FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/authors-app/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String AUTHOR_FOLDER = FOLDER + "authors/";
	final static String REPORT_FOLDER = FOLDER + "reports/";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer BAD_LINES = 0;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> AUTHORS = new TreeSet<String>();
	private static Set<String> BAD_SET = new TreeSet<String>();

	private static Map<String, Set<String>> CAT_AUTHORS = new TreeMap<String, Set<String>>();
	private static Map<String, Set<String>> CAT_REVIEWS = new TreeMap<String, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/categories.txt");
		parseReviews();
		writeCatAuthors();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATEGORIES.size() + " categories processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, with " + formatNumber(AUTHORS.size()) + " authors, with " + formatNumber(BAD_SET.size()) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("./reviews/clean-data/tab/%s.tsv", getCatName(category, isFree));
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static String getCatLabel(String category, boolean isFree)
	{
		String price = isFree ? "(F)" : "(P)";
		return String.format("%s %s", category, price);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( !LINE.equals("") )
			{	CATEGORIES.add(minorClean(LINE));	}
		}
		reader.close();
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws Exception
	{
		for ( String category : CATEGORIES )
		{
			parseFile(category, FREE);
			parseFile(category, PAID);
		}
	}

	private static void parseFile(String category, boolean isFree) throws IOException
	{
		String author = "";
		String review = "";
		BufferedReader reader = new BufferedReader(new FileReader(getCategoryFileName(category, isFree)));
		for ( String LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;

			TOKENS = LINE.split(TAB);

			if ( TOKENS.length == MAX_DATA_TOKENS
				&& TOKENS[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				if ( TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length > 1 )
				{
					int rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
					author = TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1];
					review = TOKENS[AppData.REVIEW_ID.ordinal()];
					addToMap(author, review, getCatLabel(category, isFree));
					continue;
				}
			}
			BAD_SET.add(String.format("%s%s%s", getCatLabel(category, isFree), TAB, LINE));
		}
		reader.close();
	}

	private static void addToMap(String author, String review, String catLabel)
	{
		Set<String> nestedSet;

		nestedSet = CAT_AUTHORS.containsKey(catLabel) ? CAT_AUTHORS.get(catLabel) : new HashSet<String>();
		nestedSet.add(author);
		CAT_AUTHORS.put(catLabel, nestedSet);
		AUTHORS.add(author);

		nestedSet = CAT_REVIEWS.containsKey(catLabel) ? CAT_REVIEWS.get(catLabel) : new HashSet<String>();
		nestedSet.add(review);
		CAT_REVIEWS.put(catLabel, nestedSet);
		REVIEW_COUNT++;
	}

	private static void writeCatAuthors() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(REPORT_FOLDER + "cat-author-counts.tsv"));
		for ( Entry<String, Set<String>> category : CAT_AUTHORS.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(category.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(category.getValue().size());
			S_BUILDER.append(TAB);
			S_BUILDER.append(CAT_REVIEWS.get(category.getKey()).size());
			writer.write(S_BUILDER.toString().trim());
			writer.newLine();
		}
		writer.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
