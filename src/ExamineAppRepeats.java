import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ExamineAppRepeats
{
	final static String TAB = "\t";
	final static String FOLDER = "./reviews/clean-droid/index/";

	private static Integer LINES_READ = 0;

	private static Map<String, Integer> CATS = new TreeMap<String, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		readRepeats("repeats.tsv");
		writeRepeats("repeat-cat-count", "tsv");

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(formatNumber(LINES_READ) +  " lines read in " + timeTaken);
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void readRepeats(String fileName) throws IOException
	{
		String appID;
		String[] tokens;
		Integer count;
		BufferedReader reader = new BufferedReader(new FileReader(String.format("%s%s", FOLDER, fileName)));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( line.equals("") )	{	continue;	}

			tokens = line.split(TAB);
			LINES_READ++;
			appID = tokens[0];

			for ( int i = 1; i < tokens.length; i++ )
			{
				count = CATS.containsKey(tokens[i]) ? CATS.get(tokens[i]) : 0;
				count++;
				CATS.put(tokens[i], count);
			}
		}
		reader.close();
	}

	private static void writeRepeats(String fileName, String extension) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", FOLDER, fileName, extension)));
		StringBuilder sb = new StringBuilder();

		for ( Entry<String, Integer> entry : CATS.entrySet() )
		{
			sb.setLength(0);
			sb.append(entry.getKey());
			sb.append(TAB);
			sb.append(String.valueOf(entry.getValue()));
			writer.write(sb.toString());
			writer.newLine();
		}
		writer.close();
	}

	// private static void writeList(String fileName, String extension, List<String> reviews, boolean doCount) throws IOException
	// {
	// 	BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
	// 	for ( String review : reviews )
	// 	{
	// 		if ( doCount )	{	REVIEW_COUNT++;	}
	// 		writer.write(review);
	// 		writer.newLine();
	// 	}
	// 	writer.close();
	// }
}
