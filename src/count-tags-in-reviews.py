#!/usr/bin/env python

# Requires Python 3

# OUTPUT_FILE is tab separated and has the same format as REVIEW_FILE with the tag counts appended to each line.
# Tags count columns are sorted alphabetically i.e. Bug/Crash, Ecosystem, Function, Hardware Constraints, Iacob, Monetisation, Network Connectivity, Other, Request, Sensing, Social & Personal, UX/UI

import re
from collections import defaultdict

TAG_FILE = "./assets/tags.tsv"
REVIEW_FILE = "./iacob-linguistics/free_health_reviews/287529757.tsv"
OUTPUT_FILE = "287529757-tagged-reviews.tsv"
DELIMETER = "\t"

def readtagfile():

    tags = dict()

    with open(TAG_FILE, 'r') as file:
        keys = [w.strip() for w in file.readline().split(DELIMETER)]

        for key in keys:
            tags[key] = []

        for line in file:                          # Build word lists for each tag
            words = line.split(DELIMETER)
            for i, phrase in enumerate(words):
                if phrase:
                    tags[keys[i]].append(phrase.strip())

    return tags


def counttags(title, review, tags):

    text = title + " " + review
    count = defaultdict(int)

    for k,v in tags.iteritems():
        for phrase in v:
            # Not in the middle of a word. Must be start of line or punctuation. Must end with end of line or punctuation.
            regex = re.compile("(?:^|[^a-zA-Z0-9])" + re.escape(phrase.lower()) + "(?:$|[^a-zA-Z0-9])")
            matches = regex.findall(text.lower())
            #if len(matches) > 0:
            #    print(matches)
            count[k] = count[k] + len(matches)

    return count

def processreview():

    output = []

    with open(REVIEW_FILE, 'r') as file:
        tags = readtagfile()

        for line in file:
            parts = line.split(DELIMETER)
            title = parts[8]
            review = parts[9]
            count = counttags(title, review, tags)
            print sorted(count.items())

            values = [str(x[1]) for x in sorted(count.items())]
            out = line.strip() + DELIMETER + DELIMETER.join(values)
            output.append(out)

    printoutput(output)

# Writes output data to file
def printoutput(output):

    with open(OUTPUT_FILE, 'w') as file:

        for line in output:
            file.write(line + "\n")

def sanitytest():
    REV = "The portion size Needs tweaking, portions can't really be changed. But it's a great app so far it helps keep me on track and realize what I eat especially on the go!!"
    count_dict = counttags(title="misc", review=REV, tags=readtagfile())
    print(count_dict)

def main():

    print("Start processing...")
    processreview()
    print("Finished processing")

if __name__ == '__main__':
    #sanitytest()
    main()
