import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class ConsolidateReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int DATA_TOKENS = AppData.values().length;
	final static int META_TOKENS = AppMeta.values().length;
	final static String TAB = "\t";
	final static String PIPE = "\\|\\|";
	final static String[] HEADER = {"APP_ID", "APP_NAME", "URL", "PRICE", "CURRENCY", "RANK", "REVIEW_ID", "REVIEW_DATE", "APP_VERSION", "AUTHOR_NAME", "AUTHOR_ID", "RATING", "REVIEW_TITLE", "REVIEW_BODY"};

	private static String CAT = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Map<String, String[]> METAMAP = new HashMap<String, String[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Insufficient args provided for Dataset Consolidation.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		// NAME_OF_APP = appName.replaceAll(" ","_");
		CAT = args[0];
		parseMeta(getPath(CAT, INDEX));
		parseData(getPath(CAT, DATA));
		// printMetaMap();
		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Dataset Consolidation " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/us_%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Generate filepath for out files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./out/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reports/felix/%s.csv", fileName);	}

	private static void write(String filePath, String[] tokens) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, filePath);
		writer.writeLine(S_UTIL.delimitTokens(tokens, TAB));
		// System.out.println(S_UTIL.delimitTokens(tokens, TAB));
	}

	private static void parseMeta(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.length() == 0 )	{	continue;	}
			TOKENS = S_UTIL.tokenise(LINE, PIPE);
			if( TOKENS.length != META_TOKENS )
			{	continue;	}
			METAMAP.put(TOKENS[AppMeta.APP_ID.ordinal()], TOKENS);
		}
		reader.close();
	}

	private static void printMetaMap()
	{
		for(Entry<String, String[]> entry : METAMAP.entrySet())
		{
			System.out.print(entry.getKey() + " > ");

			for(String s : entry.getValue())
		 	{	System.out.print(s + " , ");	}
		 	System.out.println();
		}
	}

	private static void parseData(String filePath) throws IOException
	{
		int i = 0;
		write(outPath(CAT), HEADER);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if ( LINE.length() == 0 )	{	continue;	}
			TOKENS = S_UTIL.tokenise(LINE, PIPE);
			if( TOKENS.length != DATA_TOKENS )
			{	continue;	}

			write(outPath(CAT), formatLine(TOKENS));
		}
		reader.close();
	}

	private static String[] formatLine(String[] tokens)
	{
		String appID = tokens[AppData.APP_ID.ordinal()];
		String[] metaToks = METAMAP.get(appID);
		String[] result = new String[14];
		result[0] = tokens[AppData.APP_ID.ordinal()];
		result[1] = metaToks[AppMeta.APP_NAME.ordinal()];
		result[2] = metaToks[AppMeta.URL.ordinal()];
		result[3] = metaToks[AppMeta.PRICE.ordinal()];
		result[4] = metaToks[AppMeta.CURRENCY.ordinal()];
		result[5] = metaToks[AppMeta.RANK.ordinal()];
		result[6] = tokens[AppData.REVIEW_ID.ordinal()];
		result[7] = tokens[AppData.REVIEW_DATE.ordinal()];
		result[8] = tokens[AppData.APP_VERSION.ordinal()];
		result[9] = tokens[AppData.AUTHOR_NAME.ordinal()];
		result[10] = tokens[AppData.AUTHOR_ID.ordinal()];
		result[11] = tokens[AppData.RATING.ordinal()];
		result[12] = tokens[AppData.REVIEW_TITLE.ordinal()];
		result[13] = tokens[AppData.REVIEW_BODY.ordinal()];
		return result;
	}
}
