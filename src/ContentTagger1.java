import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentTagger1
{
	public enum ReviewObjTokens
	{	REVIEW_ID, RATING, R_TEXT, C_TEXT	};

	final static String TAG_DELIMITER = ":";
	final static String FILE_DELIMITER = "\t";

	static Set<String> POS_WORDS = new TreeSet<String>();
	static Set<String> NEG_WORDS = new TreeSet<String>();

	static String OUT_FILE = "./reports/content-tags/test.html";
	static ReviewFactory REVIEW_FACTORY = new ReviewFactory();
	static StringBuilder S_BUILDER = new StringBuilder();

	static List<Review> ANNOTATED_REVIEWS = new ArrayList<Review>();
	static Map<String, Set<String>> CONTENT_KEYS = new HashMap<String, Set<String>>();
	static Map<String, Set<String>> TAG_MAP = new HashMap<String, Set<String>>();

	// static Set<String> STOP_WORDS = new HashSet<String>();
	// static TextCleaner CLEANER;

	public static void main(String[] args) throws IOException
	{
		loadKeywords();
		// String file = "387771637-2.tsv";
		// String path = "reviews/3/";

		String file = "287529757.tsv";
		String path = "reviews/us_free_health/20words-30reviews/";

		// loadWordSet("stop");
		loadWordSet("pos");
		loadWordSet("neg");  // WRITE THE CODE OT LOOP AND TAG PER SET
		// CLEANER = new TextCleaner();
		parseReviews(path+file);
		HTMLWriter htmlWriter = new HTMLWriter(OUT_FILE, ANNOTATED_REVIEWS);
		htmlWriter.writeReport();
	}

	private static void loadWordSet(String type) throws IOException
	{
		String fileName = "./assets/";
		switch(type)
		{
			// case "stop":	fileName += "stop-words.txt";
			// 				break;
			case "pos":		fileName += "positive-words.txt";
							break;
			case "neg":		fileName += "negative-words.txt";
							break;
			default: break;
		}
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			// if ( type.equals("stop") )
			// {	STOP_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
			if ( type.equals("pos") )
			{	POS_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
			else if ( type.equals("neg") )
			{	NEG_WORDS.add(line.toLowerCase(Locale.ENGLISH).trim());	}
		}
		reader.close();
	}

	private static void loadKeywords() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader("./assets/content-keys.txt"));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{
				String tokens[] = line.split(",");
				Set<String> keySet = new HashSet<String>();
				for ( int i = 1; i < tokens.length; i ++ )
				{	keySet.add(tokens[i]);	}
				String key = tokens[0].toLowerCase(Locale.ENGLISH).trim();
				CONTENT_KEYS.put(key, keySet);
			}
		}
		reader.close();
	}

	private static void parseReviews(String filename) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{	annotate(REVIEW_FACTORY.makeReview(line, FILE_DELIMITER));	}
		}
		reader.close();
	}


	private static void annotate(Review review)
	{
		boolean hasTag = false;
		TAG_MAP.clear();
		findConcepts(review.tokens[ReviewObjTokens.C_TEXT.ordinal()]);
		findSentiments(review.tokens[ReviewObjTokens.C_TEXT.ordinal()]);
		if ( !TAG_MAP.isEmpty() )
		{
			review.tokens[ReviewObjTokens.C_TEXT.ordinal()] = annotateKeys(review);
			ANNOTATED_REVIEWS.add(review);
		}

		// content key tags
		// sentiment word tags
		// sentence level tags
		// for(Entry<String, List<String>> entry : CONTENT_KEYS.entrySet())
		// {
		// 	for( String s : entry.getValue() )
		// 	{
		// 		if( cleanText.contains(s) )
		// 		{
		// 			String tagged = tagString(s, entry.getKey());
		// 			cleanText = cleanText.replaceAll(s, tagged);
		// 			hasTag = true;
		// 		}
		// 		if( dirtyText.contains(s) )
		// 		{
		// 			String tagged = tagString(s, entry.getKey());
		// 			dirtyText = dirtyText.replaceAll(s, tagged);
		// 			hasTag = true;
		// 		}
		// 	}
		// }
		// cleanText = parseKeySets(cleanText);
		// String result = "<tr><td class=\"reviewID\">" + tokens[Review.REVIEW_ID.ordinal()] + "</td><td class=\"rating\">"  + tokens[Review.RATING.ordinal()] + "</td><td>" + dirtyText + "</td><td>" + cleanText + "</td></tr>";
		// if ( !hasTag )
		// {	UNTAGGED.add(result);	};
		// ANNOTATED_REVIEWS.add(result);
	}

	private static void findConcepts(String text)
	{
		Set<String> tokensToTag;
		for(Entry<String, Set<String>> entry : CONTENT_KEYS.entrySet())
		{
			for( String concept : entry.getValue() )
			{
				if( text.contains(concept) )
				{	addToTagMap(concept, entry.getKey());	}
			}
		}
	}

	private static void findSentiments(String text)
	{
		String result = text;
		text = text.replaceAll("\\.", "\\|\\|");
		String[] sentences = text.split("\\|\\|");
		String tag = "";
		int negCount = 0;
		int posCount = 0;
		Map<String,List<String>> tagValues = new HashMap<String, List<String>>();
		List<String> tokensToTag = new ArrayList<String>();
		for ( String sentence : sentences )
		{
			String[] tokens = sentence.split("\\ ");
			posCount = 0;
			negCount = 0;
			for ( String token : tokens )
			{
				if ( POS_WORDS.contains(token) )
				{
					posCount++;
					// addToTagMap(token, "posword");
				}
				if ( NEG_WORDS.contains(token) )
				{
					negCount++;
					// addToTagMap(token, "negword");
				}
			}

			if ( posCount > 0 || negCount > 0 )
			{
				if( posCount != negCount )
				{	tag = negCount > posCount ? "negline" : "posline";	}
				else
				{	tag = "neuline";	}
				addToTagMap(sentence, tag);
			}
		}
	}

	private static String annotateKeys(Review review)
	{
		String result = review.tokens[ReviewObjTokens.C_TEXT.ordinal()];
		for ( Entry<String, Set<String>> entry : TAG_MAP.entrySet())
		{
			for (String tok : entry.getValue() )
			{	result = result.replaceAll("\\b"+tok+"\\b", tagString(tok, entry.getKey()));	}
		}
		return result;
	}

	private static void addToTagMap(String token, String tag)
	{
		Set<String> values = TAG_MAP.containsKey(tag) ? TAG_MAP.get(tag) : new TreeSet<String>();
		values.add(token);
		TAG_MAP.put(tag, values);
	}

	private static String tagString(String word, String tag)
	{
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		S_BUILDER.append("<span class=\"");
		S_BUILDER.append(tag);
		S_BUILDER.append("\">");
		S_BUILDER.append(word);
		S_BUILDER.append("</span>");
		return  S_BUILDER.toString();
	}
}

class HTMLWriter
{
	String fileName;
	List<Review> data;

	public HTMLWriter(String fileName, List<Review> data)
	{
		this.fileName = fileName;
		this.data = data;
	}

	public void writeReport() throws IOException
	{
		writeHTMLHeader();
		writeHTML();
		writeHTMLFooter();
	}

	private void writeHTML() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
		try
		{
			for(Review r : data)
			{
				writer.write("<tr>");
				writer.newLine();
				writer.write(r.toHTML());
				writer.newLine();
				writer.write("</td>");
				writer.newLine();
			}
			writer.write("</table>");
			writer.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}

	private void writeHTMLHeader() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));
		try
		{
			writer.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			writer.newLine();
			writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
			writer.newLine();
			writer.write("<head>");
			writer.newLine();
			writer.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"./categories.css\">");
			writer.newLine();
			writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
			writer.newLine();
			writer.write("<title>Tagged Content</title>");
			writer.newLine();
			writer.write("</head>");
			writer.newLine();
			writer.write("<body>");
			writer.newLine();
			writer.write("<table>");
			writer.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}

	private void writeHTMLFooter() throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
		try
		{
			writer.write("</table>");
			writer.newLine();
			writer.write("</body>");
			writer.newLine();
			writer.write("</html>");
			writer.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	writer.close();	}
	}
}

class Review
{
	public String[] tokens;

	public Review (String[] input)
	{	this.tokens = input;	}

	public String toHTML()
	{
		String pre = "<td>";
		String post = "</td>";
		String result = "";
		for( String s : this.tokens )
		{
			if ( s.equals(null) )
			{	s = "";	}
			result += pre + s + post;
		}
		return result;
	}
}

class ReviewFactory
{
	public enum ReviewTokens
	{	APP_ID, APP_NAME, PRICE, RANK, REVIEW_ID, AUTHOR, DATE, VERSION, RATING, R_TITLE, R_BODY, R_TWC, R_BWC, C_TITLE, C_BODY, C_TWC, C_BWC	};

	// static EnumSet<ReviewTokens> WHITELIST_TOKENS;

	public ReviewFactory()
	{
		// this.WHITELIST_TOKENS = EnumSet.of(ReviewTokens.REVIEW_ID, ReviewTokens.RATING, ReviewTokens.R_TITLE, ReviewTokens.R_BODY, ReviewTokens.C_TITLE, ReviewTokens.C_BODY);
	}

	public static Review makeReview(String line, String delimiter)
	{
		String[] tokens = line.split(delimiter);
		String[] values = new String[4];
		values[0] = tokens[ReviewTokens.REVIEW_ID.ordinal()];
		values[1] = tokens[ReviewTokens.RATING.ordinal()];
		values[2] = tokens[ReviewTokens.R_TITLE.ordinal()] + " " + tokens[ReviewTokens.R_BODY.ordinal()];
		values[3] = tokens[ReviewTokens.C_TITLE.ordinal()] + " " + tokens[ReviewTokens.C_BODY.ordinal()];

		return new Review(values);
	}
}
