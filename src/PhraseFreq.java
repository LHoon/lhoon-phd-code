import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class PhraseFreq
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};
	final static int MAX_TOKENS = AppData.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	private static String IN_FILE = "";
	private static String CAT = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static TextCleaner CLEANER;
	private static PFIDFer PFIDFER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Provide review file for text frequency analysis.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		IN_FILE = args[0];
		CAT = IN_FILE.substring(3, IN_FILE.length());

		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, sU);
		PFIDFER = new PFIDFer(sU, true, 5, true);;
		findReviews(fileInPath(IN_FILE));

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static String fileInPath(String fileName)
	{	return String.format("./reviews/data/%s_reviews.csv", fileName);	}

	private static String fileOutPath(String fileName)
	{   return String.format("./reports/5phrase-freq/%s.tsv", fileName);	}

	private static void findReviews(String filePath) throws IOException
	{
		int reviews = 0;
		int index = 0;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String appId = TOKENS[AppData.APP_ID.ordinal()];
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			review = CLEANER.correct(review);
			PFIDFER.countPhrases(appId, review);
			reviews++;
			if ( reviews == 20000 )
			{
				DelimitedWriter writer = new DelimitedWriter(OUT_DELIMITER, fileOutPath("5pfidf-" + CAT + "-" + index));
				PFIDFER.writePFs(writer, 0);
				PFIDFER.clear();
				reviews = 0;
				index++;
			}
		}
		reader.close();
	}
}
