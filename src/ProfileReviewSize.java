import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class ProfileReviewSize
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum Sentiment
	{	POS, NEG, INC_POS, INC_NEG, NEUTRAL, SPAM	};

	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer LINES_MAPPED = 0;
	private static Integer MIN_REVIEW_COUNT = 50;
	private static Integer THRESHOLD = 10;
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";

	/** DICTIONARIES: English, Auxilliary */
	// Auxilliary dictionary containing jargon, product names, etc
	private static Set<String> AUX_DICTIONARY = new HashSet<String>();
	private static Set<String> POSITIVES = new HashSet<String>();
	private static Set<String> NEGATIVES = new HashSet<String>();
	private static Set<String> STOP_WORDS = new HashSet<String>();
	private static Set<String> NOISE_WORDS = new HashSet<String>();
	private static Set<String> TAG_WORDS = new HashSet<String>();

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;
	private static BufferedWriter STAT_WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static TextCleaner CLEANER;

	private static Map<String, String> APP_NAMES = new HashMap<String, String>();
	private static Map<String, Integer> TAG_MAP = new HashMap<String, Integer>();
	private static Map<String, Integer> REVIEWS_APP = new HashMap<String, Integer>();
	private static Map<String, Integer> TERM_MAP = new HashMap<String, Integer>();
	private static Map<String, Double[]> TEN_WORDS = new HashMap<String, Double[]>();
	private static Set<String> OTHER_REVIEWS = new HashSet<String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}

		CAT_PRICE = args[0];
		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, sU);

		loadReferenceData();
		loadAppNames();
		// checkReviewCountsPerApp();
		// writeReviewCounts();
		parseReviewTextFile();
		parseTermMap();
		// printReviewCounts();
		// System.out.println("Stat file columns: ");
		// System.out.println("APP_ID, APP_NAME, REVIEW_ID, RATING, RAW_COUNT, CLEAN_COUNT, POSITIVE_COUNT, NEGATIVE_COUNT, NOISE_COUNT, STOP_COUNT, TAG_COUNT, SIGNAL_COUNT");
		// reportResults();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CAT_PRICE + TAB + formatNumber(LINES_MAPPED) + TAB + formatNumber(REVIEW_COUNT) + TAB + "reviews stabilised of" + TAB + formatNumber(LINES_READ) + TAB +  "lines read, in " + timeTaken);
	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private static void loadReferenceData() throws Exception
	{
		// System.out.println("===================================");
		// System.out.println("Loading Reference Data...");
		// System.out.println("");
		String folder = "assets";
		// AUXILLIARY DICTIONARY
		loadDictionary(NOISE_WORDS, getFilePath(folder, "aux-dictionary", "txt"));
		// APP NAMES
		loadDictionary(NOISE_WORDS, getFilePath(folder, "app-name-words", "txt"));
		// ABBREVIATION/TXT SPK
		loadDictionary(NOISE_WORDS, getFilePath(folder, "abbrv-txt-spk", "csv"));
		// POSITIVE WORD LIST
		loadDictionary(POSITIVES, getFilePath(folder, "positive-words", "txt"));
		// NEGATIVE WORD LIST
		loadDictionary(NEGATIVES, getFilePath(folder, "negative-words", "txt"));
		// AUX POSITIVE WORD LIST
		loadDictionary(POSITIVES, getFilePath(folder, "aux-positive-words", "txt"));
		// AUX NEGATIVE WORD LIST
		loadDictionary(NEGATIVES, getFilePath(folder, "aux-negative-words", "txt"));
		// STOP WORD LIST
		loadDictionary(STOP_WORDS, getFilePath(folder, "mysql-stopwords", "txt"));
		// STOP WORD LIST
		loadDictionary(STOP_WORDS, getFilePath(folder, "aux-stopwords", "txt"));
		// TAG WORD LIST
		loadDictionary(TAG_WORDS, getFilePath(folder, "sorted-lhsbtags", "txt"));

		// reportReferenceData();
	}

	public static void reportReferenceData()
	{
		System.out.println("");
		System.out.println("Reference Data Size Check...");
		System.out.println("AUX_DICTIONARY: " + AUX_DICTIONARY.size());
		System.out.println("POSITIVES: " + POSITIVES.size());
		System.out.println("NEGATIVES: " + NEGATIVES.size());
		System.out.println("NOISE: " + NOISE_WORDS.size());
		System.out.println("STOPS: " + STOP_WORDS.size());
	}

	private static void loadAppNames() throws IOException
	{
		// System.out.println("===================================");
		// System.out.println("Loading App Names...");
		// System.out.println("");
		READER = new BufferedReader(new FileReader("./reviews/index/us_" + CAT_PRICE + ".csv"));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(PIPE);
			if( TOKENS.length != MAX_META_TOKENS ) {	continue;	}
			APP_NAMES.put(TOKENS[AppMeta.APP_ID.ordinal()], TOKENS[AppMeta.APP_NAME.ordinal()]);
		}
		READER.close();
		// System.out.println("./reviews/index/us_" + CAT_PRICE + ".csv" + " loaded.");

		// System.out.println("");
		// System.out.println("App Name Size Check...");
		// System.out.println("APP_NAMES: " + APP_NAMES.size());
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getFilePath(String folder, String fileName, String extension)
	{	return String.format("./%s/%s.%s", folder, fileName, extension);	}

	/**
	 * Populates the any auxilliary dictionaries based on filename param.
	 */
	private static void loadDictionary(Set<String> theSet, String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{	theSet.add(minorClean(LINE));	}
		READER.close();
		// System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private static void loadTypos(Map<String, String> theMap, String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(",");
			theMap.put(minorClean(TOKENS[0]), minorClean(TOKENS[1]));
		}
		READER.close();
		// System.out.println(fileName + " loaded.");
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Returns the first two characters of the String param. Failing that,
	 * returns "noprefix". SHOULD BE USED THROUGHOUT THIS SCRIPT FOR CONSISTENCY
	 * PERTAINING TO ENGLISH DICTIONARY CHECKS.
	 */
	private static String getPrefix(String word)
	{	return word.length() > 2 ? new String(word.substring(0,2)) : "noprefix";	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	private static boolean lineIsValid()
	{
		if ( TOKENS.length != MAX_DATA_TOKENS ) 		{	return false;	}
		if ( TOKENS[AppData.RATING.ordinal()].equals("1")
			|| TOKENS[AppData.RATING.ordinal()].equals("2")
			|| TOKENS[AppData.RATING.ordinal()].equals("3")
			|| TOKENS[AppData.RATING.ordinal()].equals("4")
			|| TOKENS[AppData.RATING.ordinal()].equals("5") )
		{	return true;	}
		else	{	return false;	}
	}

	private static void checkReviewCountsPerApp() throws IOException
	{
		READER = new BufferedReader(new FileReader("./reviews/data/us_" + CAT_PRICE + "_reviews.csv"));
		Integer count;
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(PIPE);
			if ( !lineIsValid() )
			{
				if ( !LINE.equals("") )	{	System.out.println("line read error: " + CAT_PRICE + "||" + LINE);	}
				continue;
			}
			count = REVIEWS_APP.containsKey(TOKENS[AppData.APP_ID.ordinal()]) ? REVIEWS_APP.get(TOKENS[AppData.APP_ID.ordinal()]) : 0;
			count++;
			REVIEWS_APP.put(TOKENS[AppData.APP_ID.ordinal()], count);
		}
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 *
	private static void parseReviewTextFile() throws IOException
	{
		// System.out.println("===================================");
		// System.out.println("Parsing App Reviews...");
		// System.out.println("");

		String rawReview = "";
		String cleanReview = "";
		String details = "";
		String[] cleanTokens;
		int rawCount = 0;
		int signalCount = 0;
		int posCount = 0;
		int negCount = 0;
		int tagCount = 0;
		int noiseCount = 0;
		int stopCount  = 0;
		int revSize = 0;
		Integer count = 0;
		double sentProportion = 0.0;
		double stopProportion = 0.0;
		double sentStopProportion = 0.0;
		try
		{
			READER = new BufferedReader(new FileReader("./reviews/data/us_" + CAT_PRICE + "_reviews.csv"));
			STAT_WRITER = new BufferedWriter(new FileWriter("./thesis/lang/" + CAT_PRICE + ".tsv"));
			// WRITER = new BufferedWriter(new FileWriter("./thesis/stabilised-reviews/stabilised-" + CAT_PRICE + ".tsv"));

			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				LINES_READ++;
				TOKENS = LINE.split(PIPE);
				if ( !lineIsValid() )
				{
					if ( !LINE.equals("") )	{	System.out.println("line read error: " + CAT_PRICE + "||" + LINE);	}
					continue;
				}

				rawReview = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
				rawCount = rawReview.split("\\ ").length;
				cleanReview = CLEANER.correct(rawReview);
				cleanTokens = cleanReview.split("\\ ");
				S_BUILDER.setLength(0);

				revSize = countSizeWithoutApp(cleanTokens);
				if ( revSize <= 10 )
				{
					for ( String word : cleanTokens )
					{
						if ( POSITIVES.contains(word) )		{	posCount++;		}
						if ( NEGATIVES.contains(word) )		{	negCount++;		}
						if ( STOP_WORDS.contains(word) )	{	stopCount++;	}
						if ( NOISE_WORDS.contains(word) )	{	noiseCount++;	}
						if ( !word.equals("app") )
						{
							count = TERM_MAP.containsKey(word) ? TERM_MAP.get(word) : 0;
							count++;
							TERM_MAP.put(word, count);
						}
					}

					// sentProportion = (double) (posCount + negCount + noiseCount) / revSize;
					// stopProportion = (double) ( stopCount + noiseCount ) / revSize;
					// sentStopProportion = (double) (stopCount + posCount + negCount + noiseCount) / revSize;
					// System.out.println(cleanReview + TAB + sentProportion + TAB + stopProportion + TAB + sentStopProportion);
				}
				// signalCount = cleanTokens.length - stopCount - noiseCount;
					// System.out.println(cleanReview);

				// S_BUILDER.setLength(0);
				// S_BUILDER.append(TOKENS[AppData.APP_ID.ordinal()]);
				// S_BUILDER.append(TAB);
				// S_BUILDER.append(APP_NAMES.get(TOKENS[AppData.APP_ID.ordinal()]));
				// S_BUILDER.append(TAB);
				// S_BUILDER.append(TOKENS[AppData.REVIEW_ID.ordinal()]);
				// S_BUILDER.append(TAB);
				// S_BUILDER.append(TOKENS[AppData.RATING.ordinal()]);
				// details = S_BUILDER.toString().trim();
				// S_BUILDER.append(TAB);
				// S_BUILDER.append(cleanReview);

				// WRITER.write(S_BUILDER.toString().trim());
				// WRITER.newLine();

				S_BUILDER.setLength(0);
				S_BUILDER.append(details);
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(rawCount));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(cleanTokens.length));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(posCount));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(negCount));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(noiseCount));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(stopCount));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(tagCount));
				S_BUILDER.append(TAB);
				S_BUILDER.append(String.valueOf(signalCount));

				STAT_WRITER.write(S_BUILDER.toString().trim());
				STAT_WRITER.newLine();

				if ( REVIEW_COUNT%25000 == 0 && REVIEW_COUNT > 1 )	{	System.err.println(formatNumber(REVIEW_COUNT) + " " + CAT_PRICE + " reviews processed...");	}

				REVIEW_COUNT++;
				posCount = 0;
				negCount = 0;
				tagCount = 0;
				stopCount = 0;
				noiseCount = 0;
				signalCount = 0;
			}
		}
		catch (IOException ioe)
		{	System.err.println("I/O Error: " + ioe.getMessage());	}
		finally
		{
			READER.close();
			STAT_WRITER.close();
		}
	}
	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseReviewTextFile() throws IOException
	{
		// System.out.println("===================================");
		// System.out.println("Parsing App Reviews...");
		// System.out.println("");

		String rawReview = "";
		String cleanReview = "";
		String details = "";
		String[] cleanTokens;
		int rawCount = 0;
		int signalCount = 0;
		int posCount = 0;
		int negCount = 0;
		int tagCount = 0;
		int noiseCount = 0;
		int stopCount  = 0;
		int revSize = 0;
		Integer count = 0;
		double sentProportion = 0.0;
		double stopProportion = 0.0;
		double sentStopProportion = 0.0;
		try
		{
			READER = new BufferedReader(new FileReader("./reviews/data/us_" + CAT_PRICE + "_reviews.csv"));
			// STAT_WRITER = new BufferedWriter(new FileWriter("./thesis/lang/" + CAT_PRICE + ".tsv"));

			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				LINES_READ++;
				TOKENS = LINE.split(PIPE);
				if ( !lineIsValid() )
				{
					if ( !LINE.equals("") )	{	System.out.println("line read error: " + CAT_PRICE + "||" + LINE);	}
					continue;
				}

				rawReview = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
				rawCount = rawReview.split("\\ ").length;
				cleanReview = CLEANER.correct(rawReview);
				cleanTokens = cleanReview.split("\\ ");
				S_BUILDER.setLength(0);

				revSize = countSizeWithoutApp(cleanTokens);
				// if ( revSize <= THRESHOLD )
				// {
					// LINES_MAPPED++;
					for ( String word : cleanTokens )
					{
						if ( POSITIVES.contains(word) )		{	posCount++;		}
						if ( NEGATIVES.contains(word) )		{	negCount++;		}
						if ( STOP_WORDS.contains(word) )	{	stopCount++;	}
						if ( NOISE_WORDS.contains(word) )	{	noiseCount++;	}
						// if ( !word.equals("app") )
						// {
							count = TERM_MAP.containsKey(word) ? TERM_MAP.get(word) : 0;
							count++;
							TERM_MAP.put(word, count);
						// }
					}

				// }
				posCount = 0;
				negCount = 0;
				tagCount = 0;
				stopCount = 0;
				noiseCount = 0;
				signalCount = 0;
			}
		}
		catch (IOException ioe)
		{	System.err.println("I/O Error: " + ioe.getMessage());	}
		finally
		{
			READER.close();
			// STAT_WRITER.close();
		}
	}

	private static void parseTermMap() throws IOException
	{
		int posCount = 0;
		int negCount = 0;
		int noiseCount = 0;
		int stopCount  = 0;
		int mapSize = TERM_MAP.size();
		double sentProportion = 0.0;
		double stopProportion = 0.0;
		double sentStopProportion = 0.0;
		String term = "";
		String tag = "";

		WRITER = new BufferedWriter(new FileWriter("./thesis/lang/" + CAT_PRICE + "-tf" + ".tsv"));

		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(TERM_MAP));
		resultMap.putAll(TERM_MAP);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			term = entry.getKey();
			if ( POSITIVES.contains(term) )			{	tag = "POS";		}
			else if ( NEGATIVES.contains(term) )	{	tag = "NEG";		}
			else if ( STOP_WORDS.contains(term) )	{	tag = "STOP";		}
			else if ( NOISE_WORDS.contains(term) )	{	tag = "NOISE";	}
			S_BUILDER.setLength(0);
			S_BUILDER.append(term);
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());
			// S_BUILDER.append(TAB);
			// S_BUILDER.append(tag);
			// S_BUILDER.append(TAB);
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}

		WRITER.close();
	}

	private static void reportProgress()
	{		}

	private static int countSizeWithoutApp(String[] tokens)
	{
		int result = 0;
		for ( String tok : tokens )
		{
			if ( !tok.toLowerCase().equals("app") )	{	result++;	}
		}
		return result;
	}

	/**
	 * Write to file the Map values.
	 */
	private static void writeFile(Map<String, ?> theMap, String fileName) throws IOException
	{
		// Map<String, ?> resultMap = new TreeMap<String, ?>(new ValueComparer<String, ?>(theMap));
		// // resultMap.putAll(theMap);

		for (Entry<String, ?> entry : theMap.entrySet())
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(getFilePath("reports", fileName, "csv")));
			bw.write(entry.getKey() + "," + entry.getValue());
			bw.newLine();
			bw.close();
		}
	}

	private static void printTags() throws IOException
	{
		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(TAG_MAP));
		resultMap.putAll(TAG_MAP);

		for (Entry<String, Integer> entry : resultMap.entrySet())
		{
			System.out.println(entry.getKey() + " > " + entry.getValue());
		}
	}

	private static void writeReviewCounts() throws IOException
	{
		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(REVIEWS_APP));
		resultMap.putAll(REVIEWS_APP);
		WRITER = new BufferedWriter(new FileWriter("./thesis/reports/" + CAT_PRICE + "-apps.tsv"));

		for (Entry<String, Integer> entry : resultMap.entrySet())
		{
			if ( entry.getValue() >= MIN_REVIEW_COUNT )
			{
				WRITER.write(entry.getKey() + TAB + entry.getValue());
				WRITER.newLine();
			}
		}
		WRITER.close();
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
