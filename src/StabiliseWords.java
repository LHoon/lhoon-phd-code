import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;
import java.util.TreeMap;

import uk.ac.shef.wit.simmetrics.similaritymetrics.JaroWinkler;
import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import util.TextFile;

public class StabiliseWords
{
	//javac -classpath ../lib/joda-time-2.1-sources.jar:../lib/joda-time.jar:../lib/jollyday-0.4.7-sources.jar:../lib/jollyday.jar:../lib/stanford-corenlp-1.3.4-javadoc.jar:../lib/stanford-corenlp-1.3.4-models.jar:../lib/stanford-corenlp-1.3.4-sources.jar:../lib/stanford-corenlp-1.3.4.jar:../lib/xom.jar:../lib/simmetrics_jar_v1_6_2_d07_02_07.jar:../lib/stanford-corenlp-1.3.4:../lib/vasa-utils.jar StabiliseWords.java
	//java -Xms1224m -Xmx1224m -classpath ../lib/joda-time-2.1-sources.jar:../lib/joda-time.jar:../lib/jollyday-0.4.7-sources.jar:../lib/jollyday.jar:../lib/stanford-corenlp-1.3.4-javadoc.jar:../lib/stanford-corenlp-1.3.4-models.jar:../lib/stanford-corenlp-1.3.4-sources.jar:../lib/stanford-corenlp-1.3.4.jar:../lib/xom.jar:../lib/simmetrics_jar_v1_6_2_d07_02_07.jar:../lib/stanford-corenlp-1.3.4:../lib/vasa-utils.jar:./ StabiliseWords ../app_reviews/cleaned_reviews/reviewText.csv

	public enum LookupType
    {	REPLACEMENTS, SPELL_CORRECT, LEMMATISE, UNDUPLICATE	};

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer RAW_WORD_COUNT = 0;
	private static Integer TREATED_WORD_COUNT = 0;

	// regex for valid text
	private static Pattern VALID_PATTERN = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+");
	private static Pattern DUPE_CHAR_PATTERN = Pattern.compile("(\\w)\\1+");
	// Lemmatiser
	private static StanfordLemmatizer stanLemma = new StanfordLemmatizer();
	// Norvig Speller
	private static Spelling NORVIG_SPELLER;

	/** DICTIONARIES: English, Auxilliary */
	// English dictionary
	private static Map<String, HashSet<String>> ENG_DICTIONARY = new HashMap<String, HashSet<String>>();
	// Auxilliary dictionary containing jargon, product names, etc
	private static Set<String> AUX_DICTIONARY = new HashSet<String>();

	/** Commonly typo-ed and truncated words/expressions */
	// Abbreviations/Txt Spk, commonly typoed words and supplementary typos based on manual analysis dictionary containing expansions
	private static Map<String, String> ABBRV_TYPOS = new HashMap<String, String>();

	/** Tracking and monitoring structures */
	// Map of odd words we encounter. Key = word, Val = Frequency
	private static Map<String, Integer> ODD_WORDS = new HashMap<String, Integer>();
	// Set to track untreated unique words
	private static Set<String> RAW_UNIQUE_WORDS = new HashSet<String>();
	// Set to track treated unique words
	private static Set<String> TREATED_UNIQUE_WORDS = new HashSet<String>();

	/** Output structures */
	// Set to track stabilised reviews
	private static Set<String> TREATED_REVIEWS = new HashSet<String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}

		loadReferenceData();
		parseReviewTextFile(args[0]);
		// reportResults();
		writeFile(TREATED_REVIEWS, "./reports/stabilisedReviewText");
		writeFile(ODD_WORDS, "./reports/oddWords");

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Text stabilisation on " + formatNumber(REVIEW_COUNT) + " reviews took " + timeTaken);
	}

	private static void printMap(Set<String> theSet)
	{
		int i = 0;
		for(String entry : theSet)
		{
			i++;
			System.out.println(entry);
			if(i == 50) break;
		}
	}
	private static void printList(List<String> theList)
	{
		int i = 0;
		for(String entry : theList)
		{
			i++;
			System.out.println(entry);
			if(i == 50) break;
		}
	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private static void loadReferenceData() throws Exception
	{
		// ENGLISH DICTIONARY
		loadDictionary(getFilePath("dictionary", "txt"));
		// AUXILLIARY DICTIONARY
		loadDictionary(AUX_DICTIONARY, getFilePath("aux-dictionary", "txt"));
		// APP NAMES
		loadDictionary(AUX_DICTIONARY, getFilePath("app-name-words", "txt"));
		// ABBREVIATION/TXT SPK
		loadTypos(ABBRV_TYPOS, getFilePath("abbrv-txt-spk", "csv"));
		// COMMONLY TYPO-ED WORDS
		loadTypos(ABBRV_TYPOS, getFilePath("common-typos", "csv"));
		// TRAINED TYPOS
		loadTypos(ABBRV_TYPOS, getFilePath("aux-common-typos", "csv"));
		// Norvig speller training corpus
		NORVIG_SPELLER = new Spelling(getFilePath("corpus", "txt"));
		System.out.println("===================================");
		System.out.println("Loading of Reference Data Complete.");
		System.out.println("===================================");
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getFilePath(String fileName, String extension)
	{	return String.format("./%s/%s.%s", "assets", fileName, extension);	}

	/**
	 * Populates the dictionary map based on the text file. The prefix of the
	 * word forms the key of the map, the word itself is added to the HashSet of
	 * values associated with that prefix. Makes parsing through the dictionary
	 * for string similarity jaroWinklerAlgos MUCH faster.
	 */
	private static void loadDictionary(String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for (String line : theFile)
		{
			line = minorClean(line);
			String prefix = getPrefix(line);
			HashSet<String> values = new HashSet<String>();

			if(ENG_DICTIONARY.containsKey(prefix))
			{   values = ENG_DICTIONARY.get(prefix);    }

			values.add(line);
			ENG_DICTIONARY.put(prefix, values);
		}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the any auxilliary dictionaries based on filename param.
	 */
	private static void loadDictionary(Set<String> theSet, String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for (String line : theFile)
		{	theSet.add(minorClean(line));	}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private static void loadTypos(Map<String, String> theMap, String fileName)
	{
		TextFile theFile = new TextFile(fileName);
		for(String line : theFile)
		{
			String[] tokens = line.split(",");
			theMap.put(minorClean(tokens[0]), minorClean(tokens[1]));
		}
		theFile.close();
		System.out.println(fileName + " loaded.");
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Returns the first two characters of the String param. Failing that,
	 * returns "noprefix". SHOULD BE USED THROUGHOUT THIS SCRIPT FOR CONSISTENCY
	 * PERTAINING TO ENGLISH DICTIONARY CHECKS.
	 */
	private static String getPrefix(String word)
	{
		if (word.length() > 2)
		{   return new String(word.substring(0,2));   }
		else
		{   return "noprefix";  }
	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Dictionary checker. Run the param word against our English Dictionary
	 * data structure and return true if the word is found. Failing that, check
	 * agaist the Auxilliary dictionary, return true if found, or false if not.
	 */
	private static boolean checkDictionary(String word)
	{
		HashSet<String> subDictionary = ENG_DICTIONARY.get(getPrefix(word));
		if(subDictionary != null && subDictionary.contains(word))
		{   return true; }
		return AUX_DICTIONARY.contains(word);
	}

	/**
	 * Check if the param word has been previously stabilised from our
	 * REPLACEMENT_WORDS map. If the word has been previously stabilised, return
	 * the stabilised word, else, return an empty list.
	 */
	private static List<String> checkReplacements(String word)
	{
		ArrayList<String> resultList = new ArrayList<String>();
		String replacements = ABBRV_TYPOS.get(word);
		if(replacements == null)
		{	replacements = word;	}
		resultList.addAll(Arrays.asList(replacements.split(" ")));
		return resultList;
	}

	/**
	 * Process the lemmatisation results for the word.
	 */
	private static List<String> getLemma(String word)
	{
		List<String> lemmaList = new ArrayList<String>();
		if(Pattern.matches("[a-zA-Z]+", word))
		{   lemmaList = stanLemma.lemmatize(word);  }

		if(!lemmaList.isEmpty())
		{
			for(String lemmaWord : lemmaList)
			{	lemmaWord = minorClean(lemmaWord);	}
		}
		return lemmaList;
	}

	private static String removeDuplicateChars(List<String> rawList)
	{
		String text = listToString(rawList);

		// Matcher matcher = DUPE_CHAR_PATTERN.matcher(text);
		// while (matcher.find())
		// {
		// 	String matcherVal = matcher.group();
		// 	if(matcherVal.length() > 2)
		// 	{
		// 		String trimmedWord = word;
		// 		int wordIdx =
		// 		int index = 0;
		// 		while (!checkDictionary(trimmedWord))
		// 		{
		// 			if(index >= 5)
		// 			{	break;	}
		// 			else
		// 			{	index++;	}
		// 			switch(index)
		// 			{
		// 				case 1: trimmedWord = trimmedWord.replaceAll(matcherVal, matcherVal.substring(0,2));
		// 						break;
		// 				case 2:	trimmedWord = NORVIG_SPELLER.correct(trimmedWord);
		// 						break;
		// 				case 3:	trimmedWord = trimmedWord.replaceAll(matcherVal, matcherVal.substring(0,1));
		// 						break;
		// 				case 4:	trimmedWord = NORVIG_SPELLER.correct(trimmedWord);
		// 						break;
		// 				default:break;
		// 			}
		// 			word = trimmedWord;
		// 		}
		// 	}
		// }
		return text;
	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	private static String stringTreatment(String review)
	{
		// review = review.replaceAll("\\-", "\\ ");
		review = review.replaceAll("\\`","\\'");

		review = review.replaceAll("\\'m", "\\ am");
		review = review.replaceAll("\\'\\ ", "\\ ");
		review = review.replaceAll("\\ s ","s ");
		review = review.replaceAll("\\'ll", "\\ will");
		review = review.replaceAll("\\'re", "\\ are");
		review = review.replaceAll("\\'d","\\ would");
		review = review.replaceAll("\\'ve","\\ have");

		// review = review.replaceAll("\\ ve","\\ have");
		// review = review.replaceAll("n[sd](')?n(')?t","\\ not");
		// review = review.replaceAll("oull", "ou\\ will");

		return minorClean(review);
	}

	private static String listToString(List<String> toBeStrung)
	{
		boolean wordAdded = false;
		StringBuilder sBuilder = new StringBuilder();
		for(String word : toBeStrung)
		{
			sBuilder.append(minorClean(word));
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }// Remove the trailing spaces

		return sBuilder.toString();
	}

	/**
	 *
	 */
	private static void StabiliseText(String rawText)
	{
		List<String> textList = deconstructText(rawText);

		RAW_WORD_COUNT += textList.size();

		textList = applyReplacementLookup(textList);
		textList = applySpellCorrection(textList);

		// textList = applyReplacementLookup(textList, LookupType.SPELL_CORRECT); // Norvig Speller
		// //textList = applyReplacementLookup(textList, LEMMATISE); // Lemmatise
		// textList = applyReplacementLookup(textList, LookupType.UNDUPLICATE); // Remove dupes

		TREATED_REVIEWS.add(reconstructText(textList));
		TREATED_WORD_COUNT += textList.size();
	}

	private static List<String> applyReplacementLookup(List<String> dirtyText, int lookupType)
	{
		List<String> workingResults = new ArrayList<String>();
		dirtyText = applyReplacementLookup(dirtyText);

		// switch(lookupType)
		// {
		// 	case LookupType.SPELL_CORRECT.ordinal():
		// 		for(String s : dirtyText)
		// 		{
		// 			if(!checkDictionary(s))
		// 			{	workingResults.add(NORVIG_SPELLER.correct(word));	}
		// 			else
		// 			{	workingResults.add(s);	}
		// 		}
		// 		break;
		// 	case LookupType.LEMMATISE:
		// 		workingResults = stanLemma.lemmatize(dirtyText);
		// 		break;
		// 	case LookupType.UNDUPLICATE:

		// 		workingResults = Arrays.asList(removeDuplicateChars(word));
		// 		break;
		// 	default:break;
		// }

		return workingResults;
	}

	private static List<String> applyReplacementLookup(List<String> textList)
	{
		List<String> results = new ArrayList<String>();

		for (String word : textList)
		{	results.addAll(checkReplacements(word));	}
		return results;
	}

	private static List<String> applySpellCorrection(List<String> textList)
	{
		List<String> results = new ArrayList<String>();

		for (String word : textList)
		{
			if(checkDictionary(word) || word.matches(".*\\d.*"))
			{	results.add(word);	}
			else
			{	results.add(NORVIG_SPELLER.correct(word));	}
		}
		return results;
	}

	private static List<String> appendToList(List<String> toAdd, List<String> toReturn)
	{
		for (String word : toAdd)
		{	toReturn.add(word);	}
		return toReturn;
	}

	private static List<String> deconstructText(String text)
	{
		ArrayList<String> resultList = new ArrayList<String>();
		Matcher matcher = VALID_PATTERN.matcher(text);
		while (matcher.find())
		{   resultList.add(minorClean(matcher.group()));  }
		return resultList;
	}

	private static String reconstructText(List<String> textList)
	{
		boolean wordAdded = false;
		StringBuilder sBuilder = new StringBuilder();
		for(String word : textList)
		{
			if(!checkDictionary(minorClean(word)))
			{
				if(!word.matches(".*\\d.*"))
				{	add(ODD_WORDS, word);	}
			}
			TREATED_WORD_COUNT++;
			TREATED_UNIQUE_WORDS.add(word);
			sBuilder.append(word);
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }// Remove the trailing spaces

		return sBuilder.toString();
	}

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	/**
	 * Parse the review file line by line, grab the review title and body and
	 * split it into tokens by spaces. From there, run it through the
	 * spellchecker.
	 */
	private static void parseReviewTextFile(String fileName)
	{
		TextFile tf = new TextFile(fileName);
		for (String line : tf)
		{
			StabiliseText(stringTreatment(line));
			REVIEW_COUNT++;
			if(REVIEW_COUNT%25000 == 0)
			{
				System.out.println(formatNumber(REVIEW_COUNT) + " reviews processed. "
					+ formatNumber(RAW_UNIQUE_WORDS.size()) + " raw unique words encountered; "
					+ formatNumber(TREATED_UNIQUE_WORDS.size()) + " treated unique words.");
			}
		}
		System.out.println(formatNumber(REVIEW_COUNT) + " reviews processed in total.");
		tf.close();
	}

	private static void reportResults()
	{
		DecimalFormat df2 = new DecimalFormat("###.##");
		double percentage = Double.valueOf(df2.format((double)ODD_WORDS.size()/TREATED_UNIQUE_WORDS.size() * 100));
		System.out.println(percentage + "% of " + formatNumber(TREATED_UNIQUE_WORDS.size()) + " are odd words.");

		int difference = TREATED_UNIQUE_WORDS.size() - RAW_UNIQUE_WORDS.size();
		percentage = Double.valueOf(df2.format((double) difference/RAW_UNIQUE_WORDS.size() * 100));
		System.out.println(percentage + "% expansion in words.");
	}

	/**
	 * Write to file the Set values.
	 */
	private static void writeFile(Set<String> theSet, String fileName)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName + ".txt"));
			for (String entry : theSet)
			{
				bw.write(entry);
				bw.newLine();
			}
			bw.close();
		} catch (IOException ioe)
		{   System.err.println("Error occurred while attempting to write " + fileName);}
		theSet.clear();
	}

	/**
	 * Write to file the Map values.
	 */
	private static void writeFile(Map<String, Integer> theMap, String fileName)
	{
		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(theMap));
		resultMap.putAll(theMap);

		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName + ".txt"));
			for (Entry<String, Integer> entry : resultMap.entrySet())
			{
				bw.write(String.format("%s,%d", entry.getKey(), entry.getValue()));
				bw.newLine();
			}
			bw.close();
		} catch (IOException ioe)
		{   System.err.println("Error occurred while attempting to write " + fileName);}
		theMap.clear();
		resultMap.clear();
	}
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}

class StanfordLemmatizer
{
	protected StanfordCoreNLP pipeline;

	public StanfordLemmatizer()
	{
		// Create StanfordCoreNLP object properties, with POS tagging
		// (required for lemmatization), and lemmatization
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");

		// StanfordCoreNLP loads a lot of models, so you probably
		// only want to do this once per execution
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatize(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);

		// run all Annotators on this text
		this.pipeline.annotate(document);

		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			// Iterate over all tokens in a sentence; Retrieve and add the
			// lemma for each word into the list of lemmas
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}
}
class Spelling
{

	private final HashMap<String, Integer> nWords = new HashMap<String, Integer>();

	public Spelling(String file) throws IOException
	{
		BufferedReader in = new BufferedReader(new FileReader(file));
		Pattern p = Pattern.compile("\\w+");
		for(String temp = ""; temp != null; temp = in.readLine())
		{
			Matcher m = p.matcher(temp.toLowerCase());
			while(m.find()) nWords.put((temp = m.group()), nWords.containsKey(temp) ? nWords.get(temp) + 1 : 1);
		}
		in.close();
	}

	private final ArrayList<String> edits(String word)
	{
		ArrayList<String> result = new ArrayList<String>();
		for(int i=0; i < word.length(); ++i) result.add(word.substring(0, i) + word.substring(i+1));
		for(int i=0; i < word.length()-1; ++i) result.add(word.substring(0, i) + word.substring(i+1, i+2) + word.substring(i, i+1) + word.substring(i+2));
		for(int i=0; i < word.length(); ++i) for(char c='a'; c <= 'z'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i+1));
		for(int i=0; i <= word.length(); ++i) for(char c='a'; c <= 'z'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i));
		return result;
	}

	public final String correct(String word)
	{
		if(nWords.containsKey(word)) return word;
		ArrayList<String> list = edits(word);
		HashMap<Integer, String> candidates = new HashMap<Integer, String>();
		for(String s : list) if(nWords.containsKey(s)) candidates.put(nWords.get(s),s);
		if(candidates.size() > 0) return candidates.get(Collections.max(candidates.keySet()));
		for(String s : list) for(String w : edits(s)) if(nWords.containsKey(w)) candidates.put(nWords.get(w),w);
		return candidates.size() > 0 ? candidates.get(Collections.max(candidates.keySet())) : word;
	}
}
