import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class TabDroidReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String IN_FOLDER = "./reviews/droid/data/";
	final static String OUT_FOLDER = "./dataset/droid/data/";
	final static String HEADER = "APP_ID	REVIEW_ID	REVIEW_DATE	APP_VERSION	TITLE_WORD_COUNT	TITLE_CHAR_COUNT	BODY_WORD_COUNT	BODY_CHAR_COUNT	RATING	AUTHOR_NAME	AUTHOR_ID	REVIEW_TITLE	REVIEW_BODY	#";
	final static boolean FREE = true;
	final static boolean PAID = false;

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;

	private static List<String> BAD_LIST = new ArrayList<String>();
	private static List<String> REPEAT_LIST = new ArrayList<String>();
	private static Set<String> CATEGORIES = new HashSet<String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./dataset/droid-categories.txt");
		readCategories();
		writeList("bad-lines", "txt", BAD_LIST, false);
		writeList("repeats", "csv", REPEAT_LIST, false);

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATEGORIES.size() + " categories parsed, with " + formatNumber(REVIEW_COUNT) + " reviews tabbed, of " + formatNumber(LINES_READ) +  " lines read with " + BAD_LIST.size() + " bad lines with " + REPEAT_LIST.size() + " repeats, in " + timeTaken);
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( !line.equals("") )
			{	CATEGORIES.add(minorClean(line));	}
		}
		reader.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void readCategories() throws IOException
	{
		for ( String category : CATEGORIES )
		{
			readReviews(getCatName(category, FREE));
			readReviews(getCatName(category, PAID));
		}
	}

	private static void readReviews(String fileName) throws IOException
	{
		boolean isOk;
		Set<String> reviewSet = new HashSet<String>();
		String[] tokens;
		String clean;
		BufferedReader reader = new BufferedReader(new FileReader(String.format("%sen_%s_reviews.csv", IN_FOLDER, fileName)));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( line.equals("") )	{	continue;	}

			tokens = line.split(PIPE);
			LINES_READ++;
			if ( tokens.length == MAX_DATA_TOKENS
				&& tokens[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				clean = line.replaceAll(PIPE, TAB);
				if ( clean.split(TAB).length == MAX_DATA_TOKENS )
				{
					isOk = true;
					for ( String s : clean.split(TAB) )
					{
						if ( s.length() == 0 )	{	isOk = false;	}
					}
					if ( isOk )
					{
						if ( reviewSet.contains(clean) )
						{	REPEAT_LIST.add(String.format("%s%s%s", fileName, TAB, clean));	}
						else
						{	reviewSet.add(clean);	}
						continue;
					}
				}
			}
			BAD_LIST.add(String.format("%s%s%s", fileName, TAB, line));
			continue;
		}
		reader.close();
		writeSet(fileName, "tsv", reviewSet, true);
	}

	private static void writeSet(String fileName, String extension, Set<String> reviews, boolean doCount) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
		writer.write(HEADER);
		writer.newLine();
		for ( String review : reviews )
		{
			if ( doCount )	{	REVIEW_COUNT++;	}
			writer.write(review);
			writer.newLine();
		}
		writer.close();
	}

	private static void writeList(String fileName, String extension, List<String> reviews, boolean doCount) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(String.format("%s%s.%s", OUT_FOLDER, fileName, extension)));
		for ( String review : reviews )
		{
			if ( doCount )	{	REVIEW_COUNT++;	}
			writer.write(review);
			writer.newLine();
		}
		writer.close();
	}
}
