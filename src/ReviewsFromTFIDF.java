import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.DelimitedWriter;
import hoon.textfx.TextCleaner;
import hoon.util.StringUtil;
import hoon.util.ValueComparer;

public class ReviewsFromTFIDF
{
	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static Map<String, Set<String>> ROOT_WORDS = new HashMap<String, Set<String>>();
	private static Map<String, Set<String>> REVIEWS = new HashMap<String, Set<String>>();
	private static Map<String, Integer[]> KEY_COUNTS = new HashMap<String, Integer[]>();
	private static Map<String, Integer> REVIEW_COUNTS = new HashMap<String, Integer>();
	private static Pattern WORDS;

	private static TextCleaner CLEANER;
	private static StringUtil S_UTIL;
	private static BufferedReader READER;
	private static DelimitedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Provide review file for text frequency analysis.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT_PRICE = args[0];
		OUTFILE = "10idf-based-" + CAT_PRICE.substring(3, CAT_PRICE.length());
		WORDS = Pattern.compile("[A-Za-z]+");

		S_UTIL = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, S_UTIL);

		loadRoots("./reports/lemma-tfidf-10-freq-free_health.tsv");

		for(Entry<String, Set<String>> entry : ROOT_WORDS.entrySet())
		{
			// System.out.println(entry.getValue());
		}
		findReviews(getPath(CAT_PRICE, DATA));//"./reviews/alls/us_free_all_reviews.csv");
		writeReviews();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static void writeReviews() throws IOException
	{
		DelimitedWriter WRITER = new DelimitedWriter(OUT_DELIMITER, "./reports/"+OUTFILE+".tsv");
		// for(Entry<String, Integer[]> entry : KEY_COUNTS.entrySet())
		// {
		// 	TOKENS = new String[8];
		// 	TOKENS[0] = entry.getKey();
		// 	for ( int i = 0; i < entry.getValue().length; i++ )
		// 	{	TOKENS[i+1] = entry.getValue()[i].toString();	}
		// 	TOKENS[TOKENS.length-1] = REVIEW_COUNTS.get(entry.getKey()).toString();
		// 	WRITER.writeLine(S_UTIL.delimitTokens(TOKENS, OUT_DELIMITER));
		// }
		for(Entry<String, Set<String>> appReviews : REVIEWS.entrySet())
		{
			for ( String review : appReviews.getValue() )
			{	WRITER.writeLine(review);	}
		}
	}

	private static void findReviews(String filePath) throws IOException
	{
		String appId = "";
		String review = "";
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try	{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)	{	continue;	}

			appId = TOKENS[AppData.APP_ID.ordinal()];
			review = CLEANER.correct(TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()]);
			Integer count = REVIEW_COUNTS.containsKey(appId) ? REVIEW_COUNTS.get(appId) : new Integer(0);
			count++;
			REVIEW_COUNTS.put(appId, count);
			String key = hasRoots(appId, review);
			if ( !key.equals("") )
			{
				saveReview(appId, formatReview(TOKENS, key), rating);
			}
		}
		READER.close();
	}

	private static String formatReview(String[] tokens, String key)
	{
		String[] result = new String[5];
		result[0] = tokens[AppData.APP_ID.ordinal()];
		result[1] = tokens[AppData.REVIEW_ID.ordinal()];
		result[2] = tokens[AppData.RATING.ordinal()];
		result[3] = tokens[AppData.REVIEW_TITLE.ordinal()] + " " + tokens[AppData.REVIEW_BODY.ordinal()];
		result[4] = key;
		return S_UTIL.delimitTokens(result, OUT_DELIMITER);
	}

	private static String hasRoots(String appId, String review)
	{
		Matcher m = WORDS.matcher(review);
		String group;
		Set<String> rootSet = ROOT_WORDS.get(appId);
		while (m.find())
		{
			group = m.group();
			if(rootSet.contains(group))
			{	return group;	}
		}
		return "";
	}

	private static void loadRoots(String filePath) throws IOException
	{
		String boundary = "\\b";
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(OUT_DELIMITER);
			if( TOKENS.length != 3 || TOKENS[1].length() < 2 )	{	continue;	}
			Set<String> tokenSet = ROOT_WORDS.containsKey(TOKENS[0]) ? ROOT_WORDS.get(TOKENS[0]) : new HashSet<String>();
			tokenSet.add(TOKENS[1]);
			ROOT_WORDS.put(TOKENS[0], tokenSet);
		}
		READER.close();
	}

	private static void saveReview(String appId, String review, int rating)
	{
		Set<String> appReviews = REVIEWS.containsKey(appId) ? REVIEWS.get(appId) : new HashSet<String>();
		appReviews.add(review);
		REVIEWS.put(appId, appReviews);

		Integer[] counts = KEY_COUNTS.containsKey(appId) ? KEY_COUNTS.get(appId) : new Integer[]{0,0,0,0,0,0};
		counts[rating-1]++;
		counts[counts.length-1]++;
		KEY_COUNTS.put(appId, counts);
	}

	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}
}
