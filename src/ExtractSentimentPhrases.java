import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.concurrent.TimeUnit;
import java.util.Locale;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

// import flanagan.control.FirstOrder;
// import flanagan.analysis.Stat;

public class ExtractSentimentPhrases
{
	public enum AppData
	{   APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY  };
	public enum AppMeta
	{   APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK    };
	public enum Classification
	{   POSITIVE, NEGATIVE, INCONSISTENT_POS, INCONSISTENT_NEG, NEUTRAL, SPAM   };
	public enum Polarity
	{   POS, NEG, NEU   };
	public enum Ontology
	{	EMOT, FUNC, QUAL};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean TXT = true;
	final static boolean CSV = false;
	// Folder Paths
	final static boolean DATA = true;
	final static boolean INDEX = false;
	// Pattern.compile or as a String
	final static boolean PATTERN = true;
	final static boolean STRING = false;
	// Nest the Map with Sets or <String, String> only
	final static boolean NEST = true;
	final static boolean FLAT = false;

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Manipulator Objects
	private static Matcher THE_MATCHER;
	private static DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("MMM dd, yyyy");
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static DecimalFormat DF2 = new DecimalFormat("###.##");

	// Working Collections
	private static Integer[] COUNTS;
	private static String[] TOKENS;
	private static Map<String, DateTime> VERSIONS = new TreeMap<String, DateTime>();
	private static Set<String> WORD_SET;

	// Working Memory Objects
	private static DateTime DATE = new DateTime();
	private static long START = System.currentTimeMillis();
	private static String CAT_PRICE = "";
	private static String KEY_ID = "";
	private static String KEY_NAME = "";
	private static String LINE = "";
	private static Pattern REVIEW_PATTERN;
	private static int THRESHOLD = 10;

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/* Search Space Patterns */
	private static Pattern POSITIVE_SENTIMENTS;
	private static Pattern NEGATIVE_SENTIMENTS;
	private static Pattern NOISE_WORDS;

	private static Pattern EMOTIONAL_WORDS;
	private static Pattern FUNCTIONALITY_WORDS;
	private static Pattern QUALITY_WORDS;

	/* English and Jargon references */
	// English Language Reference
	private static Map<String, Set<String>> WORD_DICT = new TreeMap<String, Set<String>>();
	private static Map<String, String> ABBRV_TYPOS = new TreeMap<String, String>();

	// Patterns to cleanse text of non-alphanumerics
	private static Pattern ALPHA_NUMERICS = Pattern.compile("[0-9]+?|[\\w\\d/]+");

	/* Results: Datastructures to house the results of processing */
	// Sentiment Distributions
	private static Map<Classification, Integer> SENTIMENT_DIST = new HashMap<Classification, Integer>();
	private static Map<String, Integer[]> VERSIONED_SENT = new TreeMap<String, Integer[]>();
	private static Map<Classification, Map<String, Integer>> SENTIMENT_FREQ = new HashMap<Classification, Map<String, Integer>>();
	// private static Map<Integer, List<String>> NESTED_MAP;
	private static Map<String, Integer> WORD_MAP;
	private static Set<String> REVIEWS = new HashSet<String>();
	private static Map<Ontology, Set<String>> ONTOLOGY_PHRASES = new HashMap<Ontology, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("Insufficient args provided for Review Analysis Demo.");
			System.exit(1);
		}

		/* Review Extraction */
		// Load params into memory for quick reference
		CAT_PRICE = args[0];
		KEY_ID = args[1];
		KEY_NAME = getAppName(reviewPath(CAT_PRICE, INDEX));
		System.out.println("Searching for reviews pertaining to " + KEY_NAME);

		// Execute Review Extraction
		findReviews(reviewPath(CAT_PRICE, DATA));
		System.out.println(formatNumber(REVIEWS.size()) + " reviews for " + KEY_NAME + " extracted over "+ VERSIONS.size() + " versions.");
		// for(Entry<String, DateTime> entry : VERSIONS.entrySet())
		// {   System.out.println(entry.getKey()+" "+entry.getValue().toString("MMM dd, yyyy"));   }

		// Calculate and Report Processing Time
		reportTime("Review Extraction");

		/* Sentiment Analysis */
		// Load asset files into memory for lookups
		System.out.print("Loading... ");
		loadAssets();
		generateStructures();
		System.out.println("Commencing Classification.");

		// Execute Sentiment Classification
		parseReviews();
		String fileName = CAT_PRICE.substring(3) + "-" + KEY_NAME.replaceAll(" ", "-");
		// reportOntologies();
		findSentiments(Ontology.FUNC);
		// findSentiments(Ontology.EMOT);
		// findSentiments(Ontology.QUAL);

		// writeSentimentDistribution(fileName);

		// Calculate and Report Processing Time
		reportTime("Sentiment Classification");
	}

	/************************* UTILITY METHODS *************************/


	/**
	 *  Startup method to load up assets into memory.
	 */
	private static void loadAssets() throws IOException
	{
		// Pattern Sets, hence true parameter
		String sentiment = populateWordList(assetPath("positive-words", TXT));
		POSITIVE_SENTIMENTS = generatePattern(sentiment);
		sentiment = populateWordList(assetPath("negative-words", TXT)) + populateWordList(assetPath("aux-negative-words", TXT));
		NEGATIVE_SENTIMENTS = generatePattern(sentiment);
		sentiment = populateWordList(assetPath("conjunctions-determiners", TXT));
		NOISE_WORDS = generatePattern(sentiment);

		// Ontology Words
		String ont = populateWordList(assetPath("ont-emo", TXT));
		EMOTIONAL_WORDS = generatePattern(ont);
		ont = populateWordList(assetPath("ont-fun", TXT));
		FUNCTIONALITY_WORDS = generatePattern(ont);
		ont = populateWordList(assetPath("ont-qua", TXT));
		QUALITY_WORDS = generatePattern(ont);

		// Dictionary of known English words and Jargon; true = nested Map
		loadMap(WORD_DICT, assetPath("dictionary",TXT), NEST);
		loadMap(WORD_DICT, assetPath("aux-dictionary",TXT), NEST);
		// Abbreviations/Txt Speak, Common Typos, Trained Typos
		loadMap(ABBRV_TYPOS, assetPath("abbrv-txt-spk",CSV), FLAT);
		loadMap(ABBRV_TYPOS, assetPath("common-typos",CSV), FLAT);
		loadMap(ABBRV_TYPOS, assetPath("aux-common-typos",CSV), FLAT);
		System.out.print("Assets Loaded. ");
	}

	/**
	 * Method to calculate time taken for processing and prints to console.
	 * @param   type String value of the processing type to report
	 */
	private static void reportTime(String type)
	{
		long millis = System.currentTimeMillis() - START;
		String elapsed = String.format("%d min, %d sec",
						TimeUnit.MILLISECONDS.toMinutes(millis),
						TimeUnit.MILLISECONDS.toSeconds(millis) -
						TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println(type + " completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param   isData boolean value to indicate review or index file
	 * @param   fileName name of the file to be loaded, WITHOUT extension
	 * @return  String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{   return String.format("./reviews/%s/%s.csv", folder , fileName); }

	/**
	 * Generate filepath for asset files.
	 * @param   fileName name of the file to be loaded, WITHOUT extension
	 * @param   isTxt true for csv files, false for txt
	 * @return  String value constructed from ./folder/fileName.extension
	 */
	private static String assetPath(String fileName, boolean isTxt)
	{   return String.format("./assets/%s.%s", fileName, isTxt ? "txt" : "csv" );   }

	/**
	 * Generate filepath for report files.
	 * @param   fileName name of the file to be loaded, WITHOUT extension
	 * @param   isTxt true for csv files, false for txt
	 * @return  String value constructed from ./folder/fileName.extension
	 */
	private static String reportPath(String fileName, boolean isTxt)
	{   return String.format("./reports/%s.%s", fileName, isTxt ? "txt" : "csv" );  }

	/**
	 * Generate filepath for review files.
	 * @param   fileName name of the file to be loaded, WITHOUT extension
	 * @param   isData boolean value to indicate review or index file
	 * @return  String value constructed from ./folder/fileName.extension
	 */
	private static String reviewPath(String fileName, boolean isData)
	{   return String.format("./reviews/%s/%s.csv", isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));   }

	/**
	 * Generate filepath for WIP files.
	 * @param   fileName name of the file
	 * @return  String value constructed from ./wip/fileName.csv
	 */
	private static String wipPath(String fileName)
	{   return String.format("./wip/%s.csv", fileName); }

	/**
	 *  Minor String preparation, trims trailing spaces, lowercases
	 *  and enforces Locale.ENGLISH prior to returning it.
	 * @param   text String value to be processed
	 * @param   asPattern true to treat it as a Pattern, false for no
	 * @return  String value constructed from param, LowerCased and trimmed
	 */
	private static String prepareString(String text, boolean asPattern)
	{   return asPattern ? "\\b" + text.toLowerCase(Locale.ENGLISH).trim() + "\\b" : text.toLowerCase(Locale.ENGLISH).trim();   }

	/**
	 * Splits the param rawText into alphanumeric characters only and returns it
	 * as a new String. Might cause issues with memory overhead.
	 * @param   rawText raw text to be processed
	 * @return  String result of the processed text
	 */
	private static String stabiliseText(String rawText)
	{
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }
		rawText = rawText.replaceAll("'", "");
		THE_MATCHER = ALPHA_NUMERICS.matcher(rawText);
		String word;
		while (THE_MATCHER.find())
		{
			word = ABBRV_TYPOS.containsKey(THE_MATCHER.group())
					? ABBRV_TYPOS.get(THE_MATCHER.group()) : THE_MATCHER.group();
			S_BUILDER.append(prepareString(word, STRING));
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}

	/**
	 * Returns the first two characters of the String param. Failing that,
	 * returns "noprefix". SHOULD BE USED THROUGHOUT THIS SCRIPT FOR CONSISTENT
	 * ENGLISH DICTIONARY CHECK BEHAVIOUR.
	 * @param   text String value from which the prefix is extracted
	 * @return  String value of the first two characters, OR noprefix
	 */
	private static String getPrefix(String text)
	{   return text.length() > 3 ? new String(text.substring(0,3)) : "noprefix";    }

	/**
	 * Number prettification, 300000 becomes 300,000.
	 * @param   value int value of the number to be prettified
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Method to search the index file for the App Name based on the ID param.
	 * @param   filePath String value of the index file to search
	 * @return  String value of the app name matching the param ID
	 */
	private static String getAppName(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split("\\|\\|");
			if( TOKENS.length < 9 ) {   continue;   }
			if ( TOKENS[AppMeta.APP_ID.ordinal()].equals(KEY_ID) )
			{   return TOKENS[AppMeta.APP_NAME.ordinal()];  }
		}
		READER.close();
		return "";
	}

	/**
	 * Calculates the percentage of the first param from the second param and
	 * formats it to at most 2 decimal places.
	 * @param   amount first int value from which the percentage is calculated
	 * @param   sum the second int value that the first param is divided by
	 * @return  double result formatted to two decimal places
	 */
	private static double getProportion(int amount, int sum)
	{   return Double.valueOf(DF2.format((double) amount/sum * 100));   }

	/************************* FILE I/O METHODS *************************/

	/**
	 * Method to write the SENTIMENT_DIST proportions to file.
	 * @param   fileName the concatenated category, price and app name
	 */
	private static void writeSentimentDistribution(String fileName) throws IOException
	{
		int rawCount = 0;
		int sumOfReviews = 0;
		int verCount = VERSIONED_SENT.size();
		int verSum = 0;

		for ( Entry<Classification, Integer> entry :  SENTIMENT_DIST.entrySet() )
		{   sumOfReviews += entry.getValue();   }
		try
		{
			WRITER = new BufferedWriter(new FileWriter(reportPath(fileName, TXT), true));
			WRITER.write("Polarity Distributions of " + sumOfReviews + " reviews for " + KEY_NAME + ":");
			WRITER.newLine();
			for ( Classification polarity : Classification.values() )
			{
				rawCount = SENTIMENT_DIST.containsKey(polarity) ? SENTIMENT_DIST.get(polarity) : 0;
				WRITER.write(polarity + ": " + getProportion(rawCount, sumOfReviews) + "% (" + rawCount + ")");
				WRITER.newLine();
			}

			WRITER.newLine();
			WRITER.write("VERSIONED DISTRIBUTIONS:");
			WRITER.newLine();
			for ( Entry<String, Integer[]> entry : VERSIONED_SENT.entrySet() )
			{
				WRITER.newLine();
				WRITER.write("VERSION " + entry.getKey());
				WRITER.newLine();
				COUNTS = entry.getValue();
				verSum = 0;
				for ( Integer i : COUNTS )
				{   verSum += i.intValue(); }
				for ( Classification polarity : Classification.values() )
				{
					WRITER.write(polarity + ": " + getProportion(COUNTS[polarity.ordinal()].intValue(), verSum) + "% (" + COUNTS[polarity.ordinal()].intValue() + ")");
					WRITER.newLine();
				}
			}
		}
		catch (IOException ioe) {   ioe.printStackTrace();  }
		finally {   WRITER.close(); }
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param   filePath name of the file to append to
	 * @param   line String value to write to file
	 */
	private static void writeLine(String filePath, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe) {   ioe.printStackTrace();  }
		finally {   WRITER.close(); }
	}

	/************************* REVIEW EXTRACTION METHODS *************************/

	private static void determineDate(String version, String dateString)
	{
		try
		{
			DATE = DateTime.parse(dateString, DATE_FORMATTER);
			DateTime value = VERSIONS.containsKey(version) ? VERSIONS.get(version) : new DateTime();
			if ( DATE.isAfter(value) )
			{   DATE = value;   }
			VERSIONS.put(version,DATE);
		} catch (Exception e) { e.printStackTrace();    }
	}

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split("\\|\\|");
			if( TOKENS.length < 13 ) {  continue;   }
			if ( TOKENS[AppData.APP_ID.ordinal()].equals(KEY_ID) )
			{
				determineDate(TOKENS[AppData.APP_VERSION.ordinal()], TOKENS[AppData.REVIEW_DATE.ordinal()]);
				REVIEWS.add(LINE);
			}
		}
		READER.close();
	}

	/************************* UNIGRAM EXTRACTION METHODS *************************/

    /**
     * Method to extract N Gram phrases out of the line of text based on the indexOfKeyword provided and THRESHOLD for depth.
     * If THRESHOLD = 3, the phrases 1, 2, 3, Key; 1, 2, Key, 3; 1, Key, 2, 3; Key, 1, 2, 3 are extracted.
     * Phrases with only one word (i.e. only the keyword) are ignored.
     * @param 	tokens text block source which has be split by space into a String[]
     * @param 	indexOfKeyWord int index of the Key word
     * @param 	ontType Ontology type that this phrase corresponds to
     */
    private static void extractNGram(String[] tokens, int indexOfKeyWord, Ontology ontType)
    {
    	int tokenLength = tokens.length;
        StringBuilder sBuilder = new StringBuilder();

    	for(int i = 0; i <= THRESHOLD; i++)
    	{
    		int currentOffset = indexOfKeyWord - i;
    		// Loop through each permutation of results
    		for (int j = 0; j <= THRESHOLD; j++)
    		{
    			int currentIdx = currentOffset + j;
    			// Loop through each word
    			if (currentIdx >= 0 && currentIdx < tokenLength)
    			{
    				sBuilder.append(tokens[currentIdx].toString().replaceAll("[^\\p{L}\\p{N}]", "").toLowerCase(Locale.ENGLISH));
                    sBuilder.append(" ");
    			}
    		}
            // Remove the trailing space
            sBuilder.deleteCharAt(sBuilder.length() - 1);
    		// Store the phrase in the map if there are at least two words in the phrase
    		if(sBuilder.toString().matches(".*\\s+.*"))
    		{
                String treatedPhrase = sBuilder.toString().trim();
                addToSet(ontType, treatedPhrase);
// System.err.println(treatedPhrase);
            }
            sBuilder.delete(0, sBuilder.length());
        }
    }

	/************************* ONTOLOGY CLASSIFICATION METHODS *************************/

	private static void extractOntologyPhrases(String text, Ontology ontType, Pattern ontPattern)
	{
		text = new StringBuilder(text.trim()).insert(0," ").toString();
		String[] tokens = text.split(" ");
		int wordIndex = 0;

		for( String word : tokens )
		{
			if(ontPattern.matcher(" " + word + " ").find())
			{	extractNGram(tokens, wordIndex, ontType);	}
			wordIndex++;
		}
	}

	private static void addToSet(Ontology ontType, String phrase)
	{
		Set<String> theSet = ONTOLOGY_PHRASES.containsKey(ontType) ? ONTOLOGY_PHRASES.get(ontType) : new HashSet<String>();
		theSet.add(phrase);
		ONTOLOGY_PHRASES.put(ontType, theSet);
	}

	/************************* SENTIMENT CLASSIFICATION METHODS *************************/

	private static void findSentiments(Ontology ontType)
	{
		Set<String> theSet = ONTOLOGY_PHRASES.containsKey(ontType) ? ONTOLOGY_PHRASES.get(ontType) : new HashSet<String>();
		Set<String> results = new HashSet<String>();
		for( String s : theSet)
		{
			if( POSITIVE_SENTIMENTS.matcher(" " + s + " ").find() )
			results.add(s);
		}
		try
		{
			BufferedWriter bfw = new BufferedWriter(new FileWriter("./reports/pos-ont" + KEY_NAME.replaceAll(" ", "-") + ".txt", true));
			for(String s : results)
			{
				bfw.write(s);
				bfw.newLine();
			}
			bfw.close();
		} catch (Exception e) { e.printStackTrace();    }
	}

	/**
	 *  Startup method to generate the SENTIMENT_FREQ structure.
	 */
	private static void generateStructures()
	{
		for ( Classification polarity : Classification.values() )
		{
			Map<String, Integer> nestedMap = new HashMap<String, Integer>();
			SENTIMENT_FREQ.put(polarity, nestedMap);
		}

		System.out.print("Structures Generated. ");
	}

	@SuppressWarnings("unchecked")
	/**
	 * Populates the dictionary Map based on the text file. The prefix of the
	 * word forms the key of the map, the word itself is added to the HashSet of
	 * values associated with that prefix. Makes parsing through the dictionary
	 * for string similarity jaroWinklerAlgos MUCH faster.
	 * @param   theMap datastructure for the values to be added into
	 * @param   filepath filePath of the input resource to add to the Map
	 * @param   isNested true to treat it as a nested Map (with prefixes)
	 */
	private static void loadMap(Map theMap, String fileName, boolean isNested) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		String mapKey;
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			LINE = prepareString(LINE, STRING);
			if ( !isNested )
			{
				TOKENS = LINE.split(",");
				if(TOKENS.length == 2)
				{   theMap.put(TOKENS[0], TOKENS[1]);   }
			}
			else
			{
				mapKey = getPrefix(LINE);
				WORD_SET = !theMap.containsKey(mapKey) ? new TreeSet<String>() : (TreeSet<String>) theMap.get(mapKey);
				WORD_SET.add(LINE);
				theMap.put(mapKey, WORD_SET);
			}
		}
		READER.close();
	}
	/**
	 *  Generic method to load a input file word list into a param Set as
	 *  compiled regex Patterns or raw Strings, dependent on boolean param.
	 * @param   filepath filePath of the input resource to add to the Set
	 * @return  String value of the search space
	 */
	private static String populateWordList(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			S_BUILDER.append(prepareString(LINE, STRING));
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}

	/**
	 * Method to generate a Pattern from a text block. Output is in parentheses,
	 * with \b boundaries pre and post each word, and | pipes separating the
	 * words.
	 * @param   text String value to be processed into a Pattern
	 * @return  Pattern object developed from the String param
	 */
	private static Pattern generatePattern(String text)
	{
		boolean firstLine = true;
		String[] tokens = text.split(" ");
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }
		S_BUILDER.append("(");
		for ( String word : tokens )
		{
			if(!firstLine)
			{   S_BUILDER.append("|");  }
			S_BUILDER.append(prepareString(word, PATTERN));
			firstLine = false;
		}
		S_BUILDER.append(")");
		// System.out.println(S_BUILDER.toString());
		return Pattern.compile(S_BUILDER.toString());
	}

	/**
	 * Iterate through all the Words and detect matches. Return the number of
	 * matches found in the String param provided.
	 * @param   text perform the Match on
	 * @return  the number of matches
	 */
	private static int wordMatcher(String text)
	{
		int matches = 0;
		TOKENS = text.split(" ");
		for ( String word : TOKENS)
		{
			matches += ( WORD_DICT.containsKey(getPrefix(word))
						&& WORD_DICT.get(getPrefix(word)).contains(word) )
						? 1 : 0;
		}
		return matches;
	}

	/**
	 * Iterate through all the Patterns and detect matches. Return the number of
	 * matches found in the String param provided.
	 * @param   m the matcher object to iterate through to check against
	 * @param   text that has been compiled as a Pattern to perform the Match on
	 * @return  the number of matches
	 */
	private static int countMatches(Matcher m)
	{
		int matches = 0;//System.out.println();
		while ( m.find() )
		{   matches++;  }
		return matches;
	}

	/**
	 * Method to count the number of each type of sentiment. As it stands, there
	 * are only positive, negative or noise type sentiments. Index 0 = Positive,
	 * Index 1 = Negative, Index 2 = everything else.
	 * @param   text perform the counting on
	 * @return  int array of positive, negative and noise word counts
	 */
	private static int[] countSentiments(String text)
	{
		int[] result = {0,0,0};
		//REVIEW_PATTERN = generatePattern(text);
		// System.out.println(text);System.out.println("===POS===");
		result[0] = countMatches(POSITIVE_SENTIMENTS.matcher(text));
		// System.out.println("===NEG===");
		result[1] = countMatches(NEGATIVE_SENTIMENTS.matcher(text));
		// System.out.println("===NOISE===");
		result[2] = countMatches(NOISE_WORDS.matcher(text));
		// System.out.println("===DONE===");
		return result;
	}

	/**
	 * Method to count the number of words and their types. As it stands, there
	 * are only English/Jargon/App Name words, depending on the dictionaries
	 * loaded at startup. Index 0 = Total # of words in the text param,
	 * Index 1 = English/Jargon words, Index 2 = Unidentified words.
	 * @param   text perform the counting on
	 * @return  int array of total, english and unidentified word counts
	 */
	private static Integer[] countWords(String text)
	{
		COUNTS = new Integer[]{0,0,0};
		COUNTS[0] = text.split(" ").length; // # words in total
		COUNTS[1] = wordMatcher(text);      // # English or Jargon
		COUNTS[2] = COUNTS[0] - COUNTS[1];  // # unindentified words
		return COUNTS;
	}

	/**
	 * Increments the occurances in the SENTIMENT_FREQ, SENTIMENT_DIST and VERSIONED_SENT Maps.
	 * Classification is the Key, number of occurances is the Value.
	 * @param   polarity Classification Key to increment in SENTIMENT_FREQ
	 * @param   text the String of text to be tokenised and counted
	 * @param   version String value of version associated with this text
	 */
	private static void addToStructures(Classification polarity, String text, String version)
	{
		incrementInMap(polarity);
		incrementWordsInPolarity(polarity, text);
		COUNTS = VERSIONED_SENT.containsKey(version) ? VERSIONED_SENT.get(version) : new Integer[]{0,0,0,0,0,0};
		COUNTS[polarity.ordinal()] += 1;
		VERSIONED_SENT.put(version, COUNTS);
	}

	/**
	 * Increments the occurances in the SENTIMENT_DIST Map.
	 * Classification is the Key, number of occurances is the Value.
	 * @param   polarity Classification Key to increment in SENTIMENT_DIST
	 */
	private static void incrementInMap(Classification polarity)
	{
		int frequency = SENTIMENT_DIST.containsKey(polarity)
						? (SENTIMENT_DIST.get(polarity) + 1) : 1;
		SENTIMENT_DIST.put(polarity, frequency);
	}

	/**
	 * Increments the occurances in the SENTIMENT_FREQ Map.
	 * Classification is the Key, number of occurances is the Value.
	 * @param   polarity Classification Key to increment in SENTIMENT_FREQ
	 * @param   text the String of text to be tokenised and counted
	 */
	private static void incrementWordsInPolarity(Classification polarity, String text)
	{
		TOKENS = text.split(" ");
		WORD_MAP = SENTIMENT_FREQ.get(polarity);
		for ( String word : TOKENS)
		{
			int frequency = WORD_MAP.containsKey(word)
						? (WORD_MAP.get(word) + 1) : 1;
			WORD_MAP.put(word, frequency);
		}
		SENTIMENT_FREQ.put(polarity, WORD_MAP);
	}

	/**
	 * Checks if the general sentiment of the review content matches the rating
	 * that is provided as a param, then returns the Classification. Non-ID'ed
	 * words are auto classified as SPAM.
	 * @param   sentiments collection of sentiment counts
	 * @param   rating value to compare the general sentiment against
	 * @return  Classification value corresponding to the comparison result
	 */
	private static Classification checkConsistency(String text, int rating)
	{
		int[] sentCounts = countSentiments(text);
		int polarValue = calculatePolarity(sentCounts);
		Classification polarity = Classification.SPAM;

		switch (rating)
		{
			case 1: polarity = ( polarValue <= 0 ) ? Classification.NEGATIVE
								: Classification.INCONSISTENT_NEG;
					break;
			case 2: polarity = ( polarValue <= 0 ) ? Classification.NEGATIVE
								: Classification.INCONSISTENT_NEG;
					break;
			case 3: if ( polarValue == 0 )
					{
						if ( sentCounts[0] != 0 || sentCounts[1] != 0 )
						{   polarity = Classification.NEUTRAL;  }
					}
					else if ( polarValue > 0 )
					{   polarity = Classification.INCONSISTENT_POS; }
					else if ( polarValue < 0 )
					{   polarity = Classification.INCONSISTENT_NEG; }
					break;
			case 4: polarity = ( polarValue >= 0 ) ? Classification.POSITIVE
								: Classification.INCONSISTENT_POS;
					break;
			case 5: polarity = ( polarValue >= 0 ) ? Classification.POSITIVE
								: Classification.INCONSISTENT_POS;
					break;
			default:    break;
		}
		try
		{
			BufferedWriter bfw = new BufferedWriter(new FileWriter("./reports/sanity-check" + KEY_NAME.replaceAll(" ", "-") + ".txt", true));
			bfw.write(sentCounts[0] + "-" + sentCounts[1] + "=" + polarValue + " vs " + rating + " = " + polarity.toString() + "||||" + text);
			bfw.newLine();
			bfw.close();
		} catch (Exception e) { e.printStackTrace();    }
		return polarity;
	}

	/**
	 * Calculate the general sentiment from the sentiment array param. Returns
	 * negative values for NEGATIVE, positive for POSITIVE, zero for NEUTRAL.
	 * @param   sentiments collection to calculate general sentiment
	 * @return  result indicative of the general sentiment
	 */
	private static int calculatePolarity(int[] sentiments)
	{   return sentiments[Polarity.POS.ordinal()] - sentiments[Polarity.NEG.ordinal()]; }
		//(sentiments[Polarity.NEU.ordinal()] > sentiments[Polarity.POS.ordinal()] + sentiments[Polarity.NEG.ordinal()]) ? 0 : sentiments[Polarity.POS.ordinal()] - sentiments[Polarity.NEG.ordinal()]; }

	/**
	 * Parses the review file that is brought in as a param, splits each line
	 * into tokens to obtain the review content and rating, where the text is
	 * treated using stabiliseText() to lowercased, trimmed, cleaned of all
	 * non-alphanumerics and brute forced against typos. The cleaned text is
	 * then sentiment counted and checked for consistency in content and rating.
	 * @param   filePath String filepath of the review file
	 */
	private static void parseReviews()
	{
		int rating = 0;
		String review;
		String version;
		String[] reviewTokens;

		for ( String LINE : REVIEWS )
		{
			TOKENS = LINE.split("\\|\\|");
			if ( TOKENS.length < 13 )
			{   continue;   }
			try
			{
				try
				{
					rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
				} catch (NumberFormatException nfe) {   continue;   }
				review = TOKENS[AppData.REVIEW_TITLE.ordinal()]
						+ " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
				review = stabiliseText(review);
				extractOntologyPhrases(review, Ontology.EMOT, EMOTIONAL_WORDS);
				extractOntologyPhrases(review, Ontology.FUNC, FUNCTIONALITY_WORDS);
				extractOntologyPhrases(review, Ontology.QUAL, QUALITY_WORDS);
				// addToStructures(checkConsistency(review, rating), review, TOKENS[AppData.APP_VERSION.ordinal()]);
				// writeLine(reportPath(polarity.toString(), CSV), rating + "||||" + review);
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	/************************* DEBUG/CONSOLE REPORT METHODS *************************/


	/**
	 * Generic method to print SENTIMENT_DIST to console.
	 */
	private static void reportSentimentDistribution()
	{
		int sumOfReviews = 0;
		int rawCount = 0;
		for ( Entry<Classification, Integer> entry :  SENTIMENT_DIST.entrySet() )
		{   sumOfReviews += entry.getValue();   }
		for ( Classification polarity : Classification.values() )
		{
			rawCount = SENTIMENT_DIST.get(polarity);
			System.out.println(polarity + ": " + getProportion(rawCount, sumOfReviews) + "% (" + rawCount + ")");
		}
	}

	private static void reportOntologies()
	{
		Set<String> phrases;
		for ( Ontology ontType : Ontology.values() )
		{
			phrases = ONTOLOGY_PHRASES.containsKey(ontType) ? ONTOLOGY_PHRASES.get(ontType) : new HashSet<String>();
			for ( String line : phrases )
			{	System.out.println(phrases);	}
		}
	}
}
