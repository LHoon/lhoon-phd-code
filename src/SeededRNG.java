import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import hoon.txfx.TextCleaner;
import java.util.Random;

public class SeededRNG
{
	static Integer MAX = 400;
	static Integer MIN = 1;
	static Integer RANGE;
	static final Integer SEED = 1;
	static Random GENERATOR;

	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.err.println("Provide only one whole number for the number of values required.");
			System.exit(1);
		}
		try
		{	RANGE = Integer.parseInt(args[0]);	}
		catch (NumberFormatException nfe)
		{
			System.err.println("Provide only one whole number for the number of values required.");
			System.exit(2);
		}
		TextCleaner tc;
		String test = "This shouldn't be good. I can't package stuff. btw.";
		System.out.println(test);
		try
		{
			tc = new TextCleaner();
			System.out.println(tc.applyLookup(tc.stringTreatment(test)));
			// System.out.println(tc.applyLookup(test));
		}
		catch (IOException ioe)
		{	ioe.printStackTrace();	}

		// GENERATOR = new Random(SEED);
		// for(int i = 0; i < RANGE; i++)
		// {
		// 	int num = GENERATOR.nextInt((MAX+1) - MIN) + MIN;
		// 	System.out.println(num);
		// }
	}
}


class TextCleaner
{
	private Map<String, String> abbrvTypos;
	private Pattern validPattern;
	private StringBuilder sBuilder;

	public TextCleaner() throws IOException
	{
		abbrvTypos = new TreeMap<String, String>();
		validPattern = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`/]+");
		sBuilder = new StringBuilder();
		loadTypos("./assets/abbrv-txt-spk.csv");
		loadTypos("./assets/common-typos.csv");
		loadTypos("./assets/aux-common-typos.csv");	}

	/**
	 * Startup method to populate and index all text resources into a virtual
	 * indexed dictionary structure.
	 */
	private void loadReferenceData() throws IOException
	{

	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private void loadTypos(String fileName) throws IOException
	{String line = "";
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{String tokens[] = line.split(",");
			abbrvTypos.put(minorClean(tokens[0]), minorClean(tokens[1]));}
		}
		reader.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	public String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	public String stringTreatment(String review)
	{
		review = minorClean(review);
		review = review.replaceAll("\\`","\\'");
		review = review.replaceAll("won't","will not");
		review = review.replaceAll("can't","cannot");
		review = review.replaceAll("'m", "\\ am");
		review = review.replaceAll("\\ s ","s ");
		review = review.replaceAll("'ll", "\\ will");
		review = review.replaceAll("'re", "\\ are");
		review = review.replaceAll("'d","\\ would");
		review = review.replaceAll("'ve","\\ have");
		review = review.replaceAll("n't","\\ not");
		return minorClean(review);
	}

	public String applyLookup(String text)
	{
		boolean wordAdded = false;
		if ( sBuilder.length() > 0 )
		{   sBuilder.delete(0, sBuilder.length());    }

		Matcher matcher = validPattern.matcher(text);
		while (matcher.find())
		{
			String wordToAdd = minorClean(matcher.group());
			if(!wordToAdd.matches(".*\\d.*"))
			{	wordToAdd = abbrvTypos.containsKey(wordToAdd) ? wordToAdd = abbrvTypos.get(wordToAdd) : wordToAdd;	}

			sBuilder.append(wordToAdd);
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)	// Remove the trailing space
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }
		return sBuilder.toString();
	}
}
