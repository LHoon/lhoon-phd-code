import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.util.io.HtmlWriter;
import hoon.util.textfx.TextCleaner;

public class ColesTagger
{
	//Brand, Zone, Region, Store, Department, Job Level, Engagement Comments, Organisation Comments, Values Comments, The Basics Comments, Line Manager Comments
	public enum CommentTokens
	{	BRAND, ZONE, REGION, STORE, DEPT, JOB, EGMT, ORG, VAL, BASIC, LNMGR	};
	final static int NUM_TOKENS = CommentTokens.values().length;
	static List<Comment> RAW_COMMENTS = new ArrayList<Comment>();
	static List<Comment> PARSED_COMMENTS = new ArrayList<Comment>();
	static Set<String> POS_RULES = new TreeSet<String>();
	static Set<String> NEG_RULES = new TreeSet<String>();
	// static Map<String, String> POS_RULES = new TreeMap<String, String>();

	// Pattern.compile or as a String
	final static boolean PATTERN = true;
	final static boolean STRING = false;
	static boolean PRINT_TAGGED_ONLY = false;

	static TextCleaner CLEANER;
	static Set<String> POS_WORDS = new HashSet<String>();
	static Set<String> NEG_WORDS = new HashSet<String>();

	static String DELIMITER = "\\t";
	static String IN_FILE = "./comments/fake-coles.tsv";
	static String OUT_FILE = "./comments/coles.html";

	// static Pattern POS_WORDS;
	// static Pattern NEG_WORDS;
	static Pattern WHITELIST;
	static StringBuilder S_BUILDER = new StringBuilder();

	public static void main ( String[] args ) throws IOException
	{
		// POS_WORDS = parseSentiments("./assets/positive-words.txt");
		// NEG_WORDS = parseSentiments("./assets/negative-words.txt");
		loadSet(POS_WORDS, "./assets/positive-words.txt");
		loadSet(NEG_WORDS, "./assets/negative-words.txt");
		WHITELIST = Pattern.compile("[a-zA-z]+");
		parseInput();
		CLEANER = new TextCleaner("./assets", false);
		loadRules();
		if(args.length == 1)
		{
			try
			{	Boolean.parseBoolean(args[0]);	}
			catch (Exception e)
			{	PRINT_TAGGED_ONLY = true;	}
		}
		processComments();
		HtmlWriter htmlWriter = new HtmlWriter(OUT_FILE, "content.css");
		htmlWriter.writeBody(PARSED_COMMENTS);
		htmlWriter.writeFooter();
		// CLEANER.reportCorrections();
	}

	private static void loadRules()
	{
		NEG_RULES.add("not happy");
		NEG_RULES.add("do not feel");
		NEG_RULES.add("feel unsafe");
		NEG_RULES.add("feel unsure");
		NEG_RULES.add("feel unworth");
		NEG_RULES.add("feel uncomfortable");
		NEG_RULES.add("feel unappreciate");
		NEG_RULES.add("feel unable");
		NEG_RULES.add("feel uneasy");
		NEG_RULES.add("age");
		NEG_RULES.add("young");
		NEG_RULES.add("old");
		NEG_RULES.add("lack");
		NEG_RULES.add("not enough");
		NEG_RULES.add("concern");
		NEG_RULES.add("not feel");

		POS_RULES.add("morale");
		POS_RULES.add("love work");
		POS_RULES.add("place to work");
		POS_RULES.add("to be part of");
		POS_RULES.add("not get");
	}

	private static void processComments()
	{
		int index = 0;
		EnumSet<CommentTokens> tokenSet = EnumSet.of(CommentTokens.EGMT, CommentTokens.ORG, CommentTokens.VAL, CommentTokens.BASIC, CommentTokens.LNMGR);
		Comment tagged;
		String token;
		boolean hasTag = false;
		for ( Comment c : RAW_COMMENTS )
		{
			for ( CommentTokens commTok : tokenSet )
			{
				index = commTok.ordinal();
				if ( c.tokens[index].length() > 0 )
				{
					c.tokens[index] = tagRules(c.tokens[index]);
					if(c.tokens[index].contains("</span>"))
					{	hasTag = true;}
				}
			}
			if ( PRINT_TAGGED_ONLY )
			{
				if(hasTag)
				{
					PARSED_COMMENTS.add(c);
					hasTag = false;
				}
			}
			else
			{	PARSED_COMMENTS.add(c);	}
		}
	}

	private static String tagSentiments(String line)
	{
		String result = line;
		if ( line.length() != 0 )
		{
			boolean doTag = false;
			String current;
			String tagged;
// System.out.println(line);
			String tagValue = "";
			String text = CLEANER.correct(line);
System.out.println(text);
			result = text;
			Matcher m = WHITELIST.matcher(text);
			while ( m.find() )
			{
				current = m.group();
				if ( POS_WORDS.contains(current) )
				{
					doTag = true;
					tagValue = "";
				}
				else if ( NEG_WORDS.contains(current) )
				{
					doTag = true;
					tagValue = "negword";
				}
				if ( doTag )
				{
					tagged = tagString(current, tagValue);
					result = result.replaceAll("\\b"+current+"\\b", tagged);
					doTag = false;
					tagValue = "";
				}
			}
		}
		return result;
	}

	private static String tagRules(String line)
	{
		String result = line;
		if ( line.length() != 0 )
		{
			boolean doTag = false;
			String current;
			String tagged;
			String tagValue = "";
// System.out.print(line + " : ");
			String text = CLEANER.correct(line);
// System.out.println(text);
			result = text;
			text = text.replaceAll("\\.", "\\|\\|");
			text = text.replaceAll("\\?", "\\|\\|");
			text = text.replaceAll("\\!", "\\|\\|");
			String[] tokens = text.split("\\|\\|");
			// System.out.println(tokens.length);
			for ( String s : tokens)
			{

				for( String nRule : NEG_RULES )
				{
					if(s.contains(nRule))
					{
						doTag = true;
						tagValue = "negRule";
						break;
					}
				}
				for( String pRule : POS_RULES )
				{	// System.out.println(s);
					if(s.contains(pRule))
					{
						doTag = true;
						tagValue = "posRule";
						break;
					}
				}
				if ( doTag )
				{
					tagged = tagString(s, tagValue);
					result = result.replaceAll("\\b"+s+"\\b", tagged);
					doTag = false;
					tagValue = "";
				}
			}
		}
		return result;
	}

	private static String tagString(String word, String tag)
	{
		String result = "<span class=\"" + tag + "\">" + word + "</span>";
		return result;
	}

	/**
	 *	Minor String preparation, trims trailing spaces, lowercases
	 *	and enforces Locale.ENGLISH prior to returning it.
	 * @param	text String value to be processed
	 * @param	asPattern true to treat it as a Pattern, false for no
	 * @return	String value constructed from param, LowerCased and trimmed
	 */
	private static String prepareString(String text, boolean asPattern)
	{	return asPattern ? "\\b" + text.toLowerCase(Locale.ENGLISH).trim() + "\\b" : text.toLowerCase(Locale.ENGLISH).trim();	}

	/*			FILE I/O METHODS			*/
	private static void parseInput() throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(IN_FILE));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{	RAW_COMMENTS.add(new Comment(line, DELIMITER, NUM_TOKENS));	}
		}
		reader.close();
	}

	private static void loadSet(Set<String> theSet, String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{	theSet.add(line.toLowerCase(Locale.ENGLISH).trim());	}
		}
		reader.close();
	}

	/**
	 *	Generic method to load a input file word list into a param Set as
	 *	compiled regex Patterns or raw Strings, dependent on boolean param.
	 * @param	filepath filePath of the input resource to add to the Set
	 * @return	String value of the search space
	 */
	private static Pattern parseSentiments(String filePath) throws IOException
	{
		boolean firstLine = true;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}
		// for ( String line = ""; line != null; line = reader.readLine() )
		// {
		// 	S_BUILDER.append(prepareString(line, STRING));
		// 	S_BUILDER.append(" , ");
		// }
		// reader.close();
		S_BUILDER.append("(");

		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( line.length() > 1 )
			{
				if(!firstLine)
				{	S_BUILDER.append("|");	}
				S_BUILDER.append(prepareString(line, PATTERN));
				firstLine = false;
			}
		}
		S_BUILDER.append(")");
		return Pattern.compile(S_BUILDER.toString().trim());
	}
}

class Comment
{
	public String[] tokens;
	public String rawLine;

	public Comment (String input, String delimiter, int arraySize)
	{
		this.initialiseTokens(arraySize);
		this.rawLine = input;
		this.populateTokens(input.split(delimiter));
	}

	private void initialiseTokens(int arraySize)
	{
		this.tokens = new String[arraySize];
		for ( int i = 0; i < this.tokens.length; i++ )
		{	this.tokens[i] = "";	}
	}

	private void populateTokens(String[] values)
	{
		for ( int i = 0; i < values.length; i++ )
		{
			if ( !values[i].equals("") || !values[i].equals(null) )
			{	this.tokens[i] = values[i].toLowerCase(Locale.ENGLISH).trim();	}
		}
	}

	public String toHtml()
	{
		String pre = "<td>";
		String post = "</td>";
		String result = "";
		for( String s : this.tokens )
		{
			if ( s.equals(null) )
			{	s = "";	}
			result += pre + s + post;
		}
		return result;
	}

	public String toString()
	{	return this.rawLine;	}
}
	// brand, zone, region, store, dept, job, engagement, organisation, values, basics, lineMngr, line;
	// this.brand = tokens[column.BRAND.ordinal()];
		// this.zone = tokens[column.ZONE.ordinal()];
		// this.region = tokens[column.REGION.ordinal()];
		// this.store = tokens[column.STORE.ordinal()];
		// this.dept = tokens[column.DEPT.ordinal()];
		// this.job = tokens[column.JOB.ordinal()];
		// this.engagement = tokens[column.EGMT.ordinal()];
		// this.organisation = tokens[column.ORG.ordinal()];
		// this.values = tokens[column.VAL.ordinal()];
		// this.basics = tokens[column.BASIC.ordinal()];
		// this.lineMngr = tokens[column.LNMGR.ordinal()];
		// this.line = line;

	// public String getBrand()
	// {	return brand;	}

	// public void setBrand(String brand)
	// {	 this.brand = brnd;	}

	// public String getZone()
	// {	return zone;	}

	// public boolean setZone(String zone)
	// {	 this.zone = zone;	}
// }
