import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class RatingTF
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum Sentiment
	{	POS, NEG, INC_POS, INC_NEG, NEUTRAL, SPAM	};

	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};
	private static String TERM = "";
	private static String CAT_PRICE = "";
	private static String OUTFILE = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static TextCleaner CLEANER;

	private static Map<String, Integer> REVIEWS_APP = new HashMap<String, Integer>();
	private static Map<String, Integer> TERM_MAP = new HashMap<String, Integer>();
	private static Map<String, Integer[]> RATED_MAP = new HashMap<String, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		if(args.length < 1)
		{
			System.err.println("No review file provided for processing.");
			System.exit(1);
		}

		CAT_PRICE = args[0];
		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, sU);
		parseReviewTextFile();
		parseTermMap();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CAT_PRICE + TAB + formatNumber(REVIEW_COUNT) + TAB + "reviews decomposed out of" + TAB + formatNumber(LINES_READ) + TAB +  "lines read, in " + timeTaken);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 */
	private static String getFilePath(String folder, String fileName, String extension)
	{	return String.format("./%s/%s.%s", folder, fileName, extension);	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void add(Map<String, Integer> theMap, String key)
	{
		Integer value = 0;
		if(theMap.containsKey(key))
		{	value = theMap.get(key);	}
		value++;
		theMap.put(key, value);
	}

	private static boolean lineIsValid()
	{
		if ( TOKENS.length != MAX_DATA_TOKENS ) 		{	return false;	}
		if ( TOKENS[AppData.RATING.ordinal()].equals("1")
			|| TOKENS[AppData.RATING.ordinal()].equals("2")
			|| TOKENS[AppData.RATING.ordinal()].equals("3")
			|| TOKENS[AppData.RATING.ordinal()].equals("4")
			|| TOKENS[AppData.RATING.ordinal()].equals("5") )
		{	return true;	}
		else	{	return false;	}
	}

	private static String replacePunctuations(String text)
	{
		String result = text;

		return text.replaceAll("^[\\^\\.\\,\\;\\:\\\"]+$", "");
	}

	private static void parseReviewTextFile() throws Exception
	{
		int rating = 0;
		String rawReview = "";
		String cleanReview = "";
		String details = "";
		String english = "^[a-zA-Z0-9_\\^\\w\\.\\,\\;\\:\\'\\\"]+$";
		String[] cleanTokens;
		StringUtil sU = new StringUtil();

		try
		{
			READER = new BufferedReader(new FileReader("./reviews/data/us_" + CAT_PRICE + "_reviews.csv"));

			for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
			{
				LINES_READ++;
				TOKENS = LINE.split(PIPE);
				if ( !lineIsValid() )
				{
					if ( !LINE.equals("") )	{	System.out.println("line read error: " + CAT_PRICE + "||" + LINE);	}
					continue;
				}
				REVIEW_COUNT++;

				// "[^\\p{L}\\p{N}]"
				// "[^a-zA-Z0-9\\s]"
				// rawReview.split("\\s+");
				// if ( word.matches("[a-zA-Z0-9]+") )
				// .replaceAll("[\\^\\w\\.\\,\\;\\:\\'\\\"]", "");

				rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
				rawReview = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
				// cleanReview =

				cleanTokens = rawReview.toLowerCase(Locale.ENGLISH).trim().split("\\ ");


				// "^[ \\w \\d \\s \\. \\& \\+ \\- \\, \\! \\@ \\# \\$ \\% \\^ \\* \\( \\) \\; \\\\ \\/ \\| \\< \\> \\\" \\' \\? \\= \\: \\[ \\] ]*$";

				for ( String word : cleanTokens )
				{
					// word = word // to remove non alpha

					word = word.replaceFirst("^[^a-zA-Z]+", "");
					word = word.replaceAll("[^a-zA-Z]+$", "");
					if ( word.matches(english))// && word.length > 2 )
					{	addToMap(word, rating);	}
				}
			}
		}
		catch (Exception e)
		{	System.err.println("I/O Error: " + e.getMessage());	}
		finally
		{	READER.close();	}
	}

	private static void addToMap(String term, int rating)
	{
		COUNT = TERM_MAP.containsKey(term) ? TERM_MAP.get(term) : 0;
		COUNTS = RATED_MAP.containsKey(term) ? RATED_MAP.get(term) : new Integer[] {0,0,0,0,0};
		COUNT++;
		COUNTS[rating-1]++;
		TERM_MAP.put(term, COUNT);
		RATED_MAP.put(term, COUNTS);
	}

	private static void parseTermMap() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter("./thesis/lang/" + CAT_PRICE + "-tf" + ".tsv"));

		Map<String, Integer> resultMap = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(TERM_MAP));
		resultMap.putAll(TERM_MAP);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			TERM = entry.getKey();
			COUNTS = RATED_MAP.get(TERM);
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());

			for ( Integer i : COUNTS )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(i);
			}
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}

		WRITER.close();
	}

}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
