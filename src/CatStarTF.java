import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class CatStarTF
{
	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};
	final static int MAX_TOKENS = AppData.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	private static String IN_FILE = "";
	private static String CAT = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static TextCleaner CLEANER;
	private static TFIDFer TFIDFER;
	private static Stanford LEMMATISER;
	private static StringUtil S_UTIL = new StringUtil();

	private static Map<String, Set<String>> REVIEWS = new HashMap<String, Set<String>>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Provide review file for text frequency analysis.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		IN_FILE = args[0];
		CAT = IN_FILE.substring(3, IN_FILE.length());


		CLEANER = new TextCleaner("./assets", false, S_UTIL);
		TFIDFER = new TFIDFer(S_UTIL);
		LEMMATISER = new Stanford();
		findReviews(fileInPath(IN_FILE));
		DelimitedWriter writer = new DelimitedWriter(OUT_DELIMITER, "./reports/cat-star-tf/" + CAT + ".tsv");
		TFIDFER.calculateTfIdfs();
		TFIDFER.writeTFIDFs(writer, 0);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static String fileInPath(String fileName)
	{	return String.format("./reviews/data/%s_reviews.csv", fileName);	}

	private static void findReviews(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		String rating = "";
		String review = "";
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, IN_DELIMITER);
			if( TOKENS.length == MAX_TOKENS )
			{
				rating = TOKENS[AppData.RATING.ordinal()];
				review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
				review = CLEANER.correct(review);
				review = lemmatisation(review);
				review = CLEANER.correct(review);
				TFIDFER.countWords(rating, review);
			}
		}
		reader.close();
	}

	private static String lemmatisation(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
