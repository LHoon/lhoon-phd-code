import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import util.TextFile;

public class GoodShorts
{
	final static String TAB = "\t";
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();

	static Set<String> R_ID = new HashSet<String>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		READER = new BufferedReader(new FileReader("./thesis/free_entertainment-good-shorts.txt"));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{	R_ID.add(LINE.trim());	}
		READER.close();

		READER = new BufferedReader(new FileReader("./thesis/stabilised-reviews/stabilised-free_entertainment.tsv"));
		WRITER = new BufferedWriter(new FileWriter("./thesis/free_entertainment-good-shorts.tsv"));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(TAB);
			if ( TOKENS.length != 5 ) 		{	continue;	}
			if ( R_ID.contains(TOKENS[2]) )
			{
				WRITER.write(LINE);
				WRITER.newLine();
			}
		}
	}
}
