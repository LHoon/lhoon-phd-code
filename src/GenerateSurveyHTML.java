import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;

import hoon.util.*;
import hoon.io.*;

public class GenerateSurveyHTML
{
	public enum Tokens
	{	APP_ID, APP_NAME, REVIEW_ID, RATING, REV_TITLE, REV_BODY, SCORE	};

	public enum Question
	{	REVIEW, NAME, STARS, TITLE, BODY	};

	final static boolean BY_STAR = true;
	final static boolean NO_STAR = false;
	final static boolean IS_SCORED = true;
	final static boolean NO_SCORED = false;

	final static int MAX_TOKENS = Tokens.values().length;
	final static int QN_TOKENS = Question.values().length;
	final static String CSS = "./survey.css";
	final static String TAB = "\t";
	final static String TSV = "tsv";
	final static String HTML = "html";
	final static String CONSENT = "Informed consent: Hello and thank you for your participation in this survey. Please circle the option that you find best aligns with your option in each question. By participating in this survey you acknowledge your contribution to this data collection exercise for research purposes.";
	final static String PREAMBLE = "The following review is for: ";
	final static String PRE_OPTIONS = "As a <b>Consumer</b>, I find this review to be useful in my decision to download or purchase this app (please circle your option)";
	final static String[] OPTIONS = {"Not useful at all", "Not very useful", "Unsure", "Slightly Useful", "Very Useful"};

	static Integer SEED = 7;
	static boolean DO_WRITE = false;
	static int SURVEY_SIZE = 20;
	static String CATEGORY;
	static String PRICE;
	static String CAT;
	static Set<Integer> RNG_SET;
	private static Map<String, List<String>> REVIEWS = new HashMap<String, List<String>>();
	private static Set<String[]> QUESTIONS = new HashSet<String[]>();

	static StringUtil S_UTIL = new StringUtil();
	static HtmlWriter HTML_WRITER;
	static Random GENERATOR;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if ( args.length < 1 )
		{
			System.out.println("Please provide price_category parameter (free_health, paid_entertainment, etc), survey size and number of seeded surveys for extraction parameters.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CATEGORY = args[0];
		if ( args.length >= 2 )
		{
			try
			{
				SURVEY_SIZE = Integer.parseInt(args[1]);
				SEED = Integer.parseInt(args[2]);
			}
			catch ( NumberFormatException nfe )
			{
				System.err.println("Provide a valid number for review count.");
				System.exit(1);
			}
		}
		if ( args.length > 2 && args[2].equals("print") )
		{	DO_WRITE = true;	}

		PRICE = CATEGORY.split("_")[0];
		CAT = CATEGORY.split("_")[1];

		generateSurvey(IS_SCORED, BY_STAR);
		generateSurvey(NO_SCORED, BY_STAR);
		generateSurvey(NO_SCORED, NO_STAR);

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Extraction completed in " + elapsed);
	}

	private static void generateSurvey(boolean isScored, boolean isByStar) throws IOException
	{
		String code = "";
		if( isScored )	{	code = "H-";	}
		else
		{
			if ( isByStar )	{	code = "S-"; }
			else {	code = "R-"; }
		}

		loadDataFile(isScored, isByStar);
		for ( int i = 1; i < SEED+1; i++ )
		{
			GENERATOR = new Random(i);
			if ( isScored || isByStar ) {	getRandomQuestions(BY_STAR);	}
			else {	getRandomQuestions(NO_STAR);	}
			writeSurvey(code + SURVEY_SIZE + "-" + i);
		}
		REVIEWS.clear();
	}

	private static void printPaths()
	{
		System.out.println("HTML out file: " + htmlPath("htmlname"));
		System.out.println("Scored in file: " + dataPath(true, false));
		System.out.println("No Star: " + dataPath(false, NO_STAR));
		System.out.println("By Star: " + dataPath(false, BY_STAR));
	}

	private static String htmlPath(String surveyType)
	{	return String.format("./reports/survey-live/html/%s-%s.%s", CATEGORY, surveyType, HTML);	}

	private static String reportPath(String surveyType)
	{	return String.format("./reports/survey-live/html/%s-%s.%s", CATEGORY, surveyType, TSV);	}

	private static String dataPath(boolean isScored, boolean isByStar)
	{
		String suffix = isScored ? "-Scored" : isByStar ? "-RawByStar" : "-RawStraight";
		return String.format("./reports/survey-live/random/%s%s.%s", CATEGORY, suffix, TSV);
	}

	private static void loadDataFile(boolean isScored, boolean isByStar) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(dataPath(isScored, isByStar)));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			String tokens[] = line.split(TAB);
			if ( isScored )	{	if( tokens.length < MAX_TOKENS ) {	continue;	}	}
			else			{	if( tokens.length < MAX_TOKENS - 1 ) {	continue;	}	}

			persist(line, tokens[Tokens.RATING.ordinal()]);
		}
		reader.close();
	}

	private static void persist(String text, String rating)
	{
		List<String> reviewList = REVIEWS.containsKey(rating) ? REVIEWS.get(rating) : new ArrayList<String>();
		reviewList.add(text);
		REVIEWS.put(rating, reviewList);
	}

	private static void getRandomQuestions(boolean isByStar) throws IOException
	{
		String file = "";
		if ( !isByStar )
		{
			List<String> combinedList = new ArrayList<String>();
			for( Entry<String, List<String>> entry : REVIEWS.entrySet() )
			{	combinedList.addAll(entry.getValue());	}
			getNReviewsFromList(combinedList, SURVEY_SIZE, file);
		}
		else
		{
			int max = SURVEY_SIZE/5;
			for( Entry<String, List<String>> entry : REVIEWS.entrySet() )
			{	getNReviewsFromList(entry.getValue(), max, file);	}
		}
	}

	private static void getNReviewsFromList(List<String> reviewList, int numExtracted, String fileName) throws IOException
	{
		RNG_SET = getRNGset(reviewList.size(), numExtracted);
		for ( Integer i : RNG_SET )
		{	prepareQuestion(reviewList.get(i));	}
	}

	// {	REVIVEW, NAME, STARS, TITLE, BODY	};
	private static void prepareQuestion(String line)
	{
		String[] tokens = line.split(TAB);
		String[] result = new String[QN_TOKENS];
		result[Question.REVIEW.ordinal()] = tokens[Tokens.APP_ID.ordinal()] + ", " + tokens[Tokens.REVIEW_ID.ordinal()];
		result[Question.NAME.ordinal()] = tokens[Tokens.APP_NAME.ordinal()];
		result[Question.STARS.ordinal()] = tokens[Tokens.RATING.ordinal()];
		result[Question.TITLE.ordinal()] = tokens[Tokens.REV_TITLE.ordinal()];
		result[Question.BODY.ordinal()] = tokens[Tokens.REV_BODY.ordinal()];
		QUESTIONS.add(result);
	}

	private static void writeSurvey(String surveyType) throws IOException
	{
		String meta;
		String options;
		String reportLine = "";
		int qnCount = 1;
		BufferedWriter reportWriter = new BufferedWriter(new FileWriter(reportPath(surveyType), true));
		HTML_WRITER = new HtmlWriter(htmlPath(surveyType), CSS);
		HTML_WRITER.openTable();
		HTML_WRITER.writeRow(CONSENT + "<font size =\"1\">" + surveyType + "</font>" , "colspan=\"5\";");
		HTML_WRITER.writeRow("<br>", "colspan=\"5\";");

		for ( String[] question : QUESTIONS )
		{
			reportLine = question[Question.STARS.ordinal()] + TAB + question[Question.REVIEW.ordinal()];

			HTML_WRITER.writeRow("<font size=\"1\">Q" + qnCount + ", " + question[Question.REVIEW.ordinal()] + "</font>" , "colspan=\"5\";");
			// HTML_WRITER.writeRow(PREAMBLE, "colspan=\"5\";");
			meta = String.format("%s <b>%s</b>, a <b>%s</b> app in the <b>%s</b> category with <b>%s</b> star(s)", PREAMBLE, question[Question.NAME.ordinal()], PRICE, CAT, question[Question.STARS.ordinal()]);
			HTML_WRITER.writeRow(meta, "colspan=\"5\";");
			HTML_WRITER.writeRow("<br>", "colspan=\"5\";");
			HTML_WRITER.writeRow(question[Question.TITLE.ordinal()], "colspan=\"5\";");
			HTML_WRITER.writeRow(question[Question.BODY.ordinal()], "colspan=\"5\";");
			HTML_WRITER.writeRow("<br>", "colspan=\"5\";");
			HTML_WRITER.writeRow(PRE_OPTIONS, "colspan=\"5\";");
			HTML_WRITER.openRow();
			for ( int i = 0; i < OPTIONS.length; i++ )
			{	HTML_WRITER.writeCell(OPTIONS[i], "align=\"center\"");	}
			HTML_WRITER.closeRow();
			HTML_WRITER.writeRow("<hr>", "colspan=\"5\";");
			reportWriter.write(reportLine);
			reportWriter.newLine();
			qnCount++;
		}
		HTML_WRITER.closeTable();
		HTML_WRITER.writeFooter();
		reportWriter.close();
		QUESTIONS.clear();
	}

	private static Set<Integer> getRNGset(int ceiling, int count)
	{
		Set<Integer> result = new HashSet<Integer>();
		while( result.size() < count )
		{	result.add(GENERATOR.nextInt(ceiling));	}
		return result;
	}
}
