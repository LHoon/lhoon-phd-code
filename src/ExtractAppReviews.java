import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.concurrent.TimeUnit;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

// import flanagan.control.FirstOrder;
// import flanagan.analysis.Stat;


public class ExtractAppReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int MAX_TOKENS = AppData.values().length;

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static Integer WRITTEN = 0;

	private static DateTime DATE = new DateTime();
	private static String NAME_OF_APP = "";
	private static String CAT_PRICE = "";
	private static String KEY_ID = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Map<String, DateTime> VERSIONS = new TreeMap<String, DateTime>();
	private static DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("MMM dd, yyyy");

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 2)
		{
			System.out.println("Insufficient args provided for Review Extraction Phase.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT_PRICE = args[0];
		KEY_ID = args[1];
		String appName = getAppName(getPath(CAT_PRICE, INDEX));
		NAME_OF_APP = appName.replaceAll(" ","_");
		System.out.println("Searching for reviews pertaining to " + appName);
		findReviews(getPath(CAT_PRICE, DATA));
		System.out.println(formatNumber(WRITTEN) + " reviews for " + appName + " extracted over "+ VERSIONS.size() + " versions.");
		for(Entry<String, DateTime> entry : VERSIONS.entrySet())
		// {
		// 	String date = entry.getValue().toString("MMM dd, yyyy");
		// 	System.out.println(DateTime.parse(date, DATE_FORMATTER).toString());

		{	System.out.println(entry.getKey() + " " + entry.getValue().toString("MMM dd, yyyy"));	}

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}



	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./wip/%s.csv", fileName);	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String filePath, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(line);
			WRITER.newLine();
			WRITTEN++;
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split("\\|\\|");
			if( TOKENS.length != MAX_TOKENS ) {	continue;	}
			if ( TOKENS[AppData.APP_ID.ordinal()].equals(KEY_ID) )
			{
				determineDate(TOKENS[AppData.APP_VERSION.ordinal()], TOKENS[AppData.REVIEW_DATE.ordinal()]);
				writeLine(outPath(NAME_OF_APP), LINE);
			}
		}
		READER.close();
	}

	private static String getAppName(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split("\t");
			if( TOKENS.length < 9 ) {	continue;	}
			if ( TOKENS[AppMeta.APP_ID.ordinal()].equals(KEY_ID) )
			{	return TOKENS[AppMeta.APP_NAME.ordinal()];	}
		}
		READER.close();
		return "";
	}

	private static void determineDate(String version, String dateString)
	{
		try
		{
			DATE = DateTime.parse(dateString, DATE_FORMATTER);
			DateTime value = VERSIONS.containsKey(version) ? VERSIONS.get(version) : new DateTime();
			if ( DATE.isAfter(value) )
			{	DATE = value;	}
			VERSIONS.put(version,DATE);
		} catch (Exception e) {	e.printStackTrace();	}
	}
}
