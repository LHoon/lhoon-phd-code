import java.io.*;

import java.text.DecimalFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class CommentValuator
{
	public enum CommentTokens
	{	BRAND, ZONE, REGION, STORE, DEPT, JOB, EGMT, ORG, VAL, BASIC, LNMGR	};

	public enum IDF
	{	DOC, TOK, VAL	};

	final static boolean IS_ZONE = true;
	final static boolean IS_TYPE = false;
	final static int MAX_IDF_TOKENS = IDF.values().length;
	final static int MAX_COM_TOKENS = CommentTokens.values().length;
	final static String TAB = "\t";
	final static Pattern WORDS = Pattern.compile("[a-zA-z]+");
	final static Map<String, Double> EMPTY = new HashMap<String, Double>();

	private static EnumSet<CommentTokens> TOKEN_SET = EnumSet.of(CommentTokens.EGMT, CommentTokens.ORG, CommentTokens.VAL, CommentTokens.BASIC, CommentTokens.LNMGR);
	private static CommentTokens VALUE_TYPE;
	private static Double SCORE;
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Matcher MATCHER;
	private static Stanford LEMMATISER;
	private static TextCleaner CLEANER;
	private static Map<String, Map<String, Double>> Z_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<String, Map<String, Double>> T_POINTS = new HashMap<String, Map<String, Double>>();
	private static Map<Comment, Double> SCORES = new HashMap<Comment, Double>();
	private static Set<Comment> COMMENTS = new HashSet<Comment>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long start = System.currentTimeMillis();

		CLEANER = new TextCleaner("./assets", false, S_UTIL);
		// LEMMATISER = new Stanford();
		// CAT = "free_health";//args[2].substring(18, 29);

		if ( args.length == 0 )
		{
			System.out.println("Provide a type of comment (engagement, organisation, values, basics, manager) to evaluate value by.");
			System.exit(1);
		}
		convertCommentType(args[0]);
		if ( VALUE_TYPE == null )
		{
			System.out.println("Provide valid type of comment (engagement, organisation, values, basics, manager) to evaluate value by.");
			System.exit(1);
		}

		loadIDFs("./comments/coles-zone.tsv", IS_ZONE);
		loadIDFs("./comments/coles-type.tsv", IS_TYPE);

		parseReviews("./comments/50-coles.tsv");
		writeResults("./comments/coles-"+VALUE_TYPE.toString()+"-scores.tsv");

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Valuation " + "completed in " + elapsed);
	}

	private static void convertCommentType(String type)
	{
		switch ( S_UTIL.minorClean(type) )
		{
			case "engagement" : 	VALUE_TYPE = CommentTokens.EGMT;
									break;
			case "organisation" : 	VALUE_TYPE = CommentTokens.ORG;
									break;
			case "values" : 		VALUE_TYPE = CommentTokens.VAL;
									break;
			case "basics" : 		VALUE_TYPE = CommentTokens.BASIC;
									break;
			case "manager" : 		VALUE_TYPE = CommentTokens.LNMGR;
									break;
			default:	VALUE_TYPE = null;
		}
	}

	private static void writeResults(String filePath) throws IOException
	{
		DelimitedWriter writer = new DelimitedWriter(TAB, filePath);
		Map<Comment, Double> preSort = SCORES;
		Map<Comment, Double> sorted = new TreeMap<Comment, Double>(new hoon.util.ValueComparer<Comment, Double>(preSort));
		sorted.putAll(preSort);
		for ( Entry<Comment, Double> entry : sorted.entrySet() )
		{
			TOKENS = new String[7];
			for ( int i = 0; i < TOKENS.length-1; i++)
			{	TOKENS[i] = entry.getKey().tokens[i];	}
			TOKENS[TOKENS.length-1] = entry.getKey().tokens[VALUE_TYPE.ordinal()];
			// TOKENS[1] = review.getKey();
			// TOKENS[2] = new DecimalFormat("#0.0000000000000000000000").format(review.getValue());
			//PRINT ONLY THE RELEVANT COMMENT
			writer.writeLine(S_UTIL.delimitTokens(TOKENS, TAB));
			// System.out.println(S_UTIL.delimitTokens(TOKENS, TAB));
			// break;
		}
	}

	private static void parseReviews(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			if(LINE.length() <= 1)	{	continue;	}
			evaluate(new Comment(LINE, TAB, MAX_COM_TOKENS));
		}
		reader.close();
	}

	private static void evaluate(Comment commObj)
	{
		saveScore(commObj, parseTokens(CLEANER.correct(commObj.tokens[VALUE_TYPE.ordinal()]), VALUE_TYPE.toString(), commObj.tokens[CommentTokens.ZONE.ordinal()]));
	}

	private static Double parseTokens(String text, String type, String zone)
	{
		SCORE = 0.0;
		MATCHER = WORDS.matcher(text);
		Map<String, Double> nestedMap;
		String key = "";
		while ( MATCHER.find() )
		{
			LINE = S_UTIL.enforceAlphaNumerics(MATCHER.group());
			nestedMap = T_POINTS.containsKey(type) ? T_POINTS.get(type) : EMPTY;
			SCORE += nestedMap.containsKey(LINE) ? nestedMap.get(LINE) : 0.0;
			nestedMap = Z_POINTS.containsKey(zone) ? Z_POINTS.get(zone) : EMPTY;
			SCORE += nestedMap.containsKey(LINE) ? nestedMap.get(LINE) : 0.0;
		}
		return SCORE;
	}

	private static void saveScore(Comment comment, Double score)
	{
		if ( score > 0.0 )
		{
			// TOKENS = new String[MAX_COM_TOKENS + 1];
			// for ( int i = 0; i < TOKENS.length; i++)
			// {	TOKENS[i] = comment.tokens[i];	}
			// TOKENS[MAX_COM_TOKENS] = score.toString();
			// Map<String, Double> scoreMap = SCORES.containsKey(comment) ? SCORES.get(comment) : new HashMap<String, Double>();
			// scoreMap.put(S_UTIL.delimitTokens(TOKENS, TAB), score);
			SCORES.put(comment, score);
		}
	}

	private static void loadIDFs(String filePath, boolean isZone) throws IOException
	{
		Double value;
		Map<String, Double> nestedMap;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != MAX_IDF_TOKENS )	{	continue;	}
			try
			{
				value = Double.parseDouble(TOKENS[IDF.VAL.ordinal()]);
				if ( isZone )	{	nestedMap = Z_POINTS.containsKey(TOKENS[IDF.DOC.ordinal()]) ? Z_POINTS.get(TOKENS[IDF.DOC.ordinal()]) : new HashMap<String, Double>();	}
				else	{	nestedMap = T_POINTS.containsKey(TOKENS[IDF.DOC.ordinal()]) ? T_POINTS.get(TOKENS[IDF.DOC.ordinal()]) : new HashMap<String, Double>();	}

				nestedMap.put(TOKENS[IDF.TOK.ordinal()], value);

				if ( isZone )	{	Z_POINTS.put(TOKENS[IDF.DOC.ordinal()], nestedMap);	}
				else	{	T_POINTS.put(TOKENS[IDF.DOC.ordinal()], nestedMap);	}
			}
			catch (NumberFormatException nfe)	{	continue;	}
		}
		reader.close();
	}

	// private static void countWords(boolean byZone)
	// {
	// 	int index = 0;
	// 	String docID = "";
	// 	EnumSet<CommentTokens> tokenSet = EnumSet.of(CommentTokens.EGMT, CommentTokens.ORG, CommentTokens.VAL, CommentTokens.BASIC, CommentTokens.LNMGR);
	// 	for ( Comment c : COMMENTS )
	// 	{
	// 		for ( CommentTokens commTok : tokenSet )
	// 		{
	// 			index = commTok.ordinal();
	// 			if ( c.tokens[index].length() > 0 )
	// 			{
	// 				c.tokens[index] = CLEANER.correct(c.tokens[index]);
	// 				if ( byZone )
	// 				{
	// 					docID = c.tokens[CommentTokens.ZONE.ordinal()];
	// 					// TFIDFERZ.countWords(docID, c.tokens[index]);
	// 				}
	// 				else
	// 				{
	// 					docID = commTok.toString();
	// 					// TFIDFERT.countWords(docID, c.tokens[index]);
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	// private static String lemma(String text)
	// {
	// 	TOKENS = text.split("\\ ");
	// 	if ( S_BUILDER.length() > 0 )
	// 	{	S_BUILDER.delete(0, S_BUILDER.length());	}

	// 	List<String> lemmaList = LEMMATISER.lemmatise(text);
	// 	for ( String s : lemmaList)
	// 	{
	// 		S_BUILDER.append(s);
	// 		S_BUILDER.append(" ");
	// 	}
	// 	return S_BUILDER.toString().trim();
	// }
}

class Comment
{
	public String[] tokens;
	public String rawLine;

	public Comment (String input, String delimiter, int arraySize)
	{
		this.initialiseTokens(arraySize);
		this.rawLine = input;
		this.populateTokens(input.split(delimiter));
	}

	private void initialiseTokens(int arraySize)
	{
		this.tokens = new String[arraySize];
		for ( int i = 0; i < this.tokens.length; i++ )
		{	this.tokens[i] = "";	}
	}

	private void populateTokens(String[] values)
	{
		for ( int i = 0; i < values.length; i++ )
		{
			if ( !values[i].equals("") || !values[i].equals(null) )
			{	this.tokens[i] = values[i].toLowerCase(Locale.ENGLISH).trim();	}
		}
	}

	public String toHtml()
	{
		String pre = "<td>";
		String post = "</td>";
		String result = "";
		for( String s : this.tokens )
		{
			if ( s.equals(null) )
			{	s = "";	}
			result += pre + s + post;
		}
		return result;
	}

	public String toString()
	{	return this.rawLine;	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		this.pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
