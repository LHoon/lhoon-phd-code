import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class RatedAF
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	public enum AF
	{	FONE, FTWO, FTHREE, FFOUR, FFIVE, FALL, PONE, PTWO, PTHREE, PFOUR, PFIVE, PALL	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String EQUAL = "\\=";
	final static String FOLDER = "./thesis/authors-rerun/";
	final static String CAT_FOLDER = FOLDER + "categories/";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer CATS_COUNTED = 0;
	private static Integer BAD_LINES = 0;
	private static Integer CHECK = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};	// 1, 2, 3, 4, 5
	private static String AUTHOR = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Map<String, Integer> AUTHOR_MAP = new HashMap<String, Integer>();
	private static Map<String, Integer[]> RATED_MAP = new HashMap<String, Integer[]>();

	private static Map<String, Integer> CAT_AUTHOR_MAP = new HashMap<String, Integer>();
	private static Map<String, Integer[]> CAT_RATED_MAP = new HashMap<String, Integer[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/categories.txt");
		parseReviews();
		writeAF();
		// printAF();
		// writeBadLines();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATS_COUNTED + " categories processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, " + formatNumber(BAD_LINES) + " bad lines out of " + formatNumber(LINES_READ) + " lines read in " + timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("./reviews/data/us_%s_%s_reviews.csv", price, category);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )	{	CATEGORIES.add(minorClean(LINE));	}
		}
		READER.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static boolean lineIsValid()
	{
		if ( TOKENS.length != MAX_DATA_TOKENS )	{	return false;	}
		if ( TOKENS[AppData.RATING.ordinal()].equals("1")
			|| TOKENS[AppData.RATING.ordinal()].equals("2")
			|| TOKENS[AppData.RATING.ordinal()].equals("3")
			|| TOKENS[AppData.RATING.ordinal()].equals("4")
			|| TOKENS[AppData.RATING.ordinal()].equals("5")
			&& TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length == 2 )
		{	return true;	}
		return false;
	}

	private static void parseReviews() throws Exception
	{
		for ( String category : CATEGORIES )
		{
			parseFile(getCategoryFileName(category, FREE), FREE);
			parseFile(getCategoryFileName(category, PAID), PAID);
			CATS_COUNTED++;
			System.out.println(CAT_AUTHOR_MAP.size());
			System.out.println(CAT_RATED_MAP.size());
			writeCatAF(category);
		}
	}

	private static void parseFile(String fileName, boolean isFree) throws IOException
	{
		int rating = 0;
		String author = "";
		READER = new BufferedReader(new FileReader(fileName));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )	{	LINES_READ++;	}
			TOKENS = LINE.split(PIPE);
			if ( !lineIsValid() || TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length != 2)
			{
				if ( !LINE.equals("") )
				{
					BAD_SET.add(fileName + "||" + LINE);
					BAD_LINES++;
				}
				continue;
			}
			rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
			author = TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1];
			addToMap(author, rating, isFree);
		}
		READER.close();
	}

	private static void addToMap(String author, int rating, boolean isFree)
	{
		COUNT = CAT_AUTHOR_MAP.containsKey(author) ? CAT_AUTHOR_MAP.get(author) : 0;
		COUNTS = CAT_RATED_MAP.containsKey(author) ? CAT_RATED_MAP.get(author) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		COUNT++;
		if ( isFree )
		{
			COUNTS[rating-1]++;
			COUNTS[AF.FALL.ordinal()]++;
		}
		else
		{
			COUNTS[rating-1+6]++;	// PAID RATING OFFSET = 6. SEE AF ENUM
			COUNTS[AF.PALL.ordinal()]++;
		}
		CAT_AUTHOR_MAP.put(author, COUNT);
		CAT_RATED_MAP.put(author, COUNTS);
		REVIEW_COUNT++;
	}

	private static void writeCatAF(String category) throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(CAT_FOLDER + category + "-af" + ".tsv"));
		Map<String, Integer> theMap = sortByComparator(CAT_AUTHOR_MAP);
		for ( Entry<String, Integer> entry : theMap.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());

			COUNTS = CAT_RATED_MAP.containsKey(entry.getKey()) ? CAT_RATED_MAP.get(entry.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			for ( Integer i : COUNTS )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(i);
			}
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
		consolidateMaps();
	}

	private static void consolidateMaps()
	{
		Integer[] ratedCounts;
		for ( Entry<String, Integer> entry : CAT_AUTHOR_MAP.entrySet() )
		{
			COUNT = AUTHOR_MAP.containsKey(entry.getKey()) ? AUTHOR_MAP.get(entry.getKey()) : 0;
			COUNT += entry.getValue();
			AUTHOR_MAP.put(entry.getKey(), COUNT);
		}
		for ( Entry<String, Integer[]> entryR : CAT_RATED_MAP.entrySet() )
		{
			COUNTS = RATED_MAP.containsKey(entryR.getKey()) ? RATED_MAP.get(entryR.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			ratedCounts = entryR.getValue();
			for ( int i = 0; i < COUNTS.length; i++ )
			{	COUNTS[i] += ratedCounts[i];	}

			RATED_MAP.put(entryR.getKey(), COUNTS);
		}
		CAT_AUTHOR_MAP.clear();
		CAT_RATED_MAP.clear();
	}

	private static void writeAF() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "all-af" + ".tsv"));
		Map<String, Integer> resultMap = sortByComparator(AUTHOR_MAP);

		for ( Entry<String, Integer> entry : resultMap.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue());

			COUNTS = RATED_MAP.containsKey(entry.getKey()) ? RATED_MAP.get(entry.getKey()) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			for ( Integer i : COUNTS )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(i);
			}

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeBadLines() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.tsv"));

		for ( String line : BAD_SET )
		{
			WRITER.write(line.trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{   this.map = map; }

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{   return c;   }
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}
