import java.io.*;

import java.text.NumberFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class TermFreq
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};
	final static int MAX_TOKENS = AppData.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	private static String IN_FILE = "";
	private static String CAT = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();

	private static TextCleaner CLEANER;
	private static TFIDFer TFIDFER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Provide review file for text frequency analysis.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		IN_FILE = args[0];
		CAT = IN_FILE.substring(3, IN_FILE.length());

		StringUtil sU = new StringUtil();
		CLEANER = new TextCleaner("./assets", false, sU);
		TFIDFER = new TFIDFer(sU);
		findReviews(fileInPath(IN_FILE));

		// DelimitedWriter writer = new DelimitedWriter(OUT_DELIMITER, fileOutPath("star-tf-" + CAT));
		// TFIDFER.writeTFs(writer, 0);
		DelimitedWriter writer = new DelimitedWriter(OUT_DELIMITER, "./reports/cat-tfidf-" + CAT + ".tsv");
		TFIDFER.calculateTfIdfs();
		TFIDFER.writeTFIDFs(writer, 0);
		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	private static String fileInPath(String fileName)
	{	return String.format("./reviews/data/%s_reviews.csv", fileName);	}

	private static String fileOutPath(String fileName)
	{   return String.format("./reports/store-star-tf/%s.tsv", fileName);	}

	private static void findReviews(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			String appID = TOKENS[AppData.APP_ID.ordinal()];
			String rating = TOKENS[AppData.RATING.ordinal()];
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			review = CLEANER.correct(review);
			TFIDFER.countWords(appID, review);
		}
		reader.close();
	}
}
