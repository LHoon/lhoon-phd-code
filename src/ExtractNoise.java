import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class ExtractNoise
{
	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		BufferedReader reader = new BufferedReader(new FileReader(args[0]));
		BufferedWriter writer = new BufferedWriter(new FileWriter("./wip/doublepipes.csv"));
		String[] tokens;

		for ( String line = ""; line != null; line = reader.readLine() )
		{
			tokens = line.split("\\|\\|");
			if( tokens.length != 13 )
			{
				writer.write(line);
				writer.newLine();
			}
		}
		reader.close();
		writer.close();
	}
}
