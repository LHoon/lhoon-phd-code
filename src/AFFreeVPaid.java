import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class AFFreeVPaid
{
	public enum AF
	{	AUTHOR, TOTAL, FREE, PAID	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX__TOKENS = AF.values().length;
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/author-cat-clean-data/";
	final static String IN = "./thesis/author-cat-clean-data/authors/";
	final static String OUT = "./thesis/author-cat-clean-data/reports/";

	final static String FOLDER = "./thesis/author-cat-clean-data/";
	final static String CAT_FOLDER = FOLDER + "categories/";
	final static String AUTHOR_FOLDER = FOLDER + "authors/";
	final static String REPORT_FOLDER = FOLDER + "reports/";

	// Numerical indexes to track the review processing
	private static Integer REVIEW_COUNT = 0;
	private static Integer LINES_READ = 0;
	private static Integer CATS_COUNTED = 0;
	private static Integer BAD_LINES = 0;
	private static Integer COUNT = 0;
	private static Integer[] COUNTS = {0,0,0,0,0};	// 1, 2, 3, 4, 5
	private static String AUTHOR = "";

	/** File I/O */
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	private static String[] TOKENS = {"", ""};
	private static String LINE = "";
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();

	private static Map<Integer, Integer> REPORT_RANGE = new HashMap<Integer, Integer>();
	private static Map<String, Map<String, Integer[]>> CAT_AUTHOR = new HashMap<String, Map<String, Integer[]>>();
	private static Map<String, Map<String, Integer[]>> AUTHOR_CAT = new HashMap<String, Map<String, Integer[]>>();
	private static Map<String, Integer> AUTHOR_MAP = new HashMap<String, Integer>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		loadCategories("./assets/categories.txt");
		parseReviews();
		sortMap();
		writeAuthors();
		writeCatAFs();
		writeAFPerCat();
		writeBadLines();
		writeNReviewAF();
		writeCatSummary();
		writeReviewFreqs();

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(CATEGORIES.size() + " categories processed, " + formatNumber(REVIEW_COUNT) + " reviews decomposed, from " + formatNumber(AUTHOR_MAP.size()) + " authors, with " + formatNumber(BAD_SET.size()) + " bad lines out of " + formatNumber(LINES_READ) + " lines, read in " + timeTaken);
	}

	private static String getCategoryFileName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		// return String.format("./reviews/data/us_%s_reviews.csv", getCatName(category, isFree));
		return String.format("./reviews/clean-data/tab/%s.tsv", getCatName(category, isFree));
	}

	private static String getCatName(String category, boolean isFree)
	{
		String price = isFree ? "free" : "paid";
		return String.format("%s_%s", price, category);
	}

	private static void loadCategories(String fileName) throws IOException
	{
		READER = new BufferedReader(new FileReader(fileName));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( !LINE.equals("") )
			{	CATEGORIES.add(minorClean(LINE));	}
		}
		READER.close();
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws Exception
	{
		for ( String category : CATEGORIES )
		{
			parseFile(category, FREE);
			parseFile(category, PAID);
		}
	}

	private static void parseFile(String category, boolean isFree) throws IOException
	{
		int rating = 0;
		String author = "";
		READER = new BufferedReader(new FileReader(getCategoryFileName(category, isFree)));
		for ( String LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			if ( LINE.equals("") )	{	continue;	}
			LINES_READ++;

			TOKENS = LINE.split(TAB);

			if ( TOKENS.length == MAX_DATA_TOKENS
				&& TOKENS[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				if ( TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL).length > 1 )
				{
					rating = Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);
					author = TOKENS[AppData.AUTHOR_ID.ordinal()].split(EQUAL)[1];
					addToMap(author, category, rating, isFree);
					continue;
				}
			}
			BAD_SET.add(String.format("%s%s%s", getCategoryFileName(category, isFree), TAB, LINE));
		}
		READER.close();
	}

	private static void addToMap(String author, String category, int rating, boolean isFree)
	{
		COUNT = AUTHOR_MAP.containsKey(author) ? AUTHOR_MAP.get(author) : 0;
		COUNT++;
		AUTHOR_MAP.put(author, COUNT);	// Persist (AUTHOR_ID, TOTAL)

		Map<String, Integer[]> catMap = CAT_AUTHOR.containsKey(category) ? CAT_AUTHOR.get(category) : new HashMap<String, Integer[]>();
		COUNTS = catMap.containsKey(author) ? catMap.get(author) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		if ( isFree )
		{
			COUNTS[rating-1]++;
			COUNTS[AF.FALL.ordinal()]++;
		}
		else
		{
			COUNTS[rating-1+6]++;	// PAID RATING OFFSET = 6. SEE AF ENUM
			COUNTS[AF.PALL.ordinal()]++;
		}
		catMap.put(author, COUNTS); 	// Persist (author, RATINGS[])
		CAT_AUTHOR.put(category, catMap);	// Persist (AUTHOR_ID, MAP)

		Map<String, Integer[]> authorMap = AUTHOR_CAT.containsKey(author) ? AUTHOR_CAT.get(author) : new HashMap<String, Integer[]>();
		COUNTS = authorMap.containsKey(category) ? authorMap.get(category) : new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		if ( isFree )
		{
			COUNTS[rating-1]++;
			COUNTS[AF.FALL.ordinal()]++;
		}
		else
		{
			COUNTS[rating-1+6]++;	// PAID RATING OFFSET = 6. SEE AF ENUM
			COUNTS[AF.PALL.ordinal()]++;
		}
		authorMap.put(category, COUNTS);
		AUTHOR_CAT.put(author, authorMap);	// Persist (AUTHOR_ID, MAP)
		REVIEW_COUNT++;
	}

	private static void sortMap()
	{	AUTHOR_MAP = sortByComparator(AUTHOR_MAP);	}

	private static void writeReviewFreqs() throws Exception
	{
		Map<Integer, Integer> reviewFreqs = new TreeMap<Integer, Integer>();
		Integer reviewCount;
		Integer authorCount;

		for ( Entry<String, Integer> author : AUTHOR_MAP.entrySet() )
		{
			reviewCount = author.getValue();
			authorCount = reviewFreqs.containsKey(reviewCount) ? reviewFreqs.get(reviewCount) : 0;
			authorCount++;
			reviewFreqs.put(reviewCount, authorCount);
		}
		WRITER = new BufferedWriter(new FileWriter(AUTHOR_FOLDER + "frequency-report" + ".tsv"));

		for ( Entry<Integer, Integer> freq : reviewFreqs.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(freq.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(freq.getValue());
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}

		WRITER.close();
	}

	private static void writeAuthors() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(AUTHOR_FOLDER + "af-report" + ".tsv"));
		Map<String, Integer[]> authorMap;
		int freeTotal;
		int paidTotal;

		for ( Entry<String, Integer> author : AUTHOR_MAP.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(author.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(author.getValue());	//author total
			authorMap = AUTHOR_CAT.containsKey(author.getKey()) ? AUTHOR_CAT.get(author.getKey()) : new HashMap<String, Integer[]>();
			freeTotal = 0;
			paidTotal = 0;

			for ( Entry<String, Integer[]> authorEntry : authorMap.entrySet() )
			{
				COUNTS = authorEntry.getValue();
				freeTotal += COUNTS[AF.FALL.ordinal()];	// free total
				paidTotal += COUNTS[AF.PALL.ordinal()]; // paid total
			}

			S_BUILDER.append(TAB);
			S_BUILDER.append(freeTotal);
			S_BUILDER.append(TAB);
			S_BUILDER.append(paidTotal);

			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeAFPerCat() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(AUTHOR_FOLDER+ "af-per-cat.tsv"));
		Map<String, Integer[]> authorMap;
		String category = "";

		for ( Entry<String, Integer> author : AUTHOR_MAP.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(author.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(author.getValue());
			authorMap = AUTHOR_CAT.containsKey(author.getKey()) ? AUTHOR_CAT.get(author.getKey()) : new HashMap<String, Integer[]>();
			for ( Entry<String, Integer[]> authorEntry : authorMap.entrySet() )
			{
				category = authorEntry.getKey();
				COUNTS = authorEntry.getValue();
				S_BUILDER.append(TAB);
				S_BUILDER.append(category);
				S_BUILDER.append(TAB);
				S_BUILDER.append(COUNTS[AF.FALL.ordinal()]);
				S_BUILDER.append(TAB);
				S_BUILDER.append(COUNTS[AF.PALL.ordinal()]);
			}
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeCatAFs() throws Exception
	{
		int total = 0;
		for ( String category : CATEGORIES)
		{
			WRITER = new BufferedWriter(new FileWriter(CAT_FOLDER + category +"-af" + ".tsv"));
			Map<String, Integer[]> catMap = CAT_AUTHOR.get(category);

			for ( Entry<String, Integer[]> catEntry : catMap.entrySet() )
			{
				S_BUILDER.setLength(0);
				S_BUILDER.append(catEntry.getKey());
				S_BUILDER.append(TAB);
				S_BUILDER.append(AUTHOR_MAP.get(catEntry.getKey()));
				S_BUILDER.append(TAB);
				S_BUILDER.append(catEntry.getValue()[AF.FALL.ordinal()] + catEntry.getValue()[AF.PALL.ordinal()]);

				for ( Integer count : catEntry.getValue() )
				{
					S_BUILDER.append(TAB);
					S_BUILDER.append(count);
				}
				WRITER.write(S_BUILDER.toString().trim());
				WRITER.newLine();
			}
			WRITER.close();
		}
	}

	private static void writeNReviewAF() throws Exception
	{
		REPORT_RANGE.put(1, 1);
		REPORT_RANGE.put(2, 2);
		REPORT_RANGE.put(3, 3);
		REPORT_RANGE.put(4, 4);
		REPORT_RANGE.put(5, 5);
		REPORT_RANGE.put(6, 15);
		REPORT_RANGE.put(16, 50);
		REPORT_RANGE.put(51, 340);

		for ( Entry<Integer, Integer> range : REPORT_RANGE.entrySet() )
		{	getNReviewAF(range.getKey(), range.getValue());	}
	}

	private static void getNReviewAF(int min, int max) throws Exception
	{
		Map<String, Integer[]> authorMap;
		String category = "";
		Integer[] reviews = new Integer[] {0,0}; // FREE, PAID
		Map<String, Integer[]> resultMap = new TreeMap<String, Integer[]>();
		for ( Entry<String, Integer> author : AUTHOR_MAP.entrySet() )
		{
			if ( author.getValue() >= min && author.getValue() <= max )
			{
				authorMap = AUTHOR_CAT.containsKey(author.getKey()) ? AUTHOR_CAT.get(author.getKey()) : new HashMap<String, Integer[]>();
				for ( Entry<String, Integer[]> authorEntry : authorMap.entrySet() )
				{
					category = authorEntry.getKey();
					reviews = resultMap.containsKey(category) ? resultMap.get(category) : new Integer[] {0,0};
					reviews[0] += authorEntry.getValue()[AF.FALL.ordinal()];
					reviews[1] += authorEntry.getValue()[AF.PALL.ordinal()];
					resultMap.put(category, reviews);
				}
			}
		}
		writeMap(resultMap, min + "-" + max);
	}

	private static void writeMap(Map<String, Integer[]> theMap, String range) throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(REPORT_FOLDER + range + "-af" + ".tsv"));

		for ( Entry<String, Integer[]> entry: theMap.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue()[0]);
			S_BUILDER.append(TAB);
			S_BUILDER.append(entry.getValue()[1]);
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeCatSummary() throws IOException
	{
		Integer[] total;
		Map<String, Integer[]> summaryMap = new TreeMap<String, Integer[]>();
		for ( String category : CATEGORIES)
		{
			Map<String, Integer[]> catMap = CAT_AUTHOR.get(category);
			total = new Integer[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

			for ( Entry<String, Integer[]> catEntry : catMap.entrySet() )
			{
				COUNTS = catEntry.getValue();
				total[0]++;
				for ( int i = 0; i < COUNTS.length; i++ )
				{	total[i+1] += COUNTS[i];	}
			}
			summaryMap.put(category, total);
		}
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "cat-summary.tsv"));
		for ( Entry<String, Integer[]> summary : summaryMap.entrySet() )
		{
			S_BUILDER.setLength(0);
			S_BUILDER.append(summary.getKey());
			for ( Integer count : summary.getValue() )
			{
				S_BUILDER.append(TAB);
				S_BUILDER.append(count);
			}
			WRITER.write(S_BUILDER.toString().trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static void writeBadLines() throws Exception
	{
		WRITER = new BufferedWriter(new FileWriter(FOLDER + "bad-lines.txt"));

		for ( String line : BAD_SET )
		{
			WRITER.write(line.trim());
			WRITER.newLine();
		}
		WRITER.close();
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
