import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SeparateReviews
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// Random Number Generator values
	static Integer RNG_MAX = 400;
	static Integer RNG_MIN = 1;
	final static Integer RNG_SEED = 1;
	static Random GENERATOR = new Random(RNG_SEED);
	static TextCleaner CLEANER;

	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static int RATING_RNG = 5;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static Integer THRESHOLD;
	private static Integer MIN_WORDS;
	private static Integer MIN_REVIEWS;
	private static String OUTFILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static List<Integer> RNG_VALUES = new ArrayList<Integer>();
	private static Map<String, String> APP_DETAILS = new HashMap<String, String>();
	private static Map<String, Map<Integer, List<String>>> REVIEWS = new TreeMap<String, Map<Integer, List<String>>>();
	private static Set<String> PROBLEM_REVIEWS = new HashSet<String>();

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Insufficient args provided for Review Extraction Phase.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		OUTFILE = args[0];
		String category = OUTFILE;
		try
		{
			MIN_WORDS = Integer.parseInt(args[1]);
			MIN_REVIEWS = Integer.parseInt(args[2]);
		}
		catch ( Exception e )
		{
			THRESHOLD = 20;
			MIN_WORDS = 10;
			MIN_REVIEWS = 30;
		}
		// OUTFILE = category.substring(3, category.length());

		getAppDetails(category);

		CLEANER = new TextCleaner();
		GENERATOR = new Random(RNG_SEED);

		System.out.println("Extracting random reviews from " + category + ".");
		findReviews(getPath(category, DATA));
		cullReviews();
		writeAppReviews();
		// writeProblemReviews();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reviews/%s.tsv", fileName);	}

	private static void populateRNGList()
	{
		for(int i = 0; i < (THRESHOLD * RATING_RNG); i++)
		{	RNG_VALUES.add(GENERATOR.nextInt((RNG_MAX+1) - RNG_MIN) + RNG_MIN);	}
	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String filePath, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(line);
			WRITER.newLine();
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void writeProblemReviews() throws IOException
	{
		for( String problem : PROBLEM_REVIEWS )
		{	writeLine(outPath(OUTFILE+"-errors"), problem);	}
	}

	private static void writeAppReviews() throws IOException
	{
		int rngIdx = 0;
		int breakpoint = 0;
		int currentRating = 1;
		List<String> ratedList;

		for( Entry<String, Map<Integer, List<String>>> entry : REVIEWS.entrySet() )
		{
			Map<Integer, List<String>> appReviews = entry.getValue();
			for ( Entry<Integer, List<String>> nestedEntry : appReviews.entrySet() )
			{
				for ( String review : nestedEntry.getValue() )
				{
					// writeLine(outPath(OUTFILE+"/"+MIN_WORDS+"words-"+MIN_REVIEWS+"reviews/"+entry.getKey()), review);
					writeLine(outPath(OUTFILE+"/"+MIN_WORDS+"words-"+MIN_REVIEWS+"reviews/"+entry.getKey()), review);
				}
			}
			// ratedList= entry.getValue();
			// try
			// {	currentRating = Integer.parseInt(entry.getKey());	}
			// catch (NumberFormatException nfe) {	break;	}
			// breakpoint = THRESHOLD * currentRating;
			// while ( rngIdx < breakpoint )
			// {
			// 	writeLine(outPath(OUTFILE+"/"+entry.getKey()), ratedList.get);
			// 	rngIdx++;
			// 	if(rngIdx > RNG_VALUES.size())
			// 	{	break;	}
			// }
		}
	}

	private static void getAppDetails(String fileName) throws IOException
	{
		int appsFound = 0;
		String appMetaDetails = "";
		READER = new BufferedReader(new FileReader(getPath(fileName, INDEX)));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_META_TOKENS )
			{	continue;	}
			appMetaDetails = formatAppDetails(TOKENS);
			// if( TOKENS.length < 9 ) {	continue;	}
			APP_DETAILS.put(TOKENS[AppMeta.APP_ID.ordinal()], appMetaDetails);
		}
		RNG_MAX = APP_DETAILS.size();
		READER.close();
	}

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		Set<String> idSet = APP_DETAILS.keySet();
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{
				PROBLEM_REVIEWS.add(LINE);
				continue;
			}
			if ( idSet.contains(TOKENS[AppData.APP_ID.ordinal()]) )
			{
				int rating = 0;
				try
				{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
				catch (NumberFormatException nfe)
				{
					PROBLEM_REVIEWS.add(LINE);
					continue;
				}
				String formattedReview = formatTokens(TOKENS);
				if( formattedReview.length() > 0 )
				{	saveReview(TOKENS[AppData.APP_ID.ordinal()], rating, formattedReview);	}
			}
		}
		READER.close();
	}

	private static void saveReview(String appId, int rating, String review)
	{
		Map<Integer, List<String>> ratedReviews = REVIEWS.containsKey(appId) ? REVIEWS.get(appId) : new TreeMap<Integer, List<String>>();
		List<String> reviewList = ratedReviews.containsKey(rating) ? ratedReviews.get(rating) : new ArrayList<String>();
		reviewList.add(review);
		ratedReviews.put(rating, reviewList);
		REVIEWS.put(appId, ratedReviews);
	}

	private static void cullReviews()
	{
		System.out.print("Before: "+REVIEWS.size()+"\t");
		Set<String> cullSet = new HashSet<String>();
		for( Entry<String, Map<Integer, List<String>>> entry : REVIEWS.entrySet() ) // foreach app
		{
			Map<Integer, List<String>> appReviews = entry.getValue();
			for ( Entry<Integer, List<String>> nestedEntry : appReviews.entrySet() )
			{
				if( nestedEntry.getValue().size() < MIN_REVIEWS )
				{	cullSet.add(entry.getKey());	}
			}
		}

		for( Iterator<Entry<String, Map<Integer, List<String>>>> itr = REVIEWS.entrySet().iterator();itr.hasNext(); )
		{
			Entry<String, Map<Integer, List<String>>> entry = itr.next();
			if ( cullSet.contains(entry.getKey()) )
			{	itr.remove();	}
		}
		System.out.println("After: "+REVIEWS.size());
		// for( Entry<String, Map<Integer, List<String>>> entry1 : REVIEWS.entrySet() )
		// {	System.out.println(APP_DETAILS.get(entry1.getKey()));	}
	}

	private static void append(String fragment)
	{
		S_BUILDER.append(fragment);
		S_BUILDER.append(OUT_DELIMITER);
	}

	private static void wipeBuilder()
	{
		if ( S_BUILDER.length() > 0 )
		{   S_BUILDER.delete(0, S_BUILDER.length());    }
	}

	private static String formatAppDetails(String[] tokens)
	{
		wipeBuilder();
		append(tokens[AppMeta.APP_ID.ordinal()]);
		append(tokens[AppMeta.APP_NAME.ordinal()]);
		append(tokens[AppMeta.PRICE.ordinal()]);
		append(tokens[AppMeta.RANK.ordinal()]);
		return S_BUILDER.toString();
	}

	private static String formatTokens(String[] tokens)
	{
		wipeBuilder();
		String reviewTitle = CLEANER.applyLookup(tokens[AppData.REVIEW_TITLE.ordinal()]);
		String reviewBody = CLEANER.applyLookup(tokens[AppData.REVIEW_BODY.ordinal()]);
		if( (reviewTitle + reviewBody).split(" ").length >= MIN_WORDS )
		{
			S_BUILDER.append(APP_DETAILS.get(tokens[AppData.APP_ID.ordinal()]));
			append(tokens[AppData.REVIEW_ID.ordinal()]);
			String authorLine = tokens[AppData.AUTHOR_ID.ordinal()];
			String authorID = "";
			int authorIndex = authorLine.indexOf("=");
			if ( authorIndex != -1 )
			{	authorID = authorLine.substring((authorIndex + 1), authorLine.length());	}
			else
			{	authorID = authorLine;	}
			append(authorID);
			append(tokens[AppData.REVIEW_DATE.ordinal()]);
			append(tokens[AppData.APP_VERSION.ordinal()]);
			append(tokens[AppData.RATING.ordinal()]);
			append(tokens[AppData.REVIEW_TITLE.ordinal()]);
			append(tokens[AppData.REVIEW_BODY.ordinal()]);
			append(Integer.toString(tokens[AppData.REVIEW_TITLE.ordinal()].split(" ").length));
			append(Integer.toString(tokens[AppData.REVIEW_BODY.ordinal()].split(" ").length));
			append(reviewTitle);
			append(reviewBody);
			append(Integer.toString(reviewTitle.split(" ").length));
			append(Integer.toString(reviewBody.split(" ").length));
		}
		return S_BUILDER.toString().trim();
	}
}

class TextCleaner
{
	private Map<String, String> abbrvTypos;
	private Pattern validPattern;
	private StringBuilder sBuilder;

	public TextCleaner() throws IOException
	{
		abbrvTypos = new TreeMap<String, String>();
		validPattern = Pattern.compile("[0-9]\\d{0,9}(\\.\\d{1,9})+?|[\\w\\d'`.,$/]+");
		sBuilder = new StringBuilder();
		loadTypos("./assets/abbrv-txt-spk.csv");
		loadTypos("./assets/common-typos.csv");
		loadTypos("./assets/aux-common-typos.csv");
	}

	/**
	 * Populates the typo lookup map based on the textfile. The raw word forms
	 * the key of the map, with the corrected word as the value.
	 */
	private void loadTypos(String fileName) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if(line.length() > 1)
			{
				String tokens[] = line.split(",");
				abbrvTypos.put(minorClean(tokens[0]), minorClean(tokens[1]));
			}
		}
		reader.close();
	}

	/**
	 * Return the param that has been lowercased and trimmed of trailing spaces.
	 */
	public String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	/**
	 * String "cleaning". Removes commas, apostraphes, periods and dashes. Does
	 * other stuff, self explanatory based on replaceAll statements.
	 */
	public String stringTreatment(String review)
	{
		review = minorClean(review);
		review = review.replaceAll("(\\.)\\1{1,}", ".");
		review = review.replaceAll("\\`","\\'");
		review = review.replaceAll("\\&","\\and");
		review = review.replaceAll("\\bwon't\\b","will not");
		review = review.replaceAll("\\bcan t\\b"," can't ");
		review = review.replaceAll("\\bdon t\\b","don't");
		review = review.replaceAll("\\bcan't\\b","cannot");
		review = review.replaceAll("'m\\b", "\\ am");
		review = review.replaceAll("\\bs\\b","s ");
		review = review.replaceAll("'ll\\b", "\\ will");
		review = review.replaceAll("'re\\b", "\\ are");
		review = review.replaceAll("'s\\b", "\\ is");
		review = review.replaceAll("\\bi'd\\b","\\ i had");
		review = review.replaceAll("'d\\b","\\ would");
		review = review.replaceAll("'ve\\b","\\ have");
		review = review.replaceAll("n't\\b","\\ not");
		return minorClean(review);
	}

	public String applyLookup(String text)
	{
		boolean wordAdded = false;
		if ( sBuilder.length() > 0 )
		{   sBuilder.delete(0, sBuilder.length());    }

		Matcher matcher = validPattern.matcher(stringTreatment(text));
		while (matcher.find())
		{
			String wordToAdd = matcher.group();
			if(!wordToAdd.matches(".*\\d.*"))
			{	wordToAdd = abbrvTypos.containsKey(wordToAdd) ? wordToAdd = abbrvTypos.get(wordToAdd) : wordToAdd;	}

			sBuilder.append(wordToAdd);
			sBuilder.append(" ");
			wordAdded = true;
		}

		if(wordAdded)	// Remove the trailing space
		{   sBuilder.deleteCharAt(sBuilder.length() - 1);   }
		return sBuilder.toString();
	}
}
