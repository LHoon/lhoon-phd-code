import java.io.*;

import java.text.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.*;

import hoon.textfx.*;
import hoon.util.*;

public class AppReviewCounter
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC, BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	final static boolean FREE = true;
	final static boolean PAID = false;
	final static int MAX_DATA_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String PIPE = "\\|\\|";
	final static String TAB = "\t";
	final static String FOLDER = "./thesis/final/";
	final static String ZOMBIE = "354902315";

	// private static Set<String> CATEGORIES = new TreeSet<String>();
	private static Set<String> BAD_SET = new HashSet<String>();
	private static Map<Integer, Integer> ZOMBIE_REVIEWS = new TreeMap<Integer, Integer>();


	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		long startTime = System.currentTimeMillis();

		// loadCategories("./assets/categories.txt");
		parseReviews();

		for ( Entry<Integer, Integer> review : ZOMBIE_REVIEWS.entrySet() )
		{	System.out.println(review.getKey() + TAB + review.getValue() );	}

		long millis = System.currentTimeMillis() - startTime;
		String timeTaken = String.format("%d minute(s) and %d second(s).",
			TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("");
		System.out.println(timeTaken);
	}

	private static String minorClean(String text)
	{	return text.toLowerCase(Locale.ENGLISH).trim();	}

	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	private static void parseReviews() throws IOException
	{
		Integer count = 0;
		Integer size = 0;
		String appID = "";
		String[] tokens;
		BufferedReader reader = new BufferedReader(new FileReader("./reviews/clean-data/tab/free_games.tsv"));
		for ( String line = ""; line != null; line = reader.readLine() )
		{
			if ( line.equals("") )	{	continue;	}

			tokens = line.split(TAB);

			if ( tokens.length == MAX_DATA_TOKENS
				&& tokens[AppData.RATING.ordinal()].matches("1|2|3|4|5") )
			{
				if ( tokens[AppData.APP_ID.ordinal()].equals(ZOMBIE) )
				{
					appID = tokens[AppData.APP_ID.ordinal()];
					size = Integer.parseInt(tokens[AppData.TITLE_WC.ordinal()]) + Integer.parseInt(tokens[AppData.BODY_WC.ordinal()]);


					count = ZOMBIE_REVIEWS.containsKey(size) ? ZOMBIE_REVIEWS.get(size) : 0;
					count++;
					ZOMBIE_REVIEWS.put(size, count);
					continue;
				}
			}
			// BAD_SET.add(String.format("%s%s%s", getCategoryFileName(category, isFree), TAB, line));
		}
		reader.close();
		// sortMap();
	}

	// private static void sortMap()
	// {	ZOMBIE_REVIEWS = sortByComparator(ZOMBIE_REVIEWS);	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
	{

		// Convert Map to List
		List<Map.Entry<String, Integer>> list =
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
