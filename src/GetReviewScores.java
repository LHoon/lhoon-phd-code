import java.io.*;

import java.text.DecimalFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hoon.io.*;
import hoon.textfx.*;
import hoon.util.*;

public class GetReviewScores
{
	public enum Score
	{
		APP_ID, APP_NAME, REV_ID, RATING, TITLE, BODY, SCORE
	};

// Q1||405280065S||589045108||US Army APFT-Body Fat Calculator||5||Awesome App||This is a great and accurate app!  Has everything that you need at you fingertips.
	public enum Survey
	{
		QN, APP_ID, APP_NAME, REV_ID, RATING, TITLE, BODY, U1, U2, U3, U4, U5, SUM
	};

	final static int SURVEY_TOKS = Survey.values().length;
	final static int SCORE_TOKS = Score.values().length;
	final static String TAB = "\t";
	final static Pattern WORDS = Pattern.compile("[a-zA-z]+");

	private static Double HIGHEST = 0.0;//2202585031524119;
	private static Double LOWEST = 0.0;
	private static Double[] TIER = new Double[4];
	private static String FILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static StringUtil S_UTIL = new StringUtil();

	private static Map<String, Double> SCORES = new HashMap<String, Double>();
	// private static Map<String, String[]> SURVEY = new HashMap<String, String[]>();

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		// if (args.length < 1)
		// {
		// 	System.out.println("Please provide price and category parameter (free_health, paid_entertainment, etc) for analysis context.");
		// 	System.exit(1);
		// }
		long start = System.currentTimeMillis();

		parseScores("./reports/survey-10/scored/free_health-scores-moreThan10.tsv");
		parseSurvey("./reviews/survey-dataset.csv");

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review value aggregation completed in " + elapsed);
	}

	private static String outPath()
	{	return String.format("./reports/survey-scores.tsv", FILE);	}

	private static void parseScores(String filePath) throws IOException
	{
		Double value;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, TAB);
			if( TOKENS.length != SCORE_TOKS )	{	continue;	}
			try
			{
				value = Math.abs(Double.parseDouble(TOKENS[Score.SCORE.ordinal()]));
				SCORES.put(TOKENS[Score.REV_ID.ordinal()], value);
			}
			catch (NumberFormatException nfe)	{	continue;	}
		}
		reader.close();
		System.out.println(SCORES.size());
	}

	private static void parseSurvey(String filePath) throws IOException
	{
		Double value;
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		BufferedWriter writer = new BufferedWriter(new FileWriter("./reports/scored-survey.csv"));
		for ( LINE = ""; LINE != null; LINE = reader.readLine() )
		{
			TOKENS = S_UTIL.tokenise(LINE, "\\|\\|");

			if( TOKENS.length != SURVEY_TOKS )	{	continue;	}
			value = SCORES.containsKey(TOKENS[Survey.REV_ID.ordinal()]) ? SCORES.get(TOKENS[Survey.REV_ID.ordinal()]) : 0.0;
			System.out.println(LINE+"||"+value.toString());
			writer.write(LINE);
			writer.write("||");
			writer.write(value.toString());
			writer.newLine();
		}
		reader.close();
		writer.close();
	}

	private static void print()
	{
		System.out.println("Populated Tiers: " + SCORES.size());
		for ( Entry<String, Double> entry : SCORES.entrySet() )
		{	System.out.println(entry.getKey() + " > " + entry.getValue());	}
	}
}
