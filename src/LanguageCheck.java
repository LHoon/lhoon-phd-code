package hoon.textfx;

import java.io.*;
import java.util.*;

import hoon.util.StringUtil;

/**
 * Created by lhoon on 04/02/2015.
 * TFIDF Calculator.
 */
public class LanguageCheck
{
	private Set<String> languageDictionary;

	/**
	 * Just an object wrapper for a Set of valid language vocabulary.
	 */
	public LanguageCheck(StringUtil sUtil)
	{	this.languageDictionary = new HashSet<String>();	}

	public void clear()
	{	this.languageDictionary.clear();	}

	public boolean isValidWord(String text)
	{	return this.languageDictionary.contains(text);	}

	/**
	 * Populates the lookup set with textfile input.
	 */
	private void loadStructure(String fileName) throws IOException
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			for ( String line = ""; line != null; line = this.reader.readLine() )
			{
				if ( line.length() > 1 )
				{
					if ( isMap )
					{
						tokens = this.sUtil.tokenise(line, delimiter);
						theMap.put(this.sUtil.minorClean(tokens[0]), this.sUtil.minorClean(tokens[1]));
					}
					else
					{   this.engLang.add(this.sUtil.minorClean(line)); }
				}
			}
		}
		catch ( IOException ioe )
		{	System.err.println("Error reading English Language Dictionary.");	}
		finally
		{	reader.close();	}
	}
}
