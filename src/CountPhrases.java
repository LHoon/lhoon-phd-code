import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import hoon.util.textfx.TextCleaner;

public class CountPhrases
{
	public enum AppMeta
	{	APP_NAME, APP_ID, COUNTRY, URL, PRICE, CURRENCY, BIN_SIZE, VERSION, RANK	};

	public enum AppData
	{
		APP_ID, REVIEW_ID, REVIEW_DATE, APP_VERSION, TITLE_WC, TITLE_CC, BODY_WC,
		BODY_CC, RATING, AUTHOR_NAME, AUTHOR_ID, REVIEW_TITLE, REVIEW_BODY
	};

	/* Configuration flags, declared for readability */
	// File extensions
	final static boolean DATA = true;
	final static boolean INDEX = false;
	final static boolean DO_APPEND = true;
	final static boolean NO_APPEND = false;
	final static int MAX_TOKENS = AppData.values().length;
	final static int MAX_META_TOKENS = AppMeta.values().length;
	final static String IN_DELIMITER = "\\|\\|";
	final static String OUT_DELIMITER = "\t";

	/* Working Objects, to hold Collections and Matchers while reducing overhead */
	// Assign the pattern.matcher(text) from the Sets to this matcher
	private static Integer WRITTEN = 0;
	private static Integer THRESHOLD;

	private static String CAT_PRICE = "";
	private static String OUTFILE = "";
	private static String LINE = "";
	private static String[] TOKENS;
	private static StringBuilder S_BUILDER = new StringBuilder();
	private static Map<String, int[]> PHRASES = new HashMap<String, int[]>();
    private static Map<String, Integer> PRE_SORT = new HashMap<String, Integer>();
	private static Map<String, Integer> POST_SORT;

	private static TextCleaner CLEANER;
	private static TextCleaner CLEANER2;
	private static Stanford LEMMATISER;

	// IO worker Objects
	private static BufferedReader READER;
	private static BufferedWriter WRITER;

	/**
	 * @param input file name
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Provide numeric Threshold value for unigram extraction.");
			System.exit(1);
		}
		long start = System.currentTimeMillis();

		CAT_PRICE = "us_free_health";
		try
		{	THRESHOLD = Integer.parseInt(args[0]);	}
		catch ( NumberFormatException nfe )
		{	THRESHOLD = 10;	}
		OUTFILE = THRESHOLD + "word-lemma-" + CAT_PRICE.substring(3, CAT_PRICE.length());

		CLEANER = new TextCleaner("./assets", false);
		CLEANER2 = new TextCleaner("./assets", true);
		LEMMATISER = new Stanford();
		findReviews(getPath(CAT_PRICE, DATA));//"./reviews/alls/us_free_all_reviews.csv");

		POST_SORT = new TreeMap<String, Integer>(new ValueComparer<String, Integer>(PRE_SORT));
		sortPhrases(5);
		// writePhrases();

		/* Calculate and Report Processing Time*/
		long millis = System.currentTimeMillis() - start;
		String elapsed = String.format("%d min, %d sec",
			TimeUnit.MILLISECONDS.toMinutes(millis),
			TimeUnit.MILLISECONDS.toSeconds(millis) -
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println("Review Extraction " + "completed in " + elapsed);
	}

	/**
	 * Format the filename into a filepath for TextFile constructor, shortens
	 * method params for readability.
	 * @param	isData boolean value to indicate review or index file
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @return	String value constructed from ./reviews/isData/fileName.csv
	 */
	private static String formatPath(String folder, String fileName)
	{	return String.format("./reviews/%s/%s.csv", folder , fileName);	}

	/**
	 * Generate filepath for review files.
	 * @param	fileName name of the file to be loaded, WITHOUT extension
	 * @param	isData boolean value to indicate review or index file
	 * @return	String value constructed from ./folder/fileName.extension
	 */
	private static String getPath(String fileName, boolean isData)
	{   return formatPath(isData ? "data" : "index" , (isData ? fileName + "_reviews" : fileName));	}

	/**
	 * Number prettification, 300000 becomes 300,000.
	 */
	private static String formatNumber(int value)
	{   return NumberFormat.getNumberInstance(Locale.US).format(value); }

	/**
	 * Generate filepath for WIP files.
	 * @param	fileName name of the file
	 * @return	String value constructed from ./wip/fileName.csv
	 */
	private static String outPath(String fileName)
	{   return String.format("./reports/%s.tsv", fileName);	}

	/**
	 * Method to append the line param to the file at filePath.
	 * @param	filePath name of the file to append to
	 * @param	line String value to write to file
	 */
	private static void writeLine(String filePath, String line) throws IOException
	{
		try
		{
			WRITER = new BufferedWriter(new FileWriter(filePath, true));
			WRITER.write(line);
			WRITER.newLine();
			WRITTEN++;
		}
		catch (IOException ioe)	{	ioe.printStackTrace();	}
		finally	{	WRITER.close();	}
	}

	private static void writePhrases() throws IOException
	{
		for( Entry<String, Integer> entry : POST_SORT.entrySet() )
		{
			if ( S_BUILDER.length() > 0 )
			{	S_BUILDER.delete(0, S_BUILDER.length());	}

			S_BUILDER.append(entry.getKey());
			S_BUILDER.append(OUT_DELIMITER);

			int[] values = PHRASES.get(entry.getKey());
			for ( int value : values )
			{
				S_BUILDER.append(value);
				S_BUILDER.append(OUT_DELIMITER);
			}
			writeLine(outPath(OUTFILE), S_BUILDER.toString().trim());
		}
	}

	// private static void writeReviewSpread() throws IOException
	// {
	// 	for( Entry<Integer, List<String>> entry : REVIEWS.entrySet() )
	// 	{
	// 		for ( String line : entry.getValue() )
	// 		{	writeLine(outPath(OUTFILE+"-"+entry.getKey()+"stars"), line);	}
	// 	}
	// }

	private static void findReviews(String filePath) throws IOException
	{
		READER = new BufferedReader(new FileReader(filePath));
		for ( LINE = ""; LINE != null; LINE = READER.readLine() )
		{
			TOKENS = LINE.split(IN_DELIMITER);
			if( TOKENS.length != MAX_TOKENS )
			{	continue;	}
			int rating = 0;
			try
			{	rating += Integer.parseInt(TOKENS[AppData.RATING.ordinal()]);	}
			catch (NumberFormatException nfe)
			{	continue;	}
			String review = TOKENS[AppData.REVIEW_TITLE.ordinal()] + " " + TOKENS[AppData.REVIEW_BODY.ordinal()];
			review = CLEANER.correct(review);
			System.out.println(review);
			review = lemmatisation(review);
			System.out.println(review);
			review = CLEANER2.correct(review);
			System.out.println(review);
			// getNGram(review, rating);
			// String formattedReview = formatTokens(TOKENS);
			// saveReviewToStructure(formattedReview, rating);
		}
		READER.close();
	}

	private static String lemmatisation(String text)
	{
		TOKENS = text.split("\\ ");
		if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

		List<String> lemmaList = LEMMATISER.lemmatise(text);
		for ( String s : lemmaList)
		{
			S_BUILDER.append(s);
			S_BUILDER.append(" ");
		}
		return S_BUILDER.toString().trim();
	}

	/**
     * Method to extract N Gram phrases out of the line of text based on the indexOfKeyword provided and THRESHOLD for depth.
     * If THRESHOLD = 3, the phrases 1, 2, 3, Key; 1, 2, Key, 3; 1, Key, 2, 3; Key, 1, 2, 3 are extracted.
     * Phrases with only one word (i.e. only the keyword) are ignored.
     */
    private static void getNGram(String line, int rating)
    {
    	int indexOfKeyWord = 0;
    	TOKENS = line.split("\\ ");
    	int tokenLength = TOKENS.length;
    	if ( S_BUILDER.length() > 0 )
		{	S_BUILDER.delete(0, S_BUILDER.length());	}

    	for(int i = 0; i <= THRESHOLD; i++)
    	{
    		int currentOffset = indexOfKeyWord - i;
    		if ( indexOfKeyWord < 0 )	{	continue;	}
    		boolean initialWord = true;
    		// Loop through each permutation of results
    		for (int j = 0; j <= THRESHOLD; j++)
    		{
    			int currentIdx = currentOffset + j;
    			// Loop through each word
    			if (currentIdx >= 0 && currentIdx < tokenLength)
    			{
    				// Do not append space if this is the first word being appended
	    			if (!initialWord)
	    			{   S_BUILDER.append(" ");   }
	    			else
	    			{   initialWord = false;   }
	    			S_BUILDER.append(TOKENS[currentIdx].toString().toLowerCase(Locale.ENGLISH));
    			}
    		}
    		// Store the phrase in the map if there are at least two words in the phrase
    		if(S_BUILDER.toString().matches(".*\\s+.*"))
    		{	savePhrase(S_BUILDER.toString().trim(), rating);  }
            S_BUILDER.delete(0, S_BUILDER.length());
    	}
    }

	private static void savePhrase(String phrase, int rating)
	{
		int[] phraseCounts = PHRASES.containsKey(phrase) ? PHRASES.get(phrase) : new int[]{0, 0, 0, 0, 0, 0};
		phraseCounts[rating - 1]++; //+= instances;
		phraseCounts[5]++;;
		PHRASES.put(phrase, phraseCounts);
	}

    /**
     * Sort by total number of instances the phrase (map key) occurs (represented by value).
     */
    private static void sortPhrases(int index)
    {
	    for (Entry<String, int[]> entry : PHRASES.entrySet())
	    {  PRE_SORT.put(entry.getKey(), entry.getValue()[index]);    }
        POST_SORT.putAll(PRE_SORT);
    }
}

/**
 * Override the comparator to allow for value based sorting.
 */
class ValueComparer<K, V extends Comparable<V>> implements Comparator<K>
{
	private final Map<K, V> map;

	public ValueComparer(Map<K, V> map)
	{	this.map = map;	}

	// Compare two values in a map (in descending order)
	public int compare(K key1, K key2)
	{
		V value1 = this.map.get(key1);
		V value2 = this.map.get(key2);
		int c = value2.compareTo(value1);
		if (c != 0)
		{	return c;	}
		Integer hashCode1 = key1.hashCode();
		Integer hashCode2 = key2.hashCode();
		return hashCode1.compareTo(hashCode2);
	}
}

class Stanford
{
	protected StanfordCoreNLP pipeline;

	public Stanford()
	{
		// Create StanfordCoreNLP object properties, with POS tagging
		// (required for lemmatization), and lemmatization
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");

		// StanfordCoreNLP loads a lot of models, so you probably
		// only want to do this once per execution
		this.pipeline = new StanfordCoreNLP(props);
	}

	public List<String> lemmatise(String documentText)
	{
		List<String> lemmas = new LinkedList<String>();

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);
		// run all Annotators on this text
		this.pipeline.annotate(document);
		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for(CoreMap sentence: sentences)
		{
			// Iterate over all tokens in a sentence; Retrieve and add the
			// lemma for each word into the list of lemmas
			for (CoreLabel token: sentence.get(TokensAnnotation.class))
			{   lemmas.add(token.get(LemmaAnnotation.class));   }
		}
		return lemmas;
	}

	public String lemma(String text)
	{   return this.lemmatise(text).get(0);    }
}
